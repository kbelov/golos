<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.17
 * Time: 16:22
 */

namespace api\controllers\v1;

use api\components\Controller;
use api\modules\polls\components\PollsItemResponse;
use api\modules\polls\components\PollsResponse;
use common\modules\polls\helpers\PollStatusHelper;
use common\modules\polls\models\Polls;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

/**
 * Class PollsController
 *
 * @package api\controllers\v1
 */
class PollsController extends Controller
{
    /**
     * @param int    $districtId
     * @param int    $status
     *
     * @return array
     * @throws \yii\base\InvalidParamException
     */
    public function actionGetListVotes(int $districtId, int $status = -1): array
    {
        $query = Polls::find()
            ->published()
            ->visible()
            ->withDistrict($districtId);

        switch ($status) {
            case 0:
                $query->correctDate();
                break;
            case 1:
                $query->ended();
                break;
            default:
                $query->started();
        }

        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query
            ]
        );

        $data = array_map(function (Polls $model) {
            return PollsResponse::prepareToJson($model);
        }, $dataProvider->getModels());
        return [
            'listVotes' => $data
        ];
    }

    /**
     * @param int $idVote
     *
     * @return array
     * @throws \yii\base\InvalidParamException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionGetStatusAndResult(int $idVote): array
    {
        $model = Polls::find()->where([
            'id'    => $idVote
        ])
            ->byStatusSoglas(PollStatusHelper::STATUS_SOGLAS_FINISHED)
            ->correctDate()
            ->active()
            ->visible()
            ->one();
        if (empty($model)) {
            throw new NotFoundHttpException();
        }

        return PollsItemResponse::prepareToJson($model);
    }
}