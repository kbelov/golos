<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.17
 * Time: 16:30
 */

namespace api\modules\polls\components;

use api\components\Response;
use common\components\Url;
use common\modules\polls\models\Polls;
use common\modules\polls\models\PollsVotes;

/**
 * Class PollsItemResponse
 *
 * @package api\modules\polls\components
 */
class PollsItemResponse extends Response
{
    /**
     * @param Polls $model
     *
     * @return array
     * @throws \yii\base\InvalidParamException
     */
    public static function prepareToJson(Polls $model): array
    {
        $data = [];
        $data['idVote']             = $model->id;
        $data['nameVote']           = $model->name;
        $data['status']             = $model->getPublicStatusName();
        $data['dateStart']          = $model->date_start;
        $data['dateEnd']            = $model->date_end;
        $data['fullDescription']    = $model->main_description;
        $data['listResults']        = self::getResultsArray($model);
        $data['district']           = $model->area ? $model->area->name : $model->area_id;

        return $data;
    }

    /**
     * @param Polls $model
     *
     * @return array
     */
    protected static function getResultsArray(Polls $model): array
    {
        return array_map(function (PollsVotes $model) {
            return [
                'answer'        => $model->name,
                'countVoted'    => $model->getCountVoted()
            ];
        }, $model->votes);
    }
}
