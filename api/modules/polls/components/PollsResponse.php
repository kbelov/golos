<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.17
 * Time: 16:30
 */

namespace api\modules\polls\components;

use api\components\Response;
use common\components\Url;
use common\modules\polls\models\Polls;

/**
 * Class PollsResponse
 *
 * @package api\modules\polls\components
 */
class PollsResponse extends Response
{
    /**
     * @param Polls $model
     *
     * @return array
     * @throws \yii\base\InvalidParamException
     */
    public static function prepareToJson(Polls $model): array
    {
        $data = [];
        $data['idVote']         = $model->id;
        $data['nameVote']       = $model->name;
        $data['link']           = Url::to(['/polls/default/view', 'id' => $model->id], true);
        $data['shortName']      = $model->short_description;
        $data['district']       = $model->area ? $model->area->name : $model->area_id;

        return $data;
    }
}
