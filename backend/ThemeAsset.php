<?php

namespace backend;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Theme main asset bundle.
 */
class ThemeAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@backend/assets';

    /**
     * @inheritdoc
     */
    public $css = [
        'dist/css/style.min.css?v=2'
    ];
}
