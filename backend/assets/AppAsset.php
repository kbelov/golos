<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $sourcePath = '@backend/assets/dist';
    public $css = [
        'css/style.min.css?v=4',
        'css/sub.css'
    ];

    public $js = [
        'js/xd_d.js',
        'js/widget.js',
        'https://toris.gov.spb.ru/widget/widget.js'
    ];

    public $depends = [
        BackboneAsset::class
    ];

    public function init()
    {
        parent::init();
        $this->publishOptions['forceCopy'] = true;
    }
}
