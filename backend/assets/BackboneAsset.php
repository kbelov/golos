<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 19.09.17
 * Time: 12:04
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Class BackboneAsset
 *
 * @package backend\assets
 */
class BackboneAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@backend/assets';
    /**
     * @inheritdoc
     */
    public $js = [
        'dist/js/backbone-min.js'
    ];
    /**
     * @inheritdoc
     */
    public $depends = [
        UnderscoreAssets::class,
    ];
}
