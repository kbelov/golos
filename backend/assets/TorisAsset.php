<?php

namespace backend\assets;

use toris\torisRbac\helpers\TorisHelper;
use Yii;
use yii\web\AssetBundle;
use yii\web\Cookie;
use yii\web\View;

/**
 * Toris asset bundle.
 */
class TorisAsset extends AssetBundle
{
    public $sourcePath = '@backend/assets/dist';

    public $js = [
        'js/xd_d.js',
        'js/widget.js?v=3',
        'https://toris.gov.spb.ru/widget/widget.js'
    ];

    public $jsOptions = ['position' => View::POS_HEAD];

    public function init()
    {
        parent::init();
        $this->publishOptions['forceCopy'] = true;
    }
}
