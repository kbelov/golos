<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 19.09.17
 * Time: 12:09
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Class UnderscoreAssets
 *
 * @package backend\assets
 */
class UnderscoreAssets extends AssetBundle
{
    public $sourcePath = '@backend/assets';

    public $js = [
        'dist/js/underscore-min.js'
    ];
}
