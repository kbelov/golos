/**
 * Created by filatov on 27.03.2017.
 */
var AppEvent = _.extend({}, Backbone.Events);
/**
 * Created by filatov on 29.12.2016.
 */
function UsersSelectWidget() {
    var usersSelectWidget = {
        toSelectIogvs: null,
        toSelectUsers: null,
        toSelectUsersElements: null,
        selectedUsers: null,
        inputName: null,
        container: null,

        rightState: false,
        leftState: false,

        initFull: function (cont) {
            this.initRight(cont);
            this.initLeft(cont);
        },
        initLeft: function (cont) {
            if (!this.setContainer(cont)) {return;}
            this.leftState = true;
            this.toSelectIogvs = this.container.find('#toSelect .iogvs');
            this.toSelectIogvsElements = this.toSelectIogvs.find('li');
            this.toSelectIogvsElements.on('click', function () {
                usersSelectWidget.processIogvSelection(this);
            });
            usersSelectWidget.processIogvSelection(usersSelectWidget.toSelectIogvsElements.first());
        },
        initRight: function (cont) {
            if (!this.setContainer(cont)) {return;}
            this.rightState = true;
            this.toSelectUsers = this.container.find('#toSelect .users ul');
            this.selectedUsers = this.container.find('#selected .users ul');
            this.inputName = this.container.find('#input-name').text();
            this.toSelectUsers.on('click', '.add', function () {
                usersSelectWidget.processBadgeClick(this);
            });
            this.selectedUsers.on('click', '.remove', function () {
                usersSelectWidget.processBadgeClick(this);
            });
        },
        setContainer: function(contId) {
            var exist = false;
            this.container = $(contId);
            if (this.container.length) {
                exist = true;
            }
            return exist;
        },
        processIogvSelection: function (el) {
            this.makeActive(el);

            var iogvId = $(el).data('iogv-id');
            var toSelectUsersElements = usersSelectWidget.toSelectUsers.find('li');
            toSelectUsersElements.addClass('hide');
            toSelectUsersElements.each(function () {
                if ($(this).data('iogv-id') == iogvId) {
                    $(this).removeClass('hide');
                }
            });
        },
        makeActive: function (el) {
            usersSelectWidget.toSelectIogvsElements.removeClass('active');
            $(el).addClass('active');
        },
        toggleBadge: function (el) {
            var icon = el.find('.glyphicon');
            icon.toggleClass('glyphicon-plus glyphicon-minus');
            icon.toggleClass('add remove');

            return icon;
        },
        processBadgeClick: function (el) {
            var userRow = $(el).parent().parent();
            if (!userRow.hasClass('animated')) {
                userRow.addClass('animated');
            }
            userRow.removeClass('slideInRight');
            userRow.addClass('slideOutRight');
            setTimeout(function () {
                userRow.remove();
                var icon = usersSelectWidget.toggleBadge(userRow);
                usersSelectWidget.hideRow(userRow);
                userRow.toggleClass('slideOutRight slideInRight');
                if (icon.hasClass('add')) {
                    userRow.appendTo(usersSelectWidget.toSelectUsers);
                    userRow.find('input[type=hidden]').remove();
                } else {
                    userRow.append(
                        $('<input type="hidden" />').attr({
                            name: usersSelectWidget.inputName + "["+userRow.data('iogv-id')+"][]",
                            value: userRow.data('user-id')
                        })
                    );
                    userRow.appendTo(usersSelectWidget.selectedUsers);
                }
            }, 250);

        },
        hideRow: function (userRow) {
            if (this.leftState && usersSelectWidget.toSelectIogvs.find('.active').data('iogv-id') != userRow.data('iogv-id')) {
                userRow.addClass('hide');
            }
        }
    };
    return usersSelectWidget;
}

/**
 * Created by filatov on 09.01.2017.
 */
function ApproveWidget() {
    var approveWidget = {
        addrId: null,
        userId: null,
        iogvId: null,
        linkId: null,
        linkType: null,
        yesBtn: null,
        noBtn: null,
        container: null,
        yesUrl: null,
        noUrl: null,
        yesScenarioName: null,
        noScenarioName: null,
        msg: {
            approveConfirmation: null,
            declineConfirmation: null,
            approve: null,
            approveNo: null,
            declinePlaceholder: null,
            cancel: null,
            errorEmpty: null,
            errorFatal: null
        },
        ajaxError: null,

        init: function (cont) {
            this.container = $(cont);
            this.addrId = this.container.find('.addr-id').text();
            this.userId = this.container.find('.user-id').text();
            this.iogvId = this.container.find('.iogv-id').text();
            this.linkId = this.container.find('.link-id').text();
            this.linkType = this.container.find('.link-type').text();
            this.yesBtn = this.container.find('.approve-yes');
            this.noBtn = this.container.find('.approve-no');
            this.yesUrl = this.container.find('.yes-url').text();
            this.noUrl = this.container.find('.no-url').text();
            this.yesScenarioName = this.container.find('.yes-scenario-name').text();
            this.noScenarioName = this.container.find('.no-scenario-name').text();
            this.msg.approveConfirmation = this.container.find('.msg-approve-confirmation').text();
            this.msg.declineConfirmation = this.container.find('.msg-decline-confirmation').text();
            this.msg.approve = this.container.find('.msg-approve').text();
            this.msg.approveNo = this.container.find('.msg-approve-no').text();
            this.msg.declinePlaceholder = this.container.find('.msg-decline-placeholder').text();
            this.msg.cancel = this.container.find('.msg-cancel').text();
            this.msg.errorEmpty = this.container.find('.msg-error-empty').text();
            this.msg.errorFatal = this.container.find('.msg-error-fatal').text();
            this.ajaxError = function () {
                toastr.error(this.msg.errorFatal, 'Ошибка');
                swal({title: this.msg.errorFatal, type: "error"});
            };

            toastr.options = {progressBar: true};
            swal.setDefaults({
                cancelButtonText: this.msg.cancel
            });

            this.yesBtn.on('click', function () {
                approveWidget.modal(approveWidget.yesScenarioName);
            });
            this.noBtn.on('click', function () {
                approveWidget.modal(approveWidget.noScenarioName);
            });

        },
        cryptoCheck: function (options) {
            options['ajax'] = {
                dataToSign: 'Пользователь '+this.userId+' подтверждает изменение статуса АП '+this.addrId,
                linkId: this.linkId,
                scenario: options.scenario
            };
            var cryptoApprove = new CryptoApprove(options);
        },
        modal: function (scenarioName) {
            var ajaxData = {
                ajax: {
                    scenario: scenarioName
                }
            };
            if (scenarioName == this.noScenarioName) {
                this.cryptoCheck(_.extend(ajaxData, {
                    onSuccess: this.modalNo.bind(this, scenarioName)
                }));
            } else {
                this.cryptoCheck(_.extend(ajaxData, {
                    onSuccess: this.modalYes.bind(this, scenarioName)
                }));
            }
        },
        modalYes: function () {
            swal({
                showCancelButton: true,
                title: this.msg.approve,
                text: this.msg.approveConfirmation,
                type: "info",
                showLoaderOnConfirm: true,
                confirmButtonText: this.msg.approve
            }).then(function(){
                var def = approveWidget.requestYes();
                def.done(function (msgCont) {
                    approveWidget.afterRequest(msgCont);
                });
            }).catch(swal.noop);
        },
        modalNo: function () {
            swal({
                showCancelButton: true,
                title: this.msg.approveNo,
                text: this.msg.declineConfirmation,
                type: "warning",
                input: 'textarea',
                inputPlaceholder: this.msg.declinePlaceholder,
                confirmButtonText: this.msg.approveNo,
                inputValidator: function (value) {
                    return new Promise(function (resolve, reject) {
                        if (value) {
                            resolve();
                        } else {
                            reject(approveWidget.msg.errorEmpty);
                        }
                    });
                }
            }).then(function (msg) {
                var def = approveWidget.requestNo(msg);
                def.done(function (msgCont) {
                    approveWidget.afterRequest(msgCont);
                })
            }).catch(swal.noop);
        },
        requestYes: function () {
            return $.ajax({
                error: function() {
                    approveWidget.ajaxError();
                },
                url: this.yesUrl,
                data: this.getAjaxData()
            });
        },
        requestNo: function (msg) {
            var d = this.getAjaxData();
            d['msg'] = msg;
            return $.ajax({
                error: function() {
                    approveWidget.ajaxError();
                },
                url: this.noUrl,
                data: d
            });
        },
        getAjaxData: function () {
            var data = {
                addrId: this.addrId,
                userId: this.userId,
                linkType: this.linkType,
                linkId: this.linkId
            };
            if (this.iogvId != null) {
                data.iogvId = this.iogvId;
            }
            return data;
        },
        afterRequest: function (msgCont) {
            if (msgCont.status == true) {
                toastr.success(msgCont.msg);
                swal({title: msgCont.msg, type: "success"});
                approveWidget.container.hide(400);
                $.pjax.reload({"container": "#address_form"});
            } else {
                toastr.error(msgCont.msg, this.msg.approveError);
                swal({title: msgCont.msg, type: "error"});
            }
        }

    };
    return approveWidget;
}

var ChosenSelect = {
    init: function () {
        var chosen_select = $(".chosen-select");
        if (chosen_select && chosen_select.is('select')) {
            chosen_select.chosen({
                width: "100%",
                placeholder_text_multiple: "Выберите значения"
            });
        }
    }

};

$(function () {
    ChosenSelect.init();
});

$(function () {
    var chosen_select = $(".chosen-single");
    if (chosen_select && chosen_select.is('select')) {
        chosen_select.chosen({
            width: "100%",
            placeholder_text_multiple: "Выберите значения"
        });
    }
});
var addresses = {
    loadTemplate: function (url, el) {
        var template_id = el.value;
        $.ajax({
            method: 'get',
            data: {
                id: template_id
            },
            url: url,
            datatype: 'json',
            success: function (data) {
                if (data.html != undefined) {
                    $("#template").html(data.html);
                    //eval(data.js);
                    $('.i-checks').iCheck({
                        checkboxClass: 'icheckbox_square-green',
                        radioClass: 'iradio_square-green'
                    });
                }
            }
        })
    }
};

var CloneApButton = function (options) {
    var self = this;
    this.options = {};
    _.extend(this.options, !!options ? options : {});
    this.$btns = $("."+this.options.btnClass);


    this.init = function() {
        this.$btns.click(function (ev) {
            var $target = $(ev.currentTarget);
            swal({
                showCancelButton: true,
                title: 'Открыть на редактирование?',
                text: 'Это действие создаст копию выбранной АП со всеми работами в статусе "Проект"',
                type: "error",
                showLoaderOnConfirm: true,
                confirmButtonText: 'Открыть',
                cancelButtonText: 'Отмена'
            }).then(function(){
                window.location.href = $target.prop('href');
            }).catch(swal.noop);
            ev.preventDefault();
        });
    };
    this.init();
    return this;
};
var WorksAp = {
    memberInput: null,
    init: function (address_id) {
        this.memberInput = $("#selectedIdsWorks");
        var a = Backbone.View.extend({
            el: "#works-ap-widget",
            events: {
                'keyup .works-ap-widget_input': 'onchange',
                'click .works-ap-widget-item': 'choose',
                'click #submit-works-to-gati': 'submit'
            },
            initialize: function() {
                this.ev = _.extend({}, Backbone.Events);
                this.$result = this.$el.find('.works-ap-widget_result');
                this.$form = this.$el.find('form');
                this.$works = this.$form.find('#selectedIdsWorks');
                this.collection = new Members();
                this.collection.fetch();
                this.childViews = [];

                this.$selected = new memberSelectedView({
                    ev: this.ev
                });
                this.listenTo(this.collection, 'sync', this.render);
            },
            onchange: _.debounce(function (e) {
                this.collection.query = e.currentTarget.value;
                if (this.collection.query != '')
                    this.collection.fetch();
            }, 300),
            choose: function (event) {
                var id = $(event.currentTarget).attr('data-member');
                if (id != undefined) {
                    var memberModelFinded = this.collection.where({id: parseInt(id)});
                    if (memberModelFinded != undefined) {
                        _.forEach(memberModelFinded, function (model) {
                            var member = new memberModel({
                                name:       model.get('name'),
                                id:         model.get('id'),
                            });
                            this.$selected.collection.add(member);
                        }, this);
                    }
                }
            },
            submit: function (ev) {
                var $btn = $(ev.currentTarget);
                var l = $btn.ladda();
                l.ladda('start');

                $.ajax({
                    method: 'post',
                    data: {
                        works:this.$works.prop('value')
                    },
                    url: this.$form.attr('action'),
                    success: function (outMsg) {
                        setTimeout(function() {
                            toastr.success(outMsg.msg, 'Выполнено');
                        }, 1000);
                    },
                    complete: function() {
                        setTimeout(function() {
                            l.ladda('stop');
                        }, 1000);
                    }
                });
            },
            render: function (ev, model) {
                this.$result.html(model.html);
                return this;
            }
        });


        var memberModel = Backbone.Model.extend({
            getName: function () {
                return this.get('name') || '';
            }
        });
        var memberSelectedView = Backbone.View.extend({
            el: ".works-ap-widget_selected",

            initialize: function (options) {
                this.collection = new memberPicked([], {
                    ev: options.ev
                });
                this.ev = options.ev;
                this.listenTo(this.collection, 'add', this.addSelected);
                this.listenTo(this.collection, 'add remove', this.processInput);
            },
            render: function () {
                this.$el.html(this.template());
                return this;
            },
            addSelected: function (model) {
                var view = new memberSelectedViewItem({
                    model: model
                });
                this.$el.append(view.render().el);
            },
            processInput: function () {
                var members = [];
                _.each(this.collection.models, function (model) {
                    var id = model.get('id');
                    members.push(id);
                });
                WorksAp.memberInput.val(JSON.stringify(members));
            }
        });
        var memberPicked = Backbone.Collection.extend({
            model: memberModel,
            initialize: function (models, options) {
                this.ev = options.ev;
            }
        });
        var memberSelectedViewItem = Backbone.View.extend({
            tagName: 'tr',
            className: 'deck-selected-item',
            events: {
                'click .works-ap-widget-selected_remove': 'removeMember'
            },
            template: _.template(
                "<td>" +
                "<a target='_blank' href='#link' class='client-link'>" +
                "<%= name%>" +
                "</a>" +
                "</td>" +
                "<td class='text-center'>" +
                "<a class='works-ap-widget-selected_remove text-danger'>" +
                "<i class='fa fa-times-circle fa-3x'></i>" +
                "</a>" +
                "</td>"),
            initialize: function () {
                this.listenTo(this.model, 'remove', this.remove);
            },
            render: function () {
                this.$el.html(this.template(this.model.toJSON()));
                return this;
            },
            removeMember: function () {
                this.model.collection.remove(this.model);
            }
        });
        var Members = Backbone.Collection.extend({
            query: "",
            url: function () {
                return '/works/api/search-for-gati/?address_id=' + address_id + '&query=' + this.query;
            },
            parse: function (res) {
                return res.items;
            }
        });
        this.bb = new a();
    }
};
function AgreedLine() {
	this.init = function() {
		$('[data-toggle="popover"]').popover({

	});
	};
	return this;
}

function VerticalLine() {
	this.init = function(statuses_array) {
		var el = $('.timeline');
		var item = el.find('.timeline-item');
		// item.style.removeProperty('display: table-cell');
		var status = item.find('p');
		console.log(status.text());
	};
	return this;
}

/**
 * Created by filatov on 19.07.2017.
 */
var FilterOnMapView = Backbone.View.extend({
    options: {
        containerId: null,
        searchMethod: 'filter'
    },
    initialize: function (options) {
        this.options = _.extend(this.options, options || {});
        this.$container = $('#' + this.options.containerId);
        this.gisMapLib = new MyGisMap({
            url: this.options.gisUrl,
            aistoken: this.options.aistoken,
            subsystem: this.options.subSystemCode,
            modules: ['BasemapGallery', 'Export', 'Measurement', 'Network', 'Swipe', 'Toc', 'Search', 'UserLayers', 'Addressprograms']
        });
        this.gisMapLib.setGetWorkByOid(this.getWorkByOid);
    },
    render: function () {
        this.$container.html(this.gisMapLib.el);
        this.gisMapLib.render();
        if (this.options.searchMethod == 'filter') {
            _.each(this.options.data, this.filter, this);
        } else {
            _.each(this.options.data, this.search, this);
        }
        return this;
    },
    filter: function (dataObject, layerId) {
        if (!!dataObject) {
            this.gisMapLib.filterObjectsOnLayer(layerId, _.toArray(dataObject), function () {
                toastr.success('Готово');
            }, function (code, message) {
                toastr.error(message);
            })
        } else {
            toastr.error('Неверные входящие данные');
        }
    },
    search: function (dataObject, layerId) {
        if (!!dataObject) {
            this.gisMapLib.showObjectsOnLayer(layerId, _.toArray(dataObject), function () {
                toastr.success('Готово');
            }, function (code, message) {
                toastr.error(message);
            })
        } else {
            toastr.error('Неверные входящие данные');
        }
    },
    getWorkByOid: function (data, onSuccess) {
        $.ajax({
            method: 'get',
            data: data,
            url: '/works/api/get-for-map',
            success: function (outMsg) {
                onSuccess.call(this, outMsg);
                // console.log(outMsg);
            },
            error: function (xhr, textStatus, er) {
                console.log("Произошла ошибка: " + er);
            }
        });

    }

});
function GetWorks() {
    var self = this;
    var _worksContainer = null;
    var _worksBody = null;
    var _formUrl = null;
    var _className = null;

    this.init = function (containerId, formUrl, className) {
        _worksContainer = $('#'+containerId);
        _worksBody = _worksContainer.find('.modal-body');
        _formUrl = formUrl;
        _className = className;
        $('a.link-for-'+containerId).on('click', function () {
            // console.log(arguments);
            self.show($(this).data('url'));
        });

    };

    this.show = function (url) {
        $.ajax({
            type: 'post',
            url: url,
            data: {
                formUrl: _formUrl,
                className: _className
            },
            success: function(data) {
                _worksBody.html(data.html);
                eval(data.js);
                _worksContainer.modal('show');
            }
        });
    };

    return this;
}

/**
 * @author Averin Anton
 * @created 23.12.2015
 */
// define(['jquery', 'underscore', 'backbone', 'toris-ui-utils'], function ($, _, Backbone, Utils) {

    var GisMap = Backbone.View.extend({

        tagName: 'iframe',

        initialize: function (options) {
            this.queue = [];
            this.url = options.url;
            this.subsystem = options.subsystem;
            if (options.modules) {
                var gisMogules = options.modules.toString();
                this.$el.prop('src', this.url + '?aistoken=' + options.aistoken + "&modules=" + gisMogules);

            } else {
                this.$el.prop('src', this.url + '?aistoken=' + options.aistoken);
            }
            this.$el.css('height', '100%');
            this.$el.css('width', '100%');

            this.render();
        },

        render: function () {
            window.onmessage = _.bind(this.eventHandler, this);
            return this;
        },

        eventHandler: function (e) {
            var _this = this;
            var edata = e.data;
            var frame = e.source;

            if (edata.ready === true) {
                console.log('Gis Module: gis is ready');
                var request = {};
                request.subsystem = _this.subsystem;
                request.showFpLayers = false;
                frame.postMessage(request, "*");
            } else if (edata.type_request === 'subsystem_success') {
                console.log('Gis Module: subsystem loading successfull');
                if (_.isUndefined(_this.currentRequest)) {
                    _this.handleNextRequest(frame);
                }
            } else if (edata.type_request === 'subsystem_error') {
                console.log('Gis Module: subsystem loading with error');
                var item = _this.queue.shift();
                if (!_.isUndefined(item)) {
                    var onError = item.onError;
                    if (!_.isUndefined(onError) && _.isFunction(onError)) {
                        var error = _.findWhere(_this.errorCodes[item.attrs.type_request], {code: edata.code});
                        onError(error.code, error.message);
                    }
                }
            } else if (edata.type_request === 'map_click') {
                if (!_.isUndefined(_this.onSuccess) && _.isFunction(_this.onSuccess)) {
                    console.log('Gis Module: point coords is [ ' + edata.x + ' ; ' + edata.y + ' ]');
                    _this.onSuccess(edata.x, edata.y);
                }
            } else if (Utils.StringUtils.endWith(edata.type_request, '_success')) {
                if (_this.hasCurrentRequest()) {
                    var requestType = Utils.StringUtils.without(edata.type_request, '_success');
                    if (_.isEqual(requestType, _this.currentRequest.attrs.type_request)) {
                        console.log('Gis Module: ' + requestType + ' finished successfull');
                        var onSuccess = _this.currentRequest.onSuccess;
                        if (!_.isUndefined(onSuccess) && _.isFunction(onSuccess)) {
                            onSuccess();
                        }
                        _this.handleNextRequest(frame);
                    }
                }
            } else if (Utils.StringUtils.endWith(edata.type_request, '_error')) {
                if (_this.hasCurrentRequest()) {
                    var requestType = Utils.StringUtils.without(edata.type_request, '_error');
                    if (_.isEqual(requestType, _this.currentRequest.attrs.type_request)) {
                        console.log('Gis Module: ' + requestType + ' finished with error');
                        var onError = _this.currentRequest.onError;
                        if (!_.isUndefined(onError) && _.isFunction(onError)) {
                            var error = _.findWhere(_this.errorCodes[_this.currentRequest.attrs.type_request], {code: edata.code});
                            onError(error.code, error.message);
                        }

                        _this.handleNextRequest(frame);
                    }
                }
            }
        },

        hasCurrentRequest: function () {
            return !_.isNull(this.currentRequest);
        },

        handleNextRequest: function (frame) {
            this.currentRequest = null;
            var item = this.queue.shift();
            if (!_.isUndefined(item)) {
                this.currentRequest = item;
                console.log('Gis Module: current request attrs ' + JSON.stringify(this.currentRequest.attrs));
                frame.postMessage(this.currentRequest.attrs, "*");
            }
        },

        showLayer: function (layerUid, objectUids, success, error) {
            var request = {};
            request.type_request = 'showLayer';
            request.id = layerUid;
            request.clearPrev = false;

            if (!_.isNull(objectUids) && !_.isUndefined(objectUids)) {
                if (_.isArray(objectUids)) {
                    request.filter = 'OID IN (' + Utils.ArrayUtils.convertToString(objectUids, ',') + ')';
                } else {
                    request.filter = 'OID = ' + objectUids;
                }
            }

            this.putToQueue(request, success, error);
        },

        showAllLayers: function (success, error) {
            var request = {};
            request.type_request = 'getVisibleLayers';

            this.putToQueue(request, success, error);
        },

        hideLayer: function (layerUid, objectUid, success, error) {
            var request = {};
            request.type_request = 'hideLayer';
            request.id = layerUid;

            this.putToQueue(request, success, error);
        },

        showObjectsOnLayer: function (layerUid, objectUids, success, error, field) {
            var request = {};
            request.type_request = "search_layer";
            request.layerId = layerUid;
            request.clearPrev = false;

            if (!field) {
                field = 'OID';
            }

            if (_.isArray(objectUids)) {
                request.where = field + ' IN (' + Utils.ArrayUtils.convertToString(objectUids, ',') + ')';
            } else {
                request.where = field + ' = ' + objectUids;
            }

            this.showLayer(layerUid, objectUids);
            this.putToQueue(request, success, error);
        },

        filterObjectsOnLayer: function (layerUid, objectUids, success, error, field) {
            var request = {};
            request.type_request = "filter_layer";
            request.mode = 'On';
            request.LayerId = layerUid;
            request.clearPrev = false;

            if (!field) {
                field = 'OID';
            }

            if (_.isArray(objectUids)) {
                request.filter = field + ' IN (' + Utils.ArrayUtils.convertToString(objectUids, ',') + ')';
            } else {
                request.filter = field + ' = ' + objectUids;
            }

            this.showLayer(layerUid);
            this.putToQueue(request, success, error);
        },

        addObjectOnLayer: function (layerUid, objectUid, success, error) {
            var request = {};
            request.type_request = "addobj_layer";
            request.layerId = layerUid;
            request.id = objectUid;

            this.showLayer(layerUid);
            this.putToQueue(request, success, error);
        },

        moveObjectOnLayer: function (layerUid, objectUid, success, error) {
            var request = {};
            request.type_request = "moveobj_layer";
            request.layerId = layerUid;
            request.id = objectUid;

            this.showLayer(layerUid);
            this.putToQueue(request, success, error);
        },

        removeObjectFromLayer: function (layerUid, objectUid, success, error) {
            var request = {};
            request.type_request = "delobj_layer";
            request.layerId = layerUid;
            request.id = objectUid;

            this.showLayer(layerUid);
            this.putToQueue(request, success, error);
        },

        showOnEasLayer: function (easUids, success, error) {
            var request = {};
            request.type_request = "eas";
            request.id = easUids;

            this.putToQueue(request, success, error);
        },

        geolocateAddress: function (addressName, success, error) {
            var request = {};
            request.type_request = "geolocateAddress";
            request.address = addressName;

            this.putToQueue(request, success, error);
        },

        addLayersOnMap: function (layerUids, success, error) {
            var request = {};
            request.type_request = "addCluster";

            if (_.isArray(layerUids)) {
                request.layerIds = '[' + Utils.ArrayUtils.convertToString(layerUids, ',') + ']';
            } else {
                request.layerIds = layerUids;
            }

            this.putToQueue(request, success, error);
        },

        getPointCoordinates: function (callback) {
            this.onSuccess = callback;
        },

        putToQueue: function (request, success, error) {
            this.queue.push({
                attrs: request,
                onSuccess: success,
                onError: error
            });
            console.log('Gis Module: queue size is ' + this.queue.length);

            return this;
        },
        errorCodes: {
            'search_layer': [{
                code: 0,
                message: 'Слой не найден'
            }, {
                code: 1,
                message: 'Объекты не найдены'
            }, {
                code: 2,
                message: 'Неизвестная ошибка'
            }],
            'addobj_layer': [{
                code: 0,
                message: 'Слой не найден или нередактируемый'
            }, {
                code: 1,
                message: 'Объект не добавлен'
            }, {
                code: 2,
                message: 'Неизвестная ошибка'
            }],
            'moveobj_layer': [{
                code: 0,
                message: 'Слой не найден или нередактируемый'
            }, {
                code: 1,
                message: 'Объект не перемещен'
            }, {
                code: 2,
                message: 'Неизвестная ошибка'
            }],
            'filter_layer': [{
                code: 0,
                message: 'Ошибка выполнения запроса'
            }, {
                code: 1,
                message: 'Объекты не найдены'
            }, {
                code: 2,
                message: 'Неизвестная ошибка'
            }],
            'eas': [{
                code: 0,
                message: 'Ошибка выполнения запроса'
            }, {
                code: 1,
                message: 'Объекты не найдены'
            }, {
                code: 2,
                message: 'Неизвестная ошибка'
            }],
            'subsystem': [{
                code: 0,
                message: 'Ошибка выполнения запроса'
            }, {
                code: 1,
                message: 'Слой (слои) не найдены'
            }, {
                code: 2,
                message: 'Неизвестная ошибка'
            }],
            'delobj_layer': [{
                code: 0,
                message: 'Слой не найден или не редактируем'
            }, {
                code: 1,
                message: 'Объект не найден'
            }, {
                code: 2,
                message: 'Пользователь отклонил операцию'
            }],
        }
    });

    // return GisMap;
// });
/**
 * @author Averin Anton
 * @created 15.12.2015
 */
// define(['jquery', 'underscore', 'moment'], function ($, _, moment) {

    var Utils = {};

    var StringUtils = Utils.StringUtils = function () {
        return {
            without: function (string, charSeq) {
                var length = string.length - charSeq.length;
                return string.slice(0, length);
            },

            contains: function (string, charSeq) {
                return string.indexOf(charSeq) > -1;
            },

            startWith: function (string, charSeq) {
                return string.slice(0, charSeq.length) === charSeq;
            },

            endWith: function (string, charSeq) {
                return string.slice(-charSeq.length) === charSeq;
            },

            format: function () {
                // The string containing the format items (e.g. "{0}")
                // will and always has to be the first argument.
                var string = arguments[0];
                // start with the second argument (i = 1)
                for (var i = 1; i < arguments.length; i++) {
                    // "gm" = RegEx options for Global search (more than one instance)
                    // and for Multiline search
                    var regEx = new RegExp("\\{" + (i - 1) + "\\}", "gm");
                    string = string.replace(regEx, arguments[i]);
                }

                return string;
            }
        };
    }();

    var ArrayUtils = Utils.ArrayUtils = function () {

        var sortByFieldHandler = function (field) {
            return function (json1, json2) {
                return ((json1[field] == json2[field]) ? 0 : ((json1[field] > json2[field]) ? 1 : -1 ));
            };
        };

        var convertToStringHandler = function (array, separator) {
            return array.join(separator);
        };

        return {
            sortJsonArray: function (array, field) {
                array.sort(sortByFieldHandler(field));
            },

            convertToString: function (array, separator) {
                return convertToStringHandler(array, separator);
            }
        };

    }();

    var DateFormatter = Utils.DateFormatter = function () {

        return {
            getTime: function () {
                return moment().format('HHmmss');
            },

            parse: function (value, pattern) {
                if (_.isUndefined(pattern)) {
                    return moment(value).toDate();
                }

                return moment.utc(value, pattern, 'ru').toDate();
            },

            format: function (value, pattern) {
                if (_.isUndefined(pattern)) {
                    return moment(value).toDate();
                }

                return moment(value).format(pattern);
            },

            getTimeAsString: function () {
                var date = new Date();
                return date.getHours() + "" + date.getMinutes() + "" + date.getSeconds();
            }
        };
    }();

    // return Utils;
// });
/**
 * Created by filatov on 06.03.2017.
 */
var MyGisMap = GisMap.extend({
    eventHandler: function (e) {
        var _this = this;
        if (!!e.data && (!!e.data.ready || !!e.data.type_request)) {
            switch (e.data.type_request) {
                case 'selectOid':
                    if (!_.isUndefined(_this.onSelectOidSuccess) && _.isFunction(_this.onSelectOidSuccess)) {
                        _this.onSelectOidSuccess(e.data);
                    }
                    break;
                case 'getWorkByOid':
                    if (!_.isUndefined(_this.onGetWorkByOid) && _.isFunction(_this.onGetWorkByOid)) {
                        _this.onGetWorkByOid(e.data, function (msgCont) {
                            e.source.postMessage({type_request:msgCont.status, data:msgCont.data}, '*');
                        });
                    }
                    break;
            }
            GisMap.prototype.eventHandler.call(this, e);
        }
    },
    setGetWorkByOid: function (callback) {
        this.onGetWorkByOid = callback;
    },
    setSelectOidSuccess: function (callback) {
        this.onSelectOidSuccess = callback;
    },
    render: function () {
        GisMap.prototype.render.call(this);
        this.$el.prop('id', 'gis-map');
    }
});
/**
 * Created by filatov on 07.03.2017.
 */

var MapSelectBoxView = Backbone.View.extend({
    el: '#map-select-box',
    mapAddr: null,
    mapButtonFormGroup: null,
    mapSelectOption: null,
    workConstants: null,
    selectId: 'workdynamic-map_selection',
    select: null,
    button: new Backbone.View(),
    gisJsonInput: null,
    TYPE_POINT: 1,
    TYPE_LINE: 2,
    TYPE_POLYGON: 3,
    TYPE_EXISTS_OBJECT: 4,
    CREATE_ON_MAP_BTN_TYPE_SINGLE: 1, // see php Work model
    CREATE_ON_MAP_BTN_TYPE_MULTIPLE: 2, // // see php Work model
    options: {
        layer_point: null,
        layer_line: null,
        layer_polygon: null,
        createBtnType: null,
        layerId: null,
        geomType: null,
        gisJsonId: 'workdynamic-gis_json',
        disabled: false
    },
    initialize: function (options) {
        var _this = this;
        this.setOptions(options);

        var constantsText = $('#work-constants').text();
        if (!!constantsText) {
            this.workConstants = JSON.parse(constantsText);
        }

        this.mapAddr = $('#map-addr');
        this.mapButtonFormGroup = $('#map-button-form-group');
        this.mapSelectOption = $('.map-select-option');
        this.select = $('#' + this.selectId);
        this.gisJsonInput = $('#' + this.options.gisJsonId);
        this.initButton();
        this.listenTo(AppEvent, 'showOnMap:selectOid', function (params) {
            var modelData = params.serviceModelData;
            var layerId = params.serviceId;
            if (layerId && !!modelData && !!modelData.oid && modelData.oid != 'Null') {
                _this.gisJsonInput.val(JSON.stringify({
                    oid: modelData.oid,
                    layerId: layerId,
                    name: modelData.name,
                    type: _this.TYPE_EXISTS_OBJECT
                }));
                _this.initButton();
                toastr.success("Объект успешно нанесен");
                _this.button.gisModal.modal.modal('hide');
            } else {
                toastr.error("Отсутствует идентификатор объекта или слоя");
            }
        });
    },
    setOptions: function (options) {
        this.options = _.extend(this.options, JSON.parse(options) || {});
    },
    disable: function() {
        this.button.$el.find('a,button').addClass('disabled');
    },
    initButton: function () {
        this.button.undelegateEvents();
        var attrs = {
            mapSelectBox: this,
            gisModal: this.button.gisModal,
            name: ''
        };
        if (!!this.gisJsonInput.val()) {
            var json = JSON.parse(this.gisJsonInput.val());
        }

        if (this.options.disabled) {
            this.button = new BaseMapButtonView();
        }
        else {
            if (!_.isUndefined(json) && !_.isUndefined(json.type)) {
                attrs['icon'] = 'fa-exchange';
                attrs['name'] = json.name;
                attrs = _.extend(attrs, this.mapObjectTypeData(json.type));
                this.button = new UpdateMapButtonView(attrs);
                this.button.type = json.type; // todo: а надо ли это?
            } else {
                if (this.options.createBtnType == this.CREATE_ON_MAP_BTN_TYPE_SINGLE) {
                    attrs = _.extend(attrs, this.mapObjectTypeData(this.options.geomType, this.options.layerId));
                    this.button = new CreateMapButtonSingleView(attrs);
                }
                else {
                    this.button = new CreateMapButtonView(attrs);
                }

            }
        }

        this.button.render();
    },
    refreshButton: function() {
        this.gisJsonInput.val('');
        this.initButton();
    },
    mapObjectTypeData: function (type, layerId) {
        var data = {
            type: type,
            label: '',
            icon: '',
            layerId: 0,
        };

        switch (type) {
            case this.TYPE_POINT:
                data.label = 'Точка';
                data.icon = 'fa fa-tint';
                data.layerId = this.options.layer_point;
                break;
            case this.TYPE_LINE:
                data.label = 'Линия';
                data.icon = 'fa fa-minus';
                data.layerId = this.options.layer_line;
                break;
            case this.TYPE_POLYGON:
                data.label = 'Полигон';
                data.icon = 'fa fa-square-o';
                data.layerId = this.options.layer_polygon;
                break;
            case this.TYPE_EXISTS_OBJECT:
                data.label = 'Существующий объект';
                data.icon = 'fa fa-map-marker';
                break;
        }
        data.layerId = layerId || data.layerId;
        return data;
    }

});

var BaseMapButtonView = Backbone.View.extend({
    el: '#map-button',
    gisModal: null,

    initialize: function (attributes) {
        this.attributes = attributes || {};
        if (this.attributes.gisModal) {
            this.gisModal = attributes.gisModal;
        }
    },
    onClick: function () {
        this.gisModal = new GisModalView({
                mapSelectBox: this.attributes.mapSelectBox
            }
        );
        this.gisModal.render();
    },
    render: function () {
        this.$el.html(this.template(this.attributes));
        return this;
    },
    template: _.template('<div class="add-on-layer"><button href="#" class="btn btn-primary" disabled="disabled">' +
        '<i class="fa fa-ban"></i>' +
        '</button></div>'
    )
});

var CreateMapButtonView = BaseMapButtonView.extend({
    onClick: function (ev) {
        ev.preventDefault();
        BaseMapButtonView.prototype.onClick.call(this);
        toastr.info('Выберите слой и кликните по объекту');
    },
    onClickAddLayer: function (ev) {
        ev.preventDefault();
        BaseMapButtonView.prototype.onClick.call(this);
        var $target = $(ev.currentTarget);
        var type = $target.data('type');
        var typeObj = this.getTypeObject(type);
        this.gisModal.addObjectOnMap(typeObj);
    },
    getTypeObject: function (type) {
        return this.attributes.mapSelectBox.mapObjectTypeData(type);
    },
    events: function () {
        var events = {};
        events['click .add-object a'] = 'onClick';
        events['click .add-on-layer a'] = 'onClickAddLayer';
        return events;
    },
    render: function () {
        BaseMapButtonView.prototype.render.call(this);
        if (!!this.attributes.mapSelectBox.options.layer_point) {
            this.$el.find('.dropdown-menu').prepend('<li class="add-on-layer"><a href="#" data-type="'+ this.attributes.mapSelectBox.TYPE_POINT +'">Точка</a></li>');
        }
        if (!!this.attributes.mapSelectBox.options.layer_line) {
            this.$el.find('.dropdown-menu').prepend('<li class="add-on-layer"><a href="#" data-type="'+ this.attributes.mapSelectBox.TYPE_LINE +'">Линия</a></li>');
        }
        if (!!this.attributes.mapSelectBox.options.layer_polygon) {
            this.$el.find('.dropdown-menu').prepend('<li class="add-on-layer"><a href="#" data-type="'+ this.attributes.mapSelectBox.TYPE_POLYGON +'">Полигон</a></li>');
        }


    },
    template: _.template('<div class="dropdown">' +
        '<button class="btn btn-primary dropdown-toggle" type="button" id="createObjectMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Выберите тип объекта<span class="caret"></span></button>' +
        '<ul class="dropdown-menu" aria-labelledby="createObjectMenu">' +
        '<li role="separator" class="divider"></li>' +
        '<li class="add-object"><a href="#" data-type="<%= mapSelectBox.TYPE_EXISTS_OBJECT %>">Существующий объект</a></li>' +
        '</ul>' +
        '</div>'
    )
});

var CreateMapButtonSingleView = CreateMapButtonView.extend({
    getTypeObject: function (type) {
        return this.attributes.mapSelectBox.mapObjectTypeData(type, this.attributes.mapSelectBox.options.layerId);
    },
    template: _.template('<div class="add-on-layer"><a href="#" data-type="<%= type %>" class="btn btn-primary">' +
        '<i class="fa fa-plus"></i>  <%= label %>' +
        '</a></div>'
    )
});

var UpdateMapButtonView = BaseMapButtonView.extend({
    type: null,
    onClick: function () {
        BaseMapButtonView.prototype.onClick.call(this);
        if (this.type != this.attributes.mapSelectBox.TYPE_EXISTS_OBJECT) {
            this.gisModal.moveObjectOnMap();
        }
    },
    onClickRemove: function () {
        if (!!this.type) {
            if (this.type == this.attributes.mapSelectBox.TYPE_EXISTS_OBJECT) {
                this.attributes.mapSelectBox.gisJsonInput.val('');
                this.attributes.mapSelectBox.initButton();
                toastr.success('Очищено');
            }
            else {
                BaseMapButtonView.prototype.onClick.call(this);
                this.gisModal.removeObjectFromLayer();
            }
        }

    },
    events: function () {
        var events = {};
        events['click .move'] = 'onClick';
        events['click .remove'] = 'onClickRemove';
        return events;

    },
    template: _.template('<div class="btn-group">' +
        '<button class="btn btn-sm btn-primary move" type="button">' +
        '<i class="fa <%= icon %> fa-lg"></i>' +
        '<span class="m-l-xs"><%= label %></span>' +
        '</button>' +
        '<button class="btn btn-sm btn-danger remove" type="button"><i class="fa fa-trash"></i></button>' +
        '</div>' +
        '<span class="m-l-sm"><%= name %></span>')
});
/**
 * Created by filatov on 07.03.2017.
 */
var BaseGisModalView = Backbone.View.extend({
    gisUrl: 'http://gis.tp.toris.vpn/gistoris3/',
    modal: null,
    mapFrameId: 'mapFrame',
    mapFrame: null,
    subSystemCode: 'urn:eis:toris:ap',
    gisMapLib: null,
    mapModalId: 'mapModal',

    initialize: function (attributes) {
        this.attributes = attributes;
        if (!!attributes.mapSelectBox.options.gisUrl) {
            this.gisUrl = attributes.mapSelectBox.options.gisUrl;
        }
        if (!!attributes.mapSelectBox.options.subSystemCode) {
            this.subSystemCode = attributes.mapSelectBox.options.subSystemCode;
        }
        if (!!attributes.mapModalId) {
            this.mapModalId = this.attributes.mapModalId;
        }
        this.modal = $('#' + this.mapModalId);
        this.aistoken = attributes.mapSelectBox.options.aistoken;
        this.gisMapLib = new MyGisMap({
            url: this.gisUrl,
            aistoken: this.aistoken,
            subsystem: this.subSystemCode,
            modules: ['BasemapGallery', 'Export', 'Measurement', 'Network', 'Swipe', 'Toc', 'Search', 'UserLayers', 'Addressprograms']
        });

    },
    render: function () {
        this.modal.find('.modal-body #map-content').html(this.gisMapLib.el);
        this.gisMapLib.render();
        this.mapFrame = document.getElementById(this.mapFrameId);
        this.modal.modal();

    },
});

var GisModalView = BaseGisModalView.extend({
    initialize: function (attributes) {
        BaseGisModalView.prototype.initialize.call(this, attributes);
        this.gisMapLib.setSelectOidSuccess(function(edata){
            AppEvent.trigger('showOnMap:selectOid', edata);
        });
    },
    render: function () {
        BaseGisModalView.prototype.render.call(this);
        this.afterRender();
    },
    afterRender: function () {
        var json = this.attributes.mapSelectBox.gisJsonInput.val();
        if (!!json) {
            var pointInfo = JSON.parse(json);
            if (pointInfo.type == this.attributes.mapSelectBox.TYPE_EXISTS_OBJECT && pointInfo.layerId && pointInfo.oid) {
                var modalBody = this.modal.find('.modal-body');
                this.gisMapLib.showObjectsOnLayer(
                    pointInfo.layerId,
                    pointInfo.oid,
                    function() {AppSpinner.stop(modalBody)},
                    function() {AppSpinner.stop(modalBody)}
                    );
                AppSpinner.start(modalBody);
            }
        }
    },

    addObjectOnMap: function (typeObj) {
        var _this = this;
        var objectUid = this.genObjectUidForWork();

        if (!!objectUid && !!typeObj &&!!typeObj.layerId && !!typeObj.type) {
            this.gisMapLib.addObjectOnLayer(typeObj.layerId, objectUid, function () {
                toastr.success("Объект успешно нанесен");
                _this.modal.modal('hide');
                var data = {
                    oid: objectUid,
                    type: typeObj.type,
                    layerId: typeObj.layerId
                };
                _this.attributes.mapSelectBox.gisJsonInput.val(JSON.stringify(data));
                _this.attributes.mapSelectBox.initButton();
            }, function (code, message) {
                toastr.error(message);
                _this.modal.modal('hide');
            });
        } else {
            toastr.error('Ошибка в генерации ИД объекта.');
            _this.modal.modal('hide');
            _this.attributes.mapSelectBox.refreshButton();
        }
    },
    moveObjectOnMap: function () {
        var _this = this;
        var jsonData = JSON.parse(this.attributes.mapSelectBox.gisJsonInput.val());
        var typeObj = this.attributes.mapSelectBox.mapObjectTypeData(jsonData.type, jsonData.layerId);
        var objectUid = jsonData.oid;

        if (!!objectUid && !!typeObj.layerId) {
            this.gisMapLib.moveObjectOnLayer(typeObj.layerId, objectUid, function () {
                toastr.success("Объект успешно перемещен");
                _this.modal.modal('hide');
                var data = {
                    oid: objectUid,
                    type: typeObj.type,
                    layerId: typeObj.layerId
                };
                _this.attributes.mapSelectBox.gisJsonInput.val(JSON.stringify(data));
                _this.attributes.mapSelectBox.initButton();
            }, function (code, message) {
                toastr.error(message);
                _this.modal.modal('hide');
            });
        } else {
            toastr.error('Отсутствует идентификатор объекта.');
            _this.modal.modal('hide');
            _this.attributes.mapSelectBox.refreshButton();
        }
    },
    removeObjectFromLayer: function () {
        var _this = this;
        var jsonData = JSON.parse(this.attributes.mapSelectBox.gisJsonInput.val());
        var typeObj = this.attributes.mapSelectBox.mapObjectTypeData(jsonData.type, jsonData.layerId);
        var objectUid = jsonData.oid;

        if (!!typeObj && !!objectUid) {
            this.gisMapLib.removeObjectFromLayer(typeObj.layerId, objectUid, function () {
                toastr.success("Объект успешно удален");
                _this.modal.modal('hide');

                _this.attributes.mapSelectBox.refreshButton();
            }, function (code, message) {
                toastr.error(message);
                if (code == 1) {
                    _this.attributes.mapSelectBox.refreshButton();
                    toastr.error("Информация об объекте удалена в связи с его отсутствием на карте");
                }
                _this.modal.modal('hide');
            });
        } else {
            toastr.error('Ошибка при попытке удаления объекта. Неверные параметры.');
            _this.attributes.mapSelectBox.refreshButton();
        }
    },
    genObjectUidForWork: function () {
        var objectUid = null;

        $.ajax({
            url: "/works/api/object-uid",
            type: "get",
            async: false,
            success: function (data) {
                objectUid = parseInt(data);
            }
        });

        return objectUid;
    }
});

var FindAddressModalView = BaseGisModalView.extend({
    geolocateAddress: function (address) {
        var _this = this;

        if (!!address) {
            this.gisMapLib.geolocateAddress(address, function () {
                toastr.success("Адрес найден");
            }, function (code, message) {
                toastr.error(message);
                // _this.modal.modal('hide');
            });
        } else {
            toastr.error('Ошибка при попытке поиска адреса. Пожалуйста, введите адрес и повторите попытку');
        }
    }
});
/**
 * Created by filatov on 26.05.2017.
 */
var WorkBind = function (options) {
    var self = this;

    this.options = {
        checkboxClass: 'checkbox-column',
        storageKey: 'workBind',
        bindCountId: 'bind-count',
        removeAllId: 'remove-all',
        bindButtonId: 'bind-button',
        className: ''
    };
    _.extend(this.options, !!options ? options : {});

    var $checkboxes = null;
    var $bindCount = null;
    var $removeAll = null;
    var $bindButton = null;
    var collection = {};
    var laddaTimeout = 1000;
    var storageKey = self.options.storageKey+'-'+self.options.className;

    this.init = function () {
        $checkboxes = $('.' + self.options.checkboxClass);
        $bindCount = $('#' + self.options.bindCountId);
        $removeAll = $('#' + self.options.removeAllId);
        $bindButton = $('#' + self.options.bindButtonId);

        var storageCollection = JSON.parse(localStorage.getItem(storageKey));
        collection = _.allKeys(storageCollection).length == 0 ? self.options.arBound : storageCollection;
        self.updateBindCount();
        self.handleICheck();

        $checkboxes.on('ifChecked', self.add);
        $checkboxes.on('ifUnchecked', self.remove);
        $removeAll.on('click', self.removeAll);
        $bindButton.on('click', self.bind);

    };

    this.add = function (e) {
        var $target = $(e.target);
        collection[$target.val()] = $target.val();
        self.update();
    };

    this.remove = function (e) {
        var $target = $(e.target);
        delete collection[$target.val()];
        self.update();
    };

    this.update = function () {
        localStorage.setItem(storageKey, JSON.stringify(collection));
        self.updateBindCount();
    };

    this.updateBindCount = function () {
        $bindCount.text(_.allKeys(collection).length);
    };

    this.handleICheck = function () {
        $checkboxes.iCheck('uncheck');
        $checkboxes.each(function () {
            var $ch = $(this);
            if (collection[$ch.val()]) {
                $ch.iCheck('check');
            }
        });
    };

    this.removeAll = function () {
        collection = {};
        self.update();
        self.handleICheck();
    };

    this.bind = function () {
        var l = $bindButton.ladda();
        l.ladda('start');

        $.ajax({
            method: 'post',
            data: {
                id: self.options.id,
                className: self.options.className,
                works: JSON.stringify(collection)
            },
            url: self.options.bindUrl,
            success: function (outMsg) {
                setTimeout(function() {
                    toastr.success(outMsg.msg + ' <a href="'+self.options.backUrl+'"><i class="fa fa-undo"></i></a>', 'Выполнено');
                }, laddaTimeout);
            },
            complete: function() {
                setTimeout(function() {
                    l.ladda('stop');
                }, laddaTimeout);
            }
        });
    };

    this.init();
    return this;
};
var WorkStagesForm = function () {
    this.init = function () {
        $('#work-stages-multiple-input').on('afterAddRow', function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green'
            });
            console.log('calls on after initialization event');
        });
    };
    this.init();
    return this;
};

/**
 * Created by filatov on 22.03.2017.
 */
var CopyWorkView = Backbone.View.extend({
    modalView: null,
    el: '.copy-work',
    events: {
        'click': 'onClick'
    },
    initialize: function () {
        console.log(arguments);
    },
    onClick: function (ev) {
        var id = $(ev.currentTarget).data('model-id')
        this.modalView = new CopyWorkModalView({workId: id});
        this.modalView.render().modal();
    },
});

/**
 * Created by filatov on 22.03.2017.
 */
var CopyWorkModalView = Backbone.View.extend({
    url: '/works/default/copy',
    content: '',
    el: $('.copy-work'),
    initialize: function (attributes) {
        this.attributes = attributes;
        this.$modal = $('#workCopyModal');
        this.spinner();
        this.cloneRequest();
    },
    setContent: function (html) {
        this.content = html;
        return this.render();
    },
    render: function () {
        this.$modal.find('.modal-body').html(this.content);
        return this;
    },
    modal: function() {
        this.$modal.modal();
    },
    spinner: function () {
        this.setContent(AppSpinner.getHtml());
    },
    cloneRequest: function() {
        var _this = this;
        $.ajax({
            method: 'get',
            url: this.url,
            data: {id: this.attributes.workId},
            success: function (msgCont) {
                if (msgCont.status) {
                    toastr.success(msgCont.msg);
                    _this.setContent(msgCont.data.html);
                    eval(msgCont.data.js);
                } else {
                    toastr.error(msgCont.msg);
                    _this.setContent('<div class="center-block">' + msgCont.msg + '</div>');
                }


            },
            error: function (err) {
                _this.setContent('<div class="center-block">' + err.responseText + '</div>');
                toastr.error(err.responseText);
            }
        });
    }

});

var ContractProgress = function (options) {
    var self = this;
    this.widgetModel = null;
    var WidgetModel = Backbone.Model.extend({});
    var WidgetView = Backbone.View.extend({
        initialize: function () {
        },
        render: function () {
            var progressHeaderView = new ProgressHeaderView({
                model: self.widgetModel
            });
            this.$el.find('.ibox-title').html(progressHeaderView.render().$el);
            var progressBarView = new ProgressBarView({
                model: self.widgetModel
            });
            this.$el.find('#acticity-progress').html(progressBarView.render().$el);

            var stageListView = new StageListView();
            self.widget.$el.find('.ibox-content .activity-stream').html(stageListView.render().$el);

            return this;
        },
        setContainer: function () {
            this.setElement($('#'+self.widgetModel.get('containerId')));
        }
    });
    var ProgressHeaderView = Backbone.View.extend({
        initialize: function () {
            this.listenTo(this.model, 'change:percent', this.render);
        },
        render: function () {
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        },
        template: _.template('Процент выполнения договора: <%= percent%>%'),
    });
    var ProgressBarView = Backbone.View.extend({
        initialize: function () {
            this.listenTo(this.model, 'change:percent', this.render);
        },
        render: function () {
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        },
        template: _.template('<div class="progress progress-striped active progress-bar-default">' +
            '<div style="width: <%= percent%>%" role="progressbar" class="progress-bar"></div>' +
            '</div>'),
    });
    var StageModel = Backbone.Model.extend({
        url: '/works/stages/',
        query: function(data, callback, args) {
            $.ajax({
                url: this.url + args.method,
                type: 'GET',
                data: data,
                success: function(res, status, xhr){
                    if(callback && 'success' in callback) callback.success(res);
                },
                error: function(xhr, res) {
                    var msg = '/works/stages/api-change-active error.';
                    if (!!xhr.responseJSON.message) {
                        msg += ' '+xhr.responseJSON.message;
                    }
                    if(callback && 'error' in callback) {
                        callback.error(xhr);
                    } else {
                        toastr.error(msg);
                    }

                }
            }).complete( function(res) {
                if(callback && 'complete' in callback) callback.complete(res);
            });
        },
        perform: function(callback) {
            this.query(this.pick('id'), callback, {method: 'api-perform'});
            self.widgetModel.set('percent', this.get('offset') + this.get('progress'));
        },
        setActive: function(callback) {
            this.query(this.pick('id'), callback, {method: 'api-set-active'});
            self.widgetModel.set('percent', this.get('offset'));
        },
    });
    var StageCollection = Backbone.Collection.extend({
        workContractId: null,
        model: StageModel,
        url: function () {
            return '/works/stages/api-index?workContractId=' + this.workContractId
        }
    });
    var StageListView = Backbone.View.extend({
        initialize: function () {
            this.collection = new StageCollection();
            this.listenTo(this.collection, 'sync', this.render);
            this.listenTo(this.collection, 'change', self.render);

            this.collection.workContractId = self.widgetModel.get('workContractId');
            this.collection.fetch();
            this.childViews = [];
        },
        render: function () {
            this.clearChildViews();

            _.each(this.collection.models, function (model) {
                var view = new StageView({
                    model: model
                });
                this.addChildView(view);
            }, this);
            return this;
        },
        addChildView: function (view) {
            this.childViews.push(view);
            this.$el.append(view.render().$el);
        },
        clearChildViews: function () {
            _.each(this.childViews, function (child) {
                child.remove();
            });

            this.childViews = [];
        },
    });
    var StageView = Backbone.View.extend({
        className: function() {
            return this.model.get('makeup').stream_css_class
        },
        events: {
            'click .perform': 'perform',
            'click .revert': 'setActive',
            'click .set-active': 'setActive'
        },
        initialize: function (options) {
        },
        render: function () {
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        },
        template: _.template('<%= makeup.stream_badge%>' +
            '<div class="stream-panel">' +
                '<div class="stream-info">' +
                    '<%= makeup.date_label%> ' +
                    '<span>Контрольный срок</span> ' +
                    '<span class="date"><%= control_date%></span>' +
                '</div>' +
                '<div class="row"><div class="col-lg-12">' +
                    '<span class="stream-text"><%= name%></span>' +
                    '<%= makeup.action_button%>' +
                '</div></div>' +
                '<div class="progress progress-mini m-t-md">' +
                    '<div style="width: <%= progress%>%; margin-left: <%= offset%>%;" role="progressbar" class="progress-bar progress-bar-success"></div>' +
                '</div>' +
                '<small>Занимает всей работы: <%= progress%>%</small>' +
            '</div>'
        ),
        perform: function(e) {
            this.model.perform({
                success: function () {
                    self.widget.render();
                }
            });
            e.preventDefault();
        },
        setActive: function(e) {
            this.model.setActive({
                success: function () {
                    self.widget.render();
                }
            });
            e.preventDefault();
        },
    });

    this.init = function (options) {
        this.widgetModel = new WidgetModel(options || {});
        this.widget = new WidgetView();
        this.widget.setContainer();
        this.widget.render();
    };

    this.init(options);
    return this;
};
/**
 * Created by filatov on 24.03.2017.
 */
var filterTableDoubleClick = {
    init: function () {
        $('table.table tr').on('dblclick', function (e) {
            var a = $(e.currentTarget).find('a[update-action]');
            if(a.length > 0) {
                window.location.href = a.prop('href');
            }
        });
    }
};

$(function () {
    filterTableDoubleClick.init();
});
/**
 * Created by filatov on 29.03.2017.
 */
var AppSpinner = {
    html: '<div class="sync-layer"><div class="middle-box text-center">' +
    '<h1><i class="fa fa-spinner fa-spin"></i></h1>' +
    '</div></div>',
    start: function ($el) {
        $el.prepend(this.html);
    },
    stop: function ($el) {
        $el.find('.sync-layer').remove();
    },
    getHtml: function () {
        return this.html;
    }
};
/**
 * Created by filatov on 28.06.2017.
 */
/**
 * Created by filatov on 26.05.2017.
 */
var AreasImport = function (options) {
    var self = this;

    this.options = {
        importBtnId: null,
        importUrl: null,
    };
    _.extend(this.options, !!options ? options : {});

    
    var $importButton = null;
    var laddaTimeout = 1000;

    this.init = function () {
        $importButton = $('#' + self.options.importBtnId);

        $importButton.on('click', self.import);

    };

    this.import = function () {
        var l = $importButton.ladda();
        l.ladda('start');

        $.ajax({
            method: 'get',
            data: {
            },
            url: self.options.importUrl,
            success: function (outMsg) {
                setTimeout(function() {
                    toastr.success(outMsg.msg, 'Выполнено');
                }, laddaTimeout);
            },
            complete: function() {
                setTimeout(function() {
                    l.ladda('stop');
                }, laddaTimeout);
            }
        });
    };

    this.init();
    return this;
};
/**
 * Created by filatov on 26.05.2017.
 */
var AdminDashboard = function (options) {
    var self = this;

    this.options = {
        importButtonClass: 'import-action',
        deleteButtonClass: 'delete-action',
        resultAreaId: 'result-area',
        importUrl: null,
        deleteUrl: null,
        dynamicToStaticUrl: null,
    };
    _.extend(this.options, !!options ? options : {});

    var $importButtons = null;
    var $deleteButtons = null;
    var $resultArea = null;
    var laddaTimeout = 1000;

    this.init = function () {
        $importButtons = $('.' + self.options.importButtonClass);
        $deleteButtons = $('.' + self.options.deleteButtonClass);
        $resultArea = $('#' + self.options.resultAreaId);

        $importButtons.on('click', self.startImport);
        $deleteButtons.on('click', self.delete);
        $('.dynamic-to-static').on('click', self.dynamicToStatic);
    };

    this.startImport = function (ev) {
        $btn = $(ev.currentTarget);
        var l = $btn.ladda();
        l.ladda('start');

        $.ajax({
            method: 'get',
            data: {
                command: $btn.data('command'),
            },
            url: self.options.importUrl,
            success: function (outMsg) {
                setTimeout(function() {
                    toastr.success('Выполнено');
                    $resultArea.html(outMsg.msg);
                }, laddaTimeout);
            },
            complete: function() {
                setTimeout(function() {
                    l.ladda('stop');
                }, laddaTimeout);
            },
            error: function (er) {
                toastr.error('Ошибка');
                $resultArea.html(er);
            }
        });
    };

    this.delete = function (ev) {
        $btn = $(ev.currentTarget);
        var l = $btn.ladda();
        l.ladda('start');

        $.ajax({
            method: 'get',
            data: {
                method: $btn.data('command'),
            },
            url: self.options.deleteUrl,
            success: function (outMsg) {
                setTimeout(function() {
                    toastr.success('Выполнено');
                    $resultArea.html(outMsg.msg);
                }, laddaTimeout);
            },
            complete: function() {
                setTimeout(function() {
                    l.ladda('stop');
                }, laddaTimeout);
            },
            error: function () {
                toastr.error('Ошибка');
            }
        });
    };

    this.dynamicToStatic = function (ev) {
        $btn = $(ev.currentTarget);
        var l = $btn.ladda();
        l.ladda('start');

        $.ajax({
            method: 'get',
            url: self.options.dynamicToStaticUrl,
            success: function (outMsg) {
                setTimeout(function() {
                    toastr.success('Выполнено');
                    $resultArea.html(outMsg.msg);
                }, laddaTimeout);
            },
            complete: function() {
                setTimeout(function() {
                    l.ladda('stop');
                }, laddaTimeout);
            },
            error: function () {
                toastr.error('Ошибка');
            }
        });
    };

    this.init();
    return this;
};