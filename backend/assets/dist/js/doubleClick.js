/**
 * Created by filatov on 24.03.2017.
 */
var filterTableDoubleClick = {
    init: function () {
        $('table.table tr').on('dblclick', function (e) {
            var a = $(e.currentTarget).find('a[update-action]');
            if(a.length > 0) {
                window.location.href = a.prop('href');
            }
        });
    }
};

$(function () {
    filterTableDoubleClick.init();
});