/**
 * Created by filatov on 29.03.2017.
 */
var AppSpinner = {
    html: '<div class="sync-layer"><div class="middle-box text-center">' +
    '<h1><i class="fa fa-spinner fa-spin"></i></h1>' +
    '</div></div>',
    start: function ($el) {
        $el.prepend(this.html);
    },
    stop: function ($el) {
        $el.find('.sync-layer').remove();
    },
    getHtml: function () {
        return this.html;
    }
};