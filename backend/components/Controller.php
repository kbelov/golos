<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 29.09.17
 * Time: 13:21
 */

namespace backend\components;

use backend\assets\TorisAsset;
use backend\helpers\TorisHelper;
use backend\modules\toris\models\UserToris;
use backend\modules\toris\models\UserTorisRolesLinks;
use common\components\ActiveRecord;
use common\components\EventManager;
use common\components\Url;
use common\models\History;
use common\modules\users\services\AddNewAudit;
use Yii;

/**
 * Class Controller
 *
 * @package backend\components
 */
class Controller extends \yii\web\Controller
{

    /** @var ActiveRecord */
    protected $model;
    /** @var ActiveRecord */
    protected $modelClass;
    /** @var History */
    protected $historyModel;
    /** @var boolean */
    protected $auditEnable = true;

    /**
     * @param int $id
     * @return \yii\web\Response
     */
    public function actionRevive(int $id)
    {
        if (null !== $this->modelClass && class_exists($this->modelClass)) {
            $model = $this->modelClass::findOne($id);
            if (null !== $model) {
                if ($model->hasAttribute('visible')) {
                    $model->visible = true;
                    $model->save();
                }
                $url = (defined($this->modelClass . '::MODULE_VIEW_PATH')) ? $this->modelClass::MODULE_VIEW_PATH : ['index'];
                return $this->redirect([$url, 'id' => $model->primaryKey]);
            }
        }
        return $this->redirect(['index']);
    }

    /**
     * @param \yii\base\Action $action
     *
     * @return bool|\yii\web\Response
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\InvalidValueException
     */
    public function beforeAction($action)
    {
        if (!Yii::$app->getUser()->getIsGuest()) {
            $userRolesLinksModel = UserTorisRolesLinks::find()->where([
                'user_toris_id' => Yii::$app->getUser()->getId(),
            ])->all();
            $userRolesLinks = [];
            foreach ($userRolesLinksModel as $links) {
                $userRolesLinks[] = $links->name;
            }
            if (array_intersect([
                TorisHelper::ROLE_ADMIN,
                TorisHelper::ROLE_MODERATOR,
                TorisHelper::ROLE_MANAGER,
                TorisHelper::ROLE_OPERATOR_IOGV
            ], $userRolesLinks)) {
                $route = $action->getUniqueId();
                if ($route === 'site/index') {
                    Yii::$app->defaultRoute = '/polls/default/index';
                    return $this->redirect('/polls/default/index');
                }
            } elseif (array_intersect([
                TorisHelper::ROLE_MFC
            ], $userRolesLinks)) {
                $route = $action->getUniqueId();
                if ($route === 'site/index') {
                    return $this->redirect('/gifts/mfc/index');
                }
            }
        }
        if (!Yii::$app->request->isAjax) {
            $view = $this->getView();
            TorisAsset::register($view);
            $return_url = Url::toRoute(['/toris/default/access'], true);
            $logout_url = Url::toRoute(['/toris/default/logout'], true);

            $torisDomain = TorisHelper::DOMAIN;

            $view->registerJs('TorisWidget.init("' . $return_url . '", "' . $logout_url . '", true, "'.$torisDomain.'", "'. TorisHelper::CODE .'");');
        }
        return parent::beforeAction($action);
    }

    /**
     *
     */
    public function init()
    {
        parent::init();
        EventManager::init();
    }

    public function afterAction($action, $result)
    {
        if ($this->auditEnable && $this->model instanceof ActiveRecord) {
            $userToris = UserToris::findIdentity(Yii::$app->getUser()->getId());
            $audit = AddNewAudit::getInstance()
                ->setOwnerClass(get_class($this->model))
                ->setOwnerId($this->model->primaryKey)
                ->setType($action->id)
                ->setUserToris($userToris);
            if (null !== $this->historyModel) {
                $audit->setHistory($this->historyModel);
            }
            $audit->run();
        }
        return parent::afterAction($action, $result);
    }
}
