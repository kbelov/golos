<?php

namespace backend\components;

/**
 * Class GridView
 *
 * @package backend\components
 */
class GridView extends \yii\grid\GridView
{
    /**
     * @var array
     */
    public $options = [
        'style' => 'white-space:normal;'
    ];
}
