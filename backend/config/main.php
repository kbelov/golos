<?php

use backend\modules\toris\models\UserToris;
use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\log\FileTarget;
use yii\web\AssetManager;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'name'      => 'Административная панель "Голос Питера"',
    'language' => 'ru-RU',
    'bootstrap' => ['log'],
    'modules' => [
        'polls'         => \backend\modules\polls\Module::class,
        'news'          => \backend\modules\news\Module::class,
        'gifts'         => \backend\modules\gifts\Module::class,
        'blocks'        => \backend\modules\blocks\Module::class,
        'areas'         => \backend\modules\areas\Module::class,
        'templates'     => \backend\modules\templates\Module::class,
        'pages'         => \backend\modules\pages\Module::class,
        'mfc'           => \backend\modules\mfc\Module::class,
        'toris'         => \backend\modules\toris\Module::class,
        'users'         => \backend\modules\users\Module::class,
        'history'       => \backend\modules\history\Module::class,
        'gridview'      => \kartik\grid\Module::class
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
            'cookieValidationKey' => '7fdsf%dbYd&djsb#sn0mlsfo(kj^kf98dfh',
            'baseUrl' => '',
        ],
        'user' => [
            'identityClass' => UserToris::class,
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
            'loginUrl' => ['/toris/guest/login']
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => FileTarget::class,
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => FileTarget::class,
                    'logFile' => "@runtime/logs/sms.log",
                    'categories' => ['sms']
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager'   => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => [
            ],
        ],
        'formatter' => [
            'dateFormat' => 'dd.MM.yyyy',
        ],
        'assetManager' => [
            'linkAssets' => true,
            'class' => AssetManager::class,
            'bundles' => [
                BootstrapAsset::class => [
                    'css'   => [],
                    'js'    => []
                ]
            ],
        ],
    ],
    'params' => $params,
];
