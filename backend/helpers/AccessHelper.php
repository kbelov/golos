<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 04.10.17
 * Time: 15:55
 */

namespace backend\helpers;

/**
 * Class AccessHelper
 *
 * @package backend\helpers
 */
class AccessHelper
{
    const MAIN_SITE = 'app-backend/site';

    const POLLS_DEFAULT = 'polls/default';
    const POLLS_VOTES = 'polls/votes';
    const POLLS_COMMENTS = 'polls/comments';

    const AREAS_DEFAULT = 'areas/default';

    const BLOCKS_DEFAULT = 'blocks/default';

    const GIFTS_DEFAULT = 'gifts/default';
    const GIFTS_MFC = 'gifts/mfc';

    const MFC_DEFAULT = 'mfc/default';

    const NEWS_DEFAULT = 'news/default';

    const PAGES_DEFAULT = 'pages/default';

    const TEMPLATE_DEFAULT = 'templates/default';

    const USERS_DEFAULT = 'users/default';

    const ROLES_ARRAY = [
        self::POLLS_DEFAULT     => 'Ресурс голосований',
        self::POLLS_VOTES       => 'Ресурс вариантов ответов голосований',
        self::POLLS_COMMENTS    => 'Ресурс комментариев голосований',
        self::AREAS_DEFAULT     => 'Ресурс районов',
        self::BLOCKS_DEFAULT    => 'Ресурс блоков',
        self::GIFTS_DEFAULT     => 'Ресурс магазина поощрений',
        self::GIFTS_MFC         => 'Ресурс заказов',
        self::MFC_DEFAULT       => 'Ресурс МФЦ',
        self::NEWS_DEFAULT      => 'Ресурс новостей',
        self::PAGES_DEFAULT     => 'Ресурс страниц',
        self::TEMPLATE_DEFAULT  => 'Ресурс шаблонов тем',
        self::USERS_DEFAULT     => 'Ресурс пользователей'
    ];

    const ROLES_ARRAY_DESCRIPTION = [
        self::POLLS_DEFAULT     => 'Добавление голосования с указанием его наименования и полного описания, указание статуса для голосования, добавление информации для публикации на статических страницах, формирование отчета о проводимых Голосованиях в формате docx, формирование сводного отчета в формате docx, согласовывать голосования',
        self::POLLS_VOTES       => 'Добавление вариантов ответов для каждого голосования с указанием названия варианта ответа (заголовка), полного описания, а также прикреплением не менее одного (при необходимости) файла фотографии в формате jpg',
        self::POLLS_COMMENTS    => 'Просмотр и удаление комментария',
        self::AREAS_DEFAULT     => 'Добавление, редактирование, удаление внутрисистемного классификатора (справочника)',
        self::BLOCKS_DEFAULT    => 'Добавления, редактирования и архивирования контента статических страниц',
        self::GIFTS_DEFAULT     => 'Добавления, редактирования и архивирования контента статических страниц',
        self::GIFTS_MFC         => 'Подтверждения заказа, добавление статуса заказа, отмена заказа',
        self::MFC_DEFAULT       => 'Добавления, редактирования и архивирования контента статических страниц',
        self::NEWS_DEFAULT      => 'Добавления, редактирования и архивирования контента статических страниц',
        self::PAGES_DEFAULT     => 'Добавления, редактирования и архивирования контента статических страниц',
        self::TEMPLATE_DEFAULT  => 'Добавления, редактирования и архивирования контента статических страниц',
        self::USERS_DEFAULT     => 'Позволять ввод и редактирование учетных сведений о пользователе, просмотр списка зарегистрированных пользователей, просмотр учетных сведений о пользователе, просмотр назначенных пользователю ролей, блокирование учетной записи пользователя'
    ];
}
