<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 04.10.17
 * Time: 12:41
 */

namespace backend\helpers;

use backend\modules\toris\models\UserTorisRolesAccess;
use backend\modules\toris\models\UserTorisRolesLinks;
use Yii;
use yii\web\ForbiddenHttpException;

/**
 * Class TorisHelper
 *
 * @package backend\helpers
 */
class TorisHelper
{
    const DOMAIN = 'toris.gov.spb.ru';
    const CODE = 'urn:eis:ext:portal_omg';

    const ROLE_ADMIN = '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:admin';
    const ROLE_MANAGER = '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:manager';
    const ROLE_MODERATOR = '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:moderator';
    const ROLE_OPERATOR_IOGV = '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:operator_iogv';
    const ROLE_MFC = '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:mfc';

    const ROLES_ARRAY = [
        self::ROLE_ADMIN            => 'Администратор',
        self::ROLE_MANAGER          => 'Контент-менеджер',
        self::ROLE_MODERATOR        => 'Модератор',
        self::ROLE_OPERATOR_IOGV    => 'ИОГВ',
        self::ROLE_MFC              => 'МФЦ'
    ];

    const ROLES_SHORT_NAMES = [
        self::ROLE_ADMIN            => 'admin',
        self::ROLE_MODERATOR        => 'moderator',
        self::ROLE_MFC              => 'mfc',
        self::ROLE_MANAGER          => 'manager',
        self::ROLE_OPERATOR_IOGV    => 'iogv'
    ];

    /**
     * @throws \yii\web\ForbiddenHttpException
     */
    public static function checkAccess()
    {
        if (Yii::$app->getUser()->getIsGuest()) {
            throw new ForbiddenHttpException();
        }
        $userRolesLinksModel = UserTorisRolesLinks::find()->where([
            'user_toris_id' => Yii::$app->getUser()->getId(),
        ])->all();
        $userRolesLinks = [];
        foreach ($userRolesLinksModel as $links) {
            $userRolesLinks[] = $links->name;
        }
        if (in_array(self::ROLE_ADMIN, $userRolesLinks, true)) {
            return true;
        }

        $accessArray = UserTorisRolesAccess::getAccessArray();

        $route = Yii::$app->controller->module->id . '/' . Yii::$app->controller->id;
        if (isset($accessArray[$route])) {

            if(empty(array_intersect($userRolesLinks, $accessArray[$route]))) {
                throw new ForbiddenHttpException();
            }
        } else {
            throw new ForbiddenHttpException();
        }
    }

    /**
     * @param string $route
     *
     * @return bool
     */
    public static function checkAccessForPage(string $route): bool
    {
        if (Yii::$app->getUser()->getIsGuest()) {
            return false;
        }
        $userRolesLinksModel = UserTorisRolesLinks::find()->where([
            'user_toris_id' => Yii::$app->getUser()->getId(),
        ])->all();
        $userRolesLinks = [];
        foreach ($userRolesLinksModel as $links) {
            $userRolesLinks[] = $links->name;
        }

        if (in_array(self::ROLE_ADMIN, $userRolesLinks, true)) {
            return true;
        }

        $accessArray = UserTorisRolesAccess::getAccessArray();

        if (isset($accessArray[$route])) {

            if(empty(array_intersect($userRolesLinks, $accessArray[$route]))) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }
}
