<?php

namespace backend\modules\areas\controllers;

use backend\helpers\TorisHelper;
use backend\modules\areas\Module;
use backend\modules\mfc\models\Mfc;
use Yii;
use backend\modules\areas\models\Areas;
use backend\modules\areas\models\search\AreasSearch;
use backend\components\Controller;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for Areas model.
 *
 * @property Module module
 */
class DefaultController extends Controller
{
    /** @var string */
    protected $modelClass = Areas::class;

    /**
     * Lists all Areas models.
     *
     * @return mixed
     * @throws \yii\base\InvalidParamException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionIndex()
    {
        TorisHelper::checkAccess();
        $searchModel = new AreasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Areas model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\base\InvalidParamException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionView($id)
    {
        $this->model = $this->findModel($id);
        TorisHelper::checkAccess();
        return $this->render('view', [
            'model' => $this->model,
        ]);
    }

    /**
     * Creates a new Areas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws \yii\base\InvalidParamException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionCreate()
    {
        TorisHelper::checkAccess();
        $model = new Areas();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Areas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\base\InvalidParamException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        TorisHelper::checkAccess();
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Areas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException
     * @throws \Exception
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionDelete($id)
    {
        TorisHelper::checkAccess();
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     *
     * @return string
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\base\InvalidParamException
     */
    public function actionUsed($id): string
    {
        TorisHelper::checkAccess();

        $model = $this->findModel($id);

        $dataProvider = new ActiveDataProvider([
            'query' => Mfc::find()
                ->active()
                ->byArea($model)
                ->orderBy('created DESC'),
            'pagination' => [
                'pageSize' => $this->module->recordsPerPage
            ]
        ]);

        return $this->render('used', [
            'model'         => $model,
            'dataProvider'  => $dataProvider,
        ]);
    }

    /**
     * Finds the Areas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Areas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Areas::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
