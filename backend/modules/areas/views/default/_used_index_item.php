<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 06.10.17
 * Time: 14:11
 */

use common\components\Html;

/** @var \backend\modules\mfc\models\Mfc $model */

?>
<td><?=Html::a($model->name, ['/mfc/default/view', 'id' => $model->id]); ?></td>
