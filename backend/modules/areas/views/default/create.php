<?php


/* @var $this yii\web\View */
/* @var $model backend\modules\areas\models\Areas */

$this->title = 'Добавление рейона';
$this->params['breadcrumbs'][] = ['label' => 'Районы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="areas-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
