<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\areas\models\Areas */

$this->title = 'Обновление района: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Районы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="areas-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
