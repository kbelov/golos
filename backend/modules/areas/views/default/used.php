<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 06.10.17
 * Time: 14:10
 */

use yii\widgets\ListView;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \backend\modules\areas\models\Areas $model */

$this->title = 'Использование района "'.$model.'" в МФЦ';

$this->params['breadcrumbs'][] = ['label' => 'Районы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="row">
    <div class="col-lg-8">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>МФЦ</th>
            </tr>
            </thead>
            <?= ListView::widget(
                [
                    'dataProvider' => $dataProvider,
                    'layout' => "{items}\n<div class='col-lg-12'>{pager}</div>",
                    'itemView' => '_used_index_item',
                    'options' => [
                        'class' => '',
                        'tag'   => 'tbody'
                    ],
                    'itemOptions' => [
                        'class' => '',
                        'tag' => 'tr'
                    ]
                ]
            ); ?>
        </table>
    </div>
</div>
