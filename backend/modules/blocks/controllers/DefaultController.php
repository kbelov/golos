<?php

namespace backend\modules\blocks\controllers;

use backend\components\Controller;
use backend\helpers\TorisHelper;
use backend\modules\blocks\models\Blocks;
use backend\modules\blocks\models\search\BlocksSearch;
use backend\modules\blocks\Module;
use vova07\fileapi\actions\UploadAction;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * DefaultController implements the CRUD actions for Blocks model.
 *
 * @property Module module
 */
class DefaultController extends Controller
{

    protected $modelClass = Blocks::class;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'fileapi-upload' => [
                'class' => UploadAction::className(),
                'path' => $this->module->imageTempPath
            ]
        ];
    }

    /**
     * Lists all Blocks models.
     *
     * @return mixed
     * @throws \yii\base\InvalidParamException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionIndex()
    {
        TorisHelper::checkAccess();
        $searchModel = new BlocksSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Blocks model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidParamException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionView($id)
    {
        $this->model = $this->findModel($id);
        TorisHelper::checkAccess();
        return $this->render('view', [
            'model' => $this->model,
        ]);
    }

    /**
     * Creates a new Blocks model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws \yii\base\InvalidParamException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionCreate()
    {
        TorisHelper::checkAccess();
        $model = new Blocks();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Blocks model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\base\InvalidParamException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        TorisHelper::checkAccess();
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Blocks model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\db\StaleObjectException
     * @throws \Exception
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionDelete($id)
    {
        TorisHelper::checkAccess();
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Blocks model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Blocks the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Blocks::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
