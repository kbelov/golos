<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 20:26
 */

namespace backend\modules\blocks\models;

/**
 * Class Blocks
 * @package backend\modules\blocks\models
 */
class Blocks extends \common\modules\blocks\models\Blocks
{
    /**
     * @param bool $link
     * @return string
     */
    public function getExternal_linkHistoryName(bool $link): string
    {
        return ($link) ? 'Да' : 'Нет';
    }
}
