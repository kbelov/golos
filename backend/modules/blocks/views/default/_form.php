<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\fileapi\Widget as FileApi;

/* @var $this yii\web\View */
/* @var $model backend\modules\blocks\models\Blocks */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="blocks-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image')->widget(
        FileApi::className(),
        [
            'settings' => [
                'url' => ['/blocks/default/fileapi-upload']
            ]
        ]
    ) ?>

    <?= $form->field($model, 'link')->textInput() ?>

    <?= $form->field($model, 'link_name')->textInput() ?>

    <?= $form->field($model, 'external_link')->checkbox() ?>

    <?= $form->field($model, 'visible')->checkbox() ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
