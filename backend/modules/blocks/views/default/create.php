<?php


/* @var $this yii\web\View */
/* @var $model backend\modules\blocks\models\Blocks */

$this->title = 'Добавление блока';
$this->params['breadcrumbs'][] = ['label' => 'Блоки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blocks-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
