<?php

use yii\grid\ActionColumn;
use yii\grid\SerialColumn;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\blocks\models\search\BlocksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Блоки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blocks-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить блок', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => SerialColumn::class],

            'id',
            'name',
            'content',
            'external_link:boolean',
            'link:ntext',
            'link_name:ntext',
            'visible:boolean',
            'created',
            'updated',

            ['class' => ActionColumn::class],
        ],
    ]); ?>
</div>
