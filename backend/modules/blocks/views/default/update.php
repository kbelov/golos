<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\blocks\models\Blocks */

$this->title = 'Обновление блока: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Блоки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="blocks-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
