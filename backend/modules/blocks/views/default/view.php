<?php

use backend\modules\blocks\models\Blocks;
use backend\modules\history\widgets\HistoryWidget\HistoryWidget;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\blocks\models\Blocks */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Блоки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blocks-view">

    <?php if ($model->getIsVisible()): ?>
        <p>
            <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Вы уверены, что хотите удалить эту запись?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    <?php else: ?>
        <div class="alert alert-danger">
            Этот элемент удален!
        </div>
        <?= Html::a('Восстановить', ['revive', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?php endif; ?>

    <div class="row">
        <div class="col-lg-8">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'name',
                    'image',
                    'content',
                    'external_link:boolean',
                    'link:ntext',
                    'link_name:ntext',
                    'visible:boolean',
                    'created',
                    'updated',
                ],
            ]) ?>
        </div>
        <div class="col-lg-4">
            <?= HistoryWidget::widget([
                'ownerClass' => Blocks::class,
                'ownerId'    => $model->id
            ]); ?>
        </div>
    </div>

</div>
