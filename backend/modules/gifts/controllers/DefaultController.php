<?php

namespace backend\modules\gifts\controllers;

use backend\helpers\TorisHelper;
use backend\modules\gifts\Module;
use vova07\fileapi\actions\UploadAction;
use Yii;
use backend\modules\gifts\models\Gifts;
use backend\modules\gifts\models\search\GiftsSearch;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for Gifts model.
 * @property Module module
 */
class DefaultController extends Controller
{

    protected $modelClass = Gifts::class;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'fileapi-upload' => [
                'class' => UploadAction::className(),
                'path' => $this->module->imageTempPath
            ]
        ];
    }

    /**
     * Lists all Gifts models.
     *
     * @return mixed
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\base\InvalidParamException
     */
    public function actionIndex()
    {
        TorisHelper::checkAccess();
        $searchModel = new GiftsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Gifts model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidParamException
     */
    public function actionView($id)
    {
        $this->model = $this->findModel($id);
        TorisHelper::checkAccess();
        return $this->render('view', [
            'model' => $this->model,
        ]);
    }

    /**
     * Creates a new Gifts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\base\InvalidParamException
     */
    public function actionCreate()
    {
        TorisHelper::checkAccess();
        $model = new Gifts();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Gifts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidParamException
     */
    public function actionUpdate($id)
    {
        TorisHelper::checkAccess();
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Gifts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\db\StaleObjectException
     * @throws \Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        TorisHelper::checkAccess();
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Gifts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Gifts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Gifts::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
