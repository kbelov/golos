<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.17
 * Time: 12:28
 */

namespace backend\modules\gifts\controllers;

use backend\components\Controller;
use backend\helpers\TorisHelper;
use backend\modules\gifts\models\search\UserOrdersSearch;
use backend\modules\gifts\models\UserOrders;
use common\models\User;
use common\modules\gifts\helpers\UserOrderStatusHelper;
use common\modules\gifts\models\Gifts;
use common\modules\transactions\services\AddNewTransaction;
use common\services\UpdateUserPoint;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class MfcController
 *
 * @package backend\modules\gifts\controllers
 */
class MfcController extends Controller
{

    /**
     * @throws \yii\base\InvalidParamException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionIndex()
    {
        TorisHelper::checkAccess();

        $searchModel = new UserOrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserOrders model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidParamException
     */
    public function actionView(int $id)
    {
        $this->model = $this->findModel($id);
        TorisHelper::checkAccess();

        return $this->render('view', [
            'model' => $this->model,
        ]);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionReady(int $id): Response
    {
        TorisHelper::checkAccess();
        $model = $this->findModel($id);

        if (!$model->canReady()) {
            throw new ForbiddenHttpException();
        }

        $model->status = UserOrderStatusHelper::STATUS_AWAITS_DELIVERY;
        if ($model->save()) {
            Yii::$app->session->setFlash('success', 'Заказ успешно подтвержден');
        } else {
            Yii::$app->session->setFlash('danger', 'Не сохранено');
        }
        return $this->redirect(['/gifts/mfc/view', 'id' => $id]);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionDelivery(int $id): Response
    {
        TorisHelper::checkAccess();
        $model = $this->findModel($id);

        if (!$model->canDelivery()) {
            throw new ForbiddenHttpException();
        }

        $model->status = UserOrderStatusHelper::STATUS_ISSUED;
        if ($model->save()) {
            Yii::$app->session->setFlash('success', 'Заказ успешно выдан');
        } else {
            Yii::$app->session->setFlash('danger', 'Не сохранено');
        }
        return $this->redirect(['/gifts/mfc/view', 'id' => $id]);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \yii\base\Exception
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionCancel(int $id): Response
    {
        TorisHelper::checkAccess();
        $model = $this->findModel($id);

        if (!$model->canCancel()) {
            throw new ForbiddenHttpException();
        }

        $model->status = UserOrderStatusHelper::STATUS_CANCEL;
        if ($model->save()) {
            Yii::$app->session->setFlash('success', 'Заказ успешно отменен');
            // Добавляем транзакцию отката баллов
            AddNewTransaction::getInstance()
                ->setPoints($model->points)
                ->setFrom(Gifts::class)
                ->setFromId($model->gift_id)
                ->setTo(User::class)
                ->setToId($model->user_id)
                ->setUpdatePoints(false)
                ->run();

            UpdateUserPoint::getInstance()->setModel($model->user)->run();
        } else {
            Yii::$app->session->setFlash('danger', 'Не сохранено');
        }
        return $this->redirect(['/gifts/mfc/view', 'id' => $id]);
    }

    /**
     * Finds the Gifts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserOrders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(int $id)
    {
        if (($model = UserOrders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
