<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.17
 * Time: 13:07
 */

namespace backend\modules\gifts\models;

use common\modules\gifts\helpers\UserOrderStatusHelper;

/**
 * Class UserOrders
 *
 * @package backend\modules\gifts\models
 */
class UserOrders extends \common\modules\gifts\models\UserOrders
{
    /**
     * @return bool
     */
    public function canReady(): bool
    {
        return $this->status === UserOrderStatusHelper::STATUS_WAITING;
    }

    /**
     * @return bool
     */
    public function canDelivery(): bool
    {
        return $this->status === UserOrderStatusHelper::STATUS_AWAITS_DELIVERY;
    }

    /**
     * @return bool
     */
    public function canCancel(): bool
    {
        return $this->status !== UserOrderStatusHelper::STATUS_CANCEL;
    }
}
