<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.17
 * Time: 12:30
 */

namespace backend\modules\gifts\models\search;

use backend\helpers\TorisHelper;
use backend\modules\toris\models\UserTorisRolesLinks;
use common\modules\gifts\models\UserOrders;
use common\modules\mfc\models\Mfc;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class UserOrdersSearch
 *
 * @package backend\modules\gifts\models\search
 */
class UserOrdersSearch extends UserOrders
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id'], 'integer'],
            [['created', 'updated'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     * @throws \yii\base\InvalidParamException
     */
    public function search($params): ActiveDataProvider
    {
        $query = self::find();

        $userRolesLinksModel = UserTorisRolesLinks::find()->where([
            'user_toris_id' => Yii::$app->getUser()->getId(),
        ])->all();
        $userRolesLinks = [];
        foreach ($userRolesLinksModel as $links) {
            $userRolesLinks[] = $links->name;
        }
        if (!in_array(TorisHelper::ROLE_ADMIN, $userRolesLinks, true)) {
            $query->joinWith(Mfc::tableName());
            $query->andWhere([
                'in',
                Mfc::tableName().'.role',
                $userRolesLinks
            ]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created' => $this->created,
            'updated' => $this->updated,
        ]);

        return $dataProvider;
    }
}
