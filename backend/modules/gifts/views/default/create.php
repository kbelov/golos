<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\gifts\models\Gifts */

$this->title = 'Создание товара';
$this->params['breadcrumbs'][] = ['label' => 'Магазин поощрений', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gifts-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
