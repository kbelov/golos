<?php

use yii\grid\ActionColumn;
use yii\grid\SerialColumn;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\gifts\models\search\GiftsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Магазин поощрений';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gifts-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать товар', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => SerialColumn::class],

            'id',
            'name',
            'points',
            'image',
            'visible:boolean',
            'created',
            'updated',

            ['class' => ActionColumn::class],
        ],
    ]); ?>
</div>
