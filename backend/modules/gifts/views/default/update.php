<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\gifts\models\Gifts */

$this->title = 'Обновление товара: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Магазин поощрений', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="gifts-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
