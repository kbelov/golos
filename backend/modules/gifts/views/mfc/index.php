<?php

use common\widgets\grid\LinkColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\gifts\models\search\UserOrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gifts-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            [
                'attribute' => 'id',
                'label'     => '#',
                'class'     => LinkColumn::className(),
                'url'       => ['/gifts/mfc/view']
            ],
            'user',
            'gift',
            'mfc',
            'statusName'

        ],
    ]); ?>
</div>
