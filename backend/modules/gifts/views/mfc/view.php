<?php

use backend\modules\gifts\models\UserOrders;
use backend\modules\history\widgets\HistoryWidget\HistoryWidget;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\gifts\models\UserOrders */

$this->title = 'Заказ № ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gifts-view">

    <?php if ($model->canReady()): ?>
        <?= Html::a('Подтвердить готовность', ['ready', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if ($model->canDelivery()): ?>
        <?= Html::a('Подтвердить выдачу', ['delivery', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if ($model->canCancel()): ?>
        <?= Html::a('Отменить заказ', ['cancel', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>
    <?php endif; ?>

    <div class="row">
        <div class="col-lg-8">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'user',
                    'gift',
                    'mfc',
                    'statusName',
                    'created',
                    'updated',
                ],
            ]) ?>
        </div>
        <div class="col-lg-4">
            <?= HistoryWidget::widget([
                'ownerClass' => UserOrders::class,
                'ownerId'    => $model->id
            ]); ?>
        </div>
    </div>

</div>
