<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 06.10.17
 * Time: 12:23
 */

namespace backend\modules\history;

/**
 * Class Module
 *
 * @package backend\modules\history
 */
class Module extends \yii\base\Module
{
    /**
     * @var integer History per page
     */
    public $recordsPerPage = 10;

}
