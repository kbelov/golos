<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 06.10.17
 * Time: 12:52
 */

namespace backend\modules\history\controllers;

use backend\components\Controller;
use common\models\History;
use common\modules\users\models\Audit;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

/**
 * Class DefaultController
 *
 * @package backend\modules\history\controllers
 */
class DefaultController extends Controller
{
    /**
     * @param int $id
     *
     * @return string
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidParamException
     */
    public function actionView(int $id): string
    {
        $model = History::findOne($id);
        if (null === $model) {
            throw new NotFoundHttpException();
        }

        return $this->render('view', [
            'model' => $model
        ]);
    }

    public function actionAudit()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Audit::find()
                ->orderBy('created DESC'),
            'pagination' => [
                'pageSize' => 10
            ]
        ]);

        return $this->render('index', [
            'dataProvider'  => $dataProvider,
        ]);
    }
}
