<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 06.10.17
 * Time: 12:48
 */

namespace backend\modules\history\helpers;

use backend\modules\news\models\NewsForm;

/**
 * Class HistoryModulesHelper
 *
 * @package backend\modules\history\helpers
 */
class HistoryModulesHelper
{
    const MODULES_ARRAY = [
        NewsForm::class => 'news'
    ];
}
