<?php

namespace backend\modules\history\models\search;

use backend\modules\history\models\Audit;
use backend\modules\toris\models\UserToris;
use common\components\ActiveRecord;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * AuditSearch represents the model behind the search form about `backend\modules\history\models\Audit`.
 */
class AuditSearch extends Audit
{

    /** @var string */
    public $name;
    /** @var string */
    public $owner_module;
    /** @var string */
    public $type_name;
    /** @var string */
    public $fio;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'created',
                'name',
                'owner_module',
                'type_name',
                'fio'
            ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()->orderBy('created DESC, id DESC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $owner_classes = Audit::find()->select('DISTINCT(owner_class)')->asArray()->all();
        $tblJoined = [];
        $andWhere = [];
        $andWhereModule = [];
        foreach ($owner_classes as $class) {
            /** @var ActiveRecord $classModel */
            if (!class_exists($class['owner_class'])) {
                continue;
            }
            $classModel = $class['owner_class'];
            $tblName = $classModel::tableName();
            if (in_array($tblName, $tblJoined)) {
                continue;
            }
            $tblJoined[] = $tblName;
            $query->leftJoin($tblName,
                $tblName . '.id = ' . self::tableName() . '.owner_id AND '.
                self::tableName() . '.owner_class = \'' . $classModel . '\''
            );
            /** @var ActiveRecord $classModelAR */
            $classModelAR = new $classModel();
            $baseName = $classModelAR->getBaseName();
            if (null !== $baseName) {
                $andWhere[] = ['ilike', $tblName . '.' . $baseName, $this->name];
                $andWhereModule[] = ['=', self::tableName() . '.owner_class', $this->owner_module];
            }
        }

        if (!empty($andWhere)) {
            $query->andFilterWhere(array_merge(['or'], $andWhere));
        }
        if (!empty($andWhereModule)) {
            $query->andFilterWhere(array_merge(['or'], $andWhereModule));
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->created)) {
            $query->andWhere(self::tableName() . '.created::date = :created', [
                ':created' => $this->created
            ]);
        }
        $query->andFilterWhere(['=', 'type', $this->type_name]);
        if (!empty($this->fio)) {
            $query->joinWith('userToris');
            $query->andFilterWhere(['ilike', UserToris::tableName() . '.fio', $this->fio]);
        }

        return $dataProvider;
    }
}
