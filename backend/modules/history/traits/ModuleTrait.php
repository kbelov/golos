<?php

namespace backend\modules\history\traits;

use backend\modules\history\Module;
use Yii;

/**
 * Class ModuleTrait
 * @package backend\modules\history\traits
 * Implements `getModule` method, to receive current module instance.
 *
 * @property Module module
 */
trait ModuleTrait
{
    /**
     * @var \backend\modules\history\Module|null Module instance
     */
    private $_module;

    /**
     * @return \backend\modules\history\Module|null Module instance
     */
    public function getModule()
    {
        if ($this->_module === null) {
            $this->_module = Yii::$app->getModule('history');
        }
        return $this->_module;
    }
}
