<?php

use backend\modules\history\models\Audit;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\widgets\DatePicker;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\history\models\search\AuditSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Аудит';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="audit-index">

    <?php $gridColumns = [
        [
            'attribute' => 'created',
            'value' => function (Audit $model) {
                return $model->created;
            },
            'filter' => DatePicker::widget(
                [
                    'model' => $searchModel,
                    'attribute' => 'created',
                    'options' => [
                        'class' => 'form-control'
                    ],
                    'pluginOptions' => [
                        'format' => 'dd.mm.yyyy',
                    ]
                ]
            )
        ],
        [
            'attribute' => 'owner_module',
            'format' => 'html',
            'value' => function (Audit $model) {
                return $model->getModuleName();
            },
            'filter' => \kartik\select2\Select2::widget(
                [
                    'model' => $searchModel,
                    'attribute' => 'owner_module',
                    'data'  => Audit::getAllOwners(),
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Выберите ресурс'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]
            )
        ],
        [
            'attribute' => 'name',
            'filterInputOptions' => [
                'class'       => 'form-control',
                'placeholder' => $searchModel->getAttributeLabel('name')
            ],
            'format' => 'html',
            'value' => function (Audit $model) {
                if (null !== $model->owner) {
                    $moduleUrl = $model->getUrlView();
                    if (null !== $moduleUrl) {
                        return Html::a($model->owner, [$moduleUrl, 'id' => $model->owner_id]);
                    }
                    return $model->owner;
                }
                return null;
            }
        ],
        [
            'attribute' => 'type_name',
            'format' => 'html',
            'value' => function (Audit $model) {
                return $model->getTypeName();
            },
            'filter' => \kartik\select2\Select2::widget(
                [
                    'model' => $searchModel,
                    'attribute' => 'type_name',
                    'data'  => Audit::getAllTypes(),
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Выберите тип'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]
            )
        ],
        [
            'attribute' => 'fio',
            'filterInputOptions' => [
                'class'       => 'form-control',
                'placeholder' => $searchModel->getAttributeLabel('name')
            ],
            'value' => function (Audit $model) {
                return $model->userToris;
            }
        ],
        [
            'label' => 'Изменения',
            'format' => 'html',
            'value' => function (Audit $model) {
                if (null !== $model->history_id) {
                    return Html::a('Изменения',
                        ['/history/default/view', 'id' => $model->history_id], [
                            'class' => 'btn btn-primary btn-xs'
                        ]);
                }
                return null;
            }
        ],
    ]; ?>

    <?= ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'target' => ExportMenu::TARGET_SELF,
        'exportConfig' => [
            ExportMenu::FORMAT_PDF  => false,
            ExportMenu::FORMAT_CSV  => false,
            ExportMenu::FORMAT_HTML => false,
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_EXCEL => false
        ]
    ]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns
    ]); ?>
</div>
