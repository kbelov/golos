<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 07.10.2017
 * Time: 17:27
 */

use backend\components\GridView;

/** @var \yii\data\ActiveDataProvider $dataProvider */

?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel'  => $searchModel,
    'columns'      => [
        ['class' => SerialColumn::class],

        'id',
        'name',
        'content:ntext',
        'preview:ntext',
        //            'image',
        // 'creator_id',
        // 'visible:boolean',
        // 'date',
        // 'created',
        // 'updated',

        ['class' => ActionColumn::class],
    ],
]); ?>
