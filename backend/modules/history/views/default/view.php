<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 06.10.17
 * Time: 12:54
 */

use yii\db\ActiveRecord;

/** @var \yii\web\View $this */
/** @var \common\models\History $model */

$this->title = $model->owner;

$fields = array_merge($model->old_attributes, $model->new_attributes);

/**
 * @param string $attribute
 * @param array $attributes
 *
 * @param ActiveRecord $model
 * @return string
 */
function getAttribute(string $attribute, array $attributes, ActiveRecord $model): string
{
    $attributeValue = isset($attributes[$attribute]) ? strip_tags($attributes[$attribute]) : '';
    if ($model->hasMethod('get'.ucfirst($attribute).'HistoryName')) {
        $method = 'get'.ucfirst($attribute).'HistoryName';
        return $model->$method($attributeValue);
    }
    return $attributeValue;
}

/**
 * @param string $attribute
 * @param array $oldAttributes
 * @param array $newAttributes
 *
 * @param ActiveRecord $model
 * @return bool
 */
function checkChangeAttribute(string $attribute, array $oldAttributes, array $newAttributes, ActiveRecord $model): bool
{
    $oldAttribute = getAttribute($attribute, $oldAttributes, $model);
    $newAttribute = getAttribute($attribute, $newAttributes, $model);

    return $oldAttribute !== $newAttribute;
}

?>
<table class="table table-bordered">
    <thead>
    <tr>
        <th>Поле</th>
        <th>Было</th>
        <th>Стало</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($fields as $field => $value):
        $change = checkChangeAttribute($field, $model->old_attributes, $model->new_attributes, $model->owner);
        $oldAttribute = getAttribute($field, $model->old_attributes, $model->owner);
        $newAttribute = getAttribute($field, $model->new_attributes, $model->owner);
        if (!$change || (empty($oldAttribute) && empty($newAttribute))) {
            continue;
        }
        /** @var \common\components\ActiveRecord $modelOwner */
        $modelOwner = new $model->owner_class;
        $fieldName = $modelOwner->getAttributeLabel($field);
        ?>
        <tr>
            <td><?= $fieldName; ?></td>
            <td><?= $change ? '<del>' : ''; ?><?= $oldAttribute; ?><?= $change ? '</del>' : ''; ?></td>
            <td><?= $change ? '<ins>' : ''; ?><?= $newAttribute; ?><?= $change ? '</ins>' : ''; ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
