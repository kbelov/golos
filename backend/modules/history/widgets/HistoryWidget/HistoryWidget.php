<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 06.10.17
 * Time: 12:23
 */

namespace backend\modules\history\widgets\HistoryWidget;

use backend\modules\history\traits\ModuleTrait;
use common\components\ActiveRecord;
use common\models\History;
use yii\base\Widget;
use yii\data\ActiveDataProvider;

/**
 * Class HistoryWidget
 *
 * @package backend\modules\history\widgets\HistoryWidget
 */
class HistoryWidget extends Widget
{
    use ModuleTrait;

    /** @var ActiveRecord */
    public $ownerClass;
    /** @var integer */
    public $ownerId;

    /**
     * @return string
     * @throws \yii\base\InvalidParamException
     */
    public function run(): string
    {
        $model = $this->ownerClass::findOne($this->ownerId);
        if (null !== $model) {
            $dataProvider = new ActiveDataProvider([
                'query'      => History::find()
                    ->byModel($model)
                    ->orderBy('created DESC'),
                'pagination' => [
                    'pageSize' => $this->module->recordsPerPage
                ]
            ]);

            return $this->render('index', [
                'dataProvider'  => $dataProvider,
            ]);
        }
    }
}
