<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 18:11
 */

use common\components\Html;
use common\components\Url;

/** @var \common\models\History $model */

?>
<td><?= Html::a($model->user, ['/users/toris/view', 'id' => $model->user_id]); ?></td>
<td><?= $model->created; ?></td>
<td class="text-center"><a href="<?= Url::toRoute(['/history/default/view', 'id' => $model->id]); ?>" class="btn btn-primary"><i class="fa fa-eye"></i></a></td>