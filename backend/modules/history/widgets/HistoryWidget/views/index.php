<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 06.10.17
 * Time: 12:24
 */

use yii\widgets\ListView;

/** @var \yii\data\ActiveDataProvider $dataProvider */

?>
<div class="ibox box-primary widget-text-box m-t-md">
    <div class="ibox-title">
        История изменений
    </div>
    <div class="ibox-content">
        <?php if ($dataProvider->getCount() > 0): ?>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Кто изменил</th>
                    <th colspan="2">Когда изменил</th>
                </tr>
            </thead>
        <?php endif; ?>
            <?= ListView::widget(
                [
                    'dataProvider' => $dataProvider,
                    'layout' => "{items}\n<div class='col-lg-12'>{pager}</div>",
                    'itemView' => '_index_item',
                    'options' => [
                        'tag'   => 'tbody'
                    ],
                    'emptyTextOptions' => [
                        'class' => 'empty alert alert-info'
                    ],
                    'itemOptions' => [
                        'class' => '',
                        'tag' => 'tr'
                    ]
                ]
            ); ?>
        <?php if ($dataProvider->getCount() > 0): ?>
        </table>
        <?php endif; ?>
    </div>
</div>
