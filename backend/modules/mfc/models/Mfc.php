<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.17
 * Time: 11:52
 */

namespace backend\modules\mfc\models;

use backend\modules\areas\models\Areas;

/**
 * Class Mfc
 *
 * @package backend\modules\mfc\models
 */
class Mfc extends \common\modules\mfc\models\Mfc
{
    /**
     * @param string $area_id
     * @return string
     */
    public function getArea_idHistoryName(string $area_id): string
    {
        $area = Areas::findOne($area_id);
        return (null !== $area) ? $area->name : '(Район не найден)';
    }
}
