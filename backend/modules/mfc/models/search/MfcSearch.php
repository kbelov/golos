<?php

namespace backend\modules\mfc\models\search;

use backend\modules\mfc\models\Mfc;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * MfcSeach represents the model behind the search form about `backend\modules\mfc\models\Mfc`.
 */
class MfcSearch extends Mfc
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id', 'area_id'], 'integer'],
            [['address', 'name', 'created', 'updated'], 'safe'],
            [['visible'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios(): array
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params): ActiveDataProvider
    {
        $query = Mfc::find()->active();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'area_id' => $this->area_id,
            'visible' => $this->visible,
            'created' => $this->created,
            'updated' => $this->updated,
        ]);

        $query->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
