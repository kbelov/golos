<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\mfc\models\Mfc */

$this->title = 'Добавление МФЦ';
$this->params['breadcrumbs'][] = ['label' => 'МФЦ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mfc-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
