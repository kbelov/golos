<?php

use backend\modules\mfc\models\Mfc;
use yii\grid\ActionColumn;
use yii\grid\SerialColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\mfc\models\search\MfcSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'МФЦ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mfc-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить МФЦ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => SerialColumn::class],

            'id',
            [
                'attribute'  => 'area_id',
                'format'    => 'html',
                'value'  => function (Mfc $model) {
                    return Html::a($model->area, ['/areas/default/view', 'id' => $model->area_id]);
                },
            ],
            'address',
            'name',
            'visible:boolean',
            'created',
            'updated',

            ['class' => ActionColumn::class],
        ],
    ]); ?>
</div>
