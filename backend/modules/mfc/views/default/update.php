<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\mfc\models\Mfc */

$this->title = 'Update Mfc: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'МФЦ', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="mfc-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
