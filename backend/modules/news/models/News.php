<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 29.09.17
 * Time: 13:57
 */

namespace backend\modules\news\models;

use backend\modules\toris\models\UserToris;
use yii\db\ActiveQuery;

/**
 * Class News
 *
 * @package backend\modules\news\models
 */
class News extends \common\modules\news\models\News
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator(): ActiveQuery
    {
        return $this->hasOne(UserToris::className(), ['id' => 'creator_id']);
    }
}
