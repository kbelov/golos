<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 16:33
 */

namespace backend\modules\news\models;

/**
 * Class NewsForm
 * @package backend\modules\news\models
 */
class NewsForm extends News
{
    /**
     * @return array
     */
    public function rules(): array
    {
        $rules = parent::rules();
        return $rules;
    }

    /**
     * @return bool
     */
    public function beforeValidate(): bool
    {
        $this->creator_id = \Yii::$app->getUser()->getId();
        return parent::beforeValidate();
    }
}
