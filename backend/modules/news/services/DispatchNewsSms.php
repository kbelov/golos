<?php
/**
 * Created by PhpStorm.
 * User: filatov
 * Date: 12.10.2017
 * Time: 18:57
 */

namespace backend\modules\news\services;

use common\services\DispatchSms;
use yii\base\Event;
use common\modules\distribution\helpers\DispatchCodes;
use common\modules\news\models\News;
use Yii;

class DispatchNewsSms extends DispatchSms
{
    /** @var News */
    protected $newsModel;

    public function __construct()
    {
        $this->dispatchCode = DispatchCodes::CODE_NEWS_SMS;
    }

    /**
     * @return mixed
     */
    public function run()
    {
        if ($this->newsModel->visible) {
            parent::run();
        }
        return true;
    }

    /**
     * @param Event $event
     *
     * @return bool
     * @throws \yii\base\Exception
     */
    public static function eventCatcher(Event $event): bool
    {
        $newsModel = $event->sender;

        if ($newsModel instanceof News) {
            static::getInstance()->setNewsModel($newsModel)->run();
            return true;
        }

        return false;
    }

    protected function setNewsModel(News $model)
    {
        $this->newsModel = $model;
        return $this;
    }

    protected function getMsg(): string
    {
        return "На портал Голос Петербурга опубликована новая новость {$this->newsModel->name}: ".
            Yii::$app->urlManagerFrontend->createAbsoluteUrl(['/news/default/view', 'id' => $this->newsModel->id]);
    }
}
