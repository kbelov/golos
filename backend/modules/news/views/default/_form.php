<?php

use kartik\widgets\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget as Imperavi;
use vova07\fileapi\Widget as FileApi;

/* @var $this yii\web\View */
/* @var $model backend\modules\news\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin([
        'enableAjaxValidation'      => true,
        'enableClientValidation'    => false
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date')->widget(DatePicker::class, [
        'type' => DatePicker::TYPE_INPUT,
        'pluginOptions' => [
            'autoclose' => true,
            'format'    => 'yyyy-mm-dd'
        ]
    ]) ?>

    <?= $form->field($model, 'content')->widget(
        Imperavi::className(),
        [
            'settings' => [
                'minHeight' => 200,
            ]
        ]
    ) ?>

    <?= $form->field($model, 'preview')->widget(
        Imperavi::className(),
        [
            'settings' => [
                'minHeight' => 200,
            ]
        ]
    ) ?>

    <?= $form->field($model, 'image')->widget(
        FileApi::className(),
        [
            'settings' => [
                'url' => ['/news/default/fileapi-upload']
            ]
        ]
    ) ?>

    <?= $form->field($model, 'visible')->checkbox() ?>

    <?= $form->field($model, 'is_piter')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
