<?php

use backend\modules\history\widgets\HistoryWidget\HistoryWidget;
use backend\modules\news\models\News;
use backend\modules\news\models\NewsForm;
use common\components\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\news\models\News */

$this->title                   = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">

    <?php if ($model->getIsVisible()): ?>
        <p>
            <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data'  => [
                    'confirm' => 'Вы уверены, что хотите удалить эту запись?',
                    'method'  => 'post',
                ],
            ]) ?>
        </p>
    <?php else: ?>
        <div class="alert alert-danger">
            Этот элемент удален!
        </div>
        <?= Html::a('Восстановить', ['revive', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?php endif; ?>

    <div class="row">
        <div class="col-lg-8">
            <?= DetailView::widget([
                'model'      => $model,
                'attributes' => [
                    'id',
                    'name',
                    'content:ntext',
                    'preview:ntext',
                    'image',
                    [
                        'attribute'  => 'creator',
                        'format'    => 'html',
                        'value'  => function (News $model) {
                            return Html::a($model->creator, ['/users/toris/view', 'id' => $model->creator_id]);
                        },
                    ],
                    'is_piter:boolean',
                    'visible:boolean',
                    'date',
                    'created',
                    'updated',
                ],
            ]) ?>
        </div>
        <div class="col-lg-4">
            <?= HistoryWidget::widget([
                'ownerClass' => NewsForm::class,
                'ownerId'    => $model->id
            ]); ?>
        </div>
    </div>

</div>
