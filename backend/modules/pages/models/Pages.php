<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 22:29
 */

namespace backend\modules\pages\models;

/**
 * Class Pages
 * @package backend\modules\pages\models
 */
class Pages extends \common\modules\pages\models\Pages
{
    public function getShow_in_menuHistoryName(bool $show_in_menu): string
    {
        return ($show_in_menu) ? 'Да' : 'Нет';
    }
}
