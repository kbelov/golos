<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget as Imperavi;
use vova07\fileapi\Widget as FileApi;

/* @var $this yii\web\View */
/* @var $model backend\modules\pages\models\Pages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pages-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'visible')->checkbox() ?>

    <?= $form->field($model, 'show_in_menu')->checkbox() ?>

    <?= $form->field($model, 'content')->widget(
        Imperavi::className(),
        [
            'settings' => [
                'minHeight' => 200,
            ]
        ]
    ) ?>

    <?= $form->field($model, 'image')->widget(
        FileApi::className(),
        [
            'settings' => [
                'url' => ['/pages/default/fileapi-upload']
            ]
        ]
    ) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
