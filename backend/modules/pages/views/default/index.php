<?php

use yii\grid\ActionColumn;
use yii\grid\SerialColumn;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\pages\models\search\PagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Страницы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать страницу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => SerialColumn::class],

            'id',
            'name',
            'visible:boolean',
            'show_in_menu:boolean',
            'created',
            'updated',

            ['class' => ActionColumn::class],
        ],
    ]); ?>
</div>
