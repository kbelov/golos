<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 02.10.17
 * Time: 13:51
 */

namespace backend\modules\polls\controllers;

use backend\components\Controller;
use backend\helpers\TorisHelper;
use common\modules\comments\models\Comments;
use common\modules\polls\helpers\PollStatusHelper;
use common\modules\polls\models\Polls;
use common\modules\comments\models\search\CommentsSearch;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class CommentsController
 *
 * @package backend\modules\polls\controllers
 */
class CommentsController extends Controller
{
    /**
     * @param int $id
     *
     * @return string
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\base\InvalidParamException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionIndex(int $id)
    {
        TorisHelper::checkAccess();
        $poll = Polls::findOne($id);
        if (null === $poll) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $searchModel = new CommentsSearch();
        $dataProvider = $searchModel->search($poll);

        return $this->render('index', [
            'searchModel'   => $searchModel,
            'poll'          => $poll,
            'dataProvider'  => $dataProvider,
        ]);
    }

    /**
     * Deletes an existing Polls model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\db\StaleObjectException
     * @throws \Exception
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionDelete($id)
    {
        $model = Comments::findOne($id);
        if (null === $model) {
            throw new ForbiddenHttpException();
        }
        $pollsModel = Polls::findOne($model->target_id);
        if (!$pollsModel->canDeleteComments()) {
            return $this->redirect(['/polls/comments/index', 'id' => $pollsModel->id]);
        }
        $model->visible = PollStatusHelper::STATUS_NOT_ACTIVE;
        $model->save();

        return $this->redirect(['/polls/comments/index', 'id' => $pollsModel->id]);
    }
}
