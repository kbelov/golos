<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 04.10.2017
 * Time: 23:43
 */

namespace backend\modules\polls\controllers;

use backend\components\Controller;
use backend\modules\polls\helpers\ExportHelper;
use common\modules\polls\helpers\PollStatusHelper;
use common\modules\polls\models\Polls;

/**
 * Class ExportController
 * @package backend\modules\polls\controllers
 */
class ExportController extends Controller
{
    /**
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function actionCurrent()
    {
        $export = new ExportHelper();
        $polls = Polls::find()
            ->correctDate()
            ->visible()
            ->orderBy('created DESC')->all();
        $export->generateDoc($polls);
    }

    /**
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function actionAll()
    {
        $export = new ExportHelper();
        $polls = Polls::find()
            ->visible()
            ->orderBy('created DESC')->all();
        $export->generateDoc($polls);
    }
}
