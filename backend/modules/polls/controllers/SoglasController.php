<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 04.10.17
 * Time: 17:52
 */

namespace backend\modules\polls\controllers;

use backend\components\Controller;
use backend\modules\polls\models\Polls;
use backend\modules\polls\services\AddPollsSoglas;
use common\modules\polls\helpers\PollStatusHelper;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class SoglasController
 *
 * @package backend\modules\polls\controllers
 */
class SoglasController extends Controller
{
    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \yii\base\Exception
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionModerator(int $id): Response
    {
        $model = $this->findModel($id);

        if (!$model->canSetStatusSoglas(PollStatusHelper::STATUS_SOGLAS_MODERATOR)) {
            throw new ForbiddenHttpException();
        }

        $old_status = $model->status_soglas;
        $model->status_soglas = PollStatusHelper::STATUS_SOGLAS_MODERATOR;
        if ($model->save()) {
            AddPollsSoglas::getInstance()
                ->setStatusFrom($old_status)
                ->setStatusTo($model->status_soglas)
                ->setPoll($model)
                ->run();
            Yii::$app->session->setFlash('success', 'Опрос отправлен модератору');
        } else {
            Yii::$app->session->setFlash('danger', 'Не сохранено');
        }
        return $this->redirect(['/polls/default/view', 'id' => $id]);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \yii\base\Exception
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionManager(int $id): Response
    {
        $model = $this->findModel($id);

        if (!$model->canSetStatusSoglas(PollStatusHelper::STATUS_SOGLAS_MANAGER)) {
            throw new ForbiddenHttpException();
        }

        $old_status = $model->status_soglas;
        $model->status_soglas = PollStatusHelper::STATUS_SOGLAS_MANAGER;
        if ($model->save()) {
            AddPollsSoglas::getInstance()
                ->setStatusFrom($old_status)
                ->setStatusTo($model->status_soglas)
                ->setPoll($model)
                ->run();
            Yii::$app->session->setFlash('success', 'Опрос отправлен контент-менеджер');
        } else {
            Yii::$app->session->setFlash('danger', 'Не сохранено');
        }
        return $this->redirect(['/polls/default/view', 'id' => $id]);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \yii\base\Exception
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionIogv(int $id): Response
    {
        $model = $this->findModel($id);

        if (!$model->canSetStatusSoglas(PollStatusHelper::STATUS_SOGLAS_IOGV)) {
            throw new ForbiddenHttpException();
        }

        $old_status = $model->status_soglas;
        $model->status_soglas = PollStatusHelper::STATUS_SOGLAS_IOGV;
        if ($model->save()) {
            AddPollsSoglas::getInstance()
                ->setStatusFrom($old_status)
                ->setStatusTo($model->status_soglas)
                ->setPoll($model)
                ->run();
            Yii::$app->session->setFlash('success', 'Опрос отправлен в ИОГВ');
        } else {
            Yii::$app->session->setFlash('danger', 'Не сохранено');
        }
        return $this->redirect(['/polls/default/view', 'id' => $id]);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \yii\base\Exception
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionPublish(int $id): Response
    {
        $model = $this->findModel($id);

        if (!$model->canSetStatusSoglas(PollStatusHelper::STATUS_SOGLAS_FINISHED)) {
            throw new ForbiddenHttpException();
        }

        $old_status = $model->status_soglas;
        $model->status_soglas = PollStatusHelper::STATUS_SOGLAS_FINISHED;
        if ($model->save()) {
            AddPollsSoglas::getInstance()
                ->setStatusFrom($old_status)
                ->setStatusTo($model->status_soglas)
                ->setPoll($model)
                ->run();
            Yii::$app->session->setFlash('success', 'Опрос опубликован!');
        } else {
            Yii::$app->session->setFlash('danger', 'Не сохранено');
        }
        return $this->redirect(['/polls/default/view', 'id' => $id]);
    }

    /**
     * Finds the Polls model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Polls the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Polls::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
