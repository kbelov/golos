<?php

namespace backend\modules\polls\controllers;

use backend\helpers\TorisHelper;
use backend\modules\polls\models\Polls;
use common\modules\polls\Module;
use vova07\fileapi\actions\UploadAction;
use Yii;
use backend\modules\polls\models\PollsVotes;
use backend\modules\polls\models\search\PollsVotesSearch;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VotesController implements the CRUD actions for PollsVotes model.
 *
 * @property Module module
 */
class VotesController extends Controller
{

    protected $modelClass = PollsVotes::class;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'fileapi-upload' => [
                'class' => UploadAction::className(),
                'path'  => $this->module->photoTempPath
            ]
        ];
    }

    /**
     * Lists all PollsVotes models.
     *
     * @param int $id
     *
     * @return mixed
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidParamException
     */
    public function actionIndex(int $id)
    {
        TorisHelper::checkAccess();
        $poll = Polls::findOne($id);
        if (null === $poll) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $searchModel  = new PollsVotesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $poll);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'poll'         => $poll,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PollsVotes model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\base\InvalidParamException
     */
    public function actionView($id)
    {
        $this->model = $this->findModel($id);
        TorisHelper::checkAccess();

        return $this->render('view', [
            'model' => $this->model,
        ]);
    }

    /**
     * Creates a new PollsVotes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @param int $id
     *
     * @return mixed
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\base\InvalidParamException
     * @throws NotFoundHttpException
     */
    public function actionCreate($id)
    {
        TorisHelper::checkAccess();
        $poll = Polls::findOne($id);
        if (null === $poll) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        if (!$poll->canUpdate()) {
            return $this->redirect(['/polls/votes/view', 'id' => $id]);
        }
        $model = new PollsVotes([
            'poll_id' => $poll->id
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model'     => $model,
                'pollModel' => $poll
            ]);
        }
    }

    /**
     * Updates an existing PollsVotes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidParamException
     */
    public function actionUpdate($id)
    {
        TorisHelper::checkAccess();
        $model = $this->findModel($id);

        $pollModel = Polls::findOne($model->poll_id);
        if (null === $pollModel) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        if (!$pollModel->canUpdate()) {
            return $this->redirect(['/polls/votes/view', 'id' => $id]);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PollsVotes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\db\StaleObjectException
     * @throws \Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        TorisHelper::checkAccess();
        $model = $this->findModel($id);

        $pollModel = Polls::findOne($model->poll_id);
        if (null === $pollModel) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        if ($pollModel->canUpdate()) {
            $model->delete();
        }

        return $this->redirect(['/polls/votes/index', 'id' => $pollModel->id]);;
    }

    /**
     * Finds the PollsVotes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return PollsVotes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PollsVotes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
