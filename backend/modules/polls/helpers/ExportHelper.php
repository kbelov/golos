<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 04.10.2017
 * Time: 23:46
 */

namespace backend\modules\polls\helpers;

use common\modules\polls\models\Polls;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;

/**
 * Class ExportHelper
 * @package backend\modules\polls\helpers
 */
class ExportHelper
{
    /**
     * @param Polls[] $polls
     *
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function generateDoc($polls)
    {
        $phpWord = new PhpWord();

        $table_style = new \PhpOffice\PhpWord\Style\Table;
        $table_style->setWidth(100);

        $section = $phpWord->addSection();
        $table = $section->addTable(array('width' => 50 * 50, 'unit' => 'pct', 'alignment' => \PhpOffice\PhpWord\SimpleType\JcTable::CENTER));
        $table->addRow();
        $cellColSpan = ['gridSpan' => 8, 'valign' => 'center', 'align' => 'center'];
        $myFontStyle = array('bold' => true, 'align' => 'center');
        $table->addCell(null, $cellColSpan)->addText('Отчет о проводимых голосованиях.', $myFontStyle, array('align' => 'center'));
        $table->addRow();
        $table->addCell(200)->addText('ID голосования', $myFontStyle, array('align' => 'center'));
        $table->addCell(1800)->addText('Наименование голосования', $myFontStyle, array('align' => 'center'));
        $table->addCell(2000)->addText('Варианты голосования (по каждому варианту процент)', $myFontStyle, array('align' => 'center'));
        $table->addCell(1500)->addText('Район (проведения голосования)', $myFontStyle, array('align' => 'center'));
        $table->addCell(1500)->addText('Дата начала', $myFontStyle, array('align' => 'center'));
        $table->addCell(1500)->addText('Дата конца', $myFontStyle, array('align' => 'center'));
        $table->addCell(1500)->addText('Кол-во баллов', $myFontStyle, array('align' => 'center'));
        $table->addCell(1500)->addText('Статус голосования', $myFontStyle, array('align' => 'center'));

        $myFontStyle = ['align' => 'center'];
        $i = 0;
        foreach ($polls AS $poll) {
            $countVotes = $poll->countVoted;
            $votesText = '';
            foreach ($poll->votes as $vote) {
                $percent = 0;
                if ($vote->countVoted > 0) {
                    $percent = $vote->countVoted * 100 / $countVotes;
                }
                $votesText .= $vote->name.' ('.$percent.'%)<w:br/>';
            }
            $row = $table->addRow();
            $row->addCell(null)->addText($poll->id, $myFontStyle, ['align' => 'center']);
            $row->addCell(null)->addText($poll->name, $myFontStyle, ['align' => 'center']);
            $row->addCell(null)->addText($votesText, $myFontStyle, ['align' => 'center']);
            $row->addCell(null)->addText($poll->area, $myFontStyle, ['align' => 'center']);
            $row->addCell(null)->addText($poll->date_start, $myFontStyle, ['align' => 'center']);
            $row->addCell(null)->addText($poll->date_end, $myFontStyle, ['align' => 'center']);
            $row->addCell(null)->addText($poll->points, $myFontStyle, ['align' => 'center']);
            $row->addCell(null)->addText($poll->getPublicStatusName(), $myFontStyle, ['align' => 'center']);
            $i++;
        }

        $this->setHeaders();

        $objWriter = IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('php://output');
    }

    protected function setHeaders()
    {
        header("Content-Description: File Transfer");
        header('Content-Disposition: attachment; filename="выгрузка всех голосований.docx"');
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');
    }
}
