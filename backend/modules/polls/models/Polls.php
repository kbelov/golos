<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 29.09.17
 * Time: 13:35
 */

namespace backend\modules\polls\models;

use backend\helpers\TorisHelper;
use backend\modules\toris\models\UserTorisRolesLinks;
use common\components\EventManager;
use common\modules\distribution\services\DispatchNewPollEmail;
use common\modules\distribution\services\DispatchNewPollSms;
use common\modules\events\helpers\EventsHelper;
use common\modules\events\services\AddNewEvent;
use common\modules\polls\helpers\PollStatusHelper;
use Yii;

/**
 * Class Polls
 *
 * @package backend\modules\polls\models
 */
class Polls extends \common\modules\polls\models\Polls
{

    /**
     * @var array
     */
    protected $events = [];

    /**
     * @return string
     */
    public function getVisibilityName(): string
    {
        return PollStatusHelper::VISIBILITY_ARRAY[$this->visibility] ?? 'Нет статуса';
    }

    /**
     * @param bool $status
     * @return string
     */
    public function getStatusHistoryName(bool $status): string
    {
        return ($status) ? 'Да' : 'Нет';
    }

    /**
     * @param string $visibility
     * @return string
     * @internal param bool $status
     */
    public function getVisibilityHistoryName(string $visibility): string
    {
        return PollStatusHelper::VISIBILITY_ARRAY[$visibility] ?? 'Нет статуса';
    }

    /**
     * @param bool  $insert
     * @param array $changedAttributes
     *
     * @throws \yii\base\Exception
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        foreach ($this->events as $event) {
            switch ($event) {
                case EventsHelper::DISPATCH_EVENT_STATUS_UPDATE:
                    AddNewEvent::getInstance()->setType($event)->setTargetId($this->id)->run();
                    break;
            }
        }
    }

    /**
     * @param string $status
     *
     * @return bool
     */
    public function canSetStatusSoglas(string $status): bool
    {
        if ($this->status_soglas === $status) {
            return false;
        }
        $lastSoglas = $this->lastSoglas;

        if (null === $lastSoglas) {
            return false;
        }

        $userRolesModels = UserTorisRolesLinks::find()->where([
            'user_toris_id' => Yii::$app->getUser()->getId()
        ])->all();

        $userRoles = [];
        foreach ($userRolesModels as $role) {
            $userRoles[] = $role->name;
        }

        if (empty($userRoles)) {
            return false;
        }

        if (!in_array(TorisHelper::ROLE_ADMIN, $userRoles, true)) {
            // Если мы ИОГВ
            if (in_array(TorisHelper::ROLE_OPERATOR_IOGV, $userRoles, true)) {
                // Если мы отправляем модератору, то ок
                if ($this->status_soglas === PollStatusHelper::STATUS_SOGLAS_IOGV &&
                    $status === PollStatusHelper::STATUS_SOGLAS_MODERATOR) {
                    return true;
                }
                // Если мы хотим опубликовать (Предыдущий шаг должен быть от модератора)
                if ($this->status_soglas === PollStatusHelper::STATUS_SOGLAS_IOGV &&
                    $status === PollStatusHelper::STATUS_SOGLAS_FINISHED &&
                    $lastSoglas->status_from === PollStatusHelper::STATUS_SOGLAS_MODERATOR) {
                    return true;
                }

                return false;
            }
            // Если мы Менеджер
            if (in_array(TorisHelper::ROLE_MANAGER, $userRoles, true)) {
                // Если мы отправляем модератору, то ок
                if ($this->status_soglas === PollStatusHelper::STATUS_SOGLAS_MANAGER &&
                    $status === PollStatusHelper::STATUS_SOGLAS_MODERATOR) {
                    return true;
                }

                return false;
            }
            // Если мы Модератор
            if (in_array(TorisHelper::ROLE_MODERATOR, $userRoles, true)) {
                // Если мы отправляем менеджеру, то ок (можем ему всегда отправить)
                if ($this->status_soglas === PollStatusHelper::STATUS_SOGLAS_MODERATOR &&
                    $status === PollStatusHelper::STATUS_SOGLAS_MANAGER) {
                    return true;
                }
                // Если мы отправляем в ИОГВ (при условии, что до этого шаг был от менеджера
                if ($this->status_soglas === PollStatusHelper::STATUS_SOGLAS_MODERATOR &&
                    $status === PollStatusHelper::STATUS_SOGLAS_IOGV &&
                    $lastSoglas->status_from === PollStatusHelper::STATUS_SOGLAS_MANAGER) {
                    return true;
                }

                return false;
            }

            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function canUpdate(): bool
    {
        $userRolesModels = UserTorisRolesLinks::find()->where([
            'user_toris_id' => Yii::$app->getUser()->getId()
        ])->all();

        $userRoles = [];
        foreach ($userRolesModels as $role) {
            $userRoles[] = $role->name;
        }

        if (empty($userRoles)) {
            return false;
        }
        if (in_array(TorisHelper::ROLE_ADMIN, $userRoles, true)) {
            return true;
        }

        $lastSoglas = $this->lastSoglas;

        if (null === $lastSoglas) {
            return false;
        }

        if ($this->status_soglas === PollStatusHelper::STATUS_SOGLAS_FINISHED) {
            return false;
        }
        if (in_array(TorisHelper::ROLE_OPERATOR_IOGV, $userRoles, true)) {
            if ($this->status_soglas === PollStatusHelper::STATUS_SOGLAS_IOGV) {
                return true;
            }

            return false;
        }
        if (in_array(TorisHelper::ROLE_MANAGER, $userRoles, true)) {
            if ($this->status_soglas === PollStatusHelper::STATUS_SOGLAS_MANAGER) {
                return true;
            }

            return false;
        }
        if (in_array(TorisHelper::ROLE_MODERATOR, $userRoles, true)) {
            if ($this->status_soglas === PollStatusHelper::STATUS_SOGLAS_MODERATOR) {
                return true;
            }

            return false;
        }

        return false;
    }

    /** @inheritdoc */
    public function event(): array
    {
        return [
            EventManager::EVENT_POLL_UPDATE => [
                DispatchNewPollSms::class => [
                    'on' => ['status_soglas']
                ],
                DispatchNewPollEmail::class => [
                    'on' => ['status_soglas']
                ]
            ]
        ];
    }
}
