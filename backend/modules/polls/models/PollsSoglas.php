<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 04.10.17
 * Time: 18:13
 */

namespace backend\modules\polls\models;

use backend\modules\polls\models\base\PollsSoglasBase;
use common\modules\polls\helpers\PollStatusHelper;

/**
 * Class PollsSoglas
 *
 * @package backend\modules\polls\models
 */
class PollsSoglas extends PollsSoglasBase
{
    /**
     * @return string
     */
    public function getFromName(): string
    {
        return PollStatusHelper::STATUS_SOGLAS_ARRAY[$this->status_from] ?? 'Нет статуса';
    }

    /**
     * @return string
     */
    public function getToName(): string
    {
        return PollStatusHelper::STATUS_SOGLAS_ARRAY[$this->status_to] ?? 'Нет статуса';
    }

    /**
     * @return string
     */
    public function getToNameIcon(): string
    {
        return PollStatusHelper::STATUS_SOGLAS_ICONS_ARRAY[$this->status_to] ?? '';
    }
}
