<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 29.09.17
 * Time: 12:23
 */

namespace backend\modules\polls\models\base;

use backend\modules\polls\models\Polls;
use backend\modules\toris\models\UserToris;
use common\components\ActiveRecord;

/**
 * Class PollsBase
 * @package backend\modules\polls\models\base
 *
 * @property integer $id
 * @property string $status_from
 * @property string $status_to
 * @property integer $user_toris_id
 * @property integer $poll_id
 * @property string $created
 * @property string $updated
 *
 * @property Polls $poll
 * @property UserToris $userToris
 */
class PollsSoglasBase extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'polls_soglas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_from', 'status_to', 'user_toris_id', 'poll_id'], 'required'],
            [['user_toris_id', 'poll_id'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['status_from', 'status_to'], 'string', 'max' => 255],
            [['poll_id'], 'exist', 'skipOnError' => true, 'targetClass' => Polls::className(), 'targetAttribute' => ['poll_id' => 'id']],
            [['user_toris_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserToris::className(), 'targetAttribute' => ['user_toris_id' => 'id']],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoll()
    {
        return $this->hasOne(Polls::className(), ['id' => 'poll_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserToris()
    {
        return $this->hasOne(UserToris::className(), ['id' => 'user_toris_id']);
    }
}
