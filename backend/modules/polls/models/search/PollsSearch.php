<?php

namespace backend\modules\polls\models\search;

use backend\helpers\TorisHelper;
use backend\modules\toris\models\UserTorisRolesLinks;
use common\modules\polls\helpers\PollStatusHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\polls\models\Polls;
use yii\web\ForbiddenHttpException;

/**
 * PollsSearch represents the model behind the search form about `backend\modules\polls\models\Polls`.
 */
class PollsSearch extends Polls
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'area_id', 'points', 'status'], 'integer'],
            [['name', 'name_inner', 'address', 'main_description', 'short_description', 'description', 'complete_description', 'complete_decision', 'complete_result', 'date_start', 'date_end', 'created', 'updated'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     * @throws \yii\web\ForbiddenHttpException
     */
    public function search($params)
    {
        $query = Polls::find()->visible()->orderBy('created DESC');

        $userRolesModels = UserTorisRolesLinks::find()->where([
            'user_toris_id' => Yii::$app->getUser()->getId()
        ])->all();

        $userRoles = [];
        foreach ($userRolesModels as $role) {
            $userRoles[] = $role->name;
        }

        if (empty($userRoles)) {
            throw new ForbiddenHttpException();
        }

        if (!in_array(TorisHelper::ROLE_ADMIN, $userRoles, true)) {
            $findRole = false;
            if (in_array(TorisHelper::ROLE_OPERATOR_IOGV, $userRoles, true)) {
                $findRole = true;
            }
            if (in_array(TorisHelper::ROLE_MANAGER, $userRoles, true)) {
                $query->byStatusSoglas(PollStatusHelper::STATUS_SOGLAS_MANAGER);
                $findRole = true;
            }
            if (in_array(TorisHelper::ROLE_MODERATOR, $userRoles, true)) {
                $findRole = true;
            }
            if (!$findRole) {
                throw new ForbiddenHttpException();
            }
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'area_id' => $this->area_id,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
            'points' => $this->points,
            'status' => $this->status,
            'created' => $this->created,
            'updated' => $this->updated,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'name_inner', $this->name_inner])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'main_description', $this->main_description])
            ->andFilterWhere(['like', 'short_description', $this->short_description])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'complete_description', $this->complete_description])
            ->andFilterWhere(['like', 'complete_decision', $this->complete_decision])
            ->andFilterWhere(['like', 'complete_result', $this->complete_result]);

        return $dataProvider;
    }
}
