<?php

namespace backend\modules\polls\models\search;

use backend\modules\polls\models\Polls;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\polls\models\PollsVotes;

/**
 * PollsVotesSearch represents the model behind the search form about `backend\modules\polls\models\PollsVotes`.
 */
class PollsVotesSearch extends PollsVotes
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'poll_id'], 'integer'],
            [['name', 'image', 'created', 'updated'], 'safe'],
            [['visible'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param Polls $poll
     *
     * @return ActiveDataProvider
     */
    public function search($params, Polls $poll)
    {
        $query = PollsVotes::find()->active()->withPoll($poll->id);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'poll_id' => $this->poll_id,
            'visible' => $this->visible,
            'created' => $this->created,
            'updated' => $this->updated,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;
    }
}
