<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 04.10.17
 * Time: 18:13
 */

namespace backend\modules\polls\services;

use backend\modules\polls\models\Polls;
use backend\modules\polls\models\PollsSoglas;
use common\interfaces\Service;
use yii\base\Exception;

/**
 * Class AddPollsSoglas
 *
 * @package backend\modules\polls\services
 */
class AddPollsSoglas implements Service
{
    /** @var self */
    protected static $instance;

    /** @var string */
    protected $status_from;
    /** @var string */
    protected $status_to;
    /** @var Polls */
    protected $poll;

    /**
     * @return static
     * @throws \yii\base\Exception
     */
    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function run()
    {
        if (null === $this->status_from) {
            throw new Exception('Не заполнен status_from');
        }
        if (null === $this->status_to) {
            throw new Exception('Не заполнен status_to');
        }
        if (null === $this->poll) {
            throw new Exception('Не заполнены poll');
        }
        if (!$this->poll instanceof Polls) {
            throw new Exception('poll должен быть '.Polls::class);
        }

        $transaction = \Yii::$app->getDb()->beginTransaction();

        try {
            $pollsSoglas = new PollsSoglas([
                'status_from'   => $this->status_from,
                'status_to'     => $this->status_to,
                'user_toris_id' => \Yii::$app->getUser()->getId(),
                'poll_id'       => $this->poll->id,
            ]);
            $pollsSoglas->save();

            if (!$pollsSoglas->hasErrors()) {
                $transaction->commit();
            } else {
                $transaction->rollBack();
                throw new Exception('Transaction errors: ' . count($pollsSoglas->getFirstErrors()));
            }
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @param string $status_from
     *
     * @return AddPollsSoglas
     */
    public function setStatusFrom(string $status_from): AddPollsSoglas
    {
        $this->status_from = $status_from;

        return $this;
    }

    /**
     * @param string $status_to
     *
     * @return AddPollsSoglas
     */
    public function setStatusTo(string $status_to): AddPollsSoglas
    {
        $this->status_to = $status_to;

        return $this;
    }

    /**
     * @param Polls $poll
     *
     * @return AddPollsSoglas
     */
    public function setPoll(Polls $poll): AddPollsSoglas
    {
        $this->poll = $poll;

        return $this;
    }
}