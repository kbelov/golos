<?php

use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\polls\models\search\PollsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var \backend\modules\polls\models\Polls $poll */

$this->title = 'Комментарии';
$this->params['breadcrumbs'][] = ['label' => $poll->name, 'url' => ['/polls/default/view', 'id' => $poll->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="polls-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    $columns = [
        ['class' => SerialColumn::class],

        'id',
        'user',
        'content',
        'visible:boolean',
        'created',
        'updated',
    ];
    if ($poll->canDeleteComments()) {
        $columns[] = [
            'class' => ActionColumn::class,
            'template'  => '{delete}'
        ];
    }
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns,
    ]); ?>
</div>
