<?php

use backend\modules\polls\widgets\AddressSearch\AddressSearch;
use common\modules\polls\helpers\PollStatusHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget as Imperavi;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\modules\polls\models\Polls */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row polls-form">

    <div class="col-lg-12">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'status')->dropDownList(PollStatusHelper::STATUSES_ARRAY) ?>

        <?= $form->field($model, 'count_votes')->textInput() ?>

        <?= $form->field($model, 'visibility')->dropDownList(PollStatusHelper::VISIBILITY_ARRAY) ?>

        <?= $form->field($model, 'name_inner')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'date_start')->widget(DatePicker::class, [
            'type' => DatePicker::TYPE_INPUT,
            'pluginOptions' => [
                'autoclose' => true,
                'format'    => 'yyyy-mm-dd'
            ]
        ]) ?>

        <?= $form->field($model, 'date_end')->widget(DatePicker::class, [
            'type' => DatePicker::TYPE_INPUT,
            'pluginOptions' => [
                'autoclose' => true,
                'format'    => 'yyyy-mm-dd'
            ]
        ]) ?>

        <?= $form->field($model, 'points')->textInput() ?>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'address')->widget(
                    AddressSearch::className(),
                    [
                        'options' => [
                            'class'    => 'form-control'
                        ]
                    ]
                ) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'lat')->textInput([
                    'readonly' => true
                ])->label('Широта') ?>

                <?= $form->field($model, 'lon')->textInput([
                    'readonly' => true
                ])->label('Долгота') ?>

                <?= $form->field($model, 'area_id')->textInput([
                    'readonly' => true
                ])->label('id Района') ?>

                <?= $form->field($model, 'building_id')->textInput([
                    'readonly' => true
                ])->label('id строения ЕАС') ?>
            </div>
        </div>

        <?= $form->field($model, 'main_description')->widget(
            Imperavi::className(),
            [
                'settings' => [
                    'minHeight' => 200,
                ]
            ]
        ) ?>

        <?= $form->field($model, 'short_description')->widget(
            Imperavi::className(),
            [
                'settings' => [
                    'minHeight' => 200,
                ]
            ]
        ) ?>

        <?= $form->field($model, 'description')->widget(
            Imperavi::className(),
            [
                'settings' => [
                    'minHeight' => 200,
                ]
            ]
        ) ?>

        <?= $form->field($model, 'complete_description')->widget(
            Imperavi::className(),
            [
                'settings' => [
                    'minHeight' => 200,
                ]
            ]
        ) ?>

        <?= $form->field($model, 'complete_decision')->widget(
            Imperavi::className(),
            [
                'settings' => [
                    'minHeight' => 200,
                ]
            ]
        ) ?>

        <?= $form->field($model, 'complete_result')->widget(
            Imperavi::className(),
            [
                'settings' => [
                    'minHeight' => 200,
                ]
            ]
        ) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>



</div>
