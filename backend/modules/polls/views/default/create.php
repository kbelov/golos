<?php


/* @var $this yii\web\View */
/* @var $model backend\modules\polls\models\Polls */

$this->title = 'Добавление голосования';
$this->params['breadcrumbs'][] = ['label' => 'Голосования', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="polls-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
