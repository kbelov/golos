<?php

use yii\grid\ActionColumn;
use yii\grid\SerialColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\polls\models\search\PollsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Голосования';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="polls-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить голосование', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Выгрузка текущих', ['/polls/export/current'], ['class' => 'btn btn-success pull-right']) ?>
        <?= Html::a('Выгрузка всех', ['/polls/export/all'], ['class' => 'btn btn-success pull-right m-r-xs']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => SerialColumn::class],

            'id',
            'name',
            'statusName',
            'statusSoglasName',
            'name_inner',
            'address',
            // 'main_description:ntext',
            // 'short_description:ntext',
            // 'description:ntext',
            // 'complete_description:ntext',
            // 'complete_decision:ntext',
            // 'complete_result:ntext',
            // 'date_start',
            // 'date_end',
            // 'points',
            // 'status',
            // 'created_at',
            // 'updated_at',

            ['class' => ActionColumn::class],
        ],
    ]); ?>
</div>
