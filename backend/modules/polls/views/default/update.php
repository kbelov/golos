<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\polls\models\Polls */

$this->title = 'Обновление голосования: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Голосования', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновление';
?>
<div class="polls-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
