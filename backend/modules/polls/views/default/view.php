<?php

use backend\modules\history\widgets\HistoryWidget\HistoryWidget;
use backend\modules\polls\models\Polls;
use common\modules\polls\helpers\PollStatusHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\polls\models\Polls */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Голосования', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="polls-view">

    <?php if ($model->getIsVisible()): ?>
        <p>
            <?php if ($model->canUpdate()): ?>
                <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Вы уверены, что хотите удалить эту запись?',
                        'method' => 'post',
                    ],
                ]) ?>
            <?php endif; ?>

            <?= Html::a('Варианты', ['/polls/votes/index', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
            <?= Html::a('Комментарии', ['/polls/comments/index', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
            <?php if ($model->canSetStatusSoglas(PollStatusHelper::STATUS_SOGLAS_FINISHED)): ?>
                <?= Html::a('Опубликовать', ['/polls/soglas/publish', 'id' => $model->id], ['class' => 'btn btn-success pull-right m-r-xs']) ?>
            <?php endif; ?>
            <?php if ($model->canSetStatusSoglas(PollStatusHelper::STATUS_SOGLAS_IOGV)): ?>
                <?= Html::a('Отправить в ИОГВ', ['/polls/soglas/iogv', 'id' => $model->id], ['class' => 'btn btn-success pull-right m-r-xs']) ?>
            <?php endif; ?>
            <?php if ($model->canSetStatusSoglas(PollStatusHelper::STATUS_SOGLAS_MODERATOR)): ?>
                <?= Html::a('На модерацию', ['/polls/soglas/moderator', 'id' => $model->id], ['class' => 'btn btn-primary pull-right m-r-xs']) ?>
            <?php endif; ?>
            <?php if ($model->canSetStatusSoglas(PollStatusHelper::STATUS_SOGLAS_MANAGER)): ?>
                <?= Html::a('Отправить контент-менеджеру', ['/polls/soglas/manager', 'id' => $model->id], ['class' => 'btn btn-primary pull-right m-r-xs']) ?>
            <?php endif; ?>

        </p>
    <?php else: ?>
        <div class="alert alert-danger">
            Этот элемент удален!
        </div>
        <?= Html::a('Восстановить', ['revive', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?php endif; ?>

    <div class="row">
        <div class="col-lg-8">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'name',
                    'statusSoglasName',
                    'visibilityName',
                    'date_start',
                    'date_end',
                    'points',
                    'area',
                    'statusName',
                    'status:boolean',
                    'name_inner',
                    'address',
                    'main_description:ntext',
                    'short_description:ntext',
                    'description:ntext',
                    'complete_description:ntext',
                    'complete_decision:ntext',
                    'complete_result:ntext',
                    'created',
                    'updated',
                ],
            ]) ?>
        </div>
        <div class="col-lg-4">
            <?php if (!empty($model->soglasStatuses)): ?>
                <table class="table table-bordered detail-view">
                    <thead>
                        <tr>
                            <th>Новый статус</th>
                            <th>Старый статус</th>
                            <th>Дата</th>
                            <th>Кто изменил</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($model->soglasStatuses as $soglasStatus): ?>
                        <tr>
                            <td><i class="text-danger fa fa-<?= $soglasStatus->getToNameIcon(); ?>"></i>&nbsp;<?= $soglasStatus->getToName(); ?></td>
                            <td><?= $soglasStatus->getFromName(); ?></td>
                            <td><?= $soglasStatus->created; ?></td>
                            <td><?= Html::a($soglasStatus->userToris, ['/users/toris/view', 'id' => $soglasStatus->user_toris_id]); ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endif; ?>
            <?= HistoryWidget::widget([
                'ownerClass' => Polls::class,
                'ownerId'    => $model->id
            ]); ?>
        </div>
    </div>

</div>
