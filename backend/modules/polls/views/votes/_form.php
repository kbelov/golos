<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\fileapi\Widget as FileApi;
use vova07\imperavi\Widget as Imperavi;

/* @var $this yii\web\View */
/* @var $model backend\modules\polls\models\PollsVotes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="polls-votes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image')->widget(
        FileApi::className(),
        [
            'settings' => [
                'url' => ['/polls/votes/fileapi-upload']
            ]
        ]
    ) ?>

    <?= $form->field($model, 'description')->widget(
        Imperavi::className(),
        [
            'settings' => [
                'minHeight' => 200,
            ]
        ]
    ) ?>

    <?= $form->field($model, 'visible')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
