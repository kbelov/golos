<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\polls\models\PollsVotes */
/* @var $pollModel \backend\modules\polls\models\Polls */

$this->title                   = 'Добавление варианта';

$this->params['breadcrumbs'][] = ['label' => 'Голосования', 'url' => ['/polls/default/index']];
$this->params['breadcrumbs'][] = ['label' => $pollModel, 'url' => ['/polls/default/view', 'id' => $pollModel->id]];
$this->params['breadcrumbs'][] = ['label' => 'Варианты', 'url' => ['index', 'id' => $pollModel->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="polls-votes-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
