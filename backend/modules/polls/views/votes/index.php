<?php

use backend\modules\polls\models\PollsVotes;
use yii\grid\SerialColumn;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\polls\models\search\PollsVotesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $poll \backend\modules\polls\models\Polls */

$this->title                   = 'Варианты';

$this->params['breadcrumbs'][] = ['label' => 'Голосования', 'url' => ['/polls/default/index']];
$this->params['breadcrumbs'][] = ['label' => $poll->name, 'url' => ['/polls/default/view', 'id' => $poll->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="polls-votes-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить вариант', ['/polls/votes/create', 'id' => $poll->id], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => SerialColumn::class],

            'id',
            [
                'attribute'  => 'poll_id',
                'format'    => 'html',
                'value'  => function (PollsVotes $model) {
                    return Html::a($model->poll, ['/polls/default/view', 'id' => $model->poll_id]);
                },
            ],
            'name',
            'visible:boolean',
            'created',
            'updated',

            ['class' => ActionColumn::class],
        ],
    ]); ?>
</div>
