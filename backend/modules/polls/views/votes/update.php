<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\polls\models\PollsVotes */

$this->title = 'Обновить вариант: ' . $model->name;

$this->params['breadcrumbs'][] = ['label' => 'Голосования', 'url' => ['/polls/default/index']];
$this->params['breadcrumbs'][] = ['label' => $model->poll->name, 'url' => ['/polls/default/view', 'id' => $model->poll_id]];
$this->params['breadcrumbs'][] = ['label' => 'Варианты', 'url' => ['index', 'id' => $model->poll_id]];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="polls-votes-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
