<?php

use backend\modules\history\widgets\HistoryWidget\HistoryWidget;
use backend\modules\polls\models\PollsVotes;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\polls\models\PollsVotes */

$this->title = $model->name;

$this->params['breadcrumbs'][] = ['label' => 'Голосования', 'url' => ['/polls/default/index']];
$this->params['breadcrumbs'][] = ['label' => $model->poll->name, 'url' => ['/polls/default/view', 'id' => $model->poll_id]];
$this->params['breadcrumbs'][] = ['label' => 'Варианты', 'url' => ['index', 'id' => $model->poll_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="polls-votes-view">

    <?php if ($model->getIsVisible()): ?>
        <p>
            <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Вы уверены, что хотите удалить эту запись?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    <?php else: ?>
        <div class="alert alert-danger">
            Этот элемент удален!
        </div>
        <?= Html::a('Восстановить', ['revive', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?php endif; ?>

    <div class="row">
        <div class="col-lg-8">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    [
                        'attribute'  => 'poll_id',
                        'format'    => 'html',
                        'value'  => function (PollsVotes $model) {
                            return Html::a($model->poll, ['/polls/default/view', 'id' => $model->poll_id]);
                        },
                    ],
                    'name',
                    'image',
                    'visible:boolean',
                    'created',
                    'updated',
                ],
            ]) ?>
        </div>
        <div class="col-lg-4">
            <?= HistoryWidget::widget([
                'ownerClass' => PollsVotes::class,
                'ownerId'    => $model->id
            ]); ?>
        </div>
    </div>

</div>
