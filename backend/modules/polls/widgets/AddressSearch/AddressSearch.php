<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.17
 * Time: 13:47
 */

namespace backend\modules\polls\widgets\AddressSearch;

use yii\base\Model;
use yii\base\Widget;

/**
 * Class AddressSearch
 *
 * @package backend\modules\polls\widgets\AddressSearch
 */
class AddressSearch extends Widget
{
    /**
     * @var Model the data model that this widget is associated with.
     */
    public $model;

    /**
     * @var string the model attribute that this widget is associated with.
     */
    public $attribute;

    /**
     * @var array the HTML attributes for the input tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = [];

    /**
     * @throws \yii\base\InvalidParamException
     */
    public function run(): string
    {
        $this->registerAssets();

        return $this->render('index', [
            'model'     => $this->model,
            'attribute' => $this->attribute,
            'options'   => $this->options
        ]);
    }

    protected function registerAssets()
    {
        $view = $this->getView();
        Asset::register($this->getView());
        $view->registerJs('Addresses.init()');
    }

}
