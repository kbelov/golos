<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.17
 * Time: 13:51
 */

namespace backend\modules\polls\widgets\AddressSearch;

use yii\web\AssetBundle;

/**
 * Class Asset
 *
 * @package backend\modules\polls\widgets\AddressSearch
 */
class Asset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@backend/modules/polls/widgets/AddressSearch/assets';

    /**
     * @inheritdoc
     */
    public $js = [
        'js/address.widget.js',
    ];

    /**
     * @inheritdoc
     */
    public $css = [
        'css/address.widget.css'
    ];
}
