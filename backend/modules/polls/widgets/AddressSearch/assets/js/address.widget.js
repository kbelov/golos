var Addresses = {
    self: this,
    bb: null,
    init: function() {
        var a = Backbone.View.extend({
            el: "#address-widget",
            events: {
                'keyup #polls-address':  'onchange',
            },
            initialize: function() {
                this.ev = _.extend({}, Backbone.Events);
                this.$wrapper = this.$el.find('.address-widget-result');
                this.$input = this.$el.find('#polls-address');
                this.$list = this.$el.find('.address-widget-result-list');

                this.collection = new addresses();

                this.childViews = [];

                this.listenTo(this.collection, 'request', this.onRequest);
                this.listenTo(this.collection, 'sync', this.render);
                this.listenTo(this.collection, 'error', this.error);
            },
            open: function (event) {
                if (this.collection.length > 0) {
                    this.$wrapper.addClass('service-search_result--visible');
                    event.stopPropagation();
                }
            },
            onchange: _.debounce(function (e) {
                this.collection.query = e.currentTarget.value;
                if (this.collection.query !== '') {
                    this.collection.fetch();
                    this.$wrapper.addClass('service-search_result--visible')

                } else {
                    this.clear();
                }

            }, 300),
            addChildView: function (view) {
                this.childViews.push(view);
                this.$list.append(view.render().el);
            },
            clearChildViews: function () {
                _.each(this.childViews, function (child) {
                    child.remove();
                });

                this.childViews = [];
            },
            clear: function () {
                this.clearChildViews();
                this.$wrapper.removeClass('service-search_result--visible');
                this.$input.val('');
            },
            render: function () {

                this.clearChildViews();
                _.each(this.collection.models, function (model) {
                    if (!_.isNull(model.get('buildingNumber'))) {
                        var view = new addressView({
                            model: model,
                            ev: this.ev
                        });
                        this.addChildView(view);
                    }
                }, this);

                return this;
            }
        });

        var addressView = Backbone.View.extend({
            tagName: 'li',
            template: _.template(
                "<a><%= pAddress%></a>"
            ),
            events: {
                'click a': 'onClick'
            },
            initialize: function (option) {
                this.ev = option.ev;
            },
            set: function (model) {
                $("#polls-lat").val(model.get('latitude'));
                $("#polls-lon").val(model.get('longitude'));
                $("#polls-area_id").val(model.get('id_raion'));
                $("#polls-address").val(model.get('paddress'));
                $("#polls-building_id").val(model.get('id_buildin'));
            },
            render: function () {
                this.$el.html(this.template(this.model.toJSON()));
                return this;
            },
            onClick: function () {
                Addresses.bb.$wrapper.removeClass('service-search_result--visible');
                var addressModel = new AddressModel({id: this.model.get('buildingEasId')});
                this.listenTo(addressModel, 'sync', this.set);
                addressModel.fetch();
            }
        });

        var AddressModel = Backbone.Model.extend({
            id: "",
            url: function () {
                return 'http://gs.iac.spb.ru/services/getbuilding/?id=' + this.id;
            }
        });

        var addresses = Backbone.Collection.extend({
            model: AddressModel,
            query: "",
            url: function () {
                return 'https://app.toris.gov.spb.ru/address-web/rest/building/search?pAddress=' + this.query;
            }
        });

        this.bb = new a();
    }
};