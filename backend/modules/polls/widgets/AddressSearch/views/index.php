<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.17
 * Time: 13:48
 */

use common\components\Html;

/** @var \yii\base\Model $model */
/** @var string $attribute */
/** @var array $options */

?>
<div id="address-widget">
    <?= Html::activeInput('text', $model, $attribute, $options); ?>
    <div class="address-widget-result">
        <ul class="address-widget-result-list">

        </ul>
    </div>
</div>
