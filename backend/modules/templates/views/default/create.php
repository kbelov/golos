<?php


/* @var $this yii\web\View */
/* @var $model backend\modules\templates\models\Templates */

$this->title = 'Добавление шаблона';
$this->params['breadcrumbs'][] = ['label' => 'Шаблоны тем', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="templates-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
