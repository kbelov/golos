<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\templates\models\Templates */

$this->title = 'Обновление шаблона: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Шаблоны тем', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="templates-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
