<?php

use backend\modules\history\widgets\HistoryWidget\HistoryWidget;
use backend\modules\templates\models\Templates;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\templates\models\Templates */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Шаблоны тем', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="templates-view">

    <?php if ($model->getIsVisible()): ?>
        <p>
            <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Вы уверены, что хотите удалить эту запись?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    <?php else: ?>
        <div class="alert alert-danger">
            Этот элемент удален!
        </div>
        <?= Html::a('Восстановить', ['revive', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?php endif; ?>

    <div class="row">
        <div class="col-lg-8">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'name',
                    'created',
                    'updated',
                ],
            ]) ?>
        </div>
        <div class="col-lg-4">
            <?= HistoryWidget::widget([
                'ownerClass' => Templates::class,
                'ownerId'    => $model->id
            ]); ?>
        </div>
    </div>

</div>
