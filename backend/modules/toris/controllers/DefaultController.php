<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 02.09.2016
 * Time: 13:00
 */

namespace backend\modules\toris\controllers;

use backend\assets\TorisAsset;
use backend\components\Controller;
use backend\helpers\TorisHelper;
use backend\modules\toris\models\UserToris;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Response;

/**
 * Class TorisController
 *
 * @package backend\modules\toris\controllers
 */
class DefaultController extends Controller
{

    public $enableCsrfValidation = false;

    /**
     * @inheritdoc
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionAccess()
    {
        if (!Yii::$app->user->isGuest) {
            Yii::$app->user->logout();
        }
        $view = $this->getView();
        // Убираем старые js для входа в ТОРИС
        $view->js = [];

        TorisAsset::register($view);
        $return_url = Url::toRoute(['/'], true);
        $logout_url = Url::toRoute(['/toris/default/logout'], true);


        $torisDomain = TorisHelper::DOMAIN;

        $view->registerJs('TorisWidget.init("'.$return_url.'", "'.$logout_url.'", true, "'. $torisDomain .'", "'. TorisHelper::CODE .'")');

        $this->layout = 'toris';
        return $this->render(
            'access'
        );
    }

    /**
     * @return array
     * @throws \yii\base\InvalidParamException
     */
    public function actionCheck(): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $post = Yii::$app->request->post();
        $need_redirect = false;
        if (!empty($post['userData'])) {
            if (Yii::$app->user->isGuest) {
                $need_redirect = true;
                $user = UserToris::findIdentityByBx($post['userData']['bx_id']);
                if (empty($user)) {
                    $user = UserToris::createNewBxUser($post['userData']);
                }
                $user->login();
            } else {
                $user = UserToris::findOne(Yii::$app->user->id);
                $user->iogv_id = $post['userData']['iogv_id'];
                $user->refreshAistoken($post['userData']['aistoken']);
            }

            $user->email = $post['userData']['email'];
            $user->phone = $post['userData']['phone'];

            $user->setRole($post['userData']['roles']);
        }
        $return = [
            'need_redirect' => $need_redirect,
            'redirect'      => Url::toRoute(['/'], true),
            'post'          => $post
        ];
        return $return;
    }

    /**
     * @return Response
     */
    public function actionLogout(): Response
    {
        Yii::$app->user->logout();
        return $this->redirect(['/']);
    }
}