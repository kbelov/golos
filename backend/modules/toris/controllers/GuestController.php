<?php

namespace backend\modules\toris\controllers;

use backend\components\Controller;
use backend\modules\toris\models\UserToris;
use yii\filters\AccessControl;
use yii\helpers\Url;
use Yii;

/**
 * Backend controller for guest users.
 */
class GuestController extends Controller
{
    /**
     * @inheritdoc
     */
    public $layout = '//main-login';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['toris'],
                        'roles' => ['?', '@']
                    ]
                ]
            ]
        ];
    }

    /**
     * Login user.
     *
     * @throws \yii\base\InvalidParamException
     */
    public function actionLogin()
    {
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            $user = UserToris::findIdentityByBx($post['userData']['bx_id']);
            if (empty($user)) {
                $user = UserToris::createNewBxUser($post['userData']);
            }
            if ($user->login()) {
                return json_encode([
                    'redirect'  =>  Url::toRoute(['/'])
                ]);
            }

            return json_encode([
                'redirect'  =>  Url::toRoute(['/'])
            ]);
        }

        return $this->render('login');
    }
}
