<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 05.10.2017
 * Time: 21:51
 */

namespace backend\modules\toris\controllers;

use backend\components\Controller;
use backend\helpers\TorisHelper;
use backend\modules\toris\models\UserTorisRolesAccess;
use backend\modules\toris\models\UserTorisRolesAccessForm;
use Yii;

/**
 * Class RolesController
 * @package backend\modules\toris\controllers
 */
class RolesController extends Controller
{
    /**
     * Lists all Roles models.
     *
     * @return mixed
     * @throws \yii\base\InvalidParamException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionIndex()
    {
        TorisHelper::checkAccess();
        return $this->render('index');
    }

    /**
     * @param string $module
     *
     * @return string|\yii\web\Response
     * @throws \yii\base\InvalidParamException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionUpdate(string $module)
    {
        // Здесь и дальше говнокод (Спс Саня)
        TorisHelper::checkAccess();
        $accesses = UserTorisRolesAccess::find()->where([
            'module' => $module
        ])->asArray()->all();

        $roles = [];
        foreach ($accesses as $access) {
            $roles[TorisHelper::ROLES_SHORT_NAMES[$access['role']]] = true;
        }
        foreach (TorisHelper::ROLES_ARRAY as $role => $name) {
            if (!isset($roles[TorisHelper::ROLES_SHORT_NAMES[$role]])) {
                $roles[TorisHelper::ROLES_SHORT_NAMES[$role]] = false;
            }
        }

        $userTorisRolesAccessForm = new UserTorisRolesAccessForm();
        if (Yii::$app->getRequest()->getIsPost()) {
            $userTorisRolesAccessForm->load(Yii::$app->request->post());
            if ($userTorisRolesAccessForm->validate()) {
                if ($userTorisRolesAccessForm->saveRoles($module)) {
                    return $this->redirect(['update', 'module' => $module]);
                } else {
                    Yii::$app->session->setFlash('danger', 'Не сохранено');
                    return $this->refresh();
                }
            }
        }
        $userTorisRolesAccessForm->roles = $roles;

        return $this->render('update', [
            'roles'         => $roles,
            'module'        => $module
        ]);
    }
}
