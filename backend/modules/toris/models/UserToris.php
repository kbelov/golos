<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 04.10.17
 * Time: 13:52
 */

namespace backend\modules\toris\models;

use backend\modules\toris\models\base\UserTorisBase;
use common\modules\mfc\models\Mfc;
use Yii;
use yii\db\ActiveQuery;
use yii\web\IdentityInterface;
use yii\web\ServerErrorHttpException;

/**
 * Class UserToris
 *
 * @property UserTorisRolesLinks[] roles
 * @package backend\modules\toris\models
 */
class UserToris extends UserTorisBase implements IdentityInterface
{

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->fio;
    }

    /**
     * @inheritdoc
     * @return UserToris|null
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     * @return array|UserToris|\yii\db\ActiveRecord
     */
    public static function findIdentityByBx($bx_id)
    {
        return static::find()->where([
            'bx_id' => $bx_id
        ])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoles(): ActiveQuery
    {
        return $this->hasMany(UserTorisRolesLinks::className(), ['user_toris_id' => 'id']);
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'id'      => 'Номер',
            'fio'     => 'ФИО',
            'roles'   => 'Роли',
            'created' => 'Дата создания',
            'updated' => 'Дата обновления',
            'phone'     => 'Телефон',
            'email'     => 'e-mail'
        ];
    }

    /**
     * @param $data
     *
     * @return UserToris
     * @throws \yii\base\InvalidParamException
     */
    public static function createNewBxUser($data): UserToris
    {
        $user = new self();
        $user->setAttributes([
            'fio'      => $data['fio'],
            'bx_id'    => $data['bx_id'],
            'aistoken' => $data['aistoken'],
            'iogv_id'  => $data['iogv_id']
        ]);
        if ($user->validate() && $user->save(false)) {
            return $user;
        }

        return $user;
    }

    /**
     * @return bool
     * @throws \yii\web\ServerErrorHttpException
     */
    public function login(): bool
    {
        if (!Yii::$app->getUser()->login($this, 3600 * 24 * 30)) {
            throw new ServerErrorHttpException();
        }

        return true;
    }

    /**
     * @param $token
     */
    public function refreshAistoken($token)
    {
        if ($token !== $this->aistoken) {
            $this->aistoken = $token;
            $this->save();
        }
    }

    /**
     * @param array $data
     */
    public function setRole(array $data)
    {
        if (!empty($data)) {
            foreach ($data as $roleName) {
                $userRole = UserTorisRoles::findOne(['toris_code' => $roleName]);
                $mfcRole  = Mfc::find()->where([
                    'role' => $roleName
                ])->count();
                if ($userRole || $mfcRole > 0) {
                    $userRoleLink = UserTorisRolesLinks::findOne(['name' => $roleName, 'user_toris_id' => $this->id]);
                    if (!$userRoleLink) {
                        $userRoleLink                = new UserTorisRolesLinks();
                        $userRoleLink->user_toris_id = $this->id;
                        $userRoleLink->name          = $roleName;
                        $userRoleLink->save();
                    }
                }
            }
        }
        UserTorisRolesLinks::deleteAll(
            [
                'AND', 'user_toris_id = :user_toris_id', ['NOT IN', 'name', $data]
            ],
            [
                ':user_toris_id' => $this->id
            ]
        );
    }

    /**
     * Finds an identity by the given token.
     *
     * @param mixed $token the token to be looked for
     * @param mixed $type  the type of the token. The value of this parameter depends on the implementation.
     *                     For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     *
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        // TODO: Implement findIdentityByAccessToken() method.
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     *
     * @return string|int an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     * The space of such keys should be big enough to defeat potential identity attacks.
     * This is required if [[User::enableAutoLogin]] is enabled.
     *
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        // TODO: Implement getAuthKey() method.
    }

    /**
     * Validates the given auth key.
     * This is required if [[User::enableAutoLogin]] is enabled.
     *
     * @param string $authKey the given auth key
     *
     * @return bool whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        // TODO: Implement validateAuthKey() method.
    }
}
