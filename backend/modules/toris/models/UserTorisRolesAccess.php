<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 05.10.2017
 * Time: 21:39
 */

namespace backend\modules\toris\models;

use backend\modules\toris\models\base\UserTorisRolesAccessBase;

/**
 * Class UserTorisRolesAssess
 * @package backend\modules\toris\models
 */
class UserTorisRolesAccess extends UserTorisRolesAccessBase
{
    public static function getAccessArray(): array
    {
        $accesses = self::find()->asArray()->all();
        $accessArray = [];
        foreach ($accesses as $access){
            if (empty($accessArray[$access['module']])) {
                $accessArray[$access['module']] = [];
            }
            $accessArray[$access['module']][] = $access['role'];
        }
        return $accessArray;
    }
}
