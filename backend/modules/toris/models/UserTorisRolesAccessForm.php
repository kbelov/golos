<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 05.10.2017
 * Time: 22:13
 */

namespace backend\modules\toris\models;

use backend\helpers\TorisHelper;

/**
 * Class UserTorisTolesAccessForm
 * @package backend\modules\toris\models
 */
class UserTorisRolesAccessForm extends UserTorisRolesAccess
{
    /** @var array */
    public $roles = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['roles'], 'safe'],
        ];
    }

    public function saveRoles(string $module)
    {
        UserTorisRolesAccess::deleteAll([
            'module'    =>  $module
        ]);
        $shortNames = array_flip(TorisHelper::ROLES_SHORT_NAMES);
        foreach ($this->roles AS $role => $visible) {
            $rolesAcessModel = new UserTorisRolesAccess([
                'module'    => $module,
                'role'      => $shortNames[$role]
            ]);
            $rolesAcessModel->save();
        }
        return true;
    }
}
