<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 04.10.17
 * Time: 14:05
 */

namespace backend\modules\toris\models;

use backend\helpers\TorisHelper;
use backend\modules\mfc\models\Mfc;
use backend\modules\toris\models\base\UserTorisRolesLinksBase;

/**
 * Class UserTorisRolesLinks
 *
 * @package backend\modules\toris\models
 */
class UserTorisRolesLinks extends UserTorisRolesLinksBase
{
    /**
     * @return mixed|null|string
     */
    public function getRoleName()
    {
        if (isset(TorisHelper::ROLES_ARRAY[$this->name])) {
            return TorisHelper::ROLES_ARRAY[$this->name];
        }
        $mfc = Mfc::find()->active()->byRole($this->name)->one();
        if (null !== $mfc) {
            return 'МФЦ: ' . $mfc->name;
        }

        return null;
    }
}
