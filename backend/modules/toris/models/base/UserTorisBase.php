<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 04.10.17
 * Time: 13:51
 */

namespace backend\modules\toris\models\base;

use common\components\ActiveRecord;

/**
 * Class UserTorisBase
 * @property integer $id
 * @property integer $bx_id
 * @property string $iogv_id
 * @property string $fio
 * @property string $aistoken
 * @property string $created
 * @property string $updated
 * @property string $phone
 * @property string $email
 *
 * @package backend\modules\toris\models\base
 */
class UserTorisBase extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_toris';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bx_id', 'iogv_id', 'aistoken'], 'required'],
            [['bx_id'], 'integer'],
            [['aistoken', 'phone', 'email'], 'string'],
            [['created', 'updated'], 'safe'],
            [['iogv_id', 'fio'], 'string', 'max' => 255],
        ];
    }
}