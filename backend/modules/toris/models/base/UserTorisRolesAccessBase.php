<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 05.10.2017
 * Time: 21:36
 */

namespace backend\modules\toris\models\base;

use common\components\ActiveRecord;

/**
 * Class UserTorisRolesAccessBase
 * @property integer $id
 * @property string $module
 * @property string $role
 * @property string $created
 * @property string $updated
 *
 * @package backend\modules\toris\models\base
 */
class UserTorisRolesAccessBase extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_toris_roles_access';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['module', 'role'], 'required'],
            [['created', 'updated'], 'safe'],
        ];
    }
}