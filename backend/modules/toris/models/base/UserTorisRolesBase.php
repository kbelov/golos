<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 04.10.17
 * Time: 13:53
 */

namespace backend\modules\toris\models\base;

use common\components\ActiveRecord;

/**
 * Class UserTorisRolesBase
 *
 * @property integer $id
 * @property string $toris_code
 * @property string $created
 * @property string $updated
 * @package backend\modules\toris\models\base
 */
class UserTorisRolesBase extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_toris_roles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['toris_code'], 'required'],
            [['created', 'updated'], 'safe'],
            [['toris_code'], 'string', 'max' => 255],
        ];
    }

}
