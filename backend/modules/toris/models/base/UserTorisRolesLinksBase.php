<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 04.10.17
 * Time: 13:54
 */

namespace backend\modules\toris\models\base;

use backend\modules\toris\models\UserToris;
use common\components\ActiveRecord;
use yii\db\ActiveQuery;

/**
 * Class UserTorisRolesLinksBase
 *
 * @property integer $id
 * @property integer $user_toris_id
 * @property string $name
 * @property string $created
 * @property string $updated
 * @property UserToris $userToris
 * @package backend\modules\toris\models\base
 */
class UserTorisRolesLinksBase extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_toris_roles_links';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_toris_id', 'name'], 'required'],
            [['user_toris_id'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['user_toris_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserToris::className(), 'targetAttribute' => ['user_toris_id' => 'id']],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserToris(): ActiveQuery
    {
        return $this->hasOne(UserToris::className(), ['id' => 'user_toris_id']);
    }
}
