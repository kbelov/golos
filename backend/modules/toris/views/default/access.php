<?php

/** @var \yii\web\View $this */

$this->title = 'Авторизация в системе ...';

?>

<div class="middle-box text-center animated fadeInDown">
    <h1><i class="fa fa-spinner fa-spin"></i></h1>
    <h3 class="font-bold">Авторизация в системе <?=(!empty(\Yii::$app->name)) ? '"'.\Yii::$app->name.'"' : '';?>...</h3>

    <div class="error-desc">Идет процесс авторизации, не закрывайте эту страницу.</div>
</div>