<?php

/* @var $this yii\web\View */

use common\components\Url;

/* @var $searchModel backend\modules\news\models\search\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Управление уровнем доступа';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Ресурс</th>
                <th>Действие</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach (\backend\helpers\AccessHelper::ROLES_ARRAY as $module => $name): ?>
                <tr>
                    <td><?= $name; ?></td>
                    <td><a href="<?=Url::toRoute(['/toris/roles/update', 'module' => $module]); ?>" class="btn btn-primary">Изменить</a></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
