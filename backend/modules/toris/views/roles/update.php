<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 05.10.2017
 * Time: 22:12
 */

use backend\helpers\AccessHelper;
use backend\helpers\TorisHelper;
use common\components\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $accessesForm \backend\modules\toris\models\UserTorisRolesAccessForm */
/* @var array $roles */
/* @var string $module */

$this->title = 'Обновление модуля '.AccessHelper::ROLES_ARRAY[$module];

$this->params['breadcrumbs'][] = ['label' => 'Управление уровнем доступа', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="news-form">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th width="100">Включена</th>
                <th>Роль</th>
            </tr>
        </thead>
        <tbody>
        <?php $form = ActiveForm::begin();
        $shortNames = array_flip(TorisHelper::ROLES_SHORT_NAMES);
        ?>
        <?php $i = 1; foreach ($roles as $role => $value):
            if ($role === 'admin'): ?>
                <tr>
                    <td class="text-center">
                        <?= Html::checkbox('UserTorisRolesAccessForm[roles]['.$role.']', $value, [
                            'id'    => 'role'.$i,
                            'disabled'  => 'disabled'
                        ]); ?>
                    </td>
                    <td>
                        <?= Html::label(TorisHelper::ROLES_ARRAY[$shortNames[$role]], 'role'.$i); ?>
                    </td>
                </tr>
            <?php continue; endif; ?>
            <tr>
                <td class="text-center">
                    <?= Html::checkbox('UserTorisRolesAccessForm[roles]['.$role.']', $value, [
                        'id'    => 'role'.$i,
                    ]); ?>
                </td>
                <td>
                    <?= Html::label(TorisHelper::ROLES_ARRAY[$shortNames[$role]], 'role'.$i); ?>
                </td>
            </tr>
        <?php $i++; endforeach; ?>
        </tbody>
    </table>

    <div class="form-group">
        <?= Html::submitButton('Обновить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <div class="well">
        <?= AccessHelper::ROLES_ARRAY_DESCRIPTION[$module] ?? '(Нет описания)'; ?>
    </div>

</div>

