<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 05.10.17
 * Time: 12:23
 */

namespace backend\modules\users\models;

use common\components\behaviors\ChangeLog;

/**
 * Class User
 *
 * @package backend\modules\users\models
 */
class User extends \common\models\User
{
    /**
     * @var string
     */
    const MODULE_NAME = 'Пользователи';
    /**
     * @var string
     */
    const MODULE_VIEW_PATH = '/users/default/view';
    /**
     * @inheritdoc
     */
    public function behaviors(): array
    {
        $behaviors = parent::behaviors();
        $behaviors = array_merge($behaviors, [
            'ChangeLog' => [
                'class' => ChangeLog::class
            ]
        ]);
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['last_name', 'first_name', 'middle_name', 'phone', 'email', 'trusted', 'esia_code'], 'safe'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }
}
