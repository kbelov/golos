<?php

use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\users\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => SerialColumn::class],
            'last_name',
            'first_name',
            'middle_name',
            'statusName',
            'trustedName',
            [
                'label' => 'Роль',
                'value' => function (\backend\modules\users\models\User $model) {
                    return 'Пользователь';
                }
            ],
            'points',
            'phone',
            'email:email',
//            'snils',
//            'passport',
//            'passport_date',
//            'passport_info',
//            'passport_code',
            'gender',

            ['class' => ActionColumn::class, 'template' => '{view} {update}'],
        ],
    ]); ?>
</div>
