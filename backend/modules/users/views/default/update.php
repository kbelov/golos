<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\users\models\User */

$this->title = 'Обновление пользователя: ' . $model->getFio();
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->getFio(), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновление';
?>
<div class="user-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
