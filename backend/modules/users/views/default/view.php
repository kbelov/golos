<?php

use backend\modules\history\widgets\HistoryWidget\HistoryWidget;
use backend\modules\users\models\User;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\users\models\User */

$this->title = $model->getFio();
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <div class="row">
        <div class="col-lg-8">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'statusName',
                    'trustedName',
                    'created_at:dateTime',
                    'updated_at:dateTime',
                    'points',
                    'esia_code:ntext',
                    'first_name',
                    'last_name',
                    'middle_name',
                    'phone',
                    'email:email',
//                    'snils',
//                    'passport',
//                    'passport_date',
//                    'passport_info',
//                    'passport_code',
                    'gender',
                ],
            ]) ?>
        </div>
        <div class="col-lg-4">
            <?= HistoryWidget::widget([
                'ownerClass' => User::class,
                'ownerId'    => $model->id
            ]); ?>
        </div>
    </div>

</div>
