<?php

use backend\modules\toris\models\UserToris;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\users\models\search\UserTorisSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи системы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => SerialColumn::class],
            'fio',
            [
                'label' => 'Роль',
                'format' => 'html',
                'value' => function (UserToris $model) {
                    $html = '<ul>';
                    foreach ($model->roles as $role) {
                        $roleName = $role->getRoleName();
                        if (null !== $roleName) {
                            $html .= '<li class="m-b-sm"><span class="label label-primary">' . $roleName . '</span></li>';
                        }
                    }
                    $html .= '</ul>';
                    return $html;
                }
            ],
            'email',
            'phone',
            'created',
            'updated',

            ['class' => ActionColumn::class, 'template' => '{view}'],
        ],
    ]); ?>
</div>
