<?php

use backend\helpers\TorisHelper;
use backend\modules\toris\models\UserToris;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model \backend\modules\toris\models\UserToris */

$this->title = $model->fio;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи системы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <div class="row">
        <div class="col-lg-8">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'fio',
                    [
                        'attribute' => 'roles',
                        'format'    => 'html',
                        'value'     => function (UserToris $model) {
                            $html = '<ul>';
                            foreach ($model->roles as $role) {
                                $roleName = $role->getRoleName();
                                if (null !== $roleName) {
                                    $html .= '<li class="m-b-sm"><span class="label label-primary">' . $roleName . '</span></li>';
                                }
                            }
                            $html .= '</ul>';
                            return $html;
                        }
                    ],
                    'email',
                    'phone',
                    'created',
                    'updated',
                ],
            ]) ?>
        </div>
    </div>

</div>
