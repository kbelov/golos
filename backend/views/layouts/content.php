<?php
use common\widgets\Alert;
use toris\base\helpers\Audit;
use yii\widgets\Breadcrumbs;

?>
<div id="page-wrapper" class="gray-bg">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <?php if (isset($this->blocks['content-header'])) { ?>
                <h1><?= $this->blocks['content-header'] ?></h1>
            <?php } else { ?>
                <h1>
                    <?php
                    if ($this->title !== null) {
                        echo \yii\helpers\Html::encode($this->title);
                    } else {
                        echo \yii\helpers\Inflector::camel2words(
                            \yii\helpers\Inflector::id2camel($this->context->module->id)
                        );
                        echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
                    } ?>
                </h1>
            <?php } ?>
            <?=
            Breadcrumbs::widget(
                [
                    'links' => $this->params['breadcrumbs'] ?? [],
                    'tag'   => 'ol',
                    'activeItemTemplate'    => "<li class=\"active\"><strong>{link}</strong></li>\n"
                ]
            ) ?>
        </div>
    </div>

    <section class="content">
        <?= Alert::widget() ?>
        <div class="row m-t-md">
            <div class="col-lg-12">
                <div class="ibox box-primary m-b-xl">
                    <div class="ibox-content">
                        <?= $content ?>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <div class="footer">
        <div>

        </div>
    </div>
</div>