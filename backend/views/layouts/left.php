<?php
/**
 *
 */

use backend\helpers\TorisHelper;
use common\components\Url;

?>
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse" id="side-menu">
        <?= \backend\widgets\Menu::widget([
                'options' => [
                    'class' => 'nav metismenu',
                ],
                'items' => [
                    [
                        'url'       => Url::toRoute(['/polls/default/index']),
                        'label'     => 'Голосования',
                        'visible'   => TorisHelper::checkAccessForPage('polls/default')
                    ],
                    [
                        'url'       => Url::toRoute(['/news/default/index']),
                        'label'     => 'Новости',
                        'visible'   => TorisHelper::checkAccessForPage('news/default')
                    ],
                    [
                        'url'       => Url::toRoute(['/gifts/default/index']),
                        'label'     => 'Магазин поощрений',
                        'visible'   => TorisHelper::checkAccessForPage('gifts/default')
                    ],
                    [
                        'url'       => Url::toRoute(['/gifts/mfc/index']),
                        'label'     => 'Заказы',
                        'visible'   => TorisHelper::checkAccessForPage('gifts/mfc')
                    ],
                    [
                        'url'       => Url::toRoute(['/pages/default/index']),
                        'label'     => 'Страницы',
                        'visible'   => TorisHelper::checkAccessForPage('pages/default')
                    ],
                    [
                        'url'       => Url::toRoute(['/blocks/default/index']),
                        'label'     => 'Блоки',
                        'visible'   => TorisHelper::checkAccessForPage('blocks/default')
                    ],
                    [
                        'url'       => Url::toRoute(['/areas/default/index']),
                        'label'     => 'Районы',
                        'visible'   => TorisHelper::checkAccessForPage('areas/default')
                    ],
                    [
                        'url'       => Url::toRoute(['/mfc/default/index']),
                        'label'     => 'МФЦ',
                        'visible'   => TorisHelper::checkAccessForPage('mfc/default')
                    ],
                    [
                        'url'       => Url::toRoute(['/templates/default/index']),
                        'label'     => 'Шаблоны тем',
                        'visible'   => TorisHelper::checkAccessForPage('templates/default')
                    ],
                    [
                        'url'       => Url::toRoute(['/users/default/index']),
                        'label'     => 'Пользователи',
                        'visible'   => TorisHelper::checkAccessForPage('users/default')
                    ],
                    [
                        'url'       => Url::toRoute(['/users/toris/index']),
                        'label'     => 'Пользователи системы',
                        'visible'   => TorisHelper::checkAccessForPage('users/default')
                    ],
                    [
                        'url'       => Url::toRoute(['/toris/roles/index']),
                        'label'     => 'Управление уровнем доступа',
                        'visible'   => TorisHelper::checkAccessForPage('toris/roles')
                    ],
                    [
                        'url'       => Url::toRoute(['/history/audit/index']),
                        'label'     => 'Аудит',
                        'visible'   => TorisHelper::checkAccessForPage('toris/roles')
                    ]
                ]
        ]); ?>
    </div>
</nav>
