<?php

use backend\ThemeAsset;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */


    backend\assets\AppAsset::register($this);

$assetUrl = $this->getAssetManager()->getAssetUrl($this->getAssetManager()->getBundle(ThemeAsset::className()), '');

    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <link rel="shortcut icon" type="image/x-icon" href="<?= $assetUrl; ?>dist/img/favicon.png">
        <?php $this->head() ?>
    </head>
    <body style="margin-top: 41px;">
    <?php $this->beginBody() ?>
    <div id="wrapper">

        <?= $this->render(
            'left.php'
        )
        ?>
        <?= $this->render(
            'content.php',
            ['content' => $content]
        ) ?>

    </div>
    <?php $this->endBody() ?>
    </body>
    </html>
    <?php $this->endPage() ?>
