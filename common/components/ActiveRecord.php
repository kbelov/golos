<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 29.09.17
 * Time: 12:12
 */

namespace common\components;

use backend\modules\toris\models\UserToris;
use common\modules\users\services\AddNewAudit;
use Yii;
use yii\base\Event;
use yii\db\AfterSaveEvent;
use yii\db\Expression;

/**
 * Class ActiveRecord
 *
 * @package common\components
 */
class ActiveRecord extends \yii\db\ActiveRecord
{
    /**
     * Ищет модель или Создает модель с переданными атрибутами
     *
     * @param array $attributes
     *
     * @return static
     */
    public static function findOrCreate($attributes)
    {
        $model = self::findOrInit($attributes);
        if ($model->getIsNewRecord()) {
            $model->save();
        }

        return $model;
    }

    /**
     *  EventManager::EVENT_PROVIDER_UPDATE => [
     *      handler::class => [
     *          'on'     => [],
     *          'except' => [],
     *      ]
     *  ],
     *
     * @return array
     */
    public function event(): array
    {
        return [];
    }

    /**
     * Ищет модель по атрибутам или инициализирует новую с переданными атрибутами
     *
     * @param array $attributes
     *
     * @return static
     */
    public static function findOrInit($attributes)
    {
        /** @var \yii\db\ActiveRecord $class */
        $class = static::class;
        /** @var static $model */
        $model = $class::findOne($attributes);
        if (!$model) {
            $model = new $class();
            $model->setAttributes($attributes);
        }

        return $model;
    }

    /**
     * @inheritdoc
     * @return bool
     * @throws \yii\base\InvalidParamException
     */
    public function beforeValidate(): bool
    {
        if (!strpos(static::className(), 'Search')) {
            if ($this->getIsNewRecord()) {
                if ($this->hasAttribute('created')) {
                    $this->setAttribute('created', new Expression('NOW()'));
                }
            } else {
                if ($this->hasAttribute('updated')) {
                    $this->setAttribute('updated', new Expression('NOW()'));
                }
            }
        }

        return parent::beforeValidate();
    }

    /**
     * Переопределение функции
     *
     * @return mixed|string
     */
    public function __toString()
    {
        $fieldNames = ['name', 'title'];
        foreach ($fieldNames as $fieldName) {
            if ($this->hasAttribute($fieldName)) {
                return (string)$this->getAttribute($fieldName);
            }
        }

        return 'Объект класса ' . static::class;
    }

    /**
     * @return mixed|null
     */
    public function getBaseName()
    {
        $fieldNames = ['name', 'title'];
        foreach ($fieldNames as $fieldName) {
            if ($this->hasAttribute($fieldName)) {
                return $fieldName;
            }
        }
        return null;
    }

    /** @inheritdoc */
    public function getAttributeLabel($attribute): string
    {
        return Yii::t(
            'app',
            parent::getAttributeLabel($attribute)
        );
    }

    /**
     * @param bool  $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $eventName = ($insert ? self::EVENT_AFTER_INSERT : self::EVENT_AFTER_UPDATE) . '_' . $this->formName();
        Yii::$app->trigger($eventName, new AfterSaveEvent([
            'changedAttributes' => $changedAttributes,
            'sender'            => $this
        ]));
    }

    public function afterDelete()
    {
        parent::afterDelete();
        $eventName = self::EVENT_AFTER_DELETE . '_' . $this->formName();
        Yii::$app->trigger($eventName, new Event([
            'sender' => $this
        ]));
    }

    /**
     * @return bool
     */
    public function getIsVisible(): bool
    {
        if ($this->hasAttribute('visible')) {
            return $this->visible;
        }
        return true;
    }

    /**
     * @param bool $visible
     * @return string
     */
    public function getVisibleHistoryName(bool $visible): string
    {
        return ($visible) ? 'Да' : 'Нет';
    }

    public function beforeDelete()
    {
        if ($this->hasAttribute('visible')) {
            $this->visible = false;
            if ($this->save()) {
                $userToris = UserToris::findIdentity(Yii::$app->getUser()->getId());
                AddNewAudit::getInstance()
                    ->setOwnerClass(get_class($this))
                    ->setOwnerId($this->primaryKey)
                    ->setType('deleted')
                    ->setUserToris($userToris)
                    ->run();
            }
            return false;
        }
        return true;
    }
}
