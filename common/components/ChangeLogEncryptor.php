<?php
/**
 * Created by PhpStorm.
 * User: andkon
 * Date: 26.04.17
 * Time: 15:11
 */

namespace common\components;

use common\models\User;
use yii\base\Object;

/**
 * Class ChangeLogEncryptor
 *
 * @package common\components
 */
class ChangeLogEncryptor extends Object
{
    /** @var  ActiveRecord */
    protected static $model;
    /** @var  array */
    protected static $result = [];

    /**
     * @param ActiveRecord $model
     * @param string       $date
     * @param array        $oldAttributes
     * @param array        $newAttributes
     * @param int|null     $userId
     *
     * @return string
     */
    public static function encrypt(
        ActiveRecord $model,
        string $date,
        array $oldAttributes,
        array $newAttributes,
        int $userId = null
    ): string {
        self::$model  = $model;
        self::$result = [];
        if ($userId && ($user = User::findOne($userId))) {
            self::$result[] = \Yii::t('app', 'Пользователь {username}', ['username' => $user->getShortName()]);
        } else {
            self::$result[] = 'Система';
        }

        self::$result[] = $date;
        if (!count($newAttributes)) {
            self::$result[] = 'Удалил запись';
            self::$result   = self::encryptOneParam('Поле {field} было {val}', $oldAttributes);
        } elseif (!count($oldAttributes)) {
            self::$result[] = 'Добавил запись';
            self::$result   = self::encryptOneParam('Поле {field}: {val}', $newAttributes);
        } else {
            self::$result = self::encryptByTwoParams(
                'Поле {field} изменилось с {old} на {new}',
                $oldAttributes,
                $newAttributes
            );
        }

        return implode(' ' . PHP_EOL, self::$result);
    }

    /**
     * @param       $template
     * @param array $oldAttributes
     *
     * @return array
     * @internal param ActiveRecord $model
     * @internal param self::$result
     */
    protected static function encryptOneParam($template, array $oldAttributes): array
    {
        foreach ($oldAttributes as $field => $oldVal) {
            if (null === $oldVal) {
                continue;
            }
            self::$result[] = \Yii::t(
                'app',
                $template,
                [
                    'field' => self::$model->getAttributeLabel($field),
                    'val'   => self::toString($oldVal)
                ]
            );
        }

        return self::$result;
    }

    /**
     * @param $val
     *
     * @return string
     */
    protected static function toString($val): string
    {
        if (is_array($val)) {
            return count($val) ? json_encode($val) : 'пусто';
        }

        if (is_bool($val)) {
            return $val ? 'Да' : 'Нет';
        }

        if ($val === null) {
            return 'null';
        }

        if ('' === $val) {
            return 'пусто';
        }

        return (string)$val;
    }

    /**
     * @param string $template
     * @param array  $oldAttributes
     * @param array  $newAttributes
     *
     * @return array
     * @internal param ActiveRecord $model
     * @internal param self::$result
     */
    protected static function encryptByTwoParams(string $template, array $oldAttributes, array $newAttributes): array
    {
        foreach ($newAttributes as $field => $newVal) {
            $oldVal         = (array_key_exists($field, $oldAttributes)) ? $oldAttributes[$field] : null;
            self::$result[] = \Yii::t(
                'app',
                $template,
                [
                    'field' => self::$model->getAttributeLabel($field),
                    'old'   => self::toString($oldVal),
                    'new'   => self::toString($newVal)
                ]
            );
        }

        return self::$result;
    }
}
