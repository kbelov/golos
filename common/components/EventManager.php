<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 05.10.17
 * Time: 18:33
 */

namespace common\components;

use common\modules\distribution\services\DispatchNewPollEmail;
use common\modules\distribution\services\DispatchNewPollSms;
use backend\modules\news\services\DispatchNewsSms;

/**
 * Class EventManager
 *
 * @package common\components
 */
class EventManager
{
//    const EVENT_DISPATCH_SMS = 'EVENT_DISPATCH_SMS';
    const EVENT_POLL_UPDATE = ActiveRecord::EVENT_AFTER_UPDATE . '_Polls';
    const EVENT_NEWS_INSERT = ActiveRecord::EVENT_AFTER_INSERT . '_NewsForm';
    const EVENT_NEWS_UPDATE = ActiveRecord::EVENT_AFTER_UPDATE . '_NewsForm';

    const EVENTS = [
        self::EVENT_POLL_UPDATE    => [
            DispatchNewPollSms::class => 'raiseEvent',
            DispatchNewPollEmail::class => 'raiseEvent',
        ],
        self::EVENT_NEWS_INSERT    => [
            DispatchNewsSms::class => 'raiseEvent'
        ],
        self::EVENT_NEWS_UPDATE    => [
            DispatchNewsSms::class => 'raiseEvent'
        ],
    ];

    public static function init()
    {
        foreach (self::EVENTS as $eventName => $handlers) {
            /** @var array $handlers */
            foreach ($handlers as $class => $handler) {
                \Yii::$app->on($eventName, [$class, $handler]);
            }
        }
    }
}
