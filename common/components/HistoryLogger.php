<?php
/**
 * Created by PhpStorm.
 * User: andkon
 * Date: 26.04.17
 * Time: 12:48
 */

namespace common\components;

use backend\modules\polls\models\Polls;
use common\interfaces\ChangeLogger;
use common\models\History;
use yii\base\Object;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\db\Expression;

/**
 * Class HistoryLogger
 *
 * @package common\components
 */
class HistoryLogger extends Object implements ChangeLogger
{
    public $defaultEncryptor = [
        ChangeLogEncryptor::class,
        'encrypt'
    ];

    /**
     * @param ActiveRecord $model
     * @param array $oldAttributes
     * @param array $newAttributes
     * @param int|null $userId
     *
     * @param bool $isDeleted
     * @return History
     */
    public function add(ActiveRecord $model, array $oldAttributes, array $newAttributes, int $userId = null, bool $isDeleted = false): History
    {
        try {
            $history                 = new History();
            $history->owner_class    = $model::className();
            $history->owner_id       = $model->primaryKey;
            $history->user_id        = $userId;
            $history->old_attributes = $oldAttributes;
            $history->is_deleted     = $isDeleted;
            array_walk($newAttributes, function (&$val) {
                if ($val instanceof Expression && $val->expression === 'NOW()') {
                    $val =  date('Y-m-d H:i:s');
                }

                return $val;
            });
            $history->new_attributes = $newAttributes;
            $history->save();
        } catch (Exception $e) {
            if (YII_DEBUG) {
                echo $e->getMessage();
            }
            return new History();
        }

        return (!$history->hasErrors()) ? $history : new History();
    }

    /**
     * @param ActiveRecord $model
     * @param int          $limit
     * @param int          $offset
     *
     * @return array
     */
    public function getByModel(ActiveRecord $model, int $limit = 5, $offset = 0): array
    {
        /** @var History[] $logs */
        $logs = History::find()
            ->byModel($model)
            ->limit($limit)->offset($offset)->orderBy('created desc')
            ->all();

        $data      = [];
        $encryptor = null;
        if ($model->getBehavior('ChangeLogger')) {
            $encryptor = $model->getBehavior('ChangeLogger')->encryptLogFunc ?? null;
        }

        if (!$encryptor) {
            $encryptor = $this->defaultEncryptor;
        }

        foreach ($logs as $log) {
            $data[] = call_user_func_array(
                $encryptor,
                [$model, $log->created, $log->old_attributes, $log->new_attributes, $log->user_id]
            );
        }

        return $data;
    }
}
