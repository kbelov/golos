<?php
/**
 * Created by PhpStorm.
 * User: andkon
 * Date: 26.04.17
 * Time: 11:23
 */

namespace common\components\behaviors;

use backend\modules\toris\models\UserToris;
use common\components\ActiveRecord;
use common\components\traits\interfaces\ObjectFieldsInterface;
use common\modules\users\services\AddNewAudit;
use Yii;
use yii\base\Behavior;
use yii\base\Event;
use yii\base\Exception;
use yii\db\AfterSaveEvent;
use yii\helpers\Json;

/**
 * Class ChangeLog
 *
 * @package common\components\behaviors
 */
class ChangeLog extends Behavior
{
    /**
     * @var callable
     */
    public $prepareLogFunc;

    /**
     * @var callable
     */
    public $encryptLogFunc;

    public $ignoreFields = [
        'updated',
        'created',
    ];

    /** @inheritdoc */
    public function events(): array
    {
        return [
            ActiveRecord::EVENT_AFTER_DELETE => 'onDelete',
            ActiveRecord::EVENT_AFTER_INSERT => 'onChange',
            ActiveRecord::EVENT_AFTER_UPDATE => 'onChange',
        ];
    }

    /**
     * @param Event $event
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function onDelete(Event $event)
    {
        /** @var ActiveRecord $model */
        $model = $event->sender;

        $logger = $this->getLogger();
        if ($logger) {
            $userId        = $this->getUserId();
            $oldAttributes = $this->getData($model->getAttributes());

            $historyModel = $logger->add($model, $oldAttributes, [], $userId, true);
            if (null != $historyModel->id) {
                $userToris = UserToris::findIdentity(Yii::$app->getUser()->getId());
                AddNewAudit::getInstance()
                    ->setOwnerClass(get_class($model))
                    ->setOwnerId($model->primaryKey)
                    ->setType('deleted')
                    ->setUserToris($userToris)
                    ->setHistory($historyModel)
                    ->run();
            }
        }
    }

    /**
     * @return null|ChangeLogger
     * @throws \yii\base\InvalidConfigException
     */
    protected function getLogger()
    {
        if (\Yii::$app->has('ChangeLogger')) {
            return \Yii::$app->get('ChangeLogger');
        }

        return null;
    }

    /**
     * @return int|null
     */
    private function getUserId()
    {
        if (!(\Yii::$app instanceof \Yii\console\Application)) {
            return \Yii::$app->getUser()->getId();
        }

        return null;
    }

    /**
     * @param array $attributes
     *
     * @return array
     */
    protected function getData($attributes)
    {
        $data = array_diff_key($attributes, array_fill_keys($this->ignoreFields, ''));
        foreach ($data as &$val) {
            if ($val instanceof ObjectFieldsInterface) {
                $val = $val->toString();
            }
        }
        if ($this->prepareLogFunc) {
            $data = call_user_func_array($this->prepareLogFunc, [$attributes]);
        }

        return $data;
    }

    /**
     * @param AfterSaveEvent $event
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function onChange(AfterSaveEvent $event)
    {
        /** @var ActiveRecord $model */
        $model  = $event->sender;
        $logger = $this->getLogger();
        if ($logger) {
            $changedAttributes = $this->getChangeAttributes($model, $event->changedAttributes);
            $oldAttributes     = ($event->name === ActiveRecord::EVENT_AFTER_INSERT) ? [] : $changedAttributes;
            $oldAttributes     = $this->getData($oldAttributes);
            $newAttributes     = array_intersect_key($model->getAttributes(), $changedAttributes);
            $newAttributes     = $this->getData($newAttributes);

            if (count($oldAttributes) || count($newAttributes)) {
                $userId = $this->getUserId();
                $historyModel = $logger->add($model, $oldAttributes, $newAttributes, $userId);
                if (null != $historyModel->id) {
                    $userToris = UserToris::findIdentity(Yii::$app->getUser()->getId());
                    AddNewAudit::getInstance()
                        ->setOwnerClass(get_class($model))
                        ->setOwnerId($model->primaryKey)
                        ->setType('update')
                        ->setUserToris($userToris)
                        ->setHistory($historyModel)
                        ->run();
                }
            }
        }
    }

    /**
     * @param ActiveRecord $model
     * @param null|array   $changedAttributes
     *
     * @return array
     */
    private function getChangeAttributes(ActiveRecord $model, $changedAttributes = null): array
    {
        $changedAttributes = $changedAttributes ?? $model->getAttributes();
        $attributeTypes    = \Yii::$app->getDb()->getTableSchema($model::tableName())->columns;

        $changedAttributes = array_filter($changedAttributes, function ($val, $field) use ($model, $attributeTypes) {
            switch ($attributeTypes[$field]->phpType ?? false) {
                case 'integer':
                    return ((int)$model->$field !== (int)$val);
                    break;
                case 'boolean':
                    return ($val === null || (bool)$model->$field !== (bool)$val);
                    break;
                default:
                    if ($attributeTypes[$field]->dbType === 'jsonb') {
                        try {
                            $old = Json::decode($val);
                            $new = Json::decode($model->$field);

                            return $old !== $new;
                        } catch (Exception $e) {
                            return true;
                        }
                    }

                    return true;
            }
        }, ARRAY_FILTER_USE_BOTH);

        return $changedAttributes;
    }
}
