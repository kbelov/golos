<?php

echo "<?php\n";
?>

namespace <?= $generator->ns ?>;

use <?= '\\' . $generator->ns . '\\base\\' . $className . 'Base;' . "\n" ?>

/**
* Class <?= $className . PHP_EOL ?>
*
* @package <?= $generator->ns . PHP_EOL ?>
*/
class <?= $className ?> extends <?= $className . 'Base' . "\n" ?>
{

}
