<?php
/**
 * Created by PhpStorm.
 * User: andkon
 * Date: 06.05.16
 * Time: 12:36
 */

namespace common\components\traits;

use common\components\traits\interfaces\ObjectFieldsInterface;

/**
 * Class ObjectFields
 * Класс преобразовывает текстовое поле в базе в объект при работе с моделью в php
 * Для задания обрабатываемых полей используйте
 * <code>
 *  public function init()
 * {
 *      parent::init();
 *      $this->registryObjectFields(['fieldName0' => className, 'fieldName1' => className2, 'fieldName'], defaultClass);
 * }
 * </code>
 *
 * @package common\traits
 */
trait ObjectFields
{
    protected $stringIfEmpty = '[]';
    private $objectFields = [];
    private $defaultClass;

    /**
     * Распаковывает Json
     *
     * @return bool
     * @throws \RuntimeException
     */
    public function triggerObjectExtract(): bool
    {
        foreach ($this->objectFields as $field => $class) {
            if (is_int($field)) {
                $field = $class;
                $class = $this->defaultClass;
            }

            if (!is_object($this->$field)) {
                if (!class_exists($class)) {
                    throw new \RuntimeException('Class not found.');
                }
                $obj = new $class($this, $field);
                if (!$obj instanceof ObjectFieldsInterface) {
                    throw new \RuntimeException('Class ' . $class . ' not implement of ObjectFieldsInterface');
                }
            }
        }

        return true;
    }

    /**
     * Пакует массив в Json
     *
     * @return bool
     */
    public function triggerObjectImpact(): bool
    {
        foreach ($this->objectFields as $field => $class) {
            if (is_int($field)) {
                $field = $class;
            }
            if (is_object($this->$field) && $this->$field instanceof ObjectFieldsInterface) {
                $this->$field = $this->$field->toString();
                if (false === $this->$field) {
                    $this->$field = $this->stringIfEmpty;
                }
            }
        }

        return true;
    }

    /**
     * Регистрирует поля с которыми необходимо работать как с массивом
     *
     * @param array       $fields
     * @param null|string $defaultClass
     */
    protected function registryObjectFields($fields, $defaultClass = null): void
    {
        $this->objectFields = $fields;
        $this->defaultClass = $defaultClass;
        $this->registryObjectEvents();
    }

    protected function registryObjectEvents(): void
    {
        $this->on(self::EVENT_INIT, [$this, 'triggerObjectExtract']);
        $this->on(self::EVENT_AFTER_FIND, [$this, 'triggerObjectExtract']);
        $this->on(self::EVENT_AFTER_INSERT, [$this, 'triggerObjectExtract']);
        $this->on(self::EVENT_AFTER_UPDATE, [$this, 'triggerObjectExtract']);
        $this->on(self::EVENT_AFTER_REFRESH, [$this, 'triggerObjectExtract']);
        $this->on(self::EVENT_AFTER_VALIDATE, [$this, 'triggerObjectExtract']);
        $this->on(self::EVENT_BEFORE_UPDATE, [$this, 'triggerObjectImpact']);
        $this->on(self::EVENT_BEFORE_INSERT, [$this, 'triggerObjectImpact']);
        $this->on(self::EVENT_BEFORE_VALIDATE, [$this, 'triggerObjectImpact']);
    }
}
