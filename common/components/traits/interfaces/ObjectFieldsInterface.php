<?php
/**
 * Created by PhpStorm.
 * User: andkon
 * Date: 21.10.16
 * Time: 12:20
 */

namespace common\components\traits\interfaces;

use common\components\ActiveRecord;

/**
 * Interface ObjectFieldsInterface
 *
 * @package common\traits
 */
interface ObjectFieldsInterface
{
    /**
     * При создании объекта, он должен попасть в $model->$field
     * MessageParamsInterface constructor.
     *
     * @param ActiveRecord $model
     * @param string       $field
     */
    public function __construct(ActiveRecord $model, $field);

    /**
     * @return string
     */
    public function toString(): string;
}
