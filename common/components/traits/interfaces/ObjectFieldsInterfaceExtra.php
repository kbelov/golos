<?php

namespace common\components\traits\interfaces;

/**
 * Interface ObjectFieldsInterfaceExtra
 *
 * @package common\traits
 */
interface ObjectFieldsInterfaceExtra
{
    /**
     * @param $name
     *
     * @return mixed
     */
    public function __get($name);

    /**
     * @param string $name
     * @param mixed  $value
     *
     * @return mixed
     */
    public function __set($name, $value);
}
