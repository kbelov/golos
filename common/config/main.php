<?php

use yii\caching\FileCache;

return [
    'vendorPath' => dirname(__DIR__, 2) . '/vendor',
    'components' => [
        'cache' => [
            'class' => FileCache::class,
        ],
        'assetManager' => [
            'linkAssets' => true
        ],
        'ChangeLogger'      => [
            'class' => \common\components\HistoryLogger::className(),
        ],
        'mailer' => [
            'class' => yii\swiftmailer\Mailer::class,
            'viewPath' => '@common/mail',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'mail.adc.spb.ru',
                'username' => 'support@golos.adc.spb.ru',
                'password' => '69c5MG44Kz',
                'port' => '465',
                'encryption' => 'SSL',
            ],
            'enableSwiftMailerLogging' => true,
        ],
        'queue' => [
            'class' => \yii\queue\db\Queue::class,
            'db' => 'db',
            'tableName' => '{{%queue}}', // Table name
            'channel' => 'default', // Queue channel key
            'mutex' => \yii\mutex\PgsqlMutex::class, // Mutex that used to sync queries
            'mutexTimeout' => 0,
            'as log' => \yii\queue\LogBehavior::class
        ],
    ],
    'modules'   => [
        'polls' => [
            'class' => \common\modules\polls\Module::class,
        ],
        'news'  => [
            'class' => \common\modules\news\Module::class
        ],
        'gifts' => [
            'class' => common\modules\gifts\Module::class,
        ],
        'blocks' => [
            'class' => common\modules\blocks\Module::class,
        ],
        'areas' => [
            'class' => common\modules\areas\Module::class,
        ],
        'pages' => [
            'class' => common\modules\pages\Module::class,
        ],
    ]
];
