<?php
return [
    'adminEmail'                    => 'support@golos.adc.spb.ru',
    'supportEmail'                  => 'support@golos.adc.spb.ru',
    'user.passwordResetTokenExpire' => 3600,
    'esia'  => [
        'clientId'              => 'GLS002',
        'portalUrl'             => 'https://esia.gosuslugi.ru/',
        'privateKeyPath'        => Yii::getAlias('@common').'/esia/certs/openSSL_starikov_prvkey.pem',
        'privateKeyPassword'    => '1234567890',
        'certPath'              => Yii::getAlias('@common').'/esia/certs/openSSL_starikov_certif.pem',
    ]
];
