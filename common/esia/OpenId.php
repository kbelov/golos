<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.2017
 * Time: 23:42
 */

namespace common\esia;


use esia\exceptions\RequestFailException;
use esia\exceptions\SignFailException;

class OpenId extends \esia\OpenId
{
    /**
     * Method collect a token with given code
     *
     * @param $code
     * @return false|string
     * @throws SignFailException
     */
    public function getToken($code)
    {
        $this->timestamp = $this->getTimeStamp();
        $this->state = $this->getState();

        $clientSecret = $this->signPKCS7($this->scope . $this->timestamp . $this->clientId . $this->state);

        if ($clientSecret === false) {
            throw new SignFailException(SignFailException::CODE_SIGN_FAIL);
        }

        $request = [
            'client_id'     => $this->clientId,
            'code'          => $code,
            'grant_type'    => 'authorization_code',
            'client_secret' => $clientSecret,
            'state'         => $this->state,
            'redirect_uri'  => $this->redirectUrl,
            'scope'         => $this->scope,
            'timestamp'     => $this->timestamp,
            'token_type'    => 'Bearer',
            'refresh_token' => $this->state,
        ];

        $curl = curl_init();

        if ($curl === false) {
            return false;
        }

        $options = [
            CURLOPT_URL             => $this->getTokenUrl(),
            CURLOPT_POSTFIELDS      => http_build_query($request),
            CURLOPT_POST            => true,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_SSL_VERIFYPEER  => false
        ];
        $proxySettings = \Yii::$app->params['proxySettings'];
        if (!empty($proxySettings)) {
            $options[CURLOPT_PROXY] = $proxySettings['host'];
            $options[CURLOPT_PROXYUSERPWD] = $proxySettings['logpass'];
        }

        curl_setopt_array($curl, $options);

        $result = curl_exec($curl);
        $result = json_decode($result);

        $this->writeLog(print_r($result, true));

        $this->token = $result->access_token;

        # get object id from token
        $chunks = explode('.', $this->token);
        $payload = json_decode($this->base64UrlSafeDecode($chunks[1]));
        $this->oid = $payload->{'urn:esia:sbj_id'};

        $this->writeLog(var_export($payload, true));

        return $this->token;
    }

    /**
     * @return string
     */
    private function getTimeStamp()
    {
        return date("Y.m.d H:i:s O");
    }

    /**
     * Generate state with uuid
     *
     * @return string
     */
    private function getState()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0x0fff) | 0x4000,
            mt_rand(0, 0x3fff) | 0x8000,
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    /**
     * Write log
     *
     * @param string $message
     */
    private function writeLog($message)
    {
        $log = $this->log;

        if (is_callable($log)) {
            $log($message);
        }
    }

    /**
     * Url safe for base64
     *
     * @param string $string
     * @return string
     */
    private function base64UrlSafeDecode($string)
    {
        $base64 = strtr($string, '-_', '+/');

        return base64_decode($base64);
    }

    /**
     * Fetch person info from current person
     *
     * You must collect token person before
     * calling this method
     *
     * @throws \Exception
     * @return null|\stdClass
     */
    public function getPersonInfo()
    {
        $url = $this->personUrl . '/' . $this->oid;

        $request = $this->buildRequest();

        return $request->call($url);
    }

    /**
     * Fetch address from current person
     *
     * You must collect token person before
     * calling this method
     *
     * @throws \Exception
     * @return array
     */
    public function getAddressInfo()
    {
        $url = $this->personUrl . '/' . $this->oid . '/addrs';
        $request = $this->buildRequest();
        $result = $request->call($url);

        if ($result && $result->size > 0) {
            return $this->collectArrayElements($result->elements);
        }

        return null;
    }

    /**
     * Fetch address from current person
     * You must collect token person before
     * calling this method
     *
     * @param string $doc_id
     *
     * @return array|null|\stdClass
     * @throws \Exception
     * @throws \esia\exceptions\RequestFailException
     */
    public function getDocInfo(string $doc_id)
    {
        $url = $this->personUrl . '/' . $this->oid . '/docs/'.$doc_id;
        $request = $this->buildRequest();
        $result = $request->call($url);

        if ($result) {
            return $result;
        }

        return null;
    }

    /**
     * @return Request
     * @throws RequestFailException
     */
    public function buildRequest()
    {
        if (!$this->token) {
            throw new RequestFailException(RequestFailException::CODE_TOKEN_IS_EMPTY);
        }

        return new Request($this->portalUrl, $this->token);
    }
}