<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.2017
 * Time: 23:45
 */

namespace common\esia;

/**
 * Class Request
 * @package common\esia
 */
class Request extends \esia\Request
{
    /**
     * @inheritdoc
     */
    public function call($method, $withScheme = false)
    {

        $ch = $this->prepareAuthCurl();
        if(!is_resource($ch)) {
            return null;
        }

        $url = $withScheme ? $method : $this->url . $method;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $proxySettings = \Yii::$app->params['proxySettings'];
        if (!empty($proxySettings)) {
            curl_setopt($ch, CURLOPT_PROXY, $proxySettings['host']);
            curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxySettings['logpass']);
        }

        return json_decode(curl_exec($ch));
    }
}
