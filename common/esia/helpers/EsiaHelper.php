<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.17
 * Time: 18:45
 */

namespace common\esia\helpers;

use common\components\Url;
use Yii;

/**
 * Class EsiaHelper
 *
 * @package common\esia\helpers
 */
class EsiaHelper
{
    const PROFILE_PARAM_EMAIL = 'EML';
    const PROFILE_PARAM_MOBILE = 'MBT';

    const TYPE_DOC_PASSPORT = 'RF_PASSPORT';

    /**
     * @return array
     * @throws \yii\base\InvalidParamException
     */
    public static function buildConfig(): array
    {
        $paramsEsia = Yii::$app->params['esia'];
        return [
            'clientId'              => $paramsEsia['clientId'],
            'redirectUrl'           => Url::toRoute(['/site/code'], true),
            'portalUrl'             => $paramsEsia['portalUrl'],
            'privateKeyPath'        => $paramsEsia['privateKeyPath'],
            'privateKeyPassword'    => $paramsEsia['privateKeyPassword'],
            'certPath'              => $paramsEsia['certPath'],
            'tmpPath'               => Yii::getAlias('@common').'/esia/tmp',
            'scope'                 => 'fullname email mobile gender snils id_doc'
        ];
    }
}
