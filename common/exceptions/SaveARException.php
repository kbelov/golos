<?php
/**
 * Created by PhpStorm.
 * User: filatov
 * Date: 20.06.2017
 * Time: 14:14
 */

namespace common\exceptions;

use yii\base\Model;

class SaveARException extends \Exception
{
    /** @var Model */
    protected $model;
    public function __construct(Model $model, $data = [])
    {
        $this->model = $model;
        $message = $this->getModelErrors();
        parent::__construct($message, $data);
    }

    protected function getModelErrors()
    {
        $lines = [];
        foreach ($this->model->getErrors() as $errors) {
            foreach ($errors as $error) {
                if (!in_array($error, $lines, true)) {
                    $lines[] = $error;
                }
            }
        }
        return implode('. ', $lines);
    }
}