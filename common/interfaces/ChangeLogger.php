<?php
/**
 * Created by PhpStorm.
 * User: andkon
 * Date: 26.04.17
 * Time: 12:27
 */

namespace common\interfaces;

use common\models\History;
use yii\db\ActiveRecord;

/**
 * Interface ChangeLogger
 *
 * @package common\interfaces
 */
interface ChangeLogger
{
    /**
     * @param ActiveRecord $model
     * @param array $oldAttributes
     * @param array $newAttributes
     * @param int|null $userId
     *
     * @param bool $idDeleted
     * @return History
     */
    public function add(ActiveRecord $model, array $oldAttributes, array $newAttributes, int $userId = null, bool $idDeleted = false): History;

    /**
     * @param ActiveRecord $model
     * @param int          $limit
     *
     * @return array
     */
    public function getByModel(ActiveRecord $model, int $limit = 5): array;
}
