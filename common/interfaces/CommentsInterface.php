<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 02.10.17
 * Time: 17:07
 */

namespace common\interfaces;

/**
 * Interface CommentsInterface
 *
 * @package common\interfaces
 */
interface CommentsInterface
{
    public function getCommentsUrl();
}