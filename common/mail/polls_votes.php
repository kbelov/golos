<?php
use common\models\User;
use \common\modules\polls\models\UserVotes;

/**
 * Created by PhpStorm.
 * User: filatov
 * Date: 17.10.2017
 * Time: 13:02
 * @var User $user
 * @var UserVotes[] $votes
 * @var \yii\web\View $this
 */
?>
<h2>Добрый день <?= $user?> на это недели вы проголосовали:</h2>
<?= $this->render('@frontend/modules/users/views/default/_polls_votes', [
    'votes' => $votes
]); ?>
