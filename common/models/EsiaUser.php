<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 04.10.2017
 * Time: 0:52
 */

namespace common\models;

/**
 * Class EsiaUser
 * @package common\models
 */
class EsiaUser extends User
{
    public function __toString()
    {
        return 'Регистрация пользователя';
    }
}
