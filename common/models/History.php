<?php

namespace common\models;

use backend\modules\toris\models\UserToris;
use common\components\ActiveRecord;
use common\components\traits\ArrayFields;
use common\models\base\HistoryBase;

/**
 * Class History
 *
 * @property \yii\db\ActiveRecord $owner
 * @package common\models
 */
class History extends HistoryBase
{
    use ArrayFields;

    public function init()
    {
        parent::init();
        $this->registryFields(['old_attributes', 'new_attributes']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        /** @var ActiveRecord $class */
        $class = $this->owner_class;

        return $class::find()->andWhere(['id' => $this->owner_id]);
    }

    /**
     * @return UserToris
     */
    public function getUser(): UserToris
    {
        return UserToris::findIdentity($this->user_id);
    }
}
