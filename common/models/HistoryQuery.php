<?php

namespace common\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the ActiveQuery class for [[History]].
 *
 * @see History
 */
class HistoryQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return History[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return History|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param ActiveRecord $model
     *
     * @return $this
     */
    public function byModel(ActiveRecord $model)
    {
        return $this->andWhere(['owner_class' => $model::className(), 'owner_id' => $model->primaryKey]);
    }

    /**
     * @param int $userId
     * @return $this
     */
    public function byUser(int $userId)
    {
        return $this->andWhere(['user_id' => $userId]);
    }

    public function since(string $date)
    {
        $dateFormatted = date(\DateTime::ISO8601, strtotime($date));
        return $this->andWhere(['>=', 'created', $dateFormatted]);
    }
}
