<?php

namespace common\models;

use common\components\ActiveRecord;
use common\modules\distribution\models\Distribution;
use common\modules\distribution\models\UserDistribution;
use common\modules\gifts\models\UserOrders;
use common\modules\polls\models\UserVotes;
use common\modules\polls\models\UserVotesQuery;
use common\modules\transactions\helpers\TransactionStatusHelper;
use common\modules\transactions\models\Transactions;
use yii\behaviors\TimestampBehavior;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer        $id
 * @property string         $username
 * @property string         $password_hash
 * @property string         $password_reset_token
 * @property string         $email
 * @property string         $auth_key
 * @property integer        $status
 * @property integer        $created_at
 * @property integer        $updated_at
 * @property integer        $points
 * @property string         $password write-only password
 * @property string         $esia_code
 * @property string         $first_name
 * @property string         $last_name
 * @property string         $middle_name
 * @property string         $phone
 * @property string         $gender
 * @property string         $snils
 * @property string         $passport
 * @property string         $passport_date
 * @property string         $passport_info
 * @property string         $passport_code
 * @property boolean        $trusted
 * @property UserVotes[]    votes
 * @property UserOrders[]   orders
 * @property Transactions[] actions
 * @property Distribution[] distributions
 * @property Distribution[] emailDistributions
 * @property Distribution[] smsDistributions
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    const STATUS_ARRAY = [
        self::STATUS_ACTIVE  => 'Активный',
        self::STATUS_DELETED => 'Заблокирован'
    ];

    const TRUSTED_ARRAY = [
        0 => 'Не подтвержденная учётная запись',
        1 => 'Подтвержденная учётная запись'
    ];

    const POINTS_FIRST_NAME = 50;
    const POINTS_LAST_NAME = 50;
    const POINTS_MIDDLE_NAME = 50;
    const POINTS_EMAIL = 30;
    const POINTS_PHONE = 40;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }

    /**
     * @return string
     */
    public function getTrustedName(): string
    {
        return self::TRUSTED_ARRAY[$this->trusted] ?? 'Нет роли';
    }

    /**
     * @return string
     */
    public function getStatusName(): string
    {
        return self::STATUS_ARRAY[$this->status] ?? 'Нет статуса';
    }

    /**
     * @return string
     */
    public function getFio(): string
    {
        return $this->last_name . ' ' . $this->first_name . ' ' . $this->middle_name;
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'statusName'     => 'Статус',
            'status'         => 'Статус',
            'points'         => 'Баллы',
            'phone'          => 'Телефон',
            'email'          => 'e-mail',
            'snils'          => 'СНИЛС',
            'first_name'     => 'Имя',
            'last_name'      => 'Фамилия',
            'middle_name'    => 'Отчество',
            'passport'       => 'Паспорт',
            'passport_date'  => 'Дата выдачи',
            'passport_info'  => 'Кем выдан',
            'passport_code'  => 'Код пордазделения',
            'gender'         => 'Пол',
            'trusted'        => 'Статус учетной записи',
            'trustedName'    => 'Статус учетной записи',
            'created_at'     => 'Создан',
            'updated_at'     => 'Обновлен',
            'esia_code'      => 'Уникальный токен ЕСИА'
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @param string $code
     *
     * @return static
     */
    public static function findByEsiaCode(string $code)
    {
        return static::findOne(['esia_code' => $code]);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getShortName();
    }

    /**
     * @return string
     */
    public function getShortName(): string
    {
        return $this->last_name . ' ' .
            mb_substr($this->first_name, 0, 1) . '. ' .
            mb_substr($this->middle_name, 0, 1) . '.';
    }

    /**
     * Finds user by username
     *
     * @param string $username
     *
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @return UserVotesQuery
     */
    public function getVotes()
    {
        return $this->hasMany(UserVotes::className(), ['user_id' => 'id'])
            ->orderBy('created DESC');
    }

    /**
     *
     */
    public function getOrders()
    {
        return $this->hasMany(UserOrders::className(), ['user_id' => 'id'])->orderBy('created DESC');
    }

    /**
     * @return array|Transactions[]|\yii\db\ActiveRecord[]
     */
    public function getActions(): array
    {
        return Transactions::find()->where([
            'or',
            [
                'and',
                [
                    'to'     => self::class,
                    'to_id'  => $this->id,
                    'status' => TransactionStatusHelper::STATUS_RECEIVED
                ]
            ],
            [
                'and',
                [
                    'from'    => self::class,
                    'from_id' => $this->id,
                    'status'  => TransactionStatusHelper::STATUS_SENT
                ]
            ]
        ])->orderBy('created DESC')->all();
    }

    /**
     *
     */
    public function getDistributions()
    {
        return $this->hasMany(Distribution::className(), ['id' => 'distribution_id'])
            ->viaTable(UserDistribution::tableName(), ['user_id' => 'id']);
    }

    public function getEmailDistributions()
    {
        return $this->getDistributions()->andWhere(['type' => Distribution::TYPE_EMAIL]);
    }

    public function getSmsDistributions()
    {
        return $this->getDistributions()->andWhere(['type' => Distribution::TYPE_SMS]);
    }

    /**
     * Finds an identity by the given token.
     *
     * @param mixed $token the token to be looked for
     * @param mixed $type  the type of the token. The value of this parameter depends on the implementation.
     *                     For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     *
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        // TODO: Implement findIdentityByAccessToken() method.
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     * The space of such keys should be big enough to defeat potential identity attacks.
     * This is required if [[User::enableAutoLogin]] is enabled.
     *
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        // TODO: Implement getAuthKey() method.
    }

    /**
     * Validates the given auth key.
     * This is required if [[User::enableAutoLogin]] is enabled.
     *
     * @param string $authKey the given auth key
     *
     * @return bool whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        // TODO: Implement validateAuthKey() method.
    }
}
