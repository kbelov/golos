<?php

namespace common\models\base;

use backend\modules\toris\models\UserToris;
use common\components\ActiveRecord;
use common\models\HistoryQuery;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "history".
 *
 * @property integer $id
 * @property string  $owner_class
 * @property integer $owner_id
 * @property integer $user_id
 * @property array   $old_attributes
 * @property array   $new_attributes
 * @property string  $created
 * @property boolean $is_deleted
 * @property UserToris user
 */
class HistoryBase extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'history';
    }

    /**
     * @inheritdoc
     * @return HistoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new HistoryQuery(static::class);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner_class', 'owner_id'], 'required'],
            [['owner_id', 'user_id'], 'default', 'value' => null],
            [['owner_id', 'user_id'], 'integer'],
            [['is_deleted'], 'boolean'],
            [['old_attributes', 'new_attributes'], 'string'],
            [['created'], 'safe'],
            [['owner_class'], 'string', 'max' => 255],
        ];
    }
}
