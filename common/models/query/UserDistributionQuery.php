<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\modules\distribution\models\UserDistribution]].
 *
 * @see \common\modules\distribution\models\UserDistribution
 */
class UserDistributionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\modules\distribution\models\UserDistribution[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\distribution\models\UserDistribution|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
