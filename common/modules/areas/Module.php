<?php

namespace common\modules\areas;

/**
 * areas module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @var integer Posts per page
     */
    public $recordsPerPage = 10;
}
