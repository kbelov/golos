<?php

namespace common\modules\areas\models;

use common\components\behaviors\ChangeLog;
use common\modules\areas\models\base\AreasBase;

/**
 * Class Areas
 *
 * @package common\modules\areas\models
 */
class Areas extends AreasBase
{

    /**
     * @var string
     */
    const MODULE_NAME = 'Районы';
    /**
     * @var string
     */
    const MODULE_VIEW_PATH = '/areas/default/view';

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new AreasQuery(static::class);
    }

    /**
     * @inheritdoc
     */
    public function behaviors(): array
    {
        return [
            'ChangeLog' => [
                'class' => ChangeLog::class
            ]
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'id'      => 'Номер',
            'name'    => 'Название',
            'code'    => 'Код ЕАС',
            'created' => 'Дата создания',
            'updated' => 'Дата обновления'
        ];
    }
}
