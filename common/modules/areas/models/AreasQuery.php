<?php
namespace common\modules\areas\models;

use common\modules\polls\helpers\PollStatusHelper;
use common\modules\templates\models\Templates;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * Class AreasQuery
 * @package common\modules\areas\models\query
 *  Класс кастомных запросов модели [[Areas]]
 */
class AreasQuery extends ActiveQuery
{
    /**
     * Выбираем только опубликованые опросы.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function active()
    {
        $this->andWhere(Areas::tableName() . '.visible = :visible', [
            ':visible' => true
        ]);
        return $this;
    }

    /**
     * Выбираем только неопубликованые опросы.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function notActive()
    {
        $this->andWhere(Areas::tableName() . '.visible = :visible', [
            ':visible' => false
        ]);
        return $this;
    }
}
