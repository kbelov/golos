<?php

namespace common\modules\areas\models\base;

use common\components\ActiveRecord;

/**
 * This is the model class for table "areas".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property string $created
 * @property string $updated
 * @property boolean $visible
 */
class AreasBase extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'areas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code'], 'required'],
            [['visible'], 'boolean'],
            [['created', 'updated'], 'safe'],
            [['name', 'code'], 'string', 'max' => 255],
        ];
    }
}