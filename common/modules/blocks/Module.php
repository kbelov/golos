<?php

namespace common\modules\blocks;

use common\components\Url;

/**
 * blocks module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @var integer Posts per page
     */
    public $recordsPerPage = 4;

    /**
     * @var string Image path
     */
    public $imagePath = '@statics/web/blocks/images/';

    /**
     * @var string Images temporary path
     */
    public $imageTempPath = '@statics/temp/blocks/images/';

    /**
     * @var string Image URL
     */
    public $imageUrl = Url::STATICS_DOMAIN.'/blocks/images';
}
