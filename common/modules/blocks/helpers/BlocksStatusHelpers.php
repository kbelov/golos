<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 20:22
 */

namespace common\modules\blocks\helpers;

/**
 * Class BlocksStatusHelpers
 * @package common\modules\blocks\helpers
 */
class BlocksStatusHelpers
{
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;
}
