<?php

namespace common\modules\blocks\models;

use common\components\behaviors\ChangeLog;
use common\modules\blocks\models\base\BlocksBase;
use common\modules\blocks\traits\ModuleTrait;
use vova07\fileapi\behaviors\UploadBehavior;

/**
 * Class Blocks
 *
 * @package common\modules\blocks\models
 */
class Blocks extends BlocksBase
{
    use ModuleTrait;

    /**
     * @var string
     */
    const MODULE_NAME = 'Блоки';
    /**
     * @var string
     */
    const MODULE_VIEW_PATH = '/blocks/default/view';

    /**
     * @inheritdoc
     */
    public function behaviors(): array
    {
        return [
            'uploadBehavior' => [
                'class'      => UploadBehavior::className(),
                'attributes' => [
                    'image' => [
                        'path'     => $this->module->imagePath,
                        'tempPath' => $this->module->imageTempPath,
                        'url'      => $this->module->imageUrl
                    ],
                ]
            ],
            'ChangeLog'      => [
                'class' => ChangeLog::class
            ]
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'id'            => 'Номер',
            'name'          => 'Название',
            'external_link' => 'Признак внешней ссылки',
            'link'          => 'Ссылка',
            'link_name'     => 'Название ссылки',
            'visible'       => 'Видимость',
            'created'       => 'Дата создания',
            'updated'       => 'Дата обновления',
            'content'       => 'Описание',
            'image'         => 'Изображение'
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new BlocksQuery(static::class);
    }
}
