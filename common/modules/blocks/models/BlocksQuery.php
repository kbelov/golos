<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 17:36
 */

namespace common\modules\blocks\models;

use common\modules\blocks\helpers\BlocksStatusHelpers;
use yii\db\ActiveQuery;

/**
 * Class BlocksQuery
 * @package common\modules\blocks\models
 */
class BlocksQuery extends ActiveQuery
{
    /**
     * Выбираем только опубликованые блоки.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function active()
    {
        $this->andWhere(Blocks::tableName() . '.visible = :visible', [
            ':visible' => BlocksStatusHelpers::STATUS_ACTIVE
        ]);
        return $this;
    }

    /**
     * Выбираем только неопубликованые блоки.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function notActive()
    {
        $this->andWhere(Blocks::tableName() . '.visible = :visible', [
            ':visible' => BlocksStatusHelpers::STATUS_NOT_ACTIVE
        ]);
        return $this;
    }
}
