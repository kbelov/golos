<?php

namespace common\modules\blocks\models\base;

use common\components\ActiveRecord;

/**
 * This is the model class for table "blocks".
 *
 * @property integer $id
 * @property string $name
 * @property string $content
 * @property string $image
 * @property boolean $external_link
 * @property string $link
 * @property boolean $visible
 * @property string $created
 * @property string $updated
 * @property string $link_name
 */
class BlocksBase extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blocks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'content'], 'required'],
            [['external_link', 'visible'], 'boolean'],
            [['link', 'image'], 'string'],
            [['created', 'updated', 'link_name'], 'safe'],
            [['name', 'content'], 'string', 'max' => 255],
        ];
    }
}