<?php

namespace common\modules\blocks\traits;

use common\modules\blocks\Module;
use Yii;

/**
 * Class ModuleTrait
 * @package common\modules\blocks\traits
 * Implements `getModule` method, to receive current module instance.
 *
 * @property Module module
 */
trait ModuleTrait
{
    /**
     * @var \common\modules\blocks\Module|null Module instance
     */
    private $_module;

    /**
     * @return \common\modules\blocks\Module|null Module instance
     */
    public function getModule()
    {
        if ($this->_module === null) {
            $this->_module = Yii::$app->getModule('blocks');
        }
        return $this->_module;
    }
}
