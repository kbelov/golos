<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 02.10.17
 * Time: 17:19
 */

namespace common\modules\comments\helpers;

/**
 * Class CommentsLikesStatusHelper
 *
 * @package common\modules\comments\helpers
 */
class CommentsLikesStatusHelper
{
    const TYPE_LIKE = 'like';
    const TYPE_DISLIKE = 'dislike';
}
