<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 02.10.17
 * Time: 13:47
 */

namespace common\modules\comments\helpers;

/**
 * Class CommentsStatusHelper
 *
 * @package common\modules\comments\helpers
 */
class CommentsStatusHelper
{
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;
}
