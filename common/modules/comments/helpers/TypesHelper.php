<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 02.10.17
 * Time: 16:29
 */

namespace common\modules\comments\helpers;

use common\modules\polls\models\Polls;

/**
 * Class TypesHelper
 *
 * @package common\modules\comments\helpers
 */
class TypesHelper
{
    const TYPE_POLLS = 'polls';

    /** @var array */
    const TYPES_ARRAY = [
        self::TYPE_POLLS    => Polls::class
    ];
}
