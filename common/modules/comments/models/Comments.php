<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 02.10.17
 * Time: 13:46
 */

namespace common\modules\comments\models;

use common\modules\comments\helpers\CommentsLikesStatusHelper;
use common\modules\comments\models\base\CommentsBase;
use Yii;
use yii\db\ActiveQuery;

/**
 * Class Comments
 *
 * @property CommentsLikes[] likes
 * @property CommentsLikes[] dislikes
 * @package common\modules\comments\models
 */
class Comments extends CommentsBase
{
    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'id'      => 'Номер',
            'user'    => 'Пользователь',
            'content' => 'Комментарий',
            'visible' => 'Видимость',
            'created' => 'Дата создания',
            'updated' => 'Дата обновления'
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new CommentsQuery(static::class);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLikes(): ActiveQuery
    {
        return $this->hasMany(CommentsLikes::className(), ['comment_id' => 'id'])->andWhere([
            'type' => CommentsLikesStatusHelper::TYPE_LIKE,
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDislikes(): ActiveQuery
    {
        return $this->hasMany(CommentsLikes::className(), ['comment_id' => 'id'])->andWhere([
            'type' => CommentsLikesStatusHelper::TYPE_DISLIKE,
        ]);
    }

    /**
     *
     */
    public function getLikeByUser()
    {
        if (Yii::$app->getUser()->getIsGuest()) {
            return 'likes--cant-like';
        }
        if ($this->user_id === Yii::$app->getUser()->getId()) {
            return 'likes--cant-like';
        }
        $like = CommentsLikes::find()
            ->active()
            ->byUser(Yii::$app->getUser()->getId())
            ->byComment($this->id)
            ->one();
        if (null !== $like) {
            return $like->type;
        }

        return 'likes--wait-like';
    }

    /**
     * @return bool
     */
    public function canLike(): bool
    {
        if (Yii::$app->getUser()->getIsGuest()) {
            return false;
        }

        if ($this->user_id === Yii::$app->getUser()->getId()) {
            return false;
        }

        $likes = CommentsLikes::find()
            ->active()
            ->byUser(Yii::$app->getUser()->getId())
            ->byComment($this->id)
            ->count();

        if ($likes > 0) {
            return false;
        }

        return true;
    }
}
