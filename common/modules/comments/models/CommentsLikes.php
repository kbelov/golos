<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 02.10.17
 * Time: 17:18
 */

namespace common\modules\comments\models;

use common\modules\comments\models\base\CommentsLikesBase;

/**
 * Class CommentsLikes
 *
 * @package common\modules\comments\models
 */
class CommentsLikes extends CommentsLikesBase
{
    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new CommentsLikesQuery(static::class);
    }
}
