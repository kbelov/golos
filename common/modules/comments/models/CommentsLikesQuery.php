<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 17:36
 */

namespace common\modules\comments\models;


use common\components\ActiveRecord;
use common\modules\comments\helpers\CommentsStatusHelper;
use yii\db\ActiveQuery;

/**
 * Class CommentsLikesQuery
 * @package common\modules\comments\models
 */
class CommentsLikesQuery extends ActiveQuery
{
    /**
     * Выбираем только опубликованые лайки.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function active()
    {
        $this->andWhere(CommentsLikes::tableName() . '.visible = :status', [
            ':status' => CommentsStatusHelper::STATUS_ACTIVE
        ]);
        return $this;
    }

    /**
     * Выбираем только неопубликованые лайки.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function notActive()
    {
        $this->andWhere(CommentsLikes::tableName() . '.visible = :status', [
            ':status' => CommentsStatusHelper::STATUS_NOT_ACTIVE
        ]);
        return $this;
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function asType(string $type)
    {
        $this->andWhere(CommentsLikes::tableName() . '.type = :type', [
            ':type' => $type
        ]);
        return $this;
    }

    /**
     * @param int $user_id
     * @return $this
     * @internal param string $type
     *
     */
    public function byUser(int $user_id)
    {
        $this->andWhere(CommentsLikes::tableName() . '.user_id = :user_id', [
            ':user_id' => $user_id
        ]);
        return $this;
    }

    /**
     * @param int $comment_id
     * @return $this
     * @internal param int $user_id
     * @internal param string $type
     */
    public function byComment(int $comment_id)
    {
        $this->andWhere(CommentsLikes::tableName() . '.comment_id = :comment_id', [
            ':comment_id' => $comment_id
        ]);
        return $this;
    }

}
