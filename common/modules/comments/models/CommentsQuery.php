<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 17:36
 */

namespace common\modules\comments\models;


use common\components\ActiveRecord;
use common\modules\comments\helpers\CommentsStatusHelper;
use yii\db\ActiveQuery;

/**
 * Class CommentsQuery
 * @package common\modules\comments\models
 */
class CommentsQuery extends ActiveQuery
{
    /**
     * Выбираем только опубликованые новости.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function active()
    {
        $this->andWhere(Comments::tableName() . '.visible = :status', [
            ':status' => CommentsStatusHelper::STATUS_ACTIVE
        ]);
        return $this;
    }

    /**
     * Выбираем только неопубликованые новости.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function notActive()
    {
        $this->andWhere(Comments::tableName() . '.visible = :status', [
            ':status' => CommentsStatusHelper::STATUS_NOT_ACTIVE
        ]);
        return $this;
    }

    /**
     * @param ActiveRecord $model
     *
     * @return $this
     */
    public function asModel(ActiveRecord $model)
    {
        $this->andWhere(Comments::tableName() . '.type = :type AND '.Comments::tableName() . '.target_id = :target_id', [
            ':type'         => get_class($model),
            ':target_id'    => $model->id
        ]);
        return $this;
    }
}
