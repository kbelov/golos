<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 02.10.17
 * Time: 13:45
 */

namespace common\modules\comments\models\base;

use common\components\ActiveRecord;
use common\models\User;
use yii\db\ActiveQuery;

/**
 * Class CommentsBase
 *
 * @property integer $id
 * @property string $type
 * @property integer $target_id
 * @property integer $user_id
 * @property string $content
 * @property boolean $visible
 * @property string $created
 * @property string $updated
 * @property User $user
 * @package common\modules\comments\models\base
 */
class CommentsBase extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'comments';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['type', 'target_id', 'user_id', 'content'], 'required'],
            [['target_id', 'user_id'], 'integer'],
            [['content'], 'string'],
            [['visible'], 'boolean'],
            [['created', 'updated'], 'safe'],
            [['type'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser(): ActiveQuery
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
