<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 02.10.17
 * Time: 17:16
 */

namespace common\modules\comments\models\base;

use common\components\ActiveRecord;
use common\models\User;
use common\modules\comments\models\Comments;
use yii\db\ActiveQuery;

/**
 * Class CommentsLikesBase
 * @property integer $id
 * @property integer $user_id
 * @property integer $comment_id
 * @property string $type
 * @property boolean $visible
 * @property string $created
 * @property string $updated
 * @property Comments $comment
 * @property User $user
 *
 * @package common\modules\comments\models\base
 */
class CommentsLikesBase extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'comments_likes';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'comment_id', 'type'], 'required'],
            [['user_id', 'comment_id'], 'integer'],
            [['visible'], 'boolean'],
            [['created', 'updated'], 'safe'],
            [['type'], 'string', 'max' => 255],
            [['comment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Comments::className(), 'targetAttribute' => ['comment_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComment(): ActiveQuery
    {
        return $this->hasOne(Comments::className(), ['id' => 'comment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser(): ActiveQuery
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}