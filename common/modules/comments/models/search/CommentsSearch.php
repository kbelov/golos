<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 02.10.17
 * Time: 13:52
 */

namespace common\modules\comments\models\search;

use common\components\ActiveRecord;
use common\modules\comments\models\Comments;
use yii\data\ActiveDataProvider;

/**
 * Class CommentsSearch
 *
 * @package common\modules\comments\models\search
 */
class CommentsSearch extends Comments
{

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['type', 'target_id'], 'required'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param ActiveRecord $model
     *
     * @return ActiveDataProvider
     */
    public function search(ActiveRecord $model): ActiveDataProvider
    {
        $query = Comments::find()->active()->asModel($model);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}
