<?php
/**
 * Created by PhpStorm.
 * User: filatov
 * Date: 12.10.2017
 * Time: 18:31
 */
namespace common\modules\distribution\helpers;

class DispatchCodes
{
    const CODE_POLL_CHANGE_STATUS = 'poll_change_status';
    const CODE_NEW_POLL_ADDED = 'new_poll_added';
    const CODE_GET_POINTS = 'get_points';
    const CODE_POLL_HISTORY = 'poll_history';
    const CODE_NEW_POLL_SMS = 'new_poll_sms';
    const CODE_NEWS_SMS = 'news_sms';
}
