<?php

namespace common\modules\distribution\models;

class Distribution extends \common\modules\distribution\models\base\DistributionBase
{
    const TYPE_EMAIL = 1;
    const TYPE_SMS = 2;
}