<?php

namespace common\modules\distribution\models\base;

use Yii;
use common\modules\distribution\models\UserDistribution;

/**
 * This is the model class for table "distribution".
*
    * @property integer $id
    * @property string $name
    * @property integer $type
    *
            * @property UserDistribution[] $userDistributions
    */
class DistributionBase extends \yii\db\ActiveRecord
{
/**
* @inheritdoc
*/
public static function tableName()
{
return 'distribution';
}

/**
* @inheritdoc
*/
public function rules()
{
        return [
            [['name'], 'string'],
            [['type'], 'integer'],
        ];
}

/**
* @inheritdoc
*/
public function attributeLabels()
{
return [
    'id' => 'ID',
    'name' => 'Name',
    'type' => 'Type',
];
}

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getUserDistributions()
    {
    return $this->hasMany(UserDistribution::className(), ['distribution_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\modules\distribution\models\query\UserDistributionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\modules\distribution\models\query\UserDistributionQuery(get_called_class());
}
}