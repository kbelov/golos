<?php

namespace common\modules\distribution\models\base;

use common\modules\distribution\models\Distribution;
use Yii;
use common\models\User;

/**
 * This is the model class for table "user_distribution".
*
    * @property integer $id
    * @property integer $user_id
    * @property integer $distribution_id
    * @property boolean $subscribed
    *
            * @property Distribution $distribution
            * @property User $user
    */
class UserDistributionBase extends \yii\db\ActiveRecord
{
/**
* @inheritdoc
*/
public static function tableName()
{
return 'user_distribution';
}

/**
* @inheritdoc
*/
public function rules()
{
        return [
            [['user_id', 'distribution_id'], 'required'],
            [['user_id', 'distribution_id'], 'integer'],
            [['subscribed'], 'boolean'],
            [['distribution_id'], 'exist', 'skipOnError' => true,
                'targetClass' => Distribution::className(), 'targetAttribute' => ['distribution_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true,
                'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
}

/**
* @inheritdoc
*/
public function attributeLabels()
{
return [
    'id' => 'ID',
    'user_id' => 'User ID',
    'distribution_id' => 'Distribution ID',
    'subscribed' => 'Subscribed',
];
}

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getDistribution()
    {
    return $this->hasOne(Distribution::className(), ['id' => 'distribution_id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getUser()
    {
    return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\UserDistributionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\UserDistributionQuery(get_called_class());
}
}