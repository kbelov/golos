<?php

namespace common\modules\distribution\models\query;

/**
 * This is the ActiveQuery class for [[\common\modules\distribution\models\Distribution]].
 *
 * @see \common\modules\distribution\models\Distribution
 */
class UserDistributionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\modules\distribution\models\Distribution[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\distribution\models\Distribution|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
