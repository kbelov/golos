<?php
/**
 * Created by PhpStorm.
 * User: filatov
 * Date: 16.10.2017
 * Time: 16:50
 */

namespace common\modules\distribution\services;

use common\models\User;
use common\modules\distribution\helpers\DispatchCodes;
use common\modules\transactions\models\Transactions;
use common\services\DispatchEmail;
use Yii;
use yii\base\Event;
use yii\base\Exception;

class DispatchAddPointsEmail extends DispatchEmail
{
    /** @var User */
    public $user;

    /** @var Transactions */
    protected $transactionModel;

    public function __construct()
    {
        $this->dispatchCode = DispatchCodes::CODE_GET_POINTS;
    }

    public function setTransactionModel(Transactions $model)
    {
        $this->transactionModel = $model;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        if (null === $this->user) {
            throw new Exception('Не заполнено user');
        }

        if ($this->inDispatch($this->user->id)) {
            return $this->sendEmailInternal();
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    protected function sendEmailInternal()
    {
        $mail = Yii::$app->getMailer();
        return $mail
            ->compose()
            ->setTo($this->user->email)
            ->setFrom(Yii::$app->params['supportEmail'])
            ->setSubject('Начислены баллы')
            ->setHtmlBody($this->getMsg())
            ->send();
    }

    /**
     * @inheritdoc
     */
    protected function getMsg(): string
    {
        return "Вы получили {$this->transactionModel->points} баллов за 
        {$this->transactionModel->renderFromName()}";
    }

    /**
     * @return static
     * @throws \yii\base\Exception
     */
    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
            self::$instance->user = User::findIdentity(Yii::$app->getUser()->getId());
        }

        return self::$instance;
    }
}
