<?php
/**
 * Created by PhpStorm.
 * User: filatov
 * Date: 16.10.2017
 * Time: 14:39
 */

namespace common\modules\distribution\services;

use backend\modules\polls\models\Polls;
use yii\base\Event;
use common\models\User;
use common\modules\distribution\helpers\DispatchCodes;
use common\modules\polls\helpers\NewPollDispatchHelper;
use common\services\DispatchEmail;
use Yii;
use yii\queue\Job;
use yii\queue\Queue;

class DispatchNewPollEmail extends DispatchEmail implements Job
{
    /** @var NewPollDispatchHelper */
    protected $newPollDispatchHelper;

    public function __construct()
    {
        $this->dispatchCode = DispatchCodes::CODE_NEW_POLL_ADDED;
    }

    /**
     * @return mixed
     */
    public function run()
    {
        if ($this->newPollDispatchHelper->canSend()) {
            parent::run();
        }
        return false;
    }

    protected function setPollModel(Polls $model)
    {
        $this->newPollDispatchHelper = new NewPollDispatchHelper(['pollModel' => $model]);
        return $this;
    }

    /**
     * @return static
     */
    public static function getInstance()
    {
        return new self();
    }

    public static function eventCatcher(Event $event)
    {
        if ($event->sender instanceof Polls) {
            $job = static::getInstance()->setPollModel($event->sender);
            if ($job->newPollDispatchHelper->canSend()) {
                Yii::$app->queue->push($job);
            }
        }
    }

    protected function sendEmail(User $user)
    {
        $mail = Yii::$app->getMailer();
        return $mail
            ->compose()
            ->setTo($user->email)
            ->setFrom(Yii::$app->params['supportEmail'])
            ->setSubject('Добавлено новое голосование')
            ->setTextBody($this->newPollDispatchHelper->getMsg())
            ->send();
    }

    /**
     * @param Queue $queue which pushed and is handling the job
     */
    public function execute($queue)
    {
        $this->run();
    }
}
