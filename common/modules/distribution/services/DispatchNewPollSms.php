<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 05.10.17
 * Time: 18:37
 */

namespace common\modules\distribution\services;

use common\modules\polls\helpers\NewPollDispatchHelper;
use common\services\DispatchSms;
use yii\base\Event;
use common\modules\distribution\helpers\DispatchCodes;
use common\modules\polls\models\Polls;
use Yii;
use yii\queue\Job;
use yii\queue\Queue;

/**
 * Class DispatchSms
 *
 * @package common\services
 */
class DispatchNewPollSms extends DispatchSms implements Job
{
    /** @var NewPollDispatchHelper */
    protected $newPollDispatchHelper;

    public function __construct()
    {
        $this->dispatchCode = DispatchCodes::CODE_NEW_POLL_SMS;
    }

    /**
     * @return bool
     */
    protected function canSend(): bool
    {
        return $this->newPollDispatchHelper->canSend();
    }

    /**
     * @return string
     */
    protected function getMsg(): string
    {
        return $this->newPollDispatchHelper->getMsg();
    }

    /**
     * @return static
     */
    public static function getInstance()
    {
        return new self();
    }

    /**
     * @param Event $event
     *
     * @return bool
     * @throws \yii\base\Exception
     */
    public static function eventCatcher(Event $event): bool
    {
        $pollModel = $event->sender;

        if ($pollModel instanceof Polls) {
            $job = static::getInstance()->setPollModel($pollModel);
            if ($job->newPollDispatchHelper->canSend()) {
                Yii::$app->queue->push($job);
            }
            return true;
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function run()
    {
        if ($this->newPollDispatchHelper->canSend()) {
            return parent::run();
        }
        return false;
    }


    protected function setPollModel(Polls $model)
    {
        $this->newPollDispatchHelper = new NewPollDispatchHelper(['pollModel' => $model]);
        return $this;
    }

    /**
     * @param Queue $queue which pushed and is handling the job
     */
    public function execute($queue)
    {
        $this->run();
    }
}
