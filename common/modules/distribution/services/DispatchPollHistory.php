<?php
/**
 * Created by PhpStorm.
 * User: filatov
 * Date: 16.10.2017
 * Time: 17:04
 */

namespace common\modules\distribution\services;

use common\components\HistoryLogger;
use common\models\User;
use common\modules\distribution\helpers\DispatchCodes;
use common\services\DispatchEmail;
use Yii;

class DispatchPollHistory extends DispatchEmail
{
    /** @var string Период выборки */
    public $period = '-1 week';

    /** @var User */
    protected $user;

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function __construct()
    {
        $this->dispatchCode = DispatchCodes::CODE_POLL_HISTORY;
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        foreach ($this->usersToSend() as $user) {
            $this->setUser($user);
            $this->handleHistory();
        }
        return true;
    }

    public function handleHistory()
    {
        $votes = $this->user->getVotes()->since($this->period)->all();
        if (!empty($votes)) {
            $this->sendHistoryEmail($votes);
        }
    }

    /**
     * @param array $votes
     * @return bool
     */
    protected function sendHistoryEmail(array $votes)
    {
        $mail = Yii::$app->getMailer();
        return $mail
            ->compose('polls_votes', [
                'user' => $this->user,
                'votes' => $votes
            ])
            ->setTo($this->user->email)
            ->setFrom(Yii::$app->params['supportEmail'])
            ->setSubject('Об истории голосований (еженедельная подписка)')
            ->send();
    }
}
