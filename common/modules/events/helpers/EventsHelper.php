<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 05.10.17
 * Time: 19:30
 */

namespace common\modules\events\helpers;

/**
 * Class EventsHelper
 *
 * @package common\modules\events\helpers
 */
class EventsHelper
{
    const STATUS_WAITING = 0;
    const STATUS_INPROCESS = 1;
    const STATUS_COMPLETE = 2;

    const DISPATCH_EVENT_CREATE = 'event_create';

    const DISPATCH_EVENT_STATUS_UPDATE = 'event_status_update';
}
