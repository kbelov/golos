<?php

namespace common\modules\events\models\base;

use common\components\ActiveRecord;

/**
 * This is the model class for table "events".
 *
 * @property integer $id
 * @property string  $type
 * @property integer $status
 * @property string  $created
 * @property string  $updated
 * @property integer $target_id
 */
class EventsBase extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'events';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['target_id', 'status'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['type'], 'string', 'max' => 255],
        ];
    }
}
