<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 05.10.17
 * Time: 19:26
 */

namespace common\modules\events\services;

use common\interfaces\Service;
use common\modules\events\helpers\EventsHelper;
use common\modules\events\models\Events;
use yii\base\Exception;

/**
 * Class AddNewEvent
 *
 * @package common\modules\events\services
 */
class AddNewEvent implements Service
{

    /** @var string */
    protected $type;

    /** @var integer */
    protected $target_id;

    /**
     * @return static
     */
    public static function getInstance()
    {
        return new self();
    }

    /**
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function run()
    {
        if (null === $this->type) {
            throw new Exception('Не заполнены type');
        }

        $eventQuery = Events::find()->where([
            'status' => EventsHelper::STATUS_WAITING,
            'type'   => $this->type
        ]);
        if (null !== $this->target_id) {
            $eventQuery->andWhere([
                'target_id' => $this->target_id
            ]);
        }

        $countCurrentEvent = $eventQuery->count();
        if ($countCurrentEvent > 0) {
            return true;
        }

        $transaction = \Yii::$app->getDb()->beginTransaction();
        try {
            $event = new Events([
                'type' => $this->type
            ]);
            if (null !== $this->target_id) {
                $event->target_id = $this->target_id;
            }
            $event->save();

            if (!$event->hasErrors()) {
                $transaction->commit();
            } else {
                $transaction->rollBack();
                throw new Exception('Transaction errors: ' . count($event->getFirstErrors()));
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param mixed $type
     *
     * @return AddNewEvent
     */
    public function setType(string $type): AddNewEvent
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @param int $target_id
     *
     * @return AddNewEvent
     */
    public function setTargetId(int $target_id): AddNewEvent
    {
        $this->target_id = $target_id;

        return $this;
    }
}