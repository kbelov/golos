<?php

namespace common\modules\gifts;

use common\components\Url;

/**
 * gifts module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @var integer Posts per page
     */
    public $recordsPerPage = 9;

    /**
     * @var string Image path
     */
    public $imagePath = '@statics/web/gifts/images/';

    /**
     * @var string Images temporary path
     */
    public $imageTempPath = '@statics/temp/gifts/images/';

    /**
     * @var string Image URL
     */
    public $imageUrl = Url::STATICS_DOMAIN.'/gifts/images';
}
