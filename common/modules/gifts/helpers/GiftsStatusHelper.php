<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 20:03
 */

namespace common\modules\gifts\helpers;

/**
 * Class GiftsStatusHelpers
 * @package common\modules\gifts\helpers
 */
class GiftsStatusHelper
{
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;
}
