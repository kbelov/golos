<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.17
 * Time: 12:55
 */

namespace common\modules\gifts\helpers;

/**
 * Class UserOrderStatusHelper
 *
 * @package common\modules\gifts\helpers
 */
class UserOrderStatusHelper
{
    const STATUS_WAITING = 0;
    const STATUS_AWAITS_DELIVERY = 1;
    const STATUS_ISSUED = 2;
    const STATUS_CANCEL = 3;

    const STATUSES_ARRAY = [
        self::STATUS_WAITING            => 'Ожидает подтверждения',
        self::STATUS_AWAITS_DELIVERY    => 'Ожидает на выдаче',
        self::STATUS_ISSUED             => 'Выдано',
        self::STATUS_CANCEL             => 'Отменено'
    ];
}
