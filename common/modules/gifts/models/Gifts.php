<?php

namespace common\modules\gifts\models;

use common\components\behaviors\ChangeLog;
use common\modules\gifts\models\base\GiftsBase;
use common\modules\gifts\traits\ModuleTrait;
use vova07\fileapi\behaviors\UploadBehavior;

/**
 * Class Gifts
 *
 * @package common\modules\gifts\models
 */
class Gifts extends GiftsBase
{
    use ModuleTrait;

    /**
     * @var string
     */
    const MODULE_NAME = 'Магазин поощрений';
    /**
     * @var string
     */
    const MODULE_VIEW_PATH = '/gifts/default/view';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'uploadBehavior' => [
                'class'      => UploadBehavior::className(),
                'attributes' => [
                    'image' => [
                        'path'     => $this->module->imagePath,
                        'tempPath' => $this->module->imageTempPath,
                        'url'      => $this->module->imageUrl
                    ],
                ]
            ],
            'ChangeLog'      => [
                'class' => ChangeLog::class
            ]
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'id'      => 'Номер',
            'name'    => 'Название',
            'points'  => 'Баллы',
            'image'   => 'Изображение',
            'visible' => 'Видимость',
            'created' => 'Дата создания',
            'updated' => 'Дата обновления'
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new GiftsQuery(static::class);
    }
}
