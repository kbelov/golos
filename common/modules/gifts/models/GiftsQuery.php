<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 17:36
 */

namespace common\modules\gifts\models;

use common\modules\gifts\helpers\GiftsStatusHelper;
use yii\db\ActiveQuery;

/**
 * Class GiftsQuery
 * @package common\modules\gifts\models
 */
class GiftsQuery extends ActiveQuery
{
    /**
     * Выбираем только опубликованые подарки.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function active()
    {
        $this->andWhere(Gifts::tableName() . '.visible = :visible', [
            ':visible' => GiftsStatusHelper::STATUS_ACTIVE
        ]);
        return $this;
    }

    /**
     * Выбираем только неопубликованые подарки.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function notActive()
    {
        $this->andWhere(Gifts::tableName() . '.visible = :visible', [
            ':visible' => GiftsStatusHelper::STATUS_NOT_ACTIVE
        ]);
        return $this;
    }
}
