<?php

namespace common\modules\gifts\models;

use common\components\behaviors\ChangeLog;
use common\components\Html;
use common\modules\areas\models\Areas;
use common\modules\gifts\helpers\UserOrderStatusHelper;
use common\modules\gifts\models\base\UserOrdersBase;
use common\modules\mfc\models\Mfc;
use yii\helpers\ArrayHelper;

/**
 * Class UserOrders
 *
 * @package common\modules\gifts\models
 */
class UserOrders extends UserOrdersBase
{
    /**
     * @var string
     */
    const MODULE_NAME = 'Заказы';
    /**
     * @var string
     */
    const MODULE_VIEW_PATH = '/gifts/mfc/view';

    /**
     * @inheritdoc
     */
    public function behaviors(): array
    {
        return [
            'ChangeLog' => [
                'class' => ChangeLog::class
            ]
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'id'         => 'Номер',
            'user'       => 'Пользователь',
            'gift'       => 'Товар',
            'mfc'        => 'МФЦ',
            'statusName' => 'Статус',
            'created'    => 'Создан',
            'updated'    => 'Обновлен'
        ];
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->gift->name;
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new UserOrdersQuery(static::class);
    }

    /**
     * @return array
     */
    public function getMfcArray(): array
    {
        $areas = Mfc::find()->active()->asArray()->all();

        return ArrayHelper::map($areas, 'id', 'name');
    }

    /**
     * @return array
     */
    public function getAreasArray(): array
    {
        $areas = Areas::find()->active()->asArray()->all();

        return ArrayHelper::map($areas, 'id', 'name');
    }

    /**
     * @return string
     */
    public function getStatusName(): string
    {
        return UserOrderStatusHelper::STATUSES_ARRAY[$this->status] ?? '';
    }

    /**
     * @return string
     */
    public function renderStatusName(): string
    {
        $statusName = $this->getStatusName();
        if (empty($statusName)) {
            return '';
        }
        switch ($this->status) {
            case UserOrderStatusHelper::STATUS_WAITING:
                return $statusName;
                break;
            case UserOrderStatusHelper::STATUS_AWAITS_DELIVERY:
                return Html::tag('span', $statusName, [
                    'class' => 'text-success'
                ]);
                break;
            case UserOrderStatusHelper::STATUS_ISSUED:
                return Html::tag('span', $statusName, [
                    'class' => 'text-primary'
                ]);
                break;
            case UserOrderStatusHelper::STATUS_CANCEL:
                return Html::tag('span', $statusName, [
                    'class' => 'text-danger'
                ]);
                break;
        }
    }
}
