<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 17:36
 */

namespace common\modules\gifts\models;

use common\modules\gifts\helpers\GiftsStatusHelper;
use yii\db\ActiveQuery;

/**
 * Class UserOrdersQuery
 * @package common\modules\gifts\models
 */
class UserOrdersQuery extends ActiveQuery
{
    /**
     * @param int $user_id
     * @return $this
     */
    public function byUser(int $user_id)
    {
        $this->andWhere(UserOrders::tableName() . '.user_id = :user_id', [
            ':user_id' => $user_id
        ]);
        return $this;
    }

    /**
     * @param int $gift_id
     * @return $this
     */
    public function byGift(int $gift_id)
    {
        $this->andWhere(UserOrders::tableName() . '.gift_id = :gift_id', [
            ':gift_id' => $gift_id
        ]);
        return $this;
    }
}
