<?php

namespace common\modules\gifts\models\base;

use common\components\ActiveRecord;

/**
 * This is the model class for table "gifts".
 *
 * @property integer $id
 * @property string $name
 * @property integer $points
 * @property string $image
 * @property boolean $visible
 * @property string $created
 * @property string $updated
 */
class GiftsBase extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gifts';
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['name', 'points', 'image'], 'required'],
            [['points'], 'integer'],
            [['visible'], 'boolean'],
            [['created', 'updated'], 'safe'],
            [['name', 'image'], 'string', 'max' => 255],
        ];
    }
}
