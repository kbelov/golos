<?php

namespace common\modules\gifts\models\base;

use common\components\ActiveRecord;
use common\models\User;
use common\modules\mfc\models\Mfc;
use Yii;
use common\modules\gifts\models\Gifts;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "user_orders".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $gift_id
 * @property integer $points
 * @property integer $mfc_id
 * @property integer $status
 * @property string $created
 * @property string $updated
 *
 * @property Gifts $gift
 * @property User $user
 * @property Mfc $mfc
 */
class UserOrdersBase extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'user_orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'gift_id', 'mfc_id'], 'required'],
            [['user_id', 'gift_id', 'points', 'mfc_id', 'status'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['gift_id'], 'exist', 'skipOnError' => true, 'targetClass' => Gifts::className(), 'targetAttribute' => ['gift_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getGift(): ActiveQuery
    {
        return $this->hasOne(Gifts::className(), ['id' => 'gift_id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getUser(): ActiveQuery
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMfc(): ActiveQuery
    {
        return $this->hasOne(Mfc::className(), ['id' => 'mfc_id']);
    }
}