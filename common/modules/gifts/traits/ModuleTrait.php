<?php

namespace common\modules\gifts\traits;

use common\modules\gifts\Module;
use Yii;

/**
 * Class ModuleTrait
 * @package common\modules\gifts\traits
 * Implements `getModule` method, to receive current module instance.
 *
 * @property Module module
 */
trait ModuleTrait
{
    /**
     * @var \common\modules\gifts\Module|null Module instance
     */
    private $_module;

    /**
     * @return \common\modules\gifts\Module|null Module instance
     */
    public function getModule()
    {
        if ($this->_module === null) {
            $this->_module = Yii::$app->getModule('gifts');
        }
        return $this->_module;
    }
}
