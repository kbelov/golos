<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.17
 * Time: 11:50
 */

namespace common\modules\mfc\helpers;

/**
 * Class MfcStatusHelper
 *
 * @package common\modules\mfc\helpers
 */
class MfcStatusHelper
{
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;
}
