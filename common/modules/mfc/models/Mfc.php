<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.17
 * Time: 11:48
 */

namespace common\modules\mfc\models;

use common\components\behaviors\ChangeLog;
use common\modules\areas\models\Areas;
use common\modules\mfc\models\base\MfcBase;
use yii\helpers\ArrayHelper;

/**
 * Class Mfc
 *
 * @package common\modules\mfc\models
 */
class Mfc extends MfcBase
{
    /**
     * @var string
     */
    const MODULE_NAME = 'МФЦ';
    /**
     * @var string
     */
    const MODULE_VIEW_PATH = '/mfc/default/view';

    /**
     * @inheritdoc
     */
    public function behaviors(): array
    {
        return [
            'ChangeLog' => [
                'class' => ChangeLog::class
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new MfcQuery(static::class);
    }

    /**
     * @return array
     */
    public function getAreasArray(): array
    {
        $areas = Areas::find()->asArray()->all();

        return ArrayHelper::map($areas, 'id', 'name');
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'id'      => 'Номер',
            'name'    => 'Название',
            'address' => 'Адрес',
            'area_id' => 'Район',
            'role'    => 'Роль в ТОРИС',
            'visible' => 'Видимость',
            'created' => 'Создан',
            'updated' => 'Обновлен'
        ];
    }
}
