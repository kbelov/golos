<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 17:36
 */

namespace common\modules\mfc\models;

use common\modules\areas\models\Areas;
use common\modules\gifts\helpers\GiftsStatusHelper;
use common\modules\mfc\helpers\MfcStatusHelper;
use yii\db\ActiveQuery;

/**
 * Class MfcQuery
 * @package common\modules\mfc\models
 */
class MfcQuery extends ActiveQuery
{
    /**
     * Выбираем только опубликованые МФЦ.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function active()
    {
        $this->andWhere(Mfc::tableName() . '.visible = :visible', [
            ':visible' => MfcStatusHelper::STATUS_ACTIVE
        ]);
        return $this;
    }

    /**
     * Выбираем только неопубликованые МФЦ.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function notActive()
    {
        $this->andWhere(Mfc::tableName() . '.visible = :visible', [
            ':visible' => MfcStatusHelper::STATUS_NOT_ACTIVE
        ]);
        return $this;
    }

    /**
     * @param Areas $area
     *
     * @return $this
     */
    public function byArea(Areas $area)
    {
        $this->andWhere(Mfc::tableName() . '.area_id = :area_id', [
            ':area_id' => $area->id
        ]);
        return $this;
    }

    /**
     * @param string $role
     * @return $this
     * @internal param Areas $area
     *
     */
    public function byRole(string $role)
    {
        $this->andWhere(Mfc::tableName() . '.role = :role', [
            ':role' => $role
        ]);
        return $this;
    }
}
