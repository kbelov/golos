<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.17
 * Time: 11:46
 */

namespace common\modules\mfc\models\base;

use common\components\ActiveRecord;
use common\modules\areas\models\Areas;
use common\modules\gifts\models\UserOrders;
use yii\db\ActiveQuery;

/**
 * Class MfcBase
 * @property integer $id
 * @property integer $area_id
 * @property string $address
 * @property string $name
 * @property boolean $visible
 * @property string $created
 * @property string $updated
 * @property string $role
 * @property Areas $area
 * @property UserOrders[] $userOrders
 *
 * @package common\modules\mfc\models\base
 */
class MfcBase extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'mfc';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['area_id', 'address', 'name'], 'required'],
            [['area_id'], 'integer'],
            [['visible'], 'boolean'],
            [['created', 'updated'], 'safe'],
            [['address', 'name', 'role'], 'string', 'max' => 255],
            [['area_id'], 'exist', 'skipOnError' => true, 'targetClass' => Areas::className(), 'targetAttribute' => ['area_id' => 'id']],
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArea(): ActiveQuery
    {
        return $this->hasOne(Areas::className(), ['id' => 'area_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserOrders(): ActiveQuery
    {
        return $this->hasMany(UserOrders::className(), ['mfc_id' => 'id']);
    }
}
