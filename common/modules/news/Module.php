<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 29.09.17
 * Time: 12:08
 */

namespace common\modules\news;

use common\components\Url;

/**
 * Class Module
 *
 * @package common\modules\news
 */
class Module extends \yii\base\Module
{
    /**
     * @var integer Posts per page
     */
    public $recordsPerPage = 5;

    /**
     * @var string Image path
     */
    public $imagePath = '@statics/web/news/images/';

    /**
     * @var string Images temporary path
     */
    public $imageTempPath = '@statics/temp/news/images/';

    /**
     * @var string Image URL
     */
    public $imageUrl = Url::STATICS_DOMAIN.'/news/images';

}
