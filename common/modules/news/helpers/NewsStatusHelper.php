<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 17:37
 */

namespace common\modules\news\helpers;

/**
 * Class NewsStatusHelper
 * @package common\modules\news\helpers
 */
class NewsStatusHelper
{
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;

    const STATUS_IS_PITER = true;
    const STATUS_IS_PROJECT = false;
}
