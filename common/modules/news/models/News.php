<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 29.09.17
 * Time: 12:10
 */

namespace common\modules\news\models;

use backend\modules\toris\models\UserToris;
use common\components\behaviors\ChangeLog;
use common\components\traits\ArrayFields;
use common\modules\news\models\base\NewsBase;
use common\modules\news\traits\ModuleTrait;
use vova07\fileapi\behaviors\UploadBehavior;

/**
 * Class News
 *
 * @package common\modules\news\models
 */
class News extends NewsBase
{
    use ModuleTrait;
    use ArrayFields;

    /**
     * @var string
     */
    const MODULE_NAME = 'Новости';
    /**
     * @var string
     */
    const MODULE_VIEW_PATH = '/news/default/view';

    /**
     * @inheritdoc
     */
    public function behaviors(): array
    {
        return [
            'uploadBehavior' => [
                'class' => UploadBehavior::className(),
                'attributes' => [
                    'image' => [
                        'path' => $this->module->imagePath,
                        'tempPath' => $this->module->imageTempPath,
                        'url' => $this->module->imageUrl
                    ],
                ]
            ],
            'ChangeLog' => [
                'class' => ChangeLog::class
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new NewsQuery(static::class);
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'id'            => 'Номер',
            'name'          => 'Название',
            'content'       => 'Текст новости',
            'preview'       => 'Превью новости',
            'image'         => 'Изображение',
            'creator_id'    => 'Создатель',
            'creator'       => 'Создатель',
            'is_piter'      => 'Новости Питера',
            'visible'       => 'Видимость',
            'date'          => 'Дата новости',
            'created'       => 'Дата создания',
            'updated'       => 'Дата обновления'
        ];
    }
}
