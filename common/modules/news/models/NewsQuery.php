<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 17:36
 */

namespace common\modules\news\models;


use common\modules\news\helpers\NewsStatusHelper;
use yii\db\ActiveQuery;

/**
 * Class NewsQuery
 * @package common\modules\news\models
 */
class NewsQuery extends ActiveQuery
{
    /**
     * Выбираем только опубликованые новости.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function active()
    {
        $this->andWhere(News::tableName() . '.visible = :status', [
            ':status' => NewsStatusHelper::STATUS_ACTIVE
        ]);
        return $this;
    }

    /**
     * Выбираем только неопубликованые новости.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function notActive()
    {
        $this->andWhere(News::tableName() . '.visible = :status', [
            ':status' => NewsStatusHelper::STATUS_NOT_ACTIVE
        ]);
        return $this;
    }

    /**
     * @return $this
     */
    public function isPiter()
    {
        $this->andWhere(News::tableName() . '.is_piter = :is_piter', [
            ':is_piter' => NewsStatusHelper::STATUS_IS_PITER
        ]);
        return $this;
    }

    /**
     * @return $this
     */
    public function isProject()
    {
        $this->andWhere(News::tableName() . '.is_piter = :is_project', [
            ':is_project' => NewsStatusHelper::STATUS_IS_PROJECT
        ]);
        return $this;
    }
}
