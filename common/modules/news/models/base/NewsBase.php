<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 29.09.17
 * Time: 12:16
 */

namespace common\modules\news\models\base;

use common\components\ActiveRecord;
use common\models\User;
use yii\db\ActiveQuery;

/**
 * Class NewsBase
 * @property integer    $id
 * @property string     $name
 * @property string     $content
 * @property string     $preview
 * @property string     $image
 * @property integer    $creator_id
 * @property boolean    $visible
 * @property string     $date
 * @property string     $created
 * @property string     $updated
 * @property boolean    $is_piter
 * @property User       creator
 *
 * @package common\modules\news\models\base
 */
class NewsBase extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'creator_id', 'date'], 'required'],
            [['content', 'preview'], 'string'],
            [['creator_id'], 'integer'],
            [['visible', 'is_piter'], 'boolean'],
            [['date', 'created', 'updated'], 'safe'],
            [['name', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator(): ActiveQuery
    {
        return $this->hasOne(User::className(), ['id' => 'creator_id']);
    }
}
