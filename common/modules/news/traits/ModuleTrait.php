<?php

namespace common\modules\news\traits;

use common\modules\news\Module;
use Yii;

/**
 * Class ModuleTrait
 * @package common\modules\news\traits
 * Implements `getModule` method, to receive current module instance.
 *
 * @property Module module
 */
trait ModuleTrait
{
    /**
     * @var \common\modules\news\Module|null Module instance
     */
    private $_module;

    /**
     * @return \common\modules\news\Module|null Module instance
     */
    public function getModule()
    {
        if ($this->_module === null) {
            $this->_module = Yii::$app->getModule('news');
        }
        return $this->_module;
    }
}
