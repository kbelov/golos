<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 22:25
 */

namespace common\modules\pages;

use common\components\Url;

/**
 * Class Module
 * @package common\modules\pages
 */
class Module extends \yii\base\Module
{
    /**
     * @var string Image path
     */
    public $imagePath = '@statics/web/pages/images/';

    /**
     * @var string Images temporary path
     */
    public $imageTempPath = '@statics/temp/pages/images/';

    /**
     * @var string Image URL
     */
    public $imageUrl = Url::STATICS_DOMAIN.'/pages/images';
}
