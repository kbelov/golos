<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 22:26
 */

namespace common\modules\pages\helpers;

/**
 * Class PagesStatusHelper
 * @package common\modules\pages\helpers
 */
class PagesStatusHelper
{
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;
}
