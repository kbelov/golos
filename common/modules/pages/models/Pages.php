<?php

namespace common\modules\pages\models;

use common\components\behaviors\ChangeLog;
use common\modules\pages\models\base\PagesBase;
use common\modules\pages\traits\ModuleTrait;
use vova07\fileapi\behaviors\UploadBehavior;

/**
 * Class Pages
 *
 * @package common\modules\pages\models
 */
class Pages extends PagesBase
{
    use ModuleTrait;

    /**
     * @var string
     */
    const MODULE_NAME = 'Страницы';
    /**
     * @var string
     */
    const MODULE_VIEW_PATH = '/pages/default/view';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'uploadBehavior' => [
                'class'      => UploadBehavior::className(),
                'attributes' => [
                    'image' => [
                        'path'     => $this->module->imagePath,
                        'tempPath' => $this->module->imageTempPath,
                        'url'      => $this->module->imageUrl
                    ],
                ]
            ],
            'ChangeLog'      => [
                'class' => ChangeLog::class
            ]
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'id'           => 'Номер',
            'name'         => 'Название',
            'content'      => 'Описание',
            'visible'      => 'Видимость',
            'show_in_menu' => 'Показывать в меню',
            'image'        => 'Изображение',
            'created'      => 'Дата создания',
            'updated'      => 'Дата обновления'
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new PagesQuery(static::class);
    }
}
