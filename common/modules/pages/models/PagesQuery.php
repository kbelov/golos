<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 17:36
 */

namespace common\modules\pages\models;

use common\modules\pages\helpers\PagesStatusHelper;
use yii\db\ActiveQuery;

/**
 * Class PagesQuery
 * @package common\modules\gifts\models
 */
class PagesQuery extends ActiveQuery
{
    /**
     * Выбираем только опубликованые страницы.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function active()
    {
        $this->andWhere(Pages::tableName() . '.visible = :visible', [
            ':visible' => PagesStatusHelper::STATUS_ACTIVE
        ]);
        return $this;
    }

    /**
     * Выбираем только неопубликованые страницы.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function notActive()
    {
        $this->andWhere(Pages::tableName() . '.visible = :visible', [
            ':visible' => PagesStatusHelper::STATUS_NOT_ACTIVE
        ]);
        return $this;
    }

    /**
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function inMenu()
    {
        $this->andWhere(Pages::tableName() . '.show_in_menu = :show_in_menu', [
            ':show_in_menu' => true
        ]);
        return $this;
    }
}
