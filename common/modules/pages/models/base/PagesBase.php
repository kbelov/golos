<?php

namespace common\modules\pages\models\base;

use common\components\ActiveRecord;

/**
 * This is the model class for table "pages".
 *
 * @property integer $id
 * @property string $name
 * @property string $content
 * @property string $image
 * @property boolean $visible
 * @property string $created
 * @property string $updated
 * @property boolean $show_in_menu
 */
class PagesBase extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'content'], 'required'],
            [['content'], 'string'],
            [['visible', 'show_in_menu'], 'boolean'],
            [['created', 'updated'], 'safe'],
            [['name', 'image'], 'string', 'max' => 255],
        ];
    }
}