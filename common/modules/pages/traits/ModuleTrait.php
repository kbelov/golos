<?php

namespace common\modules\pages\traits;

use common\modules\pages\Module;
use Yii;

/**
 * Class ModuleTrait
 * @package common\modules\pages\traits
 * Implements `getModule` method, to receive current module instance.
 *
 * @property Module module
 */
trait ModuleTrait
{
    /**
     * @var \common\modules\pages\Module|null Module instance
     */
    private $_module;

    /**
     * @return \common\modules\pages\Module|null Module instance
     */
    public function getModule()
    {
        if ($this->_module === null) {
            $this->_module = Yii::$app->getModule('pages');
        }
        return $this->_module;
    }
}
