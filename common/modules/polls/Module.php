<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 17.05.2017
 * Time: 16:30
 */

namespace common\modules\polls;

use common\components\Url;

/**
 * Class Module
 *
 * @package common\modules\polls
 */
class Module extends \yii\base\Module
{
    /**
     * @var integer Posts per page
     */
    public $recordsPerPage = 12;

    /**
     * @var string Image path
     */
    public $photoPath = '@statics/web/votes/images/';

    /**
     * @var string Images temporary path
     */
    public $photoTempPath = '@statics/temp/votes/images/';

    /**
     * @var string Image URL
     */
    public $photoUrl = Url::STATICS_DOMAIN . '/votes/images';

    /**
     * @var string Image path
     */
    public $galleryPath = '@statics/web/polls/gallery/';

    /**
     * @var string Images temporary path
     */
    public $galleryTempPath = '@statics/temp/polls/gallery/';

    /**
     * @var string Image URL
     */
    public $galleryUrl = '/statics/polls/gallery';
}