<?php
/**
 * Created by PhpStorm.
 * User: filatov
 * Date: 16.10.2017
 * Time: 15:32
 */

namespace common\modules\polls\helpers;

use backend\modules\polls\models\Polls;
use yii\base\Object;
use Yii;

class NewPollDispatchHelper extends Object
{
    /** @var Polls */
    public $pollModel;

    /**
     * Следует ли посылать сообщение
     * @return bool
     */
    public function canSend(): bool
    {
        return $this->pollModel->status_soglas == PollStatusHelper::STATUS_SOGLAS_FINISHED
            && $this->pollModel->visible == true;
    }

    /**
     * Сообщение
     * @return string
     */
    public function getMsg(): string
    {
        return "На портале Голос Петербурга опубликовано новое Голосование {$this->pollModel->name}: ".
            Yii::$app->urlManagerFrontend->createAbsoluteUrl(['/polls/default/view', 'id' => $this->pollModel->id]);
    }
}
