<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 23.09.2017
 * Time: 18:38
 */

namespace common\modules\polls\helpers;

/**
 * Class PollStatusHelper
 * @package common\modules\polls\helpers
 */
class PollStatusHelper
{
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;

    const STATUSES_ARRAY = [
        self::STATUS_NOT_ACTIVE => 'Не активный',
        self::STATUS_ACTIVE     => 'Активный'
    ];

    const PUBLIC_STATUS_RUNNING = 0;
    const PUBLIC_STATUS_ENDED = 1;

    const PUBLIC_STATUSES_ARRAY = [
        self::PUBLIC_STATUS_RUNNING => 'В процессе',
        self::PUBLIC_STATUS_ENDED   => 'Завершен'
    ];

    const STATUS_SOGLAS_CREATED = 'created';
    const STATUS_SOGLAS_IOGV = 'iogv';
    const STATUS_SOGLAS_MODERATOR = 'moderator';
    const STATUS_SOGLAS_MANAGER = 'manager';
    const STATUS_SOGLAS_FINISHED = 'finished';

    const STATUS_SOGLAS_ARRAY = [
        self::STATUS_SOGLAS_IOGV        => 'На согласовании в ИОГВ',
        self::STATUS_SOGLAS_MANAGER     => 'На согласовании у менеджера',
        self::STATUS_SOGLAS_MODERATOR   => 'На согласовании у модератора',
        self::STATUS_SOGLAS_CREATED     => 'Создание',
        self::STATUS_SOGLAS_FINISHED    => 'Опубликован'
    ];

    const STATUS_SOGLAS_ICONS_ARRAY = [
        self::STATUS_SOGLAS_IOGV        => 'pause',
        self::STATUS_SOGLAS_MANAGER     => 'user',
        self::STATUS_SOGLAS_MODERATOR   => 'pencil',
        self::STATUS_SOGLAS_CREATED     => 'spinner',
        self::STATUS_SOGLAS_FINISHED    => 'flag-checkered'
    ];

    const VISIBILITY_FOR_TRUSTED = 'trusted';
    const VISIBILITY_FOR_ALL = 'all';

    const VISIBILITY_ARRAY = [
        self::VISIBILITY_FOR_ALL   => 'Для всех',
        self::VISIBILITY_FOR_TRUSTED => 'Только для подтвержденных'
    ];
}
