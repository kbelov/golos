<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 17.05.2017
 * Time: 16:37
 */

namespace common\modules\polls\models;

use backend\helpers\TorisHelper;
use backend\modules\polls\models\PollsSoglas;
use backend\modules\toris\models\UserTorisRolesLinks;
use common\components\behaviors\ChangeLog;
use common\components\Url;
use common\interfaces\CommentsInterface;
use common\modules\areas\models\Areas;
use common\modules\comments\models\Comments;
use common\modules\polls\helpers\PollStatusHelper;
use common\modules\polls\models\base\PollsBase;
use Yii;
use yii\db\ActiveQuery;

/**
 * Class Polls
 *
 * @package common\modules\polls\models
 * @property integer       countVoted
 * @property integer       countCommented
 * @property PollsVotes[]  votes
 * @property array         votesForm
 * @property UserVotes     userVote
 * @property UserVotes[]   userVotes
 * @property Areas         area
 * @property PollsSoglas[] soglasStatuses
 * @property PollsSoglas   lastSoglas
 */
class Polls extends PollsBase implements CommentsInterface
{
    /**
     * @var string
     */
    const MODULE_NAME = 'Опросы';
    /**
     * @var string
     */
    const MODULE_VIEW_PATH = '/polls/default/view';
    /**
     * @inheritdoc
     */
    public function behaviors(): array
    {
        return [
            'ChangeLog' => [
                'class' => ChangeLog::class
            ]
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'id'                   => 'Номер',
            'name'                 => 'Название',
            'name_inner'           => 'Название на странице опроса',
            'address'              => 'Адрес',
            'area_id'              => 'Район',
            'area'                 => 'Район',
            'main_description'     => 'Текст на главной',
            'short_description'    => 'Текст сокращенный',
            'description'          => 'Текст полный',
            'complete_description' => 'Итоги голосования',
            'complete_decision'    => 'Решение',
            'complete_result'      => 'Результат',
            'date_start'           => 'Дата начала',
            'date_end'             => 'Дата окончания',
            'points'               => 'Количество баллов',
            'status'               => 'Опубликовано',
            'public_status'        => 'Статус',
            'statusName'           => 'Статус',
            'lat'                  => 'Широта',
            'lon'                  => 'Долгота',
            'building_id'          => 'Код здания в ЕАС',
            'statusSoglasName'     => 'Статус согласования',
            'created'              => 'Создан',
            'updated'              => 'Обновлен',
            'visible'              => 'Видимость',
            'visibility'           => 'Видимость для пользователей',
            'visibilityName'       => 'Видимость для пользователей',
            'count_votes'          => 'Максимальное количество выбираемых ответов'
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getSoglasStatuses(): ActiveQuery
    {
        return $this->hasMany(PollsSoglas::className(), ['poll_id' => 'id'])->orderBy('created DESC');
    }

    /**
     * @return ActiveQuery
     */
    public function getLastSoglas(): ActiveQuery
    {
        return $this->hasOne(PollsSoglas::className(), ['poll_id' => 'id'])->orderBy('created DESC');
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new PollsQuery(static::class);
    }

    /**
     * @return integer
     */
    public function getCountVoted(): int
    {
        return UserVotes::find()->active()->byPoll($this->id)->count();
    }

    /**
     * @return integer
     */
    public function getCountCommented(): int
    {
        return Comments::find()->active()->asModel($this)->count();
    }

    /**
     * @return string
     */
    public function getStatusName(): string
    {
        return PollStatusHelper::STATUSES_ARRAY[$this->status] ?? 'Нет статуса';
    }

    /**
     * @return string
     */
    public function getStatusSoglasName(): string
    {
        return PollStatusHelper::STATUS_SOGLAS_ARRAY[$this->status_soglas] ?? 'Нет статуса';
    }

    /**
     * @return string
     */
    public function getPublicStatusName(): string
    {
        return PollStatusHelper::PUBLIC_STATUSES_ARRAY[$this->public_status] ?? 'Нет статуса';
    }

    /**
     * @return bool
     */
    public function canMakeComment(): bool
    {
        if (Yii::$app->getUser()->getIsGuest()) {
            return false;
        }

        $user_id   = Yii::$app->getUser()->getId();
        $voteModel = UserVotes::find()->byPoll($this->id)->byUser($user_id)->active()->one();

        return !empty($voteModel);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVotes(): ActiveQuery
    {
        return $this->hasMany(PollsVotes::className(), ['poll_id' => 'id']);
    }

    /**
     * @return string
     * @throws \yii\base\InvalidParamException
     */
    public function getUrl(): string
    {
        return Url::toRoute(['/polls/default/view', 'id' => $this->id], true);
    }

    /**
     * @return string
     * @throws \yii\base\InvalidParamException
     */
    public function getCommentsUrl(): string
    {
        return $this->getUrl();
    }

    /**
     * @return bool
     */
    public function isVoted(): bool
    {
        if (Yii::$app->getUser()->getIsGuest()) {
            return false;
        }

        return UserVotes::find()->active()->byPoll($this->id)->byUser(Yii::$app->getUser()->getId())->count();
    }

    /**
     * @return bool|int|string
     */
    public function getUserVote()
    {
        if (Yii::$app->getUser()->getIsGuest()) {
            return null;
        }

        return UserVotes::find()->active()->byPoll($this->id)->byUser(Yii::$app->getUser()->getId())->one();
    }

    /**
     * @return bool|int|string
     */
    public function getUserVotes()
    {
        if (Yii::$app->getUser()->getIsGuest()) {
            return null;
        }

        return UserVotes::find()->active()->byPoll($this->id)->byUser(Yii::$app->getUser()->getId())->all();
    }

    /**
     * @return bool
     */
    public function isRunning(): bool
    {
        $dateEnd = new \DateTime($this->date_end);
        $currDate = new \DateTime('now');
        return $this->visible == PollStatusHelper::STATUS_ACTIVE
            && ($currDate < $dateEnd);
    }

    /**
     * @return ActiveQuery
     */
    public function getArea(): ActiveQuery
    {
        return $this->hasOne(Areas::className(), ['code' => 'area_id']);
    }

    /**
     * @return bool
     */
    public function canDeleteComments(): bool
    {
        $userRolesModels = UserTorisRolesLinks::find()->where([
            'user_toris_id' => Yii::$app->getUser()->getId()
        ])->all();

        $userRoles = [];
        foreach ($userRolesModels as $role) {
            $userRoles[] = $role->name;
        }

        if (empty($userRoles)) {
            return false;
        }

        if (!in_array(TorisHelper::ROLE_ADMIN, $userRoles, true)) {
            if (in_array(TorisHelper::ROLE_MODERATOR, $userRoles, true)) {
                return true;
            }

            return false;
        }

        return true;
    }
}
