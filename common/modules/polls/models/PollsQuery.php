<?php
namespace common\modules\polls\models;

use common\modules\polls\helpers\PollStatusHelper;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * Class PollQuery
 * @package common\modules\polls\models\query
 *  Класс кастомных запросов модели [[Poll]]
 */
class PollsQuery extends ActiveQuery
{
    /**
     * Выбираем только опубликованые опросы.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function active()
    {
        $this->andWhere(Polls::tableName() . '.status = :status', [
            ':status' => PollStatusHelper::STATUS_ACTIVE
        ]);
        return $this;
    }

    /**
     * Выбираем только опубликованые опросы.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function visible()
    {
        $this->andWhere(Polls::tableName() . '.visible = :visible', [
            ':visible' => PollStatusHelper::STATUS_ACTIVE
        ]);
        return $this;
    }

    /**
     * Выбираем только неопубликованые опросы.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function notActive()
    {
        $this->andWhere(Polls::tableName() . '.status = :status', [
            ':status' => PollStatusHelper::STATUS_NOT_ACTIVE
        ]);
        return $this;
    }

    /**
     * @param int $districtId
     *
     * @return $this
     */
    public function withDistrict(int $districtId)
    {
        $this->andWhere(Polls::tableName() . '.area_id = :area_id', [
            ':area_id' => $districtId
        ]);
        return $this;
    }

    /**
     * @param string $status
     *
     * @return $this
     */
    public function byStatusSoglas(string $status)
    {
        $this->andWhere([
            Polls::tableName(). '.status_soglas'    => $status
        ]);

        return $this;
    }

    /**
     * @return $this
     */
    public function correctDate()
    {
        $this->andWhere(Polls::tableName() . '.date_start <= NOW()::date');
        $this->andWhere(Polls::tableName() . '.date_end >= NOW()::date');

        return $this;
    }

    /**
     * @return $this
     */
    public function ended()
    {
        $this->andWhere(Polls::tableName() . '.date_end < NOW()::date');

        return $this;
    }

    /**
     * @return $this
     */
    public function started()
    {
        $this->andWhere(Polls::tableName() . '.date_start <= NOW()::date');

        return $this;
    }

    /**
     * @return $this
     */
    public function published()
    {
        $this
            ->byStatusSoglas(PollStatusHelper::STATUS_SOGLAS_FINISHED)
            ->active();

        return $this;
    }
}
