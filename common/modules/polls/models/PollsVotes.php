<?php

namespace common\modules\polls\models;

use common\components\behaviors\ChangeLog;
use common\modules\polls\models\base\PollsVotesBase;
use common\modules\polls\traits\ModuleTrait;
use vova07\fileapi\behaviors\UploadBehavior;

/**
 * Class PollsVotes
 *
 * @property integer countVoted
 * @package common\modules\polls\models
 */
class PollsVotes extends PollsVotesBase
{
    use ModuleTrait;

    /**
     * @var string
     */
    const MODULE_NAME = 'Варианты';
    /**
     * @var string
     */
    const MODULE_VIEW_PATH = '/polls/votes/view';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'uploadBehavior' => [
                'class'      => UploadBehavior::className(),
                'attributes' => [
                    'image' => [
                        'path'     => $this->module->photoPath,
                        'tempPath' => $this->module->photoTempPath,
                        'url'      => $this->module->photoUrl
                    ],
                ]
            ],
            'ChangeLog'      => [
                'class' => ChangeLog::class
            ]
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'id'      => 'Номер',
            'poll_id' => 'Опрос',
            'poll'    => 'Опрос',
            'image'   => 'Изображение',
            'name'    => 'Название',
            'visible' => 'Видимость',
            'created' => 'Дата создания',
            'updated' => 'Дата обновления'
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new PollsVotesQuery(static::class);
    }

    /**
     * @return integer
     */
    public function getCountVoted(): int
    {
        return UserVotes::find()->active()->byPollVote($this->id)->count();
    }
}
