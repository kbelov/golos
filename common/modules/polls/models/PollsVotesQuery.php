<?php
namespace common\modules\polls\models;

use common\modules\polls\helpers\PollStatusHelper;
use yii\db\ActiveQuery;

/**
 * Class PollVotesQuery
 * @package common\modules\polls\models\query
 *  Класс кастомных запросов модели [[PollsVotes]]
 */
class PollsVotesQuery extends ActiveQuery
{
    /**
     * Выбираем только опубликованые опросы.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function active()
    {
        $this->andWhere(PollsVotes::tableName() . '.visible = :status', [
            ':status' => PollStatusHelper::STATUS_ACTIVE
        ]);
        return $this;
    }

    /**
     * Выбираем только неопубликованые опросы.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function notActive()
    {
        $this->andWhere(PollsVotes::tableName() . '.visible = :status', [
            ':status' => PollStatusHelper::STATUS_NOT_ACTIVE
        ]);
        return $this;
    }

    /**
     * @param int $poll_id
     *
     * @return $this
     */
    public function withPoll(int $poll_id)
    {
        $this->andWhere(PollsVotes::tableName() . '.poll_id = :poll_id', [
            ':poll_id' => $poll_id
        ]);
        return $this;
    }
}
