<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 02.10.17
 * Time: 18:05
 */

namespace common\modules\polls\models;

use common\modules\polls\models\base\UserVotesBase;

/**
 * Class UserVotes
 *
 * @package common\modules\polls\models
 */
class UserVotes extends UserVotesBase
{
    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new UserVotesQuery(static::class);
    }
}
