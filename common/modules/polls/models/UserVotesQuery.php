<?php
namespace common\modules\polls\models;

use common\modules\polls\helpers\PollStatusHelper;
use yii\db\ActiveQuery;

/**
 * Class UserVotesQuery
 * @package common\modules\polls\models\query
 *  Класс кастомных запросов модели [[PollsVotes]]
 */
class UserVotesQuery extends ActiveQuery
{
    /**
     * Выбираем только опубликованые результаты.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function active()
    {
        $this->andWhere(UserVotes::tableName() . '.visible = :status', [
            ':status' => PollStatusHelper::STATUS_ACTIVE
        ]);
        return $this;
    }

    /**
     * Выбираем только неопубликованые результаты.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function notActive()
    {
        $this->andWhere(UserVotes::tableName() . '.visible = :status', [
            ':status' => PollStatusHelper::STATUS_NOT_ACTIVE
        ]);
        return $this;
    }

    /**
     * @param int $user_id
     *
     * @return $this
     */
    public function byUser(int $user_id)
    {
        $this->andWhere(UserVotes::tableName() . '.user_id = :user_id', [
            ':user_id' => $user_id
        ]);
        return $this;
    }

    /**
     * @param int $poll_id
     *
     * @return $this
     */
    public function byPoll(int $poll_id)
    {
        $this->andWhere(UserVotes::tableName() . '.poll_id = :poll_id', [
            ':poll_id' => $poll_id
        ]);
        return $this;
    }

    /**
     * @param int $poll_vote_id
     *
     * @return $this
     */
    public function byPollVote(int $poll_vote_id)
    {
        $this->andWhere(UserVotes::tableName() . '.polls_vote_id = :poll_vote_id', [
            ':poll_vote_id' => $poll_vote_id
        ]);
        return $this;
    }

    public function since(string $date)
    {
        $dateFormatted = date(\DateTime::ISO8601, strtotime($date));
        return $this->andWhere([
            'or',
            ['>=', 'created', $dateFormatted],
            ['>=', 'updated', $dateFormatted]
        ]);
    }
}
