<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 29.09.17
 * Time: 12:23
 */

namespace common\modules\polls\models\base;

use common\components\ActiveRecord;

/**
 * Class PollsBase
 * @package common\modules\polls\models\base
 *
 * @property integer $id
 * @property string $name
 * @property string $name_inner
 * @property string $address
 * @property integer $area_id
 * @property string $main_description
 * @property string $short_description
 * @property string $description
 * @property string $complete_description
 * @property string $complete_decision
 * @property string $complete_result
 * @property integer $date_start
 * @property integer $date_end
 * @property integer $points
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $public_status
 * @property float $lat
 * @property float $lon
 * @property integer $building_id
 * @property integer $status_soglas
 * @property string $visibility
 * @property boolean $visible
 * @property integer $count_votes
 */
class PollsBase extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'polls';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'name',
                'name_inner',
                'area_id',
                'building_id',
                'lat',
                'lon',
                'address',
                'status',
                'date_start',
                'date_end',
                'points'
             ], 'required'],
            [['area_id', 'points', 'status', 'building_id', 'count_votes'], 'integer'],
            [['lat', 'lon'], 'double'],
            [['visible'], 'boolean'],
            [['main_description', 'short_description', 'description', 'complete_description', 'complete_decision', 'complete_result', 'status_soglas', 'visibility'], 'string'],
            [['date_start', 'date_end', 'created', 'updated'], 'safe'],
            [['name', 'name_inner', 'address'], 'string', 'max' => 255],
        ];
    }
}
