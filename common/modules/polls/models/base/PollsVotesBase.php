<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 02.10.17
 * Time: 13:04
 */

namespace common\modules\polls\models\base;

use common\components\ActiveRecord;
use common\modules\polls\models\Polls;

/**
 * Class PollsVotesBase
 *
 * @property integer $id
 * @property integer $poll_id
 * @property string $name
 * @property string $image
 * @property boolean $visible
 * @property string $created
 * @property string $updated
 * @property string $description
 * @property Polls $poll
 * @package common\modules\polls\models\base
 */
class PollsVotesBase extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'polls_votes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['poll_id', 'name'], 'required'],
            [['poll_id'], 'integer'],
            [['visible'], 'boolean'],
            [['created', 'updated', 'description'], 'safe'],
            [['name', 'image'], 'string', 'max' => 255],
            [['poll_id'], 'exist', 'skipOnError' => true, 'targetClass' => Polls::className(), 'targetAttribute' => ['poll_id' => 'id']],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoll()
    {
        return $this->hasOne(Polls::className(), ['id' => 'poll_id']);
    }
}
