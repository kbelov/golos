<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 02.10.17
 * Time: 18:03
 */

namespace common\modules\polls\models\base;

use common\components\ActiveRecord;
use common\models\User;
use common\modules\polls\models\Polls;
use common\modules\polls\models\PollsVotes;
use yii\db\ActiveQuery;

/**
 * Class UserVotesBase
 * @property integer $id
 * @property integer $poll_id
 * @property integer $polls_vote_id
 * @property integer $user_id
 * @property boolean $visible
 * @property integer $points
 * @property string $created
 * @property string $updated
 * @property Polls $poll
 * @property PollsVotes $pollsVote
 * @property User $user
 *
 * @package common\modules\polls\models\base
 */
class UserVotesBase extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'user_votes';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['poll_id', 'polls_vote_id', 'user_id', 'points'], 'required'],
            [['poll_id', 'polls_vote_id', 'user_id'], 'integer'],
            [['visible'], 'boolean'],
            [['created', 'updated'], 'safe'],
            [['poll_id'], 'exist', 'skipOnError' => true, 'targetClass' => Polls::className(), 'targetAttribute' => ['poll_id' => 'id']],
            [['polls_vote_id'], 'exist', 'skipOnError' => true, 'targetClass' => PollsVotes::className(), 'targetAttribute' => ['polls_vote_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoll(): ActiveQuery
    {
        return $this->hasOne(Polls::className(), ['id' => 'poll_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPollsVote(): ActiveQuery
    {
        return $this->hasOne(PollsVotes::className(), ['id' => 'polls_vote_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser(): ActiveQuery
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}