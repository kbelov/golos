<?php

namespace common\modules\polls\traits;

use common\modules\polls\Module;
use Yii;

/**
 * Class ModuleTrait
 * @package common\modules\polls\traits
 * Implements `getModule` method, to receive current module instance.
 *
 * @property Module module
 */
trait ModuleTrait
{
    /**
     * @var \common\modules\polls\Module|null Module instance
     */
    private $_module;

    /**
     * @return \common\modules\polls\Module|null Module instance
     */
    public function getModule()
    {
        if ($this->_module === null) {
            $this->_module = Yii::$app->getModule('polls');
        }
        return $this->_module;
    }
}
