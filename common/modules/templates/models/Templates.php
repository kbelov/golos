<?php

namespace common\modules\templates\models;

use common\components\behaviors\ChangeLog;
use common\modules\templates\models\base\TemplatesBase;

/**
 * Class Templates
 *
 * @package common\modules\templates\models
 */
class Templates extends TemplatesBase
{

    /**
     * @var string
     */
    const MODULE_NAME = 'Шаблоны тем';
    /**
     * @var string
     */
    const MODULE_VIEW_PATH = '/templates/default/view';

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new TemplatesQuery(static::class);
    }
    /**
     * @inheritdoc
     */
    public function behaviors(): array
    {
        return [
            'ChangeLog' => [
                'class' => ChangeLog::class
            ]
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            'id'      => 'Номер',
            'name'    => 'Название',
            'created' => 'Дата создания',
            'updated' => 'Дата обновления'
        ];
    }
}
