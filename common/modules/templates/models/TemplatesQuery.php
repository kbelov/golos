<?php
namespace common\modules\templates\models;

use common\modules\polls\helpers\PollStatusHelper;
use common\modules\templates\models\Templates;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * Class TemplatesQuery
 * @package common\modules\templates\models\query
 *  Класс кастомных запросов модели [[Templates]]
 */
class TemplatesQuery extends ActiveQuery
{
    /**
     * Выбираем только опубликованые опросы.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function active()
    {
        $this->andWhere(Templates::tableName() . '.visible = :visible', [
            ':visible' => true
        ]);
        return $this;
    }

    /**
     * Выбираем только неопубликованые опросы.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function notActive()
    {
        $this->andWhere(Templates::tableName() . '.visible = :visible', [
            ':visible' => false
        ]);
        return $this;
    }
}
