<?php

namespace common\modules\templates\models\base;

use common\components\ActiveRecord;

/**
 * This is the model class for table "feedback_templates".
 *
 * @property integer $id
 * @property string $name
 * @property string $created
 * @property string $updated
 * @property boolean $visible
 */
class TemplatesBase extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feedback_templates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['visible'], 'boolean'],
            [['created', 'updated'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

}