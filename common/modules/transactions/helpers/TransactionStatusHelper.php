<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 02.10.17
 * Time: 20:14
 */

namespace common\modules\transactions\helpers;

/**
 * Class TransactionStatusHelper
 *
 * @package common\modules\transactions\helpers
 */
class TransactionStatusHelper
{
    const STATUS_SENT = 'sent';
    const STATUS_RECEIVED = 'received';
}
