<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 02.10.17
 * Time: 20:00
 */

namespace common\modules\transactions\models;

use common\components\ActiveRecord;
use common\components\Html;
use common\modules\transactions\models\base\TransactionsBase;

/**
 * Class Transactions
 *
 * @property string fromName
 * @property string toName
 * @package common\modules\transactions\models
 */
class Transactions extends TransactionsBase
{
    /** @var ActiveRecord */
    protected $fromOne;
    /** @var ActiveRecord */
    protected $toOne;

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new TransactionsQuery(static::class);
    }

    /**
     * @return string|static
     */
    protected function getFromName(): string
    {
        /** @var ActiveRecord $model */
        $model = $this->from;
        $this->fromOne = $model::findOne($this->from_id);
        if (null === $this->fromOne) {
            return 'Цель не найдена';
        }
        return $this->fromOne;
    }

    /**
     * @return string
     */
    public function renderFromName(): string
    {
        $fromName = $this->fromName;
        if (null === $this->fromOne) {
            return Html::tag('span', $fromName, [
                'class' => 'text-danger'
            ]);
        }
        if (!method_exists($this->fromOne, 'getUrl')) {
            return $fromName;
        }
        return Html::a($fromName, $this->fromOne->getUrl());
    }

    /**
     * @return string|static
     */
    protected function getToName(): string
    {
        /** @var ActiveRecord $model */
        $model = $this->to;
        $this->toOne = $model::findOne($this->to_id);
        if (null === $this->toOne) {
            return 'Не найдено';
        }
        return $this->toOne;
    }

    /**
     * @return string
     */
    public function renderToName(): string
    {
        $toName = $this->toName;
        if (null === $this->toOne) {
            return Html::tag('span', $toName, [
                'class' => 'text-danger'
            ]);
        }
        if (!method_exists($this->toOne, 'getUrl')) {
            return $toName;
        }
        return Html::a($toName, $this->toOne->getUrl());
    }
}
