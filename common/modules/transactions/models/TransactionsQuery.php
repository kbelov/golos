<?php
namespace common\modules\transactions\models;

use yii\db\ActiveQuery;

/**
 * Class TransactionsQuery
 * @package common\modules\transactions\models\query
 *  Класс кастомных запросов модели [[Transactions]]
 */
class TransactionsQuery extends ActiveQuery
{

}
