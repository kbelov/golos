<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 02.10.17
 * Time: 19:59
 */

namespace common\modules\transactions\models\base;

use common\components\ActiveRecord;

/**
 * Class TransactionsBase
 * @property integer $id
 * @property string $from
 * @property integer $from_id
 * @property string $to
 * @property integer $to_id
 * @property integer $points
 * @property string $status
 * @property string $created
 * @property string $updated
 *
 * @package common\modules\transactions\models\base
 */
class TransactionsBase extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'transactions';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['from', 'from_id', 'to', 'to_id', 'points', 'status'], 'required'],
            [['from_id', 'to_id', 'points'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['from', 'to'], 'string', 'max' => 255],
        ];
    }
}
