<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 19.09.17
 * Time: 11:20
 */

namespace common\modules\transactions\services;

use common\interfaces\Service;
use common\models\User;
use common\modules\distribution\services\DispatchAddPointsEmail;
use common\modules\transactions\helpers\TransactionStatusHelper;
use common\modules\transactions\models\Transactions;
use common\services\UpdateUserPoint;
use Yii;
use yii\base\Exception;

/**
 * Class AddNewTransaction
 *
 * @package common\modules\transactions\services
 */
class AddNewTransaction implements Service
{
    /** @var self */
    protected static $instance;

    /** @var string */
    protected $from;
    /** @var integer */
    protected $from_id;
    /** @var string */
    protected $to;
    /** @var integer */
    protected $to_id;
    /** @var integer */
    protected $points;
    /** @var boolean */
    protected $update_points = true;

    /**
     * @return static
     * @throws \yii\base\Exception
     */
    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    public function run(): bool
    {
        if (null === $this->from) {
            throw new Exception('Не заполнены from');
        }
        if (null === $this->from_id) {
            throw new Exception('Не заполнены from_id');
        }
        if (null === $this->to) {
            throw new Exception('Не заполнены to');
        }
        if (null === $this->to_id) {
            throw new Exception('Не заполнены to_id');
        }
        if (null === $this->points) {
            throw new Exception('Не заполнены points');
        }

        $transaction = \Yii::$app->getDb()->beginTransaction();
        try {
            $transModel = new Transactions([
                'from'      => $this->from,
                'from_id'   => $this->from_id,
                'to'        => $this->to,
                'to_id'     => $this->to_id,
                'points'    => $this->points
            ]);
            $transModel->status = TransactionStatusHelper::STATUS_SENT;
            $transModel->save();

            $transModelReceived = new Transactions([
                'from'      => $this->from,
                'from_id'   => $this->from_id,
                'to'        => $this->to,
                'to_id'     => $this->to_id,
                'points'    => $this->points
            ]);
            $transModelReceived->status = TransactionStatusHelper::STATUS_RECEIVED;
            $transModelReceived->save();

            if (!$transModel->hasErrors() && !$transModelReceived->hasErrors()) {
                $transaction->commit();
            } else {
                $transaction->rollBack();
                throw new Exception('Transaction errors: ' . count($transModel->getFirstErrors()) . ' - '. count($transModelReceived->getFirstErrors()));
            }
        } catch (Exception $e) {
            return false;
        }

        if ($this->update_points) {
            UpdateUserPoint::getInstance()->run();
            DispatchAddPointsEmail::getInstance()->setTransactionModel($transModelReceived)->run();
        }

        return true;
    }

    /**
     * @param string $from
     *
     * @return $this
     */
    public function setFrom(string $from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * @param int $from_id
     *
     * @return $this
     */
    public function setFromId(int $from_id)
    {
        $this->from_id = $from_id;

        return $this;
    }

    /**
     * @param string $to
     *
     * @return $this
     */
    public function setTo(string $to)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * @param int $to_id
     *
     * @return $this
     */
    public function setToId(int $to_id)
    {
        $this->to_id = $to_id;

        return $this;
    }

    /**
     * @param int $points
     *
     * @return $this
     */
    public function setPoints(int $points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * @param bool $update_points
     * @return $this
     */
    public function setUpdatePoints(bool $update_points)
    {
        $this->update_points = $update_points;

        return $this;
    }
}
