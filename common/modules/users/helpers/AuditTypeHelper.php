<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 07.10.2017
 * Time: 17:38
 */

namespace common\modules\users\helpers;

/**
 * Class AuditTypeHelper
 * @package common\modules\users\helpers
 */
class AuditTypeHelper
{
    const TYPE_VIEW = 'view';
    const TYPE_UPDATE = 'update';
    const TYPE_DELETED = 'deleted';

    const TYPES_ARRAY = [
        self::TYPE_VIEW => 'Просмотр',
        self::TYPE_UPDATE => 'Обновление',
        self::TYPE_DELETED => 'Удален'
    ];
}
