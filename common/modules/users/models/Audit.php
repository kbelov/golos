<?php

namespace common\modules\users\models;

use common\components\ActiveRecord;
use common\modules\users\helpers\AuditTypeHelper;
use common\modules\users\models\base\AuditBase;

/**
 * Class Audit
 * @property ActiveRecord owner
 *
 * @package common\modules\users\models
 */
class Audit extends AuditBase
{
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Номер',
            'owner_class' => 'Owner Class',
            'owner_id' => 'Owner ID',
            'owner' => 'Ресурс',
            'owner_module'  => 'Ресурс',
            'owner_name' => 'Наименование',
            'name'  => 'Наименование',
            'history_id' => 'Изменение',
            'type' => 'Тип',
            'typeName'  => 'Тип',
            'type_name' => 'Тип',
            'userToris' => 'Пользователь',
            'fio'   => 'Пользователь',
            'created' => 'Дата создания',
            'updated' => 'Дата обновления',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        /** @var ActiveRecord $class */
        $class = $this->owner_class;

        return $class::find()->andWhere(['id' => $this->owner_id]);
    }

    /**
     * @return string
     */
    public function getTypeName(): string
    {
        return AuditTypeHelper::TYPES_ARRAY[$this->type] ?? '(Не задано)';
    }

    /**
     * @return null|string
     */
    public function getModuleName()
    {
        return (defined($this->owner_class . '::MODULE_NAME')) ? $this->owner_class::MODULE_NAME : null;
    }

    /**
     * @return null|string
     */
    public function getUrlView()
    {
        return (defined($this->owner_class . '::MODULE_VIEW_PATH')) ? $this->owner_class::MODULE_VIEW_PATH : null;
    }

    public static function getAllTypes(): array
    {
        /** @var self[] $types */
        $types = Audit::find()->select('DISTINCT(type), owner_id')->all();
        $arrayTypes = [];
        foreach ($types as $type) {
            $arrayTypes[$type->type] = $type->getTypeName();
        }
        return $arrayTypes;
    }

    /**
     * @return array
     */
    public static function getAllOwners(): array
    {
        /** @var self[] $owner_classes */
        $owner_classes = Audit::find()->select('DISTINCT(owner_class), owner_id')->all();
        $tblJoined = [];
        $arrayOwners = [];
        foreach ($owner_classes as $class) {
            /** @var ActiveRecord $classModel */
            if (!class_exists($class->owner_class)) {
                continue;
            }
            $classModel = $class->owner_class;
            $tblName = $classModel::tableName();
            if (in_array($tblName, $tblJoined)) {
                continue;
            }
            $tblJoined[] = $tblName;
            $moduleName = $class->getModuleName();
            if (!empty($moduleName)) {
                $arrayOwners[$class->owner_class] = $class->getModuleName();
            }
        }
        return $arrayOwners;
    }
}