<?php

namespace common\modules\users\models\base;

use backend\modules\toris\models\UserToris;
use common\components\ActiveRecord;
use common\models\User;

/**
 * This is the model class for table "audit".
 *
 * @property integer $id
 * @property string $owner_class
 * @property integer $owner_id
 * @property integer $history_id
 * @property string $type
 * @property integer $user_id
 * @property integer $user_toris_id
 * @property string $created
 * @property string $updated
 *
 * @property User $user
 * @property User $userToris
 */
class AuditBase extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'audit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner_class', 'owner_id', 'type'], 'required'],
            [['owner_id', 'history_id', 'user_id', 'user_toris_id'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['owner_class', 'type'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['user_toris_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserToris::className(), 'targetAttribute' => ['user_toris_id' => 'id']],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserToris()
    {
        return $this->hasOne(UserToris::className(), ['id' => 'user_toris_id']);
    }
}