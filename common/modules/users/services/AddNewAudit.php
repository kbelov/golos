<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 07.10.2017
 * Time: 16:36
 */

namespace common\modules\users\services;


use backend\modules\toris\models\UserToris;
use common\interfaces\Service;
use common\models\History;
use common\models\User;
use common\modules\users\models\Audit;
use yii\base\Exception;

/**
 * Class AddNewAudit
 * @package common\modules\users\services
 */
class AddNewAudit implements Service
{

    /** @var string */
    protected $owner_class;
    /** @var string */
    protected $owner_id;
    /** @var string */
    protected $type;
    /** @var User */
    protected $user;
    /** @var UserToris */
    protected $user_toris;
    /** @var History */
    protected $history;

    /**
     * @return static
     * @throws \yii\base\Exception
     */
    public static function getInstance()
    {
        return new self();
    }

    /**
     * @throws Exception
     */
    public function run()
    {
        if (null === $this->owner_class) {
            throw new Exception('Не заполнены owner_class');
        }
        if (null === $this->owner_id) {
            throw new Exception('Не заполнены owner_id');
        }
        if (null === $this->type) {
            throw new Exception('Не заполнены type');
        }
        $transaction = \Yii::$app->getDb()->beginTransaction();
        try {
            $auditModel = new Audit([
                'owner_class' => $this->owner_class,
                'owner_id' => $this->owner_id,
                'type' => $this->type
            ]);
            if (null !== $this->user) {
                $auditModel->user_id = $this->user->id;
            }
            if (null !== $this->user_toris) {
                $auditModel->user_toris_id = $this->user_toris->id;
            }
            if (null !== $this->history) {
                $auditModel->history_id = $this->history->id;
            }
            $auditModel->save();

            if (!$auditModel->hasErrors()) {
                $transaction->commit();
            } else {
                $transaction->rollBack();
                throw new Exception('Transaction errors: ' . count($auditModel->getFirstErrors()));
            }
        } catch (Exception $e) {
            throw new Exception('Transaction error: ' . $e->getMessage());
        }
    }

    /**
     * @param string $owner_class
     * @return AddNewAudit
     */
    public function setOwnerClass(string $owner_class): AddNewAudit
    {
        $this->owner_class = $owner_class;
        return $this;
    }

    /**
     * @param string $owner_id
     * @return AddNewAudit
     */
    public function setOwnerId(string $owner_id): AddNewAudit
    {
        $this->owner_id = $owner_id;
        return $this;
    }

    /**
     * @param string $type
     * @return AddNewAudit
     */
    public function setType(string $type): AddNewAudit
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @param User $user
     * @return AddNewAudit
     */
    public function setUser(User $user): AddNewAudit
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @param UserToris $user_toris
     * @return AddNewAudit
     */
    public function setUserToris(UserToris $user_toris): AddNewAudit
    {
        $this->user_toris = $user_toris;
        return $this;
    }

    /**
     * @param History $history
     * @return AddNewAudit
     */
    public function setHistory(History $history): AddNewAudit
    {
        $this->history = $history;
        return $this;
    }
}