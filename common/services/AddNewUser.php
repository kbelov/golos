<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 04.10.2017
 * Time: 0:03
 */

namespace common\services;

use common\esia\helpers\EsiaHelper;
use common\esia\OpenId;
use common\interfaces\Service;
use common\models\EsiaUser;
use common\models\User;
use common\modules\transactions\services\AddNewTransaction;
use yii\base\Exception;
use yii\web\BadRequestHttpException;
use yii\web\ServerErrorHttpException;

/**
 * Class AddNewUser
 * @package common\services
 */
class AddNewUser implements Service
{
    /** @var self */
    protected static $instance;

    /** @var string */
    protected $code;

    /**
     * @return static
     * @throws \yii\base\Exception
     */
    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @return mixed
     * @throws \yii\web\ServerErrorHttpException
     * @throws \esia\exceptions\RequestFailException
     * @throws \yii\base\InvalidParamException
     * @throws \yii\web\BadRequestHttpException
     * @throws \esia\exceptions\SignFailException
     * @throws \Exception
     * @throws Exception
     */
    public function run()
    {
        if (null === $this->code) {
            throw new Exception('Не заполнены code');
        }

        $esia = new OpenId(EsiaHelper::buildConfig());
        $esia->getToken($this->code);
        if (empty($esia)) {
            throw new BadRequestHttpException();
        }

        $personInfo = $esia->getPersonInfo();
        if (null === $personInfo) {
            throw new BadRequestHttpException();
        }
        $contactInfo = $esia->getContactInfo();
        if (null === $contactInfo) {
            throw new BadRequestHttpException();
        }

        $points = 0;
        $points += (!empty($personInfo->firstName)) ? User::POINTS_FIRST_NAME : 0;
        $points += (!empty($personInfo->lastName)) ? User::POINTS_LAST_NAME : 0;
        $points += (!empty($personInfo->middleName)) ? User::POINTS_MIDDLE_NAME : 0;

        if (empty($personInfo->eTag)) {
            throw new BadRequestHttpException();
        }

        $user = User::findByEsiaCode($esia->oid);

        if (empty($user)) {
            $user = new User();
        }
        $user->esia_code = $esia->oid;

        if (!empty($personInfo->firstName)) {
            $user->first_name = $personInfo->firstName;
        }
        if (!empty($personInfo->lastName)) {
            $user->last_name = $personInfo->lastName;
        }
        if (!empty($personInfo->middleName)) {
            $user->middle_name = $personInfo->middleName;
        }
        if (!empty($personInfo->snils)) {
            $user->snils = $personInfo->snils;
        }
        if (!empty($personInfo->gender)) {
            $user->gender = $personInfo->gender;
        }
        if (!empty($personInfo->trusted)) {
            $user->trusted = $personInfo->trusted;
        }

        if (!empty($personInfo->rIdDoc)) {
            $docInfo = $esia->getDocInfo($personInfo->rIdDoc);
            if (null !== $docInfo) {
                if ($docInfo->type = EsiaHelper::TYPE_DOC_PASSPORT) {
                    $user->passport = $docInfo->series.' '.$docInfo->number;
                    $user->passport_date = $docInfo->issueDate;
                    $user->passport_info = $docInfo->issuedBy;
                    $user->passport_code = $docInfo->issueId;
                }
            }
        }
        foreach ($contactInfo as $info) {
            switch ($info->type) {
                case EsiaHelper::PROFILE_PARAM_EMAIL:
                    $user->email = $info->value;
                    $points += User::POINTS_EMAIL;
                    break;
                case EsiaHelper::PROFILE_PARAM_MOBILE:
                    $user->phone = $info->value;
                    $points += User::POINTS_PHONE;
                    break;
            }
        }
        $isNewRecord = $user->getIsNewRecord();
        if ($user->save()) {
            if ($isNewRecord) {
                AddNewTransaction::getInstance()
                    ->setFrom(EsiaUser::class)
                    ->setFromId($user->id)
                    ->setTo(User::class)
                    ->setToId($user->id)
                    ->setPoints($points)
                    ->setUpdatePoints(false)
                    ->run();
            }

            return $user;
        }

        throw new ServerErrorHttpException();
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCode(string $code)
    {
        $this->code = $code;

        return $this;
    }
}