<?php
/**
 * Created by PhpStorm.
 * User: filatov
 * Date: 16.10.2017
 * Time: 15:19
 */

namespace common\services;

use common\components\EventCatcher;
use common\interfaces\Service;
use common\models\User;
use common\modules\distribution\models\Distribution;
use common\modules\distribution\models\UserDistribution;
use yii\base\Event;
use yii\db\ActiveQuery;

class Dispatch extends EventCatcher implements Service
{
    /** @var self */
    protected static $instance;

    /** @var string Уникальный код элемента рассылки, для которого будет отправлено сообщение */
    protected $dispatchCode;

    /**
     * @param Event $event
     */
    public static function eventCatcher(Event $event)
    {
    }

    /**
     * @return string sms сообщение
     */
    protected function getMsg(): string
    {
        return '';
    }

    /**
     * @return static
     */
    public static function getInstance()
    {
        if (!static::$instance) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    /**
     * @return mixed
     */
    public function run()
    {
        return true;
    }

    /**
     * @return User[]
     */
    protected function usersToSend()
    {
        $usersToSend = User::find()->from(['u' => User::tableName()])
            ->innerJoin(['ud' => UserDistribution::tableName()], 'ud.user_id = u.id')
            ->innerJoin(['d' => Distribution::tableName()], 'd.id = ud.distribution_id')
            ->where(['d.code' => $this->dispatchCode])->all();
        return $usersToSend;
    }

    /**
     * Находится ли пользователь в рассылке по коду
     * @param $userId
     * @return bool
     */
    protected function inDispatch($userId): bool
    {
        if ($this->dispatchQuery()->andWhere(['u.id' => $userId])->one()) {
            return true;
        }
        return false;
    }

    /**
     * @return ActiveQuery
     */
    protected function dispatchQuery()
    {
        return User::find()->from(['u' => User::tableName()])
            ->innerJoin(['ud' => UserDistribution::tableName()], 'ud.user_id = u.id')
            ->innerJoin(['d' => Distribution::tableName()], 'd.id = ud.distribution_id')
            ->where(['d.code' => $this->dispatchCode]);
    }
}
