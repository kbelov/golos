<?php
/**
 * Created by PhpStorm.
 * User: filatov
 * Date: 16.10.2017
 * Time: 15:25
 */

namespace common\services;

use common\models\User;
use Yii;

class DispatchEmail extends Dispatch
{
    /**
     * @return mixed
     */
    public function run()
    {
        foreach ($this->usersToSend() as $user) {
            $this->sendEmail($user);
        }
        return true;
    }

    protected function sendEmail(User $user)
    {
        $mail = Yii::$app->getMailer();
        return $mail
            ->compose()
            ->setTo($user->email)
            ->setFrom(Yii::$app->params['supportEmail'])
            ->setSubject('Новое сообщение')
            ->setTextBody($this->getMsg())
            ->send();
    }
}
