<?php
/**
 * Created by PhpStorm.
 * User: filatov
 * Date: 13.10.2017
 * Time: 9:51
 */

namespace common\services;

use common\models\User;
use Yii;

class DispatchSms extends Dispatch
{
    /**
     * @return mixed
     */
    public function run()
    {
        foreach ($this->usersToSend() as $user) {
            $this->sendSms($user);
        }
        return true;
    }

    public function sendSms(User $user)
    {
        if (YII_ENV == 'dev') {
            return [
                'sendStatus'    => true
            ];
        }
        $post = [
            'phoneNumber'   => $user->phone,
            "template"      => "NashSPbTemplate",
            "data"          => [
                "textmsg"   => $this->getMsg(),
            ],
//            "email"         => []
        ];
        $post = json_encode($post);
        $url = Yii::$app->params['sms_url'];

        $curl = curl_init();
        if ($curl === false) {
            return false;
        }
        $options = [
            CURLOPT_URL             => $url,
            CURLOPT_SSL_VERIFYPEER  => false,
            CURLOPT_FOLLOWLOCATION  => false,
            CURLOPT_TIMEOUT         => 500,
            CURLOPT_POST            => true,
            CURLOPT_POSTFIELDS => $post,
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($post)
            ]
        ];
        $proxySettings = Yii::$app->params['proxySettings'];
        if (!empty($proxySettings)) {
            $options[CURLOPT_PROXY] = $proxySettings['host'];
            $options[CURLOPT_PROXYUSERPWD] = $proxySettings['logpass'];
        }

        Yii::info(__METHOD__, 'sms');
        Yii::info($options, 'sms');
        curl_setopt_array($curl, $options);

        $result = curl_exec($curl);
        if (!$result) {
            Yii::error(curl_error($curl), 'sms');
        }

        return json_decode($result);
    }
}
