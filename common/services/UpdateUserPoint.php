<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 02.10.2017
 * Time: 22:22
 */

namespace common\services;

use common\interfaces\Service;
use common\models\User;
use common\modules\transactions\helpers\TransactionStatusHelper;
use common\modules\transactions\models\Transactions;
use Yii;
use yii\base\Exception;
use yii\db\Query;
use yii\web\ForbiddenHttpException;

/**
 * Class UpdateUserPoint
 * @package common\services
 */
class UpdateUserPoint implements Service
{
    /** @var self */
    protected static $instance;

    /** @var User */
    protected $model;

    /**
     * @return static
     * @throws \yii\base\Exception
     */
    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
            self::$instance->model = User::findIdentity(Yii::$app->getUser()->getId());
        }

        return self::$instance;
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function run()
    {
        if (null === $this->model) {
            throw new Exception('Не заполнены model');
        }

        $transaction = \Yii::$app->getDb()->beginTransaction();
        try {
            $this->model->points = $this->calcPoints();
            $this->model->save();

            if (!$this->model->hasErrors()) {
                $transaction->commit();
            } else {
                $transaction->rollBack();
                throw new Exception('Transaction errors: ' . count($this->model->getFirstErrors()));
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @return int
     */
    protected function calcPoints(): int
    {
        $query = (new Query())
            ->from(['trans' => Transactions::tableName()])
            ->where([
                'trans.status'    => TransactionStatusHelper::STATUS_RECEIVED,
                'trans.to'        => User::class,
                'trans.to_id'     => $this->model->id
            ]);
        $receivedSum = $query->sum('trans.points');
        $query = (new Query())
            ->from(['trans' => Transactions::tableName()])
            ->where([
                'trans.status'    => TransactionStatusHelper::STATUS_SENT,
                'trans.from'      => User::class,
                'trans.from_id'   => $this->model->id
            ]);
        $sentSum = $query->sum('trans.points');

        return $receivedSum - $sentSum;
    }

    /**
     * @param User $model
     * @return $this
     */
    public function setModel(User $model)
    {
        $this->model = $model;

        return $this;
    }
}