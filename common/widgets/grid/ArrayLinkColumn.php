<?php
/**
 * Created by PhpStorm.
 * User: andkon
 * Date: 04.05.17
 * Time: 17:39
 */

namespace common\widgets\grid;

use common\components\ActiveRecord;
use common\components\Html;
use yii\grid\DataColumn;

/**
 * Class ArrayLinkColumn
 *
 * @package common\widgets\grid
 */
class ArrayLinkColumn extends DataColumn
{
    public $url = '#';
    public $separator = ', ';

    /**
     * @inheritdoc
     *
     * @param ActiveRecord $model
     * @param int          $key
     * @param int          $index
     *
     * @return string
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        $content = [];
        foreach ($model->{$this->attribute} as $relationModel) {
            /** @var ActiveRecord $relationModel */
            $content[] = Html::a(
                (string)$relationModel,
                \Yii::$app->getUrlManager()->createUrl([$this->url, 'id' => $relationModel->getPrimaryKey()])
            );
        }

        return implode($this->separator, $content);
    }
}
