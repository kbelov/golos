<?php
/**
 * Created by PhpStorm.
 * User: andkon
 * Date: 02.10.14
 * Time: 19:26
 */

namespace common\widgets\grid;

use yii\db\ActiveRecord;
use yii\grid\DataColumn;
use yii\helpers\Html;

/**
 * Class LinkColumn
 *
 * @package app\common\helpers
 */
class LinkColumn extends DataColumn
{
    /** @var string|array */
    public $url = '#';
    public $externalUrl = false;
    public $title = '';

    public function init()
    {
        parent::init();
        $this->options = array_merge(['class' => 'linkColumn', 'title' => $this->title], $this->options);
    }

    /**
     * @inheritdoc
     *
     * @param ActiveRecord|mixed $model
     * @param mixed              $key
     * @param int                $index
     *
     * @return string
     */
    protected function renderDataCellContent($model, $key, $index): string
    {
        $content = parent::renderDataCellContent($model, $key, $index);
        if (!$this->externalUrl) {
            $url = '#';
            if ($model instanceof ActiveRecord && is_array($this->url)) {
                $url = $this->url;
                foreach ($url as $urlKey => $attrib_name) {
                    if (!is_int($urlKey)) {
                        $url[$urlKey] = $model->$attrib_name;
                    }
                }

                if (empty($url['id'])) {
                    $url['id'] = $model->id;
                }
            }

            $url = ($url === '#') ? '#' : \Yii::$app->getUrlManager()->createUrl($url);
        } else {
            $url                     = 'http://' . $model->{$this->externalUrl};
            $this->options['target'] = '_blank';
        }

        return Html::a($content, $url, $this->options);
    }
}
