<?php
/**
 * Created by PhpStorm.
 * User: andkon
 * Date: 03.05.17
 * Time: 16:54
 */

namespace common\widgets\grid;

use common\components\Html;
use yii\grid\Column;

/**
 * Class MoveColumn
 *
 * @package common\widgets\grid
 */
class MoveColumn extends Column
{
    public $tag = 'i';

    public $serialColumnSelector = '.serial';

    /** @inheritdoc */
    public function renderDataCellContent($model, $key, $index)
    {
        $this->options = array_merge(['class' => ['glyphicon', 'glyphicon-move']], $this->options);

        return Html::tag($this->tag, '', $this->options);
    }

    /** @inheritdoc */
    public function renderHeaderCell()
    {
        $this->headerOptions = array_merge(['style' => 'width:50px; '], $this->headerOptions);

        return parent::renderHeaderCell();
    }

    public function init()
    {
        parent::init();
        \Yii::$app->getView()->registerJs(<<<JS
  var firstIndex = parseInt($('#{$this->grid->getId()}').find('tbody tr:first > {$this->serialColumnSelector}').html());
$('#{$this->grid->getId()} .sortable-grid-view tbody').sortable({
  stop: function( event, ui ) {
  let i = firstIndex;
    $(ui.item).closest('tbody').find('tr').each(function(){
        $(this).find('{$this->serialColumnSelector}').html(i);
        i++;
    });
  }
});

JS
        );
    }
}
