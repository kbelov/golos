<?php
/**
 * Created by PhpStorm.
 * User: andkon
 * Date: 04.04.17
 * Time: 11:13
 */

namespace common\widgets\grid;

use common\components\Html;
use yii\grid\DataColumn;

/**
 * Class TagColumn
 *
 * @package common\widgets\grid
 */
class TagColumn extends DataColumn
{
    public $delimiter = ' ';

    public $options = ['class' => ['label', 'label-info']];

    public $url;

    /**
     * @inheritdoc
     *
     * @param mixed $model
     * @param mixed $key
     * @param int   $index
     *
     * @return string|null
     */
    protected function renderDataCellContent($model, $key, $index): ?string
    {
        $data = $model->{$this->attribute};
        if (!is_array($data)) {
            return parent::renderDataCellContent($model, $key, $index);
        }

        return implode(
            $this->delimiter,
            array_map(function ($val) {
                if ($this->url) {
                    $id  = $this->url['id'];
                    $out = Html::a((string)$val, [$this->url[0], $id => $val->{$id}], $this->options);
                } else {
                    $out = Html::tag('span', (string)$val, $this->options);
                }

                return $out;
            }, $data)
        );
    }
}
