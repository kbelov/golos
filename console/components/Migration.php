<?php
/**
 * Created by PhpStorm.
 * Module: andkon
 * Date: 22.03.17
 * Time: 12:30
 */

namespace console\components;

use yii\db\ColumnSchemaBuilder;

/**
 * Class Migration
 *
 * @package console\components
 */
class Migration extends \yii\db\Migration
{
    protected $tableOptions = '';

    /**
     * @return \yii\db\ColumnSchemaBuilder
     * @throws \yii\base\NotSupportedException
     */
    protected function jsonb(): ColumnSchemaBuilder
    {
        return $this->getDb()->getSchema()->createColumnSchemaBuilder('jsonb');
    }

    /**
     * @param int $precision
     * @param int $scale
     *
     * @return \yii\db\ColumnSchemaBuilder
     */
    protected function price($precision = 12, $scale = 4)
    {
        return $this->decimal($precision, $scale);
    }
}
