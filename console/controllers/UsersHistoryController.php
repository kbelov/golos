<?php
/**
 * Created by PhpStorm.
 * User: filatov
 * Date: 17.10.2017
 * Time: 11:04
 */
namespace console\controllers;

use common\modules\distribution\services\DispatchPollHistory;
use yii\console\Controller;

class UsersHistoryController extends Controller
{
    public function actionIndex()
    {
         DispatchPollHistory::getInstance()
             ->run();
    }
}
