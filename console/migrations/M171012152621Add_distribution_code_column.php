<?php
use yii\db\Migration;
use common\modules\distribution\helpers\DispatchCodes;

class M171012152621Add_distribution_code_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('distribution', 'code', $this->string());
        $this->update(
            'distribution',
            ['code' => DispatchCodes::CODE_POLL_CHANGE_STATUS],
            ['name' => 'Изменения статуса голосования в которых я проголосовал']
        );
        $this->update(
            'distribution',
            ['code' => DispatchCodes::CODE_NEW_POLL_ADDED],
            ['name' => 'О публикации новых голосований']
        );
        $this->update(
            'distribution',
            ['code' => DispatchCodes::CODE_GET_POINTS],
            ['name' => 'О полученных баллах']
        );
        $this->update(
            'distribution',
            ['code' => DispatchCodes::CODE_POLL_HISTORY],
            ['name' => 'Об истории голосований (еженедельная подписка)']
        );
        $this->update(
            'distribution',
            ['code' => DispatchCodes::CODE_NEW_POLL_SMS],
            ['name' => 'О новых голосованиях']
        );
        $this->update(
            'distribution',
            ['code' => DispatchCodes::CODE_NEWS_SMS],
            ['name' => 'О новых новостях']
        );
    }

    public function safeDown()
    {
        $this->dropColumn('distribution', 'code');
    }
}
