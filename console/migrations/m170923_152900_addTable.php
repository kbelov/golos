<?php

use yii\db\Migration;

/**
 * Class m170923_152900_addTable
 */
class m170923_152900_addTable extends Migration
{
    /**
     *
     */
    public function safeUp()
    {
        $this->createTable('polls', [
            'id'                => $this->primaryKey(),
            'name'              => $this->string()->notNull()->comment('На главной'),
            'name_inner'        => $this->string()->notNull()->comment('На странице голосования'),
            'address'           => $this->string()->null(),
            'area_id'           => $this->integer()->notNull(),
            'main_description'  => $this->text()->null()->comment('Текст на главной'),
            'short_description' => $this->text()->null()->comment('Текст сокращенный'),
            'description'       => $this->text()->null()->comment('Текст полный'),
            'complete_description'  => $this->text()->null()->comment('Итоги голосования описание'),
            'complete_decision'     => $this->text()->null()->comment('Решение'),
            'complete_result'       => $this->text()->null()->comment('Результат'),
            'date_start'        => $this->date(),
            'date_end'          => $this->date(),
            'points'            => $this->smallInteger()->notNull()->defaultValue(0),
            'status'            => $this->integer()->notNull()->defaultValue(0),
            'created_at'        => $this->dateTime(),
            'updated_at'        => $this->dateTime()
        ]);
    }

    /**
     *
     */
    public function safeDown()
    {
        $this->dropTable('polls');
    }
}
