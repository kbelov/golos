<?php

use yii\db\Migration;

/**
 * Class m170929_085441_addNewsTable
 */
class m170929_085441_addNewsTable extends Migration
{
    /**
     *
     */
    public function safeUp()
    {
        $this->createTable('news', [
            'id'    => $this->primaryKey(),
            'name'  => $this->string()->notNull(),
            'content'   => $this->text()->null(),
            'preview'   => $this->text()->null(),
            'image'     => $this->string()->null(),
            'creator_id'    => $this->integer()->notNull(),
            'visible'       => $this->boolean()->defaultValue(true),
            'date'          => $this->date()->notNull(),
            'created'       => $this->dateTime(),
            'updated'       => $this->dateTime()
        ]);
    }

    /**
     */
    public function safeDown()
    {
        $this->dropTable('news');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170929_085441_addNewsTable cannot be reverted.\n";

        return false;
    }
    */
}
