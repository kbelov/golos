<?php

use yii\db\Migration;

class m171001_150354_addIsPiterColumnNews extends Migration
{
    public function safeUp()
    {
        $this->addColumn('news', 'is_piter', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('news', 'is_piter');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171001_150354_addIsPiterColumnNews cannot be reverted.\n";

        return false;
    }
    */
}
