<?php

use yii\db\Migration;

class m171001_163943_addGifts extends Migration
{
    public function safeUp()
    {
        $this->createTable('gifts', [
            'id'    => $this->primaryKey(),
            'name'  => $this->string()->notNull(),
            'points'    => $this->integer()->notNull(),
            'image'     => $this->string()->notNull(),
            'visible'   => $this->boolean()->defaultValue(true),
            'created'   => $this->dateTime(),
            'updated'   => $this->dateTime()
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('gifts');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171001_163943_addGifts cannot be reverted.\n";

        return false;
    }
    */
}
