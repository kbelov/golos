<?php

use yii\db\Migration;

class m171001_164250_fixCreatedUpdatedPolls extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('polls', 'created_at', 'created');
        $this->renameColumn('polls', 'updated_at', 'updated');
    }

    public function safeDown()
    {
        $this->renameColumn('polls', 'created', 'created_at');
        $this->renameColumn('polls', 'updated', 'updated_at');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171001_164250_fixCreatedUpdatedPolls cannot be reverted.\n";

        return false;
    }
    */
}
