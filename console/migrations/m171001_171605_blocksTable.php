<?php

use yii\db\Migration;

class m171001_171605_blocksTable extends Migration
{
    public function safeUp()
    {
        $this->createTable('blocks', [
            'id'    => $this->primaryKey(),
            'name'  => $this->string()->notNull(),
            'content'   => $this->string()->notNull(),
            'external_link' => $this->boolean()->defaultValue(false),
            'link'          => $this->text()->null(),
            'visible'       => $this->boolean()->defaultValue(true),
            'created'       => $this->dateTime(),
            'updated'       => $this->dateTime()
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('blocks');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171001_171605_blocksTable cannot be reverted.\n";

        return false;
    }
    */
}
