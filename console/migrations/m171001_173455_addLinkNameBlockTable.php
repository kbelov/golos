<?php

use yii\db\Migration;

class m171001_173455_addLinkNameBlockTable extends Migration
{
    public function safeUp()
    {
        $this->addColumn('blocks', 'link_name', $this->string()->null());
    }

    public function safeDown()
    {
        $this->dropColumn('blocks', 'link_name');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171001_173455_addLinkNameBlockTable cannot be reverted.\n";

        return false;
    }
    */
}
