<?php

use yii\db\Migration;

class m171001_182329_areasTable extends Migration
{
    public function safeUp()
    {
        $this->createTable('areas', [
            'id'    => $this->primaryKey(),
            'name'  => $this->string()->notNull(),
            'code'  => $this->string()->notNull(),
            'created'   => $this->dateTime(),
            'updated'   => $this->dateTime()
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('areas');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171001_182329_areasTable cannot be reverted.\n";

        return false;
    }
    */
}
