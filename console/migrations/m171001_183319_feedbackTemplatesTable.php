<?php

use yii\db\Migration;

class m171001_183319_feedbackTemplatesTable extends Migration
{
    public function safeUp()
    {
        $this->createTable('feedback_templates', [
            'id'        => $this->primaryKey(),
            'name'      => $this->string()->notNull(),
            'created'   => $this->dateTime(),
            'updated'   => $this->dateTime(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('feedback_templates');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171001_183319_feedbackTemplatesTable cannot be reverted.\n";

        return false;
    }
    */
}
