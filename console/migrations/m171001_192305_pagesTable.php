<?php

use yii\db\Migration;

class m171001_192305_pagesTable extends Migration
{
    public function safeUp()
    {
        $this->createTable('pages', [
            'id'    => $this->primaryKey(),
            'name'  => $this->string()->notNull(),
            'content'   => $this->text()->notNull(),
            'image'     => $this->string()->null(),
            'visible'   => $this->boolean()->defaultValue(true),
            'created'   => $this->dateTime(),
            'updated'   => $this->dateTime()
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('pages');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171001_192305_pagesTable cannot be reverted.\n";

        return false;
    }
    */
}
