<?php

use yii\db\Migration;

class m171002_095822_pollsVotesTable extends Migration
{
    public function safeUp()
    {
        $this->createTable('polls_votes', [
            'id'    => $this->primaryKey(),
            'poll_id'   => $this->integer()->notNull(),
            'name'      => $this->string()->notNull(),
            'image'     => $this->string()->null(),
            'visible'   => $this->boolean()->defaultValue(true),
            'created'   => $this->dateTime(),
            'updated'   => $this->dateTime()
        ]);
        $this->addForeignKey('polls_votes_poll_id_fk', 'polls_votes', ['poll_id'], 'polls', ['id']);
    }

    public function safeDown()
    {
        $this->dropForeignKey('polls_votes_poll_id_fk', 'polls_votes');
        $this->dropTable('polls_votes');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171002_095822_pollsVotesTable cannot be reverted.\n";

        return false;
    }
    */
}
