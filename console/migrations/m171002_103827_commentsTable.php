<?php

use yii\db\Migration;

class m171002_103827_commentsTable extends Migration
{
    public function safeUp()
    {
        $this->createTable('comments', [
            'id'    => $this->primaryKey(),
            'type'  => $this->string()->notNull(),
            'target_id' => $this->integer()->notNull(),
            'user_id'   => $this->integer()->notNull(),
            'content'   => $this->text()->notNull(),
            'visible'   => $this->boolean()->defaultValue(true),
            'created'   => $this->dateTime(),
            'updated'   => $this->dateTime()
        ]);
        $this->addForeignKey('comments_user_id_fk', 'comments', ['user_id'], 'user', ['id']);
    }

    public function safeDown()
    {
        $this->dropForeignKey('comments_user_id_fk', 'comments');
        $this->dropTable('comments');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171002_103827_commentsTable cannot be reverted.\n";

        return false;
    }
    */
}
