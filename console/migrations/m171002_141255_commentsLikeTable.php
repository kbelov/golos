<?php

use yii\db\Migration;

class m171002_141255_commentsLikeTable extends Migration
{
    public function safeUp()
    {
        $this->createTable('comments_likes', [
            'id'            => $this->primaryKey(),
            'user_id'       => $this->integer()->notNull(),
            'comment_id'    => $this->integer()->notNull(),
            'type'          => $this->string()->notNull(),
            'visible'       => $this->boolean()->defaultValue(true),
            'created'       => $this->dateTime(),
            'updated'       => $this->dateTime()
        ]);
        $this->addForeignKey('comments_likes_user_id_fk', 'comments_likes', ['user_id'], 'user', ['id']);
        $this->addForeignKey('comments_likes_comment_id_fk', 'comments_likes', ['comment_id'], 'comments', ['id']);
    }

    public function safeDown()
    {
        $this->dropForeignKey('comments_likes_user_id_fk', 'comments_likes');
        $this->dropForeignKey('comments_likes_comment_id_fk', 'comments_likes');
        $this->dropTable('comments_likes');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171002_141255_commentsLikeTable cannot be reverted.\n";

        return false;
    }
    */
}
