<?php

use yii\db\Migration;

class m171002_142812_publicStatusPolls extends Migration
{
    public function safeUp()
    {
        $this->addColumn(
            'polls',
            'public_status',
            $this->integer()->notNull()->defaultValue(0)
        );
    }

    public function safeDown()
    {
        $this->dropColumn('polls', 'public_status');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171002_142812_publicStatusPolls cannot be reverted.\n";

        return false;
    }
    */
}
