<?php

use yii\db\Migration;

class m171002_145809_userVotesTable extends Migration
{
    public function safeUp()
    {
        $this->createTable('user_votes', [
            'id'            => $this->primaryKey(),
            'poll_id'       => $this->integer()->notNull(),
            'polls_vote_id' => $this->integer()->notNull(),
            'user_id'       => $this->integer()->notNull(),
            'visible'       => $this->boolean()->defaultValue(true),
            'created'       => $this->dateTime(),
            'updated'       => $this->dateTime()
        ]);
        $this->addForeignKey('user_votes_user_id_fk', 'user_votes', ['user_id'], 'user', ['id']);
        $this->addForeignKey('user_votes_poll_id_fk', 'user_votes', ['poll_id'], 'polls', ['id']);
        $this->addForeignKey('user_votes_poll_vote_id_fk', 'user_votes', ['polls_vote_id'], 'polls_votes', ['id']);
    }

    public function safeDown()
    {
        $this->dropForeignKey('user_votes_user_id_fk', 'user_votes');
        $this->dropForeignKey('user_votes_poll_id_fk', 'user_votes');
        $this->dropForeignKey('user_votes_poll_vote_id_fk', 'user_votes');
        $this->dropTable('user_votes');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171002_145809_userVotesTable cannot be reverted.\n";

        return false;
    }
    */
}
