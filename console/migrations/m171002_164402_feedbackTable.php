<?php

use yii\db\Migration;

class m171002_164402_feedbackTable extends Migration
{
    public function safeUp()
    {
        $this->createTable('feedback', [
            'id'                    => $this->primaryKey(),
            'email'                 => $this->string()->notNull(),
            'feedback_template'     => $this->string()->notNull(),
            'content'               => $this->text()->notNull(),
            'created'               => $this->dateTime(),
            'updated'               => $this->dateTime()
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('feedback');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171002_164402_feedbackTable cannot be reverted.\n";

        return false;
    }
    */
}
