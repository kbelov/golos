<?php

use yii\db\Migration;

class m171002_164722_addPointsToVotes extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_votes', 'points', $this->integer()->notNull()->defaultValue(0));

        $this->createTable('transactions', [
            'id'    => $this->primaryKey(),
            'from'  => $this->string()->notNull(),
            'from_id'   => $this->integer()->notNull(),
            'to'        => $this->string()->notNull(),
            'to_id'     => $this->integer()->notNull(),
            'status'    => $this->string()->notNull(),
            'points'    => $this->integer()->notNull(),
            'created'   => $this->dateTime(),
            'updated'   => $this->dateTime()
        ]);
    }

    public function safeDown()
    {
        $this->dropColumn('user_votes', 'points');
        $this->dropTable('transactions');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171002_164722_addPointsToVotes cannot be reverted.\n";

        return false;
    }
    */
}
