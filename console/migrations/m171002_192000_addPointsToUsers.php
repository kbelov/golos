<?php

use yii\db\Migration;

class m171002_192000_addPointsToUsers extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user', 'points', $this->integer()->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('user', 'points');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171002_192000_addPointsToUsers cannot be reverted.\n";

        return false;
    }
    */
}
