<?php

use yii\db\Migration;

class m171002_213642_addGiftsOrderTable extends Migration
{
    public function safeUp()
    {
        $this->createTable('user_orders', [
            'id'    => $this->primaryKey(),
            'user_id'   => $this->integer()->notNull(),
            'gift_id'   => $this->integer()->notNull(),
            'points'    => $this->integer()->notNull()->defaultValue(0),
            'mfc_id'    => $this->integer()->null(),
            'created'   => $this->dateTime(),
            'updated'   => $this->dateTime()
        ]);
        $this->addForeignKey('user_orders_user_id_fk', 'user_orders', ['user_id'], 'user', ['id']);
        $this->addForeignKey('user_orders_gifts_id_fk', 'user_orders', ['gift_id'], 'gifts', ['id']);
    }

    public function safeDown()
    {
        $this->dropForeignKey('user_orders_gifts_id_fk', 'user_orders');
        $this->dropForeignKey('user_orders_user_id_fk', 'user_orders');
        $this->dropTable('user_orders');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171002_213642_addGiftsOrderTable cannot be reverted.\n";

        return false;
    }
    */
}
