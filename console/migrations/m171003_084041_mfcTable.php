<?php

use yii\db\Migration;

class m171003_084041_mfcTable extends Migration
{
    public function safeUp()
    {
        $this->createTable('mfc', [
            'id'    => $this->primaryKey(),
            'area_id'   => $this->integer()->notNull(),
            'address'   => $this->string()->notNull(),
            'name'      => $this->string()->notNull(),
            'visible'   => $this->boolean()->defaultValue(true),
            'created'   => $this->dateTime(),
            'updated'   => $this->dateTime()
        ]);
        $this->addForeignKey('user_orders_mfc_id_fk', 'user_orders', ['mfc_id'], 'mfc', ['id']);
        $this->addForeignKey('mfc_area_id_fk', 'mfc', ['area_id'], 'areas', ['id']);
    }

    public function safeDown()
    {
        $this->dropForeignKey('mfc_area_id_fk', 'mfc');
        $this->dropForeignKey('user_orders_mfc_id_fk', 'user_orders');
        $this->dropTable('mfc');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171003_084041_mfcTable cannot be reverted.\n";

        return false;
    }
    */
}
