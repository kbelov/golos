<?php

use yii\db\Migration;

class m171003_093241_addStatusToUserOrder extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_orders', 'status', $this->integer()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->addColumn('user_orders', 'status');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171003_093241_addStatusToUserOrder cannot be reverted.\n";

        return false;
    }
    */
}
