<?php

use yii\db\Migration;

class m171003_111314_addCoordsToPoll extends Migration
{
    public function safeUp()
    {
        $this->addColumn('polls', 'lat', $this->float()->null());
        $this->addColumn('polls', 'lon', $this->float()->null());
        $this->addColumn('polls', 'building_id', $this->integer()->null());
    }

    public function safeDown()
    {
        $this->dropColumn('polls', 'lat');
        $this->dropColumn('polls', 'lon');
        $this->dropColumn('polls', 'building_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171003_111314_addCoordsToPoll cannot be reverted.\n";

        return false;
    }
    */
}
