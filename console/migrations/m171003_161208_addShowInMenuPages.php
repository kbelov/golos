<?php

use yii\db\Migration;

class m171003_161208_addShowInMenuPages extends Migration
{
    public function safeUp()
    {
        $this->addColumn('pages', 'show_in_menu', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('pages', 'show_in_menu');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171003_161208_addShowInMenuPages cannot be reverted.\n";

        return false;
    }
    */
}
