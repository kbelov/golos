<?php

use yii\db\Migration;

class m171003_210140_addEsiaCodeToUser extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user', 'esia_code', $this->text()->null());
    }

    public function safeDown()
    {
        $this->dropColumn('user', 'esia_code');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171003_210140_addEsiaCodeToUser cannot be reverted.\n";

        return false;
    }
    */
}
