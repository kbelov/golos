<?php

use yii\db\Migration;

class m171003_211023_addColumnsUser extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user', 'first_name', $this->string());
        $this->addColumn('user', 'last_name', $this->string());
        $this->addColumn('user', 'middle_name', $this->string());
        $this->addColumn('user', 'phone', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('user', 'first_name');
        $this->dropColumn('user', 'last_name');
        $this->dropColumn('user', 'middle_name');
        $this->dropColumn('user', 'phone');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171003_211023_addColumnsUser cannot be reverted.\n";

        return false;
    }
    */
}
