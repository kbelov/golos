<?php

use yii\db\Migration;

class m171003_212143_removeColumnsFromUser extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('user', 'username');
        $this->dropColumn('user', 'password_hash');
        $this->dropColumn('user', 'password_reset_token');
        $this->dropColumn('user', 'auth_key');

        $this->dropColumn('user', 'email');
        $this->addColumn('user', 'email', $this->string()->null());
    }

    public function safeDown()
    {
        $this->alterColumn('user', 'email', $this->string()->notNull()->unique());

        $this->addColumn('user', 'auth_key', $this->string(32)->notNull());
        $this->addColumn('user', 'password_reset_token', $this->string()->unique());
        $this->addColumn('user', 'password_hash', $this->string()->notNull());
        $this->addColumn('user', 'username', $this->string()->notNull()->unique());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171003_212143_removeColumnsFromUser cannot be reverted.\n";

        return false;
    }
    */
}
