<?php

use yii\db\Migration;

class m171004_102613_addUsersTorisTable extends Migration
{
    public function safeUp()
    {
        $this->createTable('user_toris', [
            'id'        => $this->primaryKey(),
            'bx_id'     => $this->integer()->notNull(),
            'iogv_id'   => $this->string()->notNull(),
            'fio'       => $this->string()->null(),
            'aistoken'  => $this->text()->notNull(),
            'created'   => $this->dateTime(),
            'updated'   => $this->dateTime()
        ]);

        $this->createTable('user_toris_roles', [
            'id'            => $this->primaryKey(),
            'toris_code'    => $this->string()->notNull(),
            'created'       => $this->dateTime(),
            'updated'       => $this->dateTime()
        ]);

        $this->createTable('user_toris_roles_links', [
            'id'            => $this->primaryKey(),
            'user_toris_id' => $this->integer()->notNull(),
            'name'          => $this->string()->notNull(),
            'created'       => $this->dateTime(),
            'updated'       => $this->dateTime()
        ]);

        $this->addForeignKey('user_toris_roles_links_user_id_fk', 'user_toris_roles_links', ['user_toris_id'], 'user_toris', ['id']);
    }

    public function safeDown()
    {
        $this->dropForeignKey('user_toris_roles_links_user_id_fk', 'user_toris_roles_links');

        $this->dropTable('user_toris_roles_links');
        $this->dropTable('user_toris_roles');
        $this->dropTable('user_toris');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171004_102613_addUsersTorisTable cannot be reverted.\n";

        return false;
    }
    */
}
