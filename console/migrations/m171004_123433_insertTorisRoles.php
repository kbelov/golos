<?php

use yii\db\Expression;
use yii\db\Migration;

class m171004_123433_insertTorisRoles extends Migration
{
    public function safeUp()
    {
        $now = new Expression('NOW()');
        $this->batchInsert('user_toris_roles', [
            'toris_code',
            'created'
        ], [
            ['[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:manager', $now],
            ['[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:moderator', $now],
            ['[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:operator_iogv', $now],
            ['[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:admin', $now],
            ['[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:mfc', $now],
        ]);
    }

    public function safeDown()
    {
        $this->truncateTable('user_toris_roles');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171004_123433_insertTorisRoles cannot be reverted.\n";

        return false;
    }
    */
}
