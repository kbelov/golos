<?php

use yii\db\Migration;

class m171004_143053_addStatusSoglasPoll extends Migration
{
    public function safeUp()
    {
        $this->addColumn('polls', 'status_soglas', $this->string()->notNull()->defaultValue('iogv'));
    }

    public function safeDown()
    {
        $this->dropColumn('polls', 'status_soglas');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171004_143053_addStatusSoglasPoll cannot be reverted.\n";

        return false;
    }
    */
}
