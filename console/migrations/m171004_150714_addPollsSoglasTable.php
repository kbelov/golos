<?php

use yii\db\Migration;

class m171004_150714_addPollsSoglasTable extends Migration
{
    public function safeUp()
    {
        $this->createTable('polls_soglas', [
            'id'                => $this->primaryKey(),
            'status_from'       => $this->string()->notNull(),
            'status_to'         => $this->string()->notNull(),
            'user_toris_id'     => $this->integer()->notNull(),
            'poll_id'           => $this->integer()->notNull(),
            'created'           => $this->dateTime(),
            'updated'           => $this->dateTime(),
        ]);

        $this->addForeignKey('polls_soglas_user_toris_id_fk', 'polls_soglas', ['user_toris_id'], 'user_toris', ['id']);
        $this->addForeignKey('polls_soglas_poll_id_fk', 'polls_soglas', ['poll_id'], 'polls', ['id']);
    }

    public function safeDown()
    {
        $this->dropForeignKey('polls_soglas_poll_id_fk', 'polls_soglas');
        $this->dropForeignKey('polls_soglas_user_toris_id_fk', 'polls_soglas');

        $this->dropTable('polls_soglas');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171004_150714_addPollsSoglasTable cannot be reverted.\n";

        return false;
    }
    */
}
