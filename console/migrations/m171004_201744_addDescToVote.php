<?php

use yii\db\Migration;

class m171004_201744_addDescToVote extends Migration
{
    public function safeUp()
    {
        $this->addColumn('polls_votes', 'description', $this->text()->null());
    }

    public function safeDown()
    {
        $this->dropColumn('polls_votes', 'description');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171004_201744_addDescToVote cannot be reverted.\n";

        return false;
    }
    */
}
