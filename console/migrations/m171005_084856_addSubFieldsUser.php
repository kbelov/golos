<?php

use yii\db\Migration;

class m171005_084856_addSubFieldsUser extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user', 'snils', $this->string()->null());
        $this->addColumn('user', 'passport', $this->string()->null());
        $this->addColumn('user', 'passport_date', $this->string()->null());
        $this->addColumn('user', 'passport_info', $this->string()->null());
        $this->addColumn('user', 'passport_code', $this->string()->null());
        $this->addColumn('user', 'gender', $this->string()->null());
    }

    public function safeDown()
    {
        $this->dropColumn('user', 'snils');
        $this->dropColumn('user', 'passport');
        $this->dropColumn('user', 'passport_date');
        $this->dropColumn('user', 'passport_info');
        $this->dropColumn('user', 'passport_code');
        $this->dropColumn('user', 'gender');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171005_084856_addSubFieldsUser cannot be reverted.\n";

        return false;
    }
    */
}
