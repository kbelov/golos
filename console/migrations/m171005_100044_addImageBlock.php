<?php

use yii\db\Migration;

class m171005_100044_addImageBlock extends Migration
{
    public function safeUp()
    {
        $this->addColumn('blocks', 'image', $this->string()->null());
    }

    public function safeDown()
    {
        $this->dropColumn('blocks', 'image');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171005_100044_addImageBlock cannot be reverted.\n";

        return false;
    }
    */
}
