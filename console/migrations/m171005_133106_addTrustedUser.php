<?php

use yii\db\Migration;

class m171005_133106_addTrustedUser extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user', 'trusted', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('user', 'trusted');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171005_133106_addTrustedUser cannot be reverted.\n";

        return false;
    }
    */
}
