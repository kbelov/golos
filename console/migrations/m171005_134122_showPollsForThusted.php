<?php

use yii\db\Migration;

class m171005_134122_showPollsForThusted extends Migration
{
    public function safeUp()
    {
        $this->addColumn('polls', 'visibility', $this->string()->null());
    }

    public function safeDown()
    {
        $this->dropColumn('polls', 'visibility');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171005_134122_showPollsForThusted cannot be reverted.\n";

        return false;
    }
    */
}
