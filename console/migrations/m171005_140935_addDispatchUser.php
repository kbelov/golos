<?php

use yii\db\Migration;

class m171005_140935_addDispatchUser extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user', 'dispatch_sms', $this->boolean()->defaultValue(true));
        $this->addColumn('user', 'dispatch_email', $this->boolean()->defaultValue(true));
    }

    public function safeDown()
    {
        $this->dropColumn('user', 'dispatch_sms');
        $this->dropColumn('user', 'dispatch_email');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171005_140935_addDispatchUser cannot be reverted.\n";

        return false;
    }
    */
}
