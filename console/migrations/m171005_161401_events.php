<?php

use yii\db\Migration;

class m171005_161401_events extends Migration
{
    public function safeUp()
    {
        $this->createTable('events', [
            'id'        => $this->primaryKey(),
            'target_id' => $this->integer()->null(),
            'type'      => $this->string()->notNull(),
            'status'    => $this->integer()->notNull()->defaultValue(0),
            'created'   => $this->dateTime(),
            'updated'   => $this->dateTime()
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('events');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171005_161401_events cannot be reverted.\n";

        return false;
    }
    */
}
