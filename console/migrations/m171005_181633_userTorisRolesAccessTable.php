<?php

use yii\db\Expression;
use yii\db\Migration;

class m171005_181633_userTorisRolesAccessTable extends Migration
{

    const MAIN_SITE = 'app-backend/site';

    const POLLS_DEFAULT = 'polls/default';
    const POLLS_VOTES = 'polls/votes';
    const POLLS_COMMENTS = 'polls/comments';

    const AREAS_DEFAULT = 'areas/default';

    const BLOCKS_DEFAULT = 'blocks/default';

    const GIFTS_DEFAULT = 'gifts/default';
    const GIFTS_MFC = 'gifts/mfc';

    const MFC_DEFAULT = 'mfc/default';

    const NEWS_DEFAULT = 'news/default';

    const PAGES_DEFAULT = 'pages/default';

    const TEMPLATE_DEFAULT = 'templates/default';

    const USERS_DEFAULT = 'users/default';

    const ACCESS_ARRAY = [
        self::MAIN_SITE => [
            '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:admin',
            '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:manager',
            '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:mfc',
            '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:moderator',
            '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:operator_iogv'
        ],
        self::POLLS_DEFAULT => [
            '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:admin',
            '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:moderator',
            '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:operator_iogv',
            '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:manager'
        ],
        self::POLLS_VOTES   => [
            '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:admin',
            '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:moderator',
            '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:operator_iogv',
            '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:manager'
        ],
        self::POLLS_COMMENTS   => [
            '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:admin',
            '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:moderator',
            '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:operator_iogv',
            '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:manager'
        ],
        self::AREAS_DEFAULT => [
            '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:admin'
        ],
        self::BLOCKS_DEFAULT => [
            '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:admin'
        ],
        self::GIFTS_DEFAULT => [
            '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:admin'
        ],
        self::GIFTS_MFC => [
            '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:admin',
            '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:mfc'
        ],
        self::MFC_DEFAULT => [
            '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:admin'
        ],
        self::NEWS_DEFAULT => [
            '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:admin'
        ],
        self::PAGES_DEFAULT => [
            '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:admin'
        ],
        self::TEMPLATE_DEFAULT => [
            '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:admin'
        ],
        self::USERS_DEFAULT => [
            '[urn:eis:ext:portal_omg]urn:eis:ext:portal_omg:admin'
        ]
    ];
    
    public function safeUp()
    {
        $this->createTable('user_toris_roles_access', [
            'id'        => $this->primaryKey(),
            'module'    => $this->string()->notNull(),
            'role'      => $this->string()->notNull(),
            'created'   => $this->dateTime(),
            'updated'   => $this->dateTime()
        ]);
        $this->createIndex('user_toris_roles_access_module', 'user_toris_roles_access', 'module');
        $now = new Expression('NOW()');

        $insert = [];
        foreach (self::ACCESS_ARRAY as $module => $roles) {
            foreach ($roles as $role) {
                $insert[] = [
                    $module,
                    $role,
                    $now
                ];
            }
        }

        $this->batchInsert('user_toris_roles_access', [
            'module',
            'role',
            'created'
        ], $insert);
    }

    public function safeDown()
    {
        $this->dropTable('user_toris_roles_access');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171005_181633_userTorisRolesAccessTable cannot be reverted.\n";

        return false;
    }
    */
}
