<?php

use console\components\Migration;

/**
 * Class m171006_091703_addHistoryTable
 */
class m171006_091703_addHistoryTable extends Migration
{
    public function safeUp()
    {
        $this->createTable('history', [
            'id'             => $this->primaryKey(),
            'owner_class'    => $this->string()->notNull(),
            'owner_id'       => $this->integer()->notNull(),
            'user_id'        => $this->integer(),
            'old_attributes' => $this->jsonb(),
            'new_attributes' => $this->jsonb(),
            'created'        => $this->dateTime(),
        ]);

        $this->createIndex('history_owner_idx', 'history', ['owner_class', 'owner_id']);
    }

    public function safeDown()
    {
        $this->dropTable('history');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171006_091703_addHistoryTable cannot be reverted.\n";

        return false;
    }
    */
}
