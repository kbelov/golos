<?php

use yii\db\Migration;

class m171006_133405_addMFCRole extends Migration
{
    public function safeUp()
    {
        $this->addColumn('mfc', 'role', $this->string()->null());
    }

    public function safeDown()
    {
        $this->dropColumn('mfc', 'role');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171006_133405_addMFCRole cannot be reverted.\n";

        return false;
    }
    */
}
