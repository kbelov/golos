<?php

use yii\db\Migration;

class m171007_123546_addPropsUserToris extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_toris', 'phone', $this->string()->null());
        $this->addColumn('user_toris', 'email', $this->string()->null());
    }

    public function safeDown()
    {
        $this->dropColumn('user_toris', 'phone');
        $this->dropColumn('user_toris', 'email');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171007_123546_addPropsUserToris cannot be reverted.\n";

        return false;
    }
    */
}
