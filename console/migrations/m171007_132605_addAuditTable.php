<?php

use yii\db\Migration;

class m171007_132605_addAuditTable extends Migration
{
    public function safeUp()
    {
        $this->createTable('audit', [
            'id' => $this->primaryKey(),
            'owner_class' => $this->string()->notNull(),
            'owner_id' => $this->integer()->notNull(),
            'history_id'    => $this->integer()->null(),
            'type'  => $this->string()->notNull(),
            'user_id'   => $this->integer()->null(),
            'user_toris_id' => $this->integer()->null(),
            'created'   => $this->dateTime(),
            'updated'   => $this->dateTime()
        ]);
        $this->addForeignKey('audit_user_id_fk', 'audit', ['user_id'], 'user', ['id']);
        $this->addForeignKey('audit_user_toris_id_fk', 'audit', ['user_toris_id'], 'user_toris', ['id']);
    }

    public function safeDown()
    {
        $this->dropTable('audit');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171007_132605_addAuditTable cannot be reverted.\n";

        return false;
    }
    */
}
