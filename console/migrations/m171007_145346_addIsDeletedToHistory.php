<?php

use yii\db\Migration;

class m171007_145346_addIsDeletedToHistory extends Migration
{
    public function safeUp()
    {
        $this->addColumn('history', 'is_deleted', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('history', 'is_deleted');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171007_145346_addIsDeletedToHistory cannot be reverted.\n";

        return false;
    }
    */
}
