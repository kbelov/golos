<?php

use yii\db\Migration;

class m171007_153926_addVisibleToPolls extends Migration
{
    public function safeUp()
    {
        $this->addColumn('polls', 'visible', $this->boolean()->defaultValue(true));
    }

    public function safeDown()
    {
        $this->dropColumn('polls', 'visible');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171007_153926_addVisibleToPolls cannot be reverted.\n";

        return false;
    }
    */
}
