<?php

use yii\db\Migration;

class m171007_155458_addVisibleAreas extends Migration
{
    public function safeUp()
    {
        $this->addColumn('areas', 'visible', $this->boolean()->defaultValue(true));
        $this->addColumn('feedback_templates', 'visible', $this->boolean()->defaultValue(true));
    }

    public function safeDown()
    {
        $this->dropColumn('areas', 'visible');
        $this->dropColumn('feedback_templates', 'visible');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171007_155458_addVisibleAreas cannot be reverted.\n";

        return false;
    }
    */
}
