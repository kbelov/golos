<?php

use yii\db\Migration;

class m171007_213855_addNeedCountVotesToPoll extends Migration
{
    public function safeUp()
    {
        $this->addColumn('polls', 'count_votes', $this->integer()->defaultValue(1));
    }

    public function safeDown()
    {
        $this->dropColumn('polls', 'count_votes');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171007_213855_addNeedCountVotesToPoll cannot be reverted.\n";

        return false;
    }
    */
}
