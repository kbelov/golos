<?php

use yii\db\Migration;

class m171011_102616_delete_form_likes extends Migration
{
    public function safeUp()
    {
        $this->delete('comments_likes');
    }
}
