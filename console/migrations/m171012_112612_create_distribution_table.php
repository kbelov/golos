<?php

use yii\db\Migration;

/**
 * Handles the creation of table `distribution`.
 */
class m171012_112612_create_distribution_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('distribution', [
            'id' => $this->primaryKey(),
            'name' => $this->text(),
            'type' => $this->smallInteger(),
        ]);
        $this->batchInsert('distribution', ['name', 'type'], [
            ['Изменения статуса голосования в которых я проголосовал', 1],
            ['О публикации новых голосований', 1],
            ['О полученных баллах', 1],
            ['Об истории голосований (еженедельная подписка)', 1],
            ['О новых голосованиях', 2],
            ['О новых новостях', 2],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('distribution');
    }
}
