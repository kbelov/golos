<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_distribution`.
 */
class m171012_112917_create_user_distribution_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('user_distribution', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'distribution_id' => $this->integer()->notNull(),
            'subscribed' => $this->boolean()
        ]);
        $this->addForeignKey(
            'fk_user_distribution_to_user',
            'user_distribution',
            'user_id',
            'user',
            'id',
            'cascade'
        );
        $this->addForeignKey(
            'fk_user_distribution_to_distribution',
            'user_distribution',
            'distribution_id',
            'distribution',
            'id',
            'cascade'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('user_distribution');
    }
}
