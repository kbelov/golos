<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'pgsql:host=golos-db1.lan.iac.spb.ru;dbname=golos',
            'username' => 'golos',
            'password' => 'neaRohpie3ira8Ei',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
        ],
        'urlManagerFrontend' => [
            'class' => \yii\web\UrlManager::class,
            'baseUrl' => '',
            'hostInfo' => 'http://golos.iac.spb.ru',
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
        ],
    ],
];
