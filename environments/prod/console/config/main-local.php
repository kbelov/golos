<?php
return [
    'components' => [
        'urlManager' => [
            'class' => \yii\web\UrlManager::class,
            'baseUrl' => '',
            'hostInfo' => 'http://golos.iac.spb.ru',
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
        ],
    ]
];
