<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.17
 * Time: 15:18
 */

namespace frontend;

use backend\assets\UnderscoreAssets;
use yii\web\AssetBundle;

/**
 * Class MapAsset
 *
 * @package frontend
 */
class MapAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@frontend/assets';

    /**
     * @inheritdoc
     */
    public $css = [
        'https://cdnjs.cloudflare.com/ajax/libs/openlayers/4.3.2/ol.css',
    ];

    /**
     * @inheritdoc
     */
    public $js = [
        'https://cdn.polyfill.io/v2/polyfill.min.js?features=requestAnimationFrame,Element.prototype.classList,URL',
        'https://cdnjs.cloudflare.com/ajax/libs/openlayers/4.3.2/ol.js',
        'dist/js/map.js?v=2'
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        ThemeAsset::class,
        UnderscoreAssets::class
    ];
}
