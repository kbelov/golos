<?php

use frontend\ThemeAsset;
use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetManager;
use yii\web\JqueryAsset;
use yii\log\FileTarget;
use common\models\User;
use yii\web\YiiAsset;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'name'      => 'Голос Петербурга',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    'controllerNamespace' => 'frontend\controllers',
    'defaultRoute' => '/site/index',
    'modules' => [
        'polls' => [
            'class' => \frontend\modules\polls\Module::class
        ],
        'news'  => [
            'class' => \frontend\modules\news\Module::class
        ],
        'gifts'  => [
            'class' => \frontend\modules\gifts\Module::class
        ],
        'pages'  => [
            'class' => \frontend\modules\pages\Module::class
        ],
        'comments'  => [
            'class' => \frontend\modules\comments\Module::class
        ],
        'users'  => [
            'class' => \frontend\modules\users\Module::class
        ]
    ],
    'components' => [
        'request' => [
            'baseUrl' => '',
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => User::class,
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => FileTarget::class,
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['yii\swiftmailer\Logger::add'],
                    'logFile' => '@runtime/logs/swift-mail.log',
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'formatter' => [
            'dateFormat' => 'dd MMMM yyyy года',
            'locale' => 'ru-RU'
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => require __DIR__ . '/rules.php'
        ],
        'assetManager' => [
            'class' => AssetManager::class,
            'bundles' => [
                JqueryAsset::class => [
                    'js'    => []
                ],
                YiiAsset::class => [
                    'depends'   => [
                        ThemeAsset::class
                    ]
                ],
                BootstrapAsset::class => [
                    'css'   => [],
                    'js'    => []
                ],
                BootstrapPluginAsset::class => [
                    'js'    => []
                ]
            ],
        ],
    ],
    'params' => $params,
];
