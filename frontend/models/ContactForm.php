<?php

namespace frontend\models;

use common\modules\templates\models\Templates;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $email;
    public $subject;
    public $body;
    public $verifyCode;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['email', 'subject', 'body'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
            'subject'    => 'Тема ответа',
            'body'       => 'Комментарий'
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     *
     * @return bool whether the email was sent
     */
    public function sendEmail($email)
    {
        $template = Templates::findOne($this->subject);
        if (!empty($template)) {
            $mail       = Yii::$app->getMailer();
            $this->body = $this->email . ' : ' . $this->body;

            return $mail
                ->compose()
                ->setTo($email)
                ->setFrom(Yii::$app->params['supportEmail'])
                ->setSubject($template->name)
                ->setTextBody($this->body)
                ->send();
        }
    }

    public function getTemplatesArray(): array
    {
        $templates = Templates::find()->active()->asArray()->all();

        return ArrayHelper::map($templates, 'id', 'name');
    }
}
