<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 20:30
 */

namespace frontend\modules\blocks\widgets\BlocksWidget;

use frontend\modules\blocks\models\Blocks;
use yii\base\Widget;

/**
 * Class BlocksWidget
 * @package frontend\modules\blocks\widgets\BlocksWidget
 */
class BlocksWidget extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        $this->registerAssets();
        $blocks = Blocks::find()->active()->orderBy('created DESC')->all();
        return $this->render('index', [
            'blocks'    => $blocks
        ]);
    }

    protected function registerAssets()
    {
        $view = $this->getView();
        BlocksWidgetAsset::register($view);
        $view->registerJs("var swiperOnMain = new SwiperOnMain()");
    }
}
