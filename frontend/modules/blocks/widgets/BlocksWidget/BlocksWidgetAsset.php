<?php
/**
 * Created by PhpStorm.
 * User: filatov
 * Date: 16.10.2017
 * Time: 18:20
 */

namespace frontend\modules\blocks\widgets\BlocksWidget;

use frontend\ThemeAsset;

class BlocksWidgetAsset extends ThemeAsset
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@frontend/modules/blocks/widgets/BlocksWidget/assets/';

    public $js = [
        'js/SwiperOnMain.js'
    ];
}
