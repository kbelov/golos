var SwiperOnMain = function (options) {
    var self = this;
    this.init = function () {
        var swiper = new Swiper('.swiper-container', {
            pagination: '.swiper-pagination',
            paginationClickable: true,
            spaceBetween: 30,
            onSlideChangeEnd: this.onClick.bind(this),
            onInit: this.onClick.bind(this)
        });
    };

    this.onClick = function (e) {
        var slide = $('.swiper-wrapper').children(),
            currentBgUrl = $(slide[e.activeIndex]).children('.widget-side__content').attr('data-url');
        if (!!currentBgUrl) {
            $('.widget-side-container').css({
                'background-image': "url('"+ currentBgUrl +"')",
                'background-size' : 'cover'
            });
        }
    };
    this.init();
    return this;
};
