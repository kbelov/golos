<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 23.09.2017
 * Time: 17:23
 */

use frontend\ThemeAsset;

/** @var \backend\modules\blocks\models\Blocks[] $blocks */

$assetUrl = $this->getAssetManager()->getAssetUrl($this->getAssetManager()->getBundle(ThemeAsset::className()), '');

?>
<div class="widget-side">
    <div class="widget-side__logo"><a href="/"><img src="<?=$assetUrl;?>dist/img/Logo.png"></a></div>
    <div class="widget-side__contents">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?php foreach ($blocks as $block): ?>
                <div class="swiper-slide">
                    <div class="widget-side__content" data-url="<?= (!empty($block->image)) ? $block->urlAttribute('image') : ''; ?>">
                        <h3><?= $block->name; ?></h3>
                        <p><?= $block->content; ?></p>
                        <?php if (!empty($block->link_name) && !empty($block->link)): ?>
                            <a class="widget-side__link" <?= ($block->external_link) ? 'target="_blank"' : ''; ?>
                               href="<?= $block->link; ?>"><?= $block->link_name; ?>
                                <div class="i i-<?= ($block->external_link) ? 'external' : 'arrow'; ?>"></div>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="swiper-pagination"></div>
    </div>
</div>
