<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.17
 * Time: 19:26
 */

namespace frontend\modules\comments\controllers;

use common\modules\comments\helpers\CommentsLikesStatusHelper;
use common\modules\comments\models\Comments;
use frontend\modules\comments\services\AddLikeService;
use Yii;
use yii\rest\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class ApiController
 *
 * @package frontend\modules\comments\controllers
 */
class ApiController extends Controller
{
    /**
     * @param int $id
     *
     * @return array
     * @throws \yii\web\ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionLike(int $id)
    {
        $model = Comments::findOne($id);
        if (null === $model) {
            throw new NotFoundHttpException();
        }

        if (!$model->canLike()) {
            throw new ForbiddenHttpException();
        }

        $added = AddLikeService::getInstance()
            ->setCommentModel($model)
            ->setType(CommentsLikesStatusHelper::TYPE_LIKE)
            ->run();

        if ($added) {
            Yii::$app->response->statusCode = 201;
        } else {
            Yii::$app->response->statusCode = 400;
        }

        return [];
    }

    /**
     * @param int $id
     *
     * @return array
     * @throws \yii\web\ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionDislike(int $id)
    {
        $model = Comments::findOne($id);
        if (null === $model) {
            throw new NotFoundHttpException();
        }

        if (!$model->canLike()) {
            throw new ForbiddenHttpException();
        }

        $added = AddLikeService::getInstance()
            ->setCommentModel($model)
            ->setType(CommentsLikesStatusHelper::TYPE_DISLIKE)
            ->run();

        if ($added) {
            Yii::$app->response->statusCode = 201;
        } else {
            Yii::$app->response->statusCode = 400;
        }

        return [];
    }
}
