<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 02.10.17
 * Time: 16:27
 */

namespace frontend\modules\comments\controllers;

use common\components\ActiveRecord;
use common\modules\comments\helpers\TypesHelper;
use frontend\controllers\Controller;
use frontend\modules\comments\models\CommentsForm;
use Yii;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class DefaultController
 *
 * @package frontend\modules\comments\controllers
 */
class DefaultController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors(): array
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'send' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @param string $type
     * @param int    $target_id
     *
     * @return array|Response
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\base\InvalidParamException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionSend(string $type, int $target_id)
    {

        $types = TypesHelper::TYPES_ARRAY;
        if (!isset($types[$type])) {
            throw new NotFoundHttpException();
        }

        $model = $this->findModel($type, $target_id);

        $commentsForm = new CommentsForm([
            'target_id' => $target_id,
            'type'      => $types[$type]
        ]);

        if ($commentsForm->load(Yii::$app->request->post())) {

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($commentsForm);
            }
            if ($commentsForm->validate()) {
                if ($commentsForm->save(false)) {
                    Yii::$app->session->setFlash('success', 'Комментарий успешно добавлен');
                } else {
                    Yii::$app->session->setFlash('danger', 'Не сохранено');
                }
                return $this->redirect($model->getCommentsUrl());
            }

        }

        throw new BadRequestHttpException();

    }

    /**
     * @param string $type
     * @param int    $target_id
     *
     * @return ActiveRecord
     * @throws NotFoundHttpException
     */
    protected function findModel(string $type, int $target_id): ActiveRecord
    {
        $types = TypesHelper::TYPES_ARRAY;
        if (!isset($types[$type])) {
            throw new NotFoundHttpException();
        }
        /** @var ActiveRecord $model */
        $model = $types[$type];
        $model = $model::find()->where([
            'id'    => $target_id
        ])->active()->one();
        if (null === $model) {
            throw new NotFoundHttpException();
        }
        return $model;
    }
}
