<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 02.10.17
 * Time: 16:37
 */

namespace frontend\modules\comments\models;

use common\modules\comments\models\Comments;

/**
 * Class CommentsForm
 *
 * @package frontend\modules\comments\models
 */
class CommentsForm extends Comments
{
    /**
     * @inheritdoc
     */
    public function beforeValidate(): bool
    {
        $this->user_id = \Yii::$app->user->id;
        return parent::beforeValidate();
    }

}
