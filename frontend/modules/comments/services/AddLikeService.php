<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.17
 * Time: 19:30
 */

namespace frontend\modules\comments\services;

use common\interfaces\Service;
use common\modules\comments\models\Comments;
use common\modules\comments\models\CommentsLikes;
use Yii;
use yii\base\Exception;

/**
 * Class AddLikeService
 *
 * @package frontend\modules\comments\services
 */
class AddLikeService implements Service
{
    /** @var self */
    protected static $instance;

    /** @var Comments */
    protected $commentModel;
    /** @var string */
    protected $type;

    /**
     * @return static
     * @throws \yii\base\Exception
     */
    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function run()
    {
        if (null === $this->commentModel) {
            throw new Exception('Не заполнены commentForm');
        }
        if (null === $this->type) {
            throw new Exception('Не заполнены type');
        }

        $transaction = \Yii::$app->getDb()->beginTransaction();
        try {
            $likeModel = new CommentsLikes([
                'user_id'       => Yii::$app->getUser()->getId(),
                'comment_id'    => $this->commentModel->id,
                'type'          => $this->type,
            ]);

            $likeModel->save();

            if (!$likeModel->hasErrors()) {
                $transaction->commit();
            } else {
                $transaction->rollBack();
                throw new Exception('Transaction errors: ' . count($likeModel->getFirstErrors()));
            }
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setType(string $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @param Comments $commentModel
     *
     * @return $this
     */
    public function setCommentModel(Comments $commentModel)
    {
        $this->commentModel = $commentModel;

        return $this;
    }
}
