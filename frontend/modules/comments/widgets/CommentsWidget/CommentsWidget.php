<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 02.10.17
 * Time: 14:07
 */

namespace frontend\modules\comments\widgets\CommentsWidget;

use common\modules\comments\models\Comments;
use frontend\modules\comments\models\CommentsForm;
use yii\base\Widget;

/**
 * Class CommentsWidget
 *
 * @package frontend\modules\comments\widgets\CommentsWidget
 */
class CommentsWidget extends Widget
{
    public $model;

    /**
     * @return string
     * @throws \yii\base\InvalidParamException
     */
    public function run(): string
    {
        $comments = Comments::find()->asModel($this->model)->orderBy('created DESC')->active()->limit(10)->all();

        $commentsForm = new CommentsForm();

        return $this->render('index', [
            'comments'      => $comments,
            'model'         => $this->model,
            'commentsForm'  => $commentsForm
        ]);
    }
}
