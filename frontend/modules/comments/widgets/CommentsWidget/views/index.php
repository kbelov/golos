<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 02.10.17
 * Time: 14:08
 */

use common\components\Html;
use common\modules\comments\helpers\CommentsLikesStatusHelper;
use common\modules\comments\helpers\TypesHelper;
use yii\widgets\ActiveForm;

/** @var \common\modules\comments\models\Comments[] $comments */
/** @var \common\components\ActiveRecord $model */
/** @var \frontend\modules\comments\models\CommentsForm $commentsForm */

?>

<section>
    <h4>Комментарии к голосованию</h4>
    <div class="comments">
        <?php foreach ($comments as $comment):
            $userLike = $comment->getLikeByUser();
            $statusLike = $userLike;
            $likeClass = '';
            if ($userLike === CommentsLikesStatusHelper::TYPE_LIKE) {
                $likeClass = ' i-liked';
            }
            $dislikeClass = '';
            if ($userLike === CommentsLikesStatusHelper::TYPE_DISLIKE) {
                $dislikeClass = ' i-disliked';
            }
            ?>
            <div class="comment">
                <div class="comment__header"><?= $comment->user; ?></div>
                <div class="comment__body">
                    <p><?= $comment->content; ?></p>
                </div>
                <div class="comment__footer">
                    <div class="meta__group">
                        <div class="meta">
                            <div class="meta__date"><?= $comment->created; ?></div>
                        </div>
                        <div class="meta">
                            <div class="meta__data text-right <?=$statusLike; ?>" data-id="<?= $comment->id; ?>">
                                <div class="i i-like<?= $likeClass; ?>"></div><?= count($comment->likes); ?>
                                <div class="i i-dislike<?= $dislikeClass; ?>"></div><?= count($comment->dislikes); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</section>
<?php if ($model->hasMethod('canMakeComment') && $model->canMakeComment()): ?>
    <section>
        <?php $form = ActiveForm::begin([
            'enableAjaxValidation'      => true,
            'enableClientValidation'    => false,
            'validateOnBlur'            => false,
            'validateOnChange'          => false,
            'validateOnType'            => false,
            'action' => \common\components\Url::toRoute([
                '/comments/default/send',
                'type'          => TypesHelper::TYPE_POLLS,
                'target_id'     => $model->id
            ])
        ]); ?>
        <?= $form->field($commentsForm, 'content')->textarea([
            'placeholder'   => 'Текст комментария'
        ])->label(false) ?>
            <div class="form-group">
                <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </section>
<?php endif; ?>

