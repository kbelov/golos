<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 20:02
 */

namespace frontend\modules\gifts\controllers;

use common\modules\gifts\models\UserOrders;
use frontend\controllers\Controller;
use frontend\modules\gifts\models\forms\UserOrderForm;
use frontend\modules\gifts\models\Gifts;
use frontend\modules\gifts\Module;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class DefaultController
 * @package frontend\modules\gifts\controllers
 *
 * @property Module $module
 */
class DefaultController extends Controller
{
    /**
     * @throws \yii\base\InvalidParamException
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Gifts::find()->active()->orderBy('created DESC'),
            'pagination' => [
                'pageSize' => $this->module->recordsPerPage
            ]
        ]);

        return $this->render('index', [
            'dataProvider'  => $dataProvider,
        ]);
    }

    /**
     * Buy page.
     *
     * @param integer $id News ID
     *
     * @return mixed
     * @throws \yii\base\InvalidParamException
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionBuy(int $id)
    {
        $model = Gifts::find()->where([
            'id'    => $id
        ])->active()->one();
        if (null === $model) {
            throw new NotFoundHttpException();
        }

        if (!$model->canBuy()) {
            throw new ForbiddenHttpException();
        }

        $userOrderForm = new UserOrderForm([
            'gift_id' => $model->id,
        ]);

        if (Yii::$app->getRequest()->getIsPost()) {
            if ($userOrderForm->load(Yii::$app->request->post())) {
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($userOrderForm);
                }
                if ($userOrderForm->validate()) {
                    if ($userOrderForm->save(false)) {
                        Yii::$app->session->setFlash('success', 'Заказ # '.$userOrderForm->id.' успешно подтвержден! Перейдите в личный кабинет в раздел магазин поощрений. Когда статус заказа смениться на "Ожидает на выдаче" его можно будет забрать в МФЦ.');
                    } else {
                        Yii::$app->session->setFlash('danger', 'Не сохранено');
                    }
                    return $this->redirect(['/gifts/default/index', 'id' => $id]);
                }
            }
        }

        return $this->render('buy', [
            'model'         => $model,
            'userOrderForm' => $userOrderForm
        ]);
    }
}
