<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 20:00
 */

namespace frontend\modules\gifts\models;

use common\models\User;
use Yii;

/**
 * Class Gifts
 * @package frontend\modules\gifts\models
 */
class Gifts extends \common\modules\gifts\models\Gifts
{
    public function canBuy(): bool
    {
        if (Yii::$app->getUser()->getIsGuest()) {
            return false;
        }

        $user = User::findIdentity(Yii::$app->getUser()->getId());
        if ($user->points > $this->points) {
            return true;
        }

        return false;
    }
}
