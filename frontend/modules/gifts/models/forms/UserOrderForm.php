<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.2017
 * Time: 0:59
 */

namespace frontend\modules\gifts\models\forms;

use common\modules\gifts\models\UserOrders;
use common\modules\transactions\services\AddNewTransaction;

/**
 * Class UserOrderForm
 * @package frontend\modules\gifts\models\forms
 */
class UserOrderForm extends UserOrders
{
    /** @var integer */
    public $area_id;

    public function beforeValidate(): bool
    {
        $this->user_id = \Yii::$app->getUser()->getId();
        $this->points = $this->gift->points;
        return parent::beforeValidate();
    }

    /**
     * @param bool  $insert
     * @param array $changedAttributes
     *
     * @throws \yii\base\Exception
     */
    public function afterSave($insert, $changedAttributes)
    {
        AddNewTransaction::getInstance()
            ->setFrom(get_class($this->user))
            ->setFromId($this->user_id)
            ->setTo(get_class($this->gift))
            ->setToId($this->gift_id)
            ->setPoints($this->gift->points)
            ->run();
    }
}
