<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 18:11
 */

use common\components\Url;

/** @var \frontend\modules\gifts\models\Gifts $model */

?>
<div class="shop__item-container">
    <div class="shop__item">
        <div class="shop__item__img-container">
            <img class="shop__item__img img-responsive" src="<?=$model->urlAttribute('image'); ?>"/>
            <div class="shop__item__price"><?= $model->points; ?> баллов</div>
        </div>
        <div class="shop__item__title"><?= $model->name; ?></div>
        <?php if ($model->canBuy()): ?>
            <div class="shop__item__buy">
                <a class="btn btn-primary" href="<?= Url::toRoute(['/gifts/default/buy', 'id' => $model->id]); ?>">Заказать</a>
            </div>
        <?php endif; ?>
    </div>
</div>
