<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.2017
 * Time: 0:50
 */

use common\components\Html;
use yii\widgets\ActiveForm;

/** @var \frontend\modules\gifts\models\Gifts $model */
/** @var \yii\web\View $this */
/** @var \frontend\modules\gifts\models\forms\UserOrderForm $userOrderForm */

$this->title = $model->name;
?>
<section>
    <div class="row">
        <div class="col-md-12">
            <header class="section-header">
                <h2><?= $this->title; ?></h2>
            </header>
        </div>
        <div class="col-md-12">
            <h3>Вы действительно хотите приобрести "<?= $model->name; ?>"?</h3>
        </div>
        <div class="col-md-4" data-key="5">
            <div class="shop__item-container">
                <div class="shop__item">
                    <div class="shop__item__img-container">
                        <img class="shop__item__img img-responsive" src="<?=$model->urlAttribute('image'); ?>"/>
                        <div class="shop__item__price"><?= $model->points; ?> баллов</div>
                    </div>
                    <div class="shop__item__title"><?= $model->name; ?></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?php $form = ActiveForm::begin([
                'enableAjaxValidation'      => true,
                'enableClientValidation'    => false,
                'validateOnBlur'            => false,
                'validateOnChange'          => false,
                'validateOnType'            => false,
            ]); ?>
            <?= $form
                ->field($userOrderForm, 'area_id')
                ->dropDownList($userOrderForm->getAreasArray(), [
                    'prompt'    => 'Район'
                ])
                ->label(false); ?>
            <?= $form
                ->field($userOrderForm, 'mfc_id')
                ->dropDownList($userOrderForm->getMfcArray(), [
                    'prompt'    => 'МФЦ'
                ])
                ->label(false); ?>
            <div class="form-group text-center">
                <?= Html::submitButton('заберу здесь', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</section>
