<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 20:06
 */

use yii\widgets\ListView;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Магазин поощрений';
$this->params['noTitle'] = true;
?>
<section>
    <div class="row">
        <div class="col-md-12">
            <header class="section-header">
                <h2><?= $this->title; ?></h2>
            </header>
        </div>
        <div class="col-md-12">
            <?= ListView::widget(
                [
                    'dataProvider' => $dataProvider,
                    'layout' => "{items}\n<div class='col-lg-12'>{pager}</div>",
                    'itemView' => '_index_item',
                    'options' => [
                        'class' => 'row',
                    ],
                    'itemOptions' => [
                        'class' => 'col-md-4',
                        'tag' => 'div'
                    ]
                ]
            ); ?>
        </div>
    </div>
</section>
