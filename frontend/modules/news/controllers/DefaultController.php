<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 17:46
 */

namespace frontend\modules\news\controllers;

use frontend\controllers\Controller;
use frontend\modules\news\models\News;
use frontend\modules\news\Module;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

/**
 * Class DefaultController
 * @package frontend\modules\news\controllers
 * @property Module module
 */
class DefaultController extends Controller
{
    /**
     * @throws \yii\base\InvalidParamException
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => News::find()->isPiter()->active()->orderBy('date DESC'),
            'pagination' => [
                'pageSize' => $this->module->recordsPerPage
            ]
        ]);

        return $this->render('index', [
            'dataProvider'  => $dataProvider,
        ]);
    }

    /**
     * News page.
     *
     * @param integer $id News ID
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView(int $id)
    {
        $model = News::find()->where([
            'id'    => $id
        ])->active()->isPiter()->one();
        if (null === $model) {
            throw new NotFoundHttpException();
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }
}
