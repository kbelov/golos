<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 29.09.17
 * Time: 12:22
 */

namespace frontend\modules\news\models;

use Yii;

/**
 * Class News
 *
 * @package frontend\modules\news\models
 */
class News extends \common\modules\news\models\News
{
    /**
     * @return string
     */
    public function getDate()
    {
        return Yii::$app->getFormatter()->asDate($this->date);
    }
}
