<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 18:21
 */
?>
<?php
use frontend\helpers\DateFormatHelper;

/** @var \frontend\modules\news\models\News $model */
/** @var \yii\web\View $this */

$this->title = $model->name;

?>
<section>
    <div class="row">
        <div class="col-md-12">
            <header class="section-header">
                <h2><?= $this->title; ?></h2>
            </header>
        </div>
        <div class="col-md-12">
            <div class="article-content">
                <?php if (!empty($model->image)) : ?>
                    <img class="article__img img-responsive" src="<?= $model->urlAttribute('image'); ?>">
                <?php endif; ?>
                <div class="article-content__text">
                    <?= $model->content; ?>
                </div>
                <div class="meta__group">
                    <div class="meta meta__alignment">
                        <div class="meta__date"><?=Yii::$app->formatter->asDate($model->date, DateFormatHelper::FULL_DATE);?></div>
                        <div class="socials__share"><a class="socials__share__item" href="#"><i class="fa fa-facebook"></i></a><a class="socials__share__item" href="#"><i class="fa fa-paper-plane"></i></a><a class="socials__share__item share__item--tw" href="#"><i class="fa fa-twitter"></i></a><a class="socials__share__item share__item--vk" href="#"><i class="fa fa-vk"></i></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
