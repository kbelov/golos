<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 18:11
 */

use common\components\Url;

/** @var \frontend\modules\news\models\News $model */

?>
<a class="article" href="<?= Url::toRoute([
    '/news/project/view', 'id'  => $model->id
]); ?>">
    <header class="article__header">
        <?php if (null !== $model->image): ?>
            <div class="article__header__img"
                 style="background-image: url(<?=$model->urlAttribute('image');?>);">
            </div>
        <?php endif; ?>
        <h3 class="article__title"><?= $model->name; ?></h3>
    </header>
    <?php if (!empty($model->preview)): ?>
        <div class="article__text">
            <?= $model->preview; ?>
        </div>
    <?php endif; ?>
    <footer class="article__footer">
        <div class="meta__group">
            <div class="meta">
                <div class="meta__date"><?= $model->getDate(); ?></div>
            </div>
        </div>
    </footer>
</a>
