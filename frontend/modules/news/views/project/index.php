<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 17:49
 */

use yii\widgets\ListView;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::$app->name;
$this->params['noTitle'] = true;
?>
<section>
    <div class="row">
        <div class="col-md-12">
            <header class="section-header">
                <h2>Новости</h2>
            </header>
            <div class="row">
                <div class="grid">
                    <div class="grid-sizer"></div>
                    <?= ListView::widget(
                        [
                            'dataProvider' => $dataProvider,
                            'layout' => "{items}\n<div class='col-lg-12'>{pager}</div>",
                            'itemView' => '_index_item',
                            'options' => [
                                'class' => 'news-list',
                                'tag'   => false
                            ],
                            'itemOptions' => [
                                'class' => 'grid-item',
                                'tag' => 'div'
                            ]
                        ]
                    ); ?>
                </div>
            </div>
        </div>
    </div>
</section>