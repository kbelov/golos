<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 18:21
 */

/** @var \frontend\modules\news\models\News $model */
/** @var \yii\web\View $this */

$this->title = $model->name;

?>
<section>
    <div class="row">
        <div class="col-md-12">
            <header class="section-header">
                <h2><?= $this->title; ?></h2>
            </header>
        </div>
        <div class="col-md-12">
            <div class="article-content">
                <?php if (!empty($model->image)): ?>
                    <img class="article__img img-responsive" src="<?= $model->urlAttribute('image'); ?>">
                <?php endif; ?>
                <div class="article-content__text">
                    <?= $model->content; ?>
                </div>
            </div>
        </div>
    </div>
</section>
