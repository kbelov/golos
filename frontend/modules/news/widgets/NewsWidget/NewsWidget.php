<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 17:34
 */

namespace frontend\modules\news\widgets\NewsWidget;

use frontend\modules\news\models\News;
use yii\base\Widget;

/**
 * Class NewsWidget
 * @package frontend\modules\news\widgets\NewsWidget
 */
class NewsWidget extends Widget
{
    /** @var integer */
    public $limit = 3;

    /** @var bool */
    public $isPiter = true;

    /**
     * @throws \yii\base\InvalidParamException
     */
    public function run()
    {
        $newsQuery = News::find()->active()->limit($this->limit)->orderBy('date DESC');
        if ($this->isPiter) {
            $newsQuery->isPiter();
        } else {
            $newsQuery->isProject();
        }
        $news = $newsQuery->all();
        return $this->render('index', [
            'isPiter'   => $this->isPiter,
            'news'      => $news
        ]);
    }
}
