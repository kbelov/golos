<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 17:34
 */

use common\components\Url;

/** @var \frontend\modules\news\models\News[] $news */
/** @var boolean $isPiter */

?>
<?php if (!empty($news)): ?>
<section>
    <div class="row">
        <div class="col-md-12">
            <header class="section-header">
                <?php if ($isPiter): ?>
                    <h2>В Питере</h2>
                    <a class="btn btn-default"
                       href="<?= Url::toRoute([
                           '/news/default/index'
                       ]); ?>">Все новости</a>
                <?php else: ?>
                    <h2>Новости</h2>
                    <a class="btn btn-default"
                       href="<?= Url::toRoute([
                           '/news/project/index'
                       ]); ?>">Все новости</a>
                <?php endif; ?>
            </header>
            <div class="row">
                <?php foreach ($news as $new):
                    if ($new->is_piter) {
                        $url = Url::toRoute(['/news/default/view', 'id' => $new->id]);
                    } else {
                        $url = Url::toRoute(['/news/project/view', 'id' => $new->id]);
                    }
                    ?>
                    <div class="grid-item">
                        <a class="article"
                           href="<?= $url; ?>">
                            <header class="article__header">
                                <?php if (null !== $new->image): ?>
                                    <div class="article__header__img" style="background-image: url(<?=$new->urlAttribute('image');?>);"></div>
                                <?php endif; ?>
                                <h3 class="article__title"><?= $new->name; ?></h3>
                            </header>
                            <?php if (!empty($new->preview)): ?>
                                <div class="article__text">
                                    <?= $new->preview; ?>
                                </div>
                            <?php endif; ?>
                            <footer class="article__footer">
                                <div class="meta__group">
                                    <div class="meta">
                                        <div class="meta__date"><?= $new->getDate(); ?></div>
                                    </div>
                                </div>
                            </footer>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>

