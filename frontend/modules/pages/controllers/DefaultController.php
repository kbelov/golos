<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 22:38
 */

namespace frontend\modules\pages\controllers;

use frontend\controllers\Controller;
use frontend\modules\pages\models\Pages;
use yii\web\NotFoundHttpException;

/**
 * Class DefaultController
 * @package frontend\modules\pages\controllers
 */
class DefaultController extends Controller
{
    /**
     * Pages page.
     *
     * @param integer $id News ID
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView(int $id)
    {
        $model = Pages::find()->where([
            'id'    => $id
        ])->active()->one();
        if (null === $model) {
            throw new NotFoundHttpException();
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }
}
