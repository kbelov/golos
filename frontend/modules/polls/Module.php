<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 23.09.2017
 * Time: 19:00
 */

namespace frontend\modules\polls;

/**
 * Class Module
 * @package frontend\modules\polls
 */
class Module extends \common\modules\polls\Module
{
    public $controllerNamespace = 'frontend\modules\polls\controllers';
}
