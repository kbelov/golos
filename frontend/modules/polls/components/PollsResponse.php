<?php
/**
 * Created by PhpStorm.
 * User: andkon
 * Date: 04.08.17
 * Time: 16:07
 */

namespace frontend\modules\polls\components;

use api\components\Response;
use frontend\modules\polls\models\Polls;
use yii\helpers\Url;

/**
 * Class CategoryResponse
 *
 * @package api\modules\material\components
 */
class PollsResponse extends Response
{
    /**
     * @param Polls $model
     *
     * @return array
     * @throws \yii\base\InvalidParamException
     */
    public static function prepareToJson(Polls $model): array
    {
        $data = [];
        $data['title']       = $model->name;
        $data['text']        = $model->short_description;
        $data['start']       = $model->date_start;
        $data['end']         = $model->date_end;
        $data['pollStatus']  = $model->getPublicStatusName();
        $data['pollStatusId'] = $model->public_status;
        $data['yourStatus']  = $model->isVoted() ? 'Вы проголосовали' : '';
        $data['link']        = Url::to(['/polls/default/view', 'id' => $model->id], true);
        $data['coord']       = [
            $model->lon,
            $model->lat
        ];

        return $data;
    }
}
