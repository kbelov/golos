<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.17
 * Time: 15:32
 */

namespace frontend\modules\polls\controllers;

use common\modules\polls\helpers\PollStatusHelper;
use frontend\modules\polls\components\PollsResponse;
use frontend\modules\polls\models\Polls;
use yii\data\ActiveDataProvider;
use yii\rest\Controller;

/**
 * Class ApiController
 *
 * @package frontend\modules\polls\controllers
 */
class ApiController extends Controller
{
    /**
     * @throws \yii\base\InvalidParamException
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider(
            [
                'query' => Polls::find()
                    ->byStatusSoglas(PollStatusHelper::STATUS_SOGLAS_FINISHED)
                    ->correctDate()
                    ->visible()
                    ->active()
            ]
        );

        $data = array_map(function (Polls $model) {
            return PollsResponse::prepareToJson($model);
        }, $dataProvider->getModels());

        return [
            'features' => $data
        ];
    }
}