<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 23.09.2017
 * Time: 18:58
 */

namespace frontend\modules\polls\controllers;


use common\modules\polls\helpers\PollStatusHelper;
use frontend\modules\polls\models\forms\UserVoteForm;
use frontend\modules\polls\models\Polls;
use frontend\controllers\Controller;
use frontend\modules\polls\Module;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class DefaultController
 * @property Module module
 *
 * @package frontend\modules\polls\controllers
 */
class DefaultController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors(): array
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'vote' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return string
     * @throws \yii\base\InvalidParamException
     */
    public function actionIndex(): string
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Polls::find()
                ->byStatusSoglas(PollStatusHelper::STATUS_SOGLAS_FINISHED)
                ->correctDate()
                ->active()
                ->visible()
                ->orderBy('created DESC'),
            'pagination' => [
                'pageSize' => $this->module->recordsPerPage
            ]
        ]);

        return $this->render('index', [
            'dataProvider'  => $dataProvider,
        ]);
    }

    /**
     * @return string
     * @throws \yii\base\InvalidParamException
     */
    public function actionAll(): string
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Polls::find()
                ->byStatusSoglas(PollStatusHelper::STATUS_SOGLAS_FINISHED)
                ->active()
                ->visible()
                ->orderBy('created DESC'),
            'pagination' => [
                'pageSize' => $this->module->recordsPerPage
            ]
        ]);

        return $this->render('all', [
            'dataProvider'  => $dataProvider,
        ]);
    }

    /**
     * @param int $id
     *
     * @return string
     * @throws \yii\web\HttpException
     * @throws \yii\base\InvalidParamException
     */
    public function actionView(int $id): string
    {
        $poll = $this->findModel($id);

        return $this->render('view', [
            'model' => $poll
        ]);
    }

    /**
     * @param int $id
     *
     * @return array|string|Response
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\HttpException
     * @throws \yii\base\InvalidParamException
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionVote(int $id)
    {
        $poll = Polls::find()
            ->where([
                'id'    => $id
            ])
            ->byStatusSoglas(PollStatusHelper::STATUS_SOGLAS_FINISHED)
            ->correctDate()
            ->active()
            ->one();

        if (null === $poll) {
            throw new NotFoundHttpException();
        }

        if (!$poll->canVote()) {
            throw new ForbiddenHttpException();
        }

        $voteForm = new UserVoteForm([
            'poll_id'   => $id,
            'points'    => $poll->points
        ]);

        if ($voteForm->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($voteForm);
            }
            if ($voteForm->validate()) {
                if ($voteForm->saveForm()) {
                    return $this->render('success', [
                        'userVote'  => $voteForm
                    ]);
                } else {
                    Yii::$app->session->setFlash('danger', 'Не сохранено');
                }
                return $this->redirect(['/polls/default/view', 'id' => $id]);
            }

        }

        throw new BadRequestHttpException();
    }

    /**
     * Find model by ID.
     *
     * @param integer|array $id Post ID
     *
     * @return Polls Model
     *
     * @throws HttpException 404 error if post not found
     */
    protected function findModel($id)
    {
        if (is_array($id)) {
            $model = Polls::findAll($id);
        } else {
            $model = Polls::findOne($id);
        }
        if ($model !== null) {
            return $model;
        }

        throw new HttpException(404);
    }
}
