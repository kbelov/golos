<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.17
 * Time: 15:17
 */

namespace frontend\modules\polls\controllers;

use frontend\controllers\Controller;

/**
 * Class MapController
 *
 * @package frontend\modules\polls\controllers
 */
class MapController extends Controller
{
    /**
     * @throws \yii\base\InvalidParamException
     */
    public function actionIndex()
    {
        $this->layout = 'map';
        return $this->render('index');
    }
}
