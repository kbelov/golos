<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 29.09.17
 * Time: 12:22
 */

namespace frontend\modules\polls\models;

use common\modules\polls\helpers\PollStatusHelper;
use common\modules\polls\models\PollsVotes;
use common\modules\polls\models\UserVotes;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class Polls
 *
 * @package frontend\modules\polls\models
 */
class Polls extends \common\modules\polls\models\Polls
{
    /**
     * @return bool
     */
    public function canVote(): bool
    {
        if (Yii::$app->getUser()->getIsGuest()) {
            return false;
        }

        $user_id = Yii::$app->getUser()->getId();

        $voteModel = UserVotes::find()->byPoll($this->id)->byUser($user_id)->active()->one();
        if (!empty($voteModel)) {
            return false;
        }

        if ($this->visibility === PollStatusHelper::VISIBILITY_FOR_TRUSTED) {
            if (!Yii::$app->getUser()->getIdentity()->trusted) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return array
     */
    public function getVotesForm(): array
    {
        $votes = PollsVotes::find()->active()->withPoll($this->id)->asArray()->all();
        return ArrayHelper::map($votes, 'id', 'name');
    }
}
