<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 02.10.17
 * Time: 18:08
 */

namespace frontend\modules\polls\models\forms;

use common\modules\polls\models\PollsVotes;
use common\modules\transactions\services\AddNewTransaction;
use common\modules\polls\models\UserVotes;
use Yii;
use yii\base\Exception;

/**
 * Class UserVoteForm
 * @property PollsVotes[] pollsVotes
 *
 * @package frontend\modules\polls\models\forms
 */
class UserVoteForm extends UserVotes
{

    public $polls_vote_ids = [];

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['polls_vote_ids', 'poll_id', 'points'], 'required']
        ];
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getPollsVotes()
    {
        return PollsVotes::find()->where([
            'in',
            'id',
            $this->polls_vote_ids
        ])->all();
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate(): bool
    {
        $this->user_id = \Yii::$app->user->id;
        return parent::beforeValidate();
    }

    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    public function saveForm()
    {
        $transaction = \Yii::$app->getDb()->beginTransaction();
        try {
            $hasError = false;
            foreach ($this->polls_vote_ids as $vote_id) {
                $voteForm = new UserVotes([
                    'poll_id'       => $this->poll_id,
                    'points'        => $this->points,
                    'polls_vote_id' => $vote_id,
                    'user_id'       => Yii::$app->getUser()->getId()
                ]);
                $voteForm->save();
                if ($voteForm->hasErrors()) {
                    $hasError = true;
                    break;
                }
            }
            if (!$hasError) {
                $transaction->commit();
            } else {
                $transaction->rollBack();
                throw new Exception('Transaction errors');
            }
        } catch (Exception $e) {
            return false;
        }
        return AddNewTransaction::getInstance()
            ->setFrom(get_class($this->poll))
            ->setFromId($this->poll_id)
            ->setTo(get_class($this->user))
            ->setToId($this->user_id)
            ->setPoints($this->poll->points)
            ->run();
    }
}
