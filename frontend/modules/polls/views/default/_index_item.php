<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 18:11
 */

use frontend\helpers\DateFormatHelper;

/** @var \frontend\modules\polls\models\Polls $model */

$isVoted = $model->isVoted();

?>
<a class="article article-poll <?= $isVoted ? 'article--voted' : ''; ?>"
   href="<?=\yii\helpers\Url::toRoute(['/polls/default/view', 'id' => $model->id]); ?>"
>
    <header>
        <h3 class="article__title"><?=$model->name;?></h3>
    </header>
    <div class="article__text">
        <?= $model->short_description; ?>
    </div>
    <footer class="article__footer">
        <div class="meta__group">
            <div class="meta">
                <div class="meta__label">Начало</div>
                <div class="meta__data"><?=Yii::$app->formatter->asDate($model->date_start, DateFormatHelper::FULL_DATE);?></div>
            </div>
            <div class="meta">
                <div class="meta__label">Завершение</div>
                <div class="meta__data"><?=Yii::$app->formatter->asDate($model->date_end, DateFormatHelper::FULL_DATE);?></div>
            </div>
        </div>
        <div class="meta__group">
            <div class="meta">
                <div class="meta__label">Проголосовало</div>
                <div class="meta__data">
                    <div class="i i-user"></div><?= $model->countVoted; ?>
                </div>
            </div>
            <div class="meta">
                <div class="meta__label">Комментариев</div>
                <div class="meta__data">
                    <div class="i i-comment"></div><?= $model->countCommented; ?>
                </div>
            </div>
        </div>
        <div class="meta__group">
            <div class="meta">
                <div class="meta__bonus">
                    <?= \Yii::t('app',
                        '{n, plural, one{# балл} few{# балла} many{# баллов} other{# баллов}}',
                        [
                            'n' => $model->points
                        ]); ?>
                </div>
            </div>
        </div>

        <?php if (!$isVoted && $model->isRunning()): ?>
            <div class="article__footer__btns">
                <button class="btn btn-primary" type="button">Голосовать</button>
            </div>
        <?php else: ?>
            <div class="article__footer__btns">
                <button class="btn btn-success" type="button">О голосовании</button>
            </div>
        <?php endif; ?>
    </footer>
</a>
