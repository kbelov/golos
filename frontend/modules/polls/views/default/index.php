<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 02.10.2017
 * Time: 23:12
 */

use yii\widgets\ListView;

/** @var \yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Текущие голосования';

?>

<section>
    <div class="row">
        <div class="col-md-12">
            <header class="section-header">
                <h2><?= $this->title; ?></h2><a class="btn btn-default" href="<?= \yii\helpers\Url::toRoute(['/polls/default/all']);?>">Все голосования</a>
            </header>
            <div class="row">
                <div class="grid">
                    <div class="grid-sizer"></div>
                    <?= ListView::widget(
                        [
                            'dataProvider' => $dataProvider,
                            'layout' => "{items}\n<div class='col-lg-12'>{pager}</div>",
                            'itemView' => '_index_item',
                            'options' => [
                                'class' => 'polls-list',
                                'tag'   => false
                            ],
                            'itemOptions' => [
                                'class' => 'grid-item',
                                'tag' => 'div'
                            ]
                        ]
                    ); ?>
                </div>
            </div>
        </div>
    </div>
</section>