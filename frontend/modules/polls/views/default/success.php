<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 02.10.17
 * Time: 18:19
 */

use common\components\Url;

/** @var \frontend\modules\polls\models\forms\UserVoteForm $userVote */
/** @var \yii\web\View $this */

$this->title = $userVote->poll->name;
?>

<section>
    <div class="row">
        <div class="col-md-12">
            <header class="section-header">
                <h2>Ваш голос принят</h2>
            </header>
            <h1><?= $userVote->poll->name; ?></h1>
            <div class="poll__final">
                <div class="poll__final__title"><?= $userVote->poll->name_inner; ?></div>
                <div class="poll__final__content">
                    <?php
                        foreach ($userVote->pollsVotes as $vote) {
                            echo $vote->name . '; ';
                        }
                    ?>
                </div>
            </div>
            <div class="poll__final-bonus">Вы получили <?= $userVote->poll->points; ?> баллов</div>
            <div class="text-center">
                <a class="btn btn-primary" href="<?= Url::toRoute(['/']); ?>">вернуться на главную</a>
            </div>
        </div>
    </div>
</section>
