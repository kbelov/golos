<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 23.09.2017
 * Time: 19:00
 */

use common\components\Html;
use frontend\modules\comments\widgets\CommentsWidget\CommentsWidget;
use frontend\modules\polls\models\forms\UserVoteForm;
use frontend\ThemeAsset;
use yii\widgets\ActiveForm;
use frontend\helpers\DateFormatHelper;

/** @var \frontend\modules\polls\models\Polls $model */
/** @var \yii\web\View $this */

$this->title = $model->name;
$assetUrl = $this->getAssetManager()->getAssetUrl($this->getAssetManager()->getBundle(ThemeAsset::className()), '');

?>
<section>
    <div class="row">
        <div class="col-md-12">
            <header>
                <h1><?=$model->name; ?></h1>
            </header>
            <div class="article-content">
                <?php if (!empty($model->short_description)): ?>
                    <div class="epigraph">
                        <?= $model->short_description; ?>
                    </div>
                <?php endif; ?>
                <?php if (!empty($model->description)): ?>
                    <div class="article-content__text">
                        <?= $model->description; ?>
                    </div>
                <?php endif; ?>

                <div class="photos">
                    <?php foreach ($model->votes as $vote):
                        if (empty($vote->image)) continue;
                        ?>
                        <img width="200px" class="photo" src="<?=$vote->urlAttribute('image'); ?>">
                    <?php endforeach; ?>
                </div>
                <?php if ($model->isRunning() && $model->canVote()):
                    $userVoteForm = new UserVoteForm();
                    ?>
                    <h4><?=$model->name_inner; ?></h4>
                    <?php $form = ActiveForm::begin([
                        'enableAjaxValidation'      => true,
                        'enableClientValidation'    => false,
                        'validateOnBlur'            => false,
                        'validateOnChange'          => false,
                        'validateOnType'            => false,
                        'action' => \common\components\Url::toRoute([
                            '/polls/default/vote',
                            'id'    => $model->id
                        ]),
                    ]); ?>
                        <?= Html::hiddenInput('count_votes', $model->count_votes, ['id' => 'count-votes']);?>
                        <?= $form->field($userVoteForm, 'polls_vote_ids[]')->checkboxList(
                                $model->getVotesForm(),
                                [
                                    'item' => function ($index, $label, $name, $checked, $value) use ($assetUrl) {
                                        $vote = \common\modules\polls\models\PollsVotes::findOne($value);
                                        $html = '<div class="form-group"><div class="checkbox-circle">' .
                                            Html::checkbox($name, $checked, [
                                                'id'    => 'vote'. $index,
                                                'value' => $value
                                            ]) .
                                            '<label class="control-label vote-info__label" for="vote'.$index.'">
                                                <div class="check"></div>' . $label . '
                                            </label><a data-toggle="modal" data-target="#voteInfo'.$index.'" class="vote-info__icon"><img src="' . $assetUrl . 'dist/img/icons/info.svg"></a></div></div>';
                                        $html .=   '<div class="modal inmodal" id="voteInfo'.$index.'" tabindex="-1" role="dialog" aria-hidden="true">
                                                        <div class="modal-dialog modal-lg">
                                                            <div class="modal-content animated bounceInRight">
                                                                <div class="modal-body">'.((!empty($vote->image)) ? '<img width="100%" style="margin-bottom: 20px;" src="'.$vote->urlAttribute('image').'" />' : '').$vote->description.'</div>
                                                            </div>
                                                        </div>
                                                    </div>';
                                        return $html;
                                    },
                                ]
                            )->label(false); ?>
                        <div class="form-group">
                            <?= Html::submitButton('Завершить Голосование', ['class' => 'btn btn-primary']) ?>
                        </div>
                    <?php ActiveForm::end(); ?>
                <?php endif; ?>
                <div class="meta__group">
                    <div class="meta meta__alignment">
                        <div class="meta__date"><?=Yii::$app->formatter->asDate($model->date_start, DateFormatHelper::FULL_DATE);?></div>
                        <div class="socials__share"><a class="socials__share__item" href="#"><i class="fa fa-facebook"></i></a><a class="socials__share__item" href="#"><i class="fa fa-paper-plane"></i></a><a class="socials__share__item share__item--tw" href="#"><i class="fa fa-twitter"></i></a><a class="socials__share__item share__item--vk" href="#"><i class="fa fa-vk"></i></a></div>
                    </div>
                </div>
                <div class="row">
                    <div class="poll__finish__status_block">
                        <div class="col-md-4">
                            <div class="poll__finish__status__date">
                                <p class="poll__finish__status__title">Даты проведения</p>
                                <p class="poll__finish__status__date__text">с <?= Yii::$app->formatter->asDate($model->date_start, 'dd.MM.yyy'); ?> по <?= Yii::$app->formatter->asDate($model->date_end, 'dd.MM.yyy'); ?></p>
                            </div>
                        </div>
                        <?php if ($model->isRunning()): ?>
                            <div class="col-md-4">
                                <div class="poll__finish__status">
                                    <p class="poll__finish__status__title">Статус проекта</p>
                                    <p class="poll__finish__status__text">Активный</p>
                                </div>
                            </div>
                        <?php else:?>
                            <div class="col-md-4">
                                <div class="poll__finish__status">
                                    <p class="poll__finish__status__title">Статус проекта</p>
                                    <p class="poll__finish__status__text">Завершен</p>
                                </div>
                            </div>
                        <?php endif;?>

                        <div class="col-md-4">
                            <?php if ($model->isVoted()): ?>
                                <div class="poll__finish__status">
                                    <p class="poll__finish__status__voted">Вы проголосовали</p>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <?php if ($model->isVoted()): ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="poll__finish">
                                <div class="poll__finish__chose__title">
                                    <h4>Ваш выбор</h4>
                                </div>
                                <div class="poll__finish__chose__text">
                                    <h4>за голосование вы получили баллов: <?= $model->points; ?></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="poll__final__finish">
                                <div class="poll__final__finish__content">
                                    <?php $i = 0; foreach ($model->userVotes as $vote): ?>
                                        <?php if ($i > 0) { echo '; ';} ?>
                                        <?= $vote->pollsVote; ?>
                                    <?php $i++; endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if (!empty($model->complete_description)): ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="poll__outcome">
                                <h4>Итоги голосования</h4>
                                <?= $model->complete_description; ?>
                            </div>
                            <?php if (!empty($model->complete_decision)): ?>
                                <div class="poll__solution">
                                    <h4 class="poll__solution__title">Решение</h4>
                                    <div class="poll__solution__content"><?= $model->complete_decision; ?></div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if (!empty($model->complete_result)): ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="poll__result">
                                <h4>Результат</h4>
                                <?= $model->complete_result; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="poll__total">
                            <div class="poll__total__header">
                                <span>Всего проголосовало человек: </span>
                                <span class="poll__total__header__count"> <?= $model->countVoted; ?> </span>
                            </div>
                            <?php foreach ($model->votes as $vote) :
                                $count = $vote->countVoted;
                                $countVotes = $model->countVoted;
                                if ($model->countVoted > 0) {
                                    $percent = $count * 100 / $countVotes;
                                } else {
                                    $percent = 0;
                                }
                                ?>
                            <div class="poll__total__progress">
                                <h5><?= $vote; ?></h5>
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:<?= $percent; ?>%;"></div>
                                    </div>
                                    <div class="col-md-2"><span>голосов: <?= $vote->countVoted; ?>, ~<?= round($percent); ?>%</span></div>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if ($model->isRunning()): ?>
    <?= CommentsWidget::widget([
        'model' => \common\modules\polls\models\Polls::findOne($model->id)
    ]); ?>
<?php endif; ?>

