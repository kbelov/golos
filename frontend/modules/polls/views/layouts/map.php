<?php

/* @var $this \yii\web\View */
/* @var $content string */

use frontend\ThemeAsset;
use yii\helpers\Html;

ThemeAsset::register($this);

$assetUrl = $this->getAssetManager()->getAssetUrl($this->getAssetManager()->getBundle(ThemeAsset::className()), '');

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" type="image/x-icon" href="<?= $assetUrl; ?>dist/img/favicon.png">
    <?php $this->head() ?>
</head>
<body class="map-full">
<?php $this->beginBody() ?>
    <div class="container-fluid">
        <div class="row">
            <?= $this->render('sidebar-menu'); ?>
        </div>
        <div class="row">
            <?= $content ?>
        </div>
    </div>
<?= \frontend\widgets\MetricsWidget\MetricsWidget::widget(); ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
