<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.17
 * Time: 15:20
 */

use frontend\MapAsset;

/** @var \yii\web\View $this */

MapAsset::register($this);

$this->title = 'Проекты на карте';

?>
<div id="map"></div>
<div class="ol-popup" id="popup"><a class="ol-popup-closer" href="#" id="popup-closer">X</a>
    <div id="popup-content"></div>
</div>