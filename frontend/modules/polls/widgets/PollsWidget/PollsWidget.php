<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 23.09.2017
 * Time: 18:32
 */

namespace frontend\modules\polls\widgets\PollsWidget;

use common\modules\polls\helpers\PollStatusHelper;
use common\modules\polls\models\Polls;
use yii\base\Widget;

/**
 * Class PollsWidget
 * @package frontend\modules\widgets\PollsWidget
 */
class PollsWidget extends Widget
{

    /** @var int */
    public $limit = 6;

    /**
     * @return string
     * @throws \yii\base\InvalidParamException
     */
    public function run()
    {
        $polls = Polls::find()
            ->byStatusSoglas(PollStatusHelper::STATUS_SOGLAS_FINISHED)
            ->correctDate()
            ->active()
            ->visible()
            ->limit($this->limit)
            ->all();
        return $this->render('index',[
            'polls'    => $polls
        ]);
    }
}