<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 23.09.2017
 * Time: 18:34
 */

use frontend\helpers\DateFormatHelper;

/** @var \common\modules\polls\models\Polls[] $polls */

?>
<?php if (!empty($polls)): ?>
<section>
    <div class="row">
        <div class="col-md-12">
            <header class="section-header">
                <h2>Текущие голосования</h2><a class="btn btn-default" href="<?= \yii\helpers\Url::toRoute(['/polls/default/index']);?>">Все текущие голосования</a>
            </header>
            <div class="row">
                <div class="grid">
                    <div class="grid-sizer"></div>
                    <?php foreach ($polls AS $poll): $isVoted = $poll->isVoted(); ?>
                        <div class="grid-item">
                            <a class="article article-poll <?= $isVoted ? 'article--voted' : ''; ?>"
                               href="<?=\yii\helpers\Url::toRoute(['/polls/default/view', 'id' => $poll->id]); ?>"
                            >
                                <header>
                                    <h3 class="article__title"><?=$poll->name;?></h3>
                                </header>
                                <div class="article__text">
                                    <?= $poll->short_description; ?>
                                </div>
                                <footer class="article__footer">
                                    <div class="meta__group">
                                        <div class="meta">
                                            <div class="meta__label">Начало</div>
                                            <div class="meta__data"><?=Yii::$app->formatter->asDate($poll->date_start, DateFormatHelper::FULL_DATE);?></div>
                                        </div>
                                        <div class="meta">
                                            <div class="meta__label">Завершение</div>
                                            <div class="meta__data"><?=Yii::$app->formatter->asDate($poll->date_end, DateFormatHelper::FULL_DATE);?></div>
                                        </div>
                                    </div>
                                    <div class="meta__group">
                                        <div class="meta">
                                            <div class="meta__label">Проголосовало</div>
                                            <div class="meta__data">
                                                <div class="i i-user"></div><?= $poll->countVoted; ?>
                                            </div>
                                        </div>
                                        <div class="meta">
                                            <div class="meta__label">Комментариев</div>
                                            <div class="meta__data">
                                                <div class="i i-comment"></div><?= $poll->countCommented; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="meta__group">
                                        <div class="meta">
                                            <div class="meta__bonus">
                                                <?= \Yii::t('app',
                                                '{n, plural, one{# балл} few{# балла} many{# баллов} other{# баллов}}',
                                                [
                                                    'n' => $poll->points
                                                ]); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if (!$isVoted): ?>
                                        <div class="article__footer__btns">
                                            <button class="btn btn-primary" type="button">Голосовать</button>
                                        </div>
                                    <?php else: ?>
                                        <div class="article__footer__btns">
                                            <button class="btn btn-success" type="button">О голосовании</button>
                                        </div>
                                    <?php endif; ?>
                                </footer>
                            </a>
                        </div>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
