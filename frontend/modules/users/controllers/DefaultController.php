<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.2017
 * Time: 1:30
 */

namespace frontend\modules\users\controllers;

use frontend\controllers\Controller;
use frontend\modules\users\models\UserForm;
use common\exceptions\SaveARException;
use Yii;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class DefaultController
 * @package frontend\modules\users\controllers
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'polls', 'gifts', 'transactions'],
                'rules' => [
                    [
                        'actions' => ['index', 'polls', 'gifts', 'transactions'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @throws \yii\base\InvalidParamException
     */
    public function actionIndex()
    {
        $userForm = UserForm::findOne(\Yii::$app->getUser()->getId());

        if (Yii::$app->request->getIsPost()) {
            if ($userForm->load(Yii::$app->request->post())) {
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($userForm);
                }
                if ($userForm->validate()) {
                    try {
                        if ($userForm->save()) {
                            Yii::$app->session->setFlash('success', 'Профиль успешно обновлен');
                        } else {
                            Yii::$app->session->setFlash('danger', 'Не сохранено');
                        }
                    } catch (SaveARException $e) {
                        Yii::$app->session->setFlash('danger', "Не сохранено: ".$e->getMessage());
                    }

                    return $this->refresh();
                }
            }
        }

        return $this->render('index', [
            'userForm'  => $userForm
        ]);
    }

    /**
     * @throws \yii\base\InvalidParamException
     */
    public function actionPolls()
    {
        return $this->render('polls');
    }

    /**
     * @throws \yii\base\InvalidParamException
     */
    public function actionGifts()
    {
        return $this->render('gifts');
    }

    /**
     * @throws \yii\base\InvalidParamException
     */
    public function actionTransactions()
    {
        return $this->render('transactions');
    }
}
