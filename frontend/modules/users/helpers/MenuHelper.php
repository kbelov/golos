<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.2017
 * Time: 1:37
 */

namespace frontend\modules\users\helpers;

/**
 * Class MenuHelper
 * @package frontend\modules\users\helpers
 */
class MenuHelper
{
    const MENU_PROFILE = 'profile';
    const MENU_GIFTS = 'gifts';
    const MENU_POLLS = 'polls';
    const MENU_TRANSACTIONS = 'transactions';
}
