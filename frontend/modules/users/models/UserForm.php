<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 05.10.17
 * Time: 17:12
 */

namespace frontend\modules\users\models;

use common\models\User;
use common\modules\distribution\models\Distribution;
use common\modules\distribution\models\UserDistribution;
use yii\helpers\ArrayHelper;
use common\exceptions\SaveARException;

/**
 * Class UserForm
 *
 * @package frontend\modules\users\models
 * @property array postDistributions
 */
class UserForm extends User
{
    /** @var array Для сохранения данных из формы */
    protected $postDistributionsData = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['postDistributions'], 'safe']
        ];
    }

    /**
     * @return array Массив идентификаторов рассылок для сохранения
     */
    public function getPostDistributions(): array
    {
        if (empty($this->postDistributionsData)) {
            $out = ArrayHelper::getColumn($this->distributions, 'id');
        } else {
            $out = $this->postDistributionsData;
        }
        return $out;
    }

    public function setPostDistributions($value)
    {
        $this->postDistributionsData = $value;
    }

    /**
     * @return array Список email рассылок для checkboxList - ['id' => 'name']
     */
    public function emailDistributionsList(): array
    {
        $emailDist = Distribution::find()->where(['type' => Distribution::TYPE_EMAIL])->all();
        return ArrayHelper::map($emailDist, 'id', 'name');
    }

    /**
     * @return array Список sms рассылок для checkboxList - ['id' => 'name']
     */
    public function smsDistributionsList(): array
    {
        $emailDist = Distribution::find()->where(['type' => Distribution::TYPE_SMS])->all();
        return ArrayHelper::map($emailDist, 'id', 'name');
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->saveDistributions();
    }

    /**
     * Сохранение рассылок
     * @throws SaveARException
     */
    protected function saveDistributions()
    {
        $tr = \Yii::$app->db->beginTransaction();
        UserDistribution::deleteAll(['user_id' => $this->id]);
        foreach ($this->postDistributionsData as $distributionId) {
            $link = new UserDistribution(['user_id' => $this->id, 'distribution_id' => $distributionId]);
            if (!$link->save()) {
                $tr->rollBack();
                throw new SaveARException($link);
            }
        }
        $tr->commit();
    }

}
