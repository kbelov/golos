<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.2017
 * Time: 1:34
 */

use common\components\Url;
use frontend\modules\users\helpers\MenuHelper;

/** @var string $type */

?>
<div class="row">
    <div class="col-md-12">
        <a href="<?= Url::toRoute(['/users/default/index']); ?>" class="btn btn-<?= ($type === MenuHelper::MENU_PROFILE) ? 'success' : 'default'; ?>">Профиль</a>
        <a href="<?= Url::toRoute(['/users/default/polls']); ?>" class="btn btn-<?= ($type === MenuHelper::MENU_POLLS) ? 'success' : 'default'; ?>">Голосования</a>
        <a href="<?= Url::toRoute(['/users/default/gifts']); ?>" class="btn btn-<?= ($type === MenuHelper::MENU_GIFTS) ? 'success' : 'default'; ?>">Магазин поощрений</a>
        <a href="<?= Url::toRoute(['/users/default/transactions']); ?>" class="btn btn-<?= ($type === MenuHelper::MENU_TRANSACTIONS) ? 'success' : 'default'; ?>">Транзакции</a>
    </div>
</div>
