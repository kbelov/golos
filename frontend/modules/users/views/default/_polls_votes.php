<?php
use common\components\Html;
use common\modules\polls\models\UserVotes;

/**
 * Created by PhpStorm.
 * User: filatov
 * Date: 17.10.2017
 * Time: 12:49
 * @var UserVotes[] $votes
 */
?>
<?php if (empty($votes)) :?>
    Вы еще не голосовали
<?php else :?>
    <table class="table">
        <thead>
        <tr>
            <th>
                Название опроса
            </th>
            <th>
                За что проголосовали
            </th>
            <th>
                Дата
            </th>
            <th>
                Заработанные баллы
            </th>
        </tr>
        </thead>
        <tbody>
        <?php $currPoll = null;
        foreach ($votes as $vote) :?>
            <tr>
                <td><?= Html::a($vote->poll->name, ['/polls/default/view', 'id' => $vote->poll->id]); ?></td>
                <td><?= $vote->pollsVote->name; ?></td>
                <td><?= $vote->created; ?></td>
                <td>
                    <?php if (null !== $currPoll && $vote->poll->id === $currPoll) :?>
                        <span class="text-primary">-//-</span>
                    <?php else :?>
                        <span class="text-success"><b>+ <?= $vote->points; ?></b></span>
                    <?php endif; ?>
                </td>
            </tr>
            <?php $currPoll = $vote->poll->id;?>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>