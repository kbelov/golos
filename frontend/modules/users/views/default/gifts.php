<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.2017
 * Time: 1:33
 */

use common\components\Html;
use common\components\Url;
use frontend\modules\users\helpers\MenuHelper;

/** @var \yii\web\View $this */

$this->title = 'Личный кабинет';
$user = \common\models\User::findIdentity(Yii::$app->getUser()->getId());
?>
<section>
    <div class="row">
        <div class="col-md-12">
            <header>
                <h1><?=$this->title; ?></h1>
            </header>
        </div>
    </div>
    <?= $this->render('_menu', [
        'type'  => MenuHelper::MENU_GIFTS
    ]); ?>
</section>
<section>
    <div class="row">
        <div class="col-md-12">
            <h3>Баллов: <?= $user->points; ?></h3>
        </div>
        <div class="col-md-12">
            <?php if (empty($user->orders)): ?>
                Вы еще не совершали покупку
            <?php else: ?>
                <table class="table">
                    <thead>
                    <tr>
                        <th>
                            Номер
                        </th>
                        <th>
                            Товар
                        </th>
                        <th>
                            Где забирать
                        </th>
                        <th>
                            Дата заказа
                        </th>
                        <th>
                            Потрачено баллов
                        </th>
                        <th>
                            Статус
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($user->orders as $order): ?>
                        <tr>
                            <td><?= $order->id; ?></td>
                            <td><?= $order->gift->name; ?></td>
                            <td><?= $order->mfc; ?></td>
                            <td><?= $order->created;?></td>
                            <td><span class="text-danger"><b>- <?= $order->points; ?></b></span></td>
                            <td><?= $order->renderStatusName(); ?></td>
                        </tr>
                    <? endforeach; ?>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>
        <div class="col-md-12">
            <a href="<?= Url::toRoute(['/gifts/default/index']); ?>" class="btn btn-primary pull-right">Перейти в магазин поощрений</a>
        </div>
    </div>
</section>