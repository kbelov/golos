<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.2017
 * Time: 1:33
 */

use common\components\Html;
use common\models\User;
use frontend\modules\users\helpers\MenuHelper;
use yii\widgets\ActiveForm;
use frontend\modules\users\models\UserForm;

/** @var \yii\web\View $this */
/** @var UserForm $userForm */

$this->title = 'Личный кабинет';
?>
<section>
    <div class="row">
        <div class="col-md-12">
            <header>
                <h1><?=$this->title; ?></h1>
            </header>
        </div>
    </div>
    <?= $this->render('_menu', [
        'type'  => MenuHelper::MENU_PROFILE
    ]); ?>
</section>
<section>
    <div class="row">
        <div class="col-md-12">
            <h3>Баллов: <?= $userForm->points; ?></h3>
        </div>
        <div class="col-md-12">
            <table class="table">
                <thead>
                <tr>
                    <th></th>
                    <th></th>
                    <th>Баллы</th>
                </tr>
                </thead>
                <tbody>
                <?php if (!empty($userForm->last_name)) : ?>
                    <tr>
                        <td>
                            Фамилия
                        </td>
                        <td>
                            <?= $userForm->last_name; ?>
                        </td>
                        <td>
                            <span class="text-success"><b>+<?= User::POINTS_LAST_NAME; ?></b></span>
                        </td>
                    </tr>
                <?php endif; ?>
                <?php if (!empty($userForm->first_name)) : ?>
                    <tr>
                        <td>
                            Имя
                        </td>
                        <td>
                            <?= $userForm->first_name; ?>
                        </td>
                        <td>
                            <span class="text-success"><b>+<?= User::POINTS_FIRST_NAME; ?></b></span>
                        </td>
                    </tr>
                <?php endif; ?>
                <?php if (!empty($userForm->middle_name)) : ?>
                    <tr>
                        <td>
                            Отчество
                        </td>
                        <td>
                            <?= $userForm->middle_name; ?>
                        </td>
                        <td>
                            <span class="text-success"><b>+<?= User::POINTS_MIDDLE_NAME; ?></b></span>
                        </td>
                    </tr>
                <?php endif; ?>
                <?php if (!empty($userForm->phone)) : ?>
                    <tr>
                        <td>
                            Телефон
                        </td>
                        <td>
                            <?= $userForm->phone; ?>
                        </td>
                        <td>
                            <span class="text-success"><b>+<?= User::POINTS_PHONE; ?></b></span>
                        </td>
                    </tr>
                <?php endif; ?>
                <?php if (!empty($userForm->email)) : ?>
                    <tr>
                        <td>
                            e-mail
                        </td>
                        <td>
                            <?= $userForm->email; ?>
                        </td>
                        <td>
                            <span class="text-success"><b>+<?= User::POINTS_EMAIL; ?></b></span>
                        </td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4"><h3>Получать уведомления</h3></div>
                <div class="col-md-8">
                    <div style="margin-bottom: 15px;"><b>По электронной почте</b></div>
                    <?php $form = ActiveForm::begin([
                        'enableAjaxValidation'      => true,
                        'enableClientValidation'    => false,
                        'validateOnBlur'            => false,
                        'validateOnChange'          => false,
                        'validateOnType'            => false,
                    ]); ?>


                    <?= $form->field($userForm, 'postDistributions')->checkBoxList(
                        $userForm->emailDistributionsList(),
                        [
                            'item' => function ($index, $label, $name, $checked, $value) {
                                $html = "<div class='row'><div class='col-md-12'><label>";
                                $html .= Html::checkbox($name, $checked, [
                                    'value' => $value
                                ]);
                                $html .= " $label";
                                $html .= '</label></div></div>';
                                return $html;
                            },
                        ]
                    )->label(false); ?>

                    <hr>

                    <div style="margin-bottom: 15px;"><b>По смс на указанный мобильный телефон</b></div>
                    <?= $form->field($userForm, 'postDistributions')->checkBoxList(
                        $userForm->smsDistributionsList(),
                        [
                            'unselect' => null,
                            'item' => function ($index, $label, $name, $checked, $value) {
                                $html = "<div class='row'><div class='col-md-12'><label>";
                                $html .= Html::checkbox($name, $checked, [
                                    'value' => $value
                                ]);
                                $html .= " $label";
                                $html .= '</label></div></div>';
                                return $html;
                            },
                        ]
                    )->label(false); ?>

                    <div class="form-group">
                        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>