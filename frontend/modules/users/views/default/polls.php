<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.2017
 * Time: 1:33
 */

use common\components\Html;
use frontend\modules\users\helpers\MenuHelper;

/** @var \yii\web\View $this */

$this->title = 'Личный кабинет';
$user = \common\models\User::findIdentity(Yii::$app->getUser()->getId());
?>
<section>
    <div class="row">
        <div class="col-md-12">
            <header>
                <h1><?=$this->title; ?></h1>
            </header>
        </div>
    </div>
    <?= $this->render('_menu', [
        'type'  => MenuHelper::MENU_POLLS
    ]); ?>
</section>
<section>
    <div class="row">
        <div class="col-md-12">
            <h3>Баллов: <?= $user->points; ?></h3>
        </div>
        <div class="col-md-12">
            <?= $this->render('_polls_votes', ['votes' => $user->votes])?>
        </div>
    </div>
</section>