<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 03.10.2017
 * Time: 1:33
 */

use common\components\Html;
use common\modules\transactions\helpers\TransactionStatusHelper;
use frontend\modules\users\helpers\MenuHelper;

/** @var \yii\web\View $this */

$this->title = 'Личный кабинет';
$user = \common\models\User::findIdentity(Yii::$app->getUser()->getId());
?>
<section>
    <div class="row">
        <div class="col-md-12">
            <header>
                <h1><?=$this->title; ?></h1>
            </header>
        </div>
    </div>
    <?= $this->render('_menu', [
        'type'  => MenuHelper::MENU_TRANSACTIONS
    ]); ?>
</section>
<section>
    <div class="row">
        <div class="col-md-12">
            <h3>Баллов: <?= $user->points; ?></h3>
        </div>
        <div class="col-md-12">
            <?php if (empty($user->actions)): ?>
                Вы еще не голосовали
            <?php else: ?>
                <table class="table">
                    <thead>
                    <tr>
                        <th>
                            Откуда
                        </th>
                        <th>
                            Куда
                        </th>
                        <th>
                            Дата
                        </th>
                        <th>
                            Баллы
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($user->actions as $action): ?>
                        <tr>
                            <td><?= $action->renderFromName(); ?></td>
                            <td><?= $action->renderToName(); ?></td>
                            <td><?= $action->created;?></td>
                            <td>
                                <?php if ($action->status === TransactionStatusHelper::STATUS_RECEIVED): ?>
                                    <span class="text-success"><b>+ <?= $action->points; ?></b></span>
                                <?php else: ?>
                                    <span class="text-danger"><b>-<?= $action->points; ?></b></span>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <? endforeach; ?>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>
    </div>
</section>