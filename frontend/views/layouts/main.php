<?php

/* @var $this \yii\web\View */
/* @var $content string */

use common\components\Url;
use common\widgets\Alert;
use frontend\modules\blocks\widgets\BlocksWidget\BlocksWidget;
use frontend\ThemeAsset;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

ThemeAsset::register($this);

$assetUrl = $this->getAssetManager()->getAssetUrl($this->getAssetManager()->getBundle(ThemeAsset::className()), '');

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" type="image/x-icon" href="<?= $assetUrl; ?>dist/img/favicon.png">
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrapper">
    <div class="widget-side-container">
        <?= BlocksWidget::widget(); ?>
    </div>
    <div class="content-container">
        <div class="container">
            <div class="row">
                <?= $this->render('sidebar-menu'); ?>
            </div>
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
            <section class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="footer__feedback">
                                <div style="padding: 0 20px; background: #fff;"><a class="btn btn-default" href="<?= Url::toRoute(['/site/feedback']); ?>">Обратная связь</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="footer__item">
                                <p>Узнай результаты и подпишись на новости</p>
                                <div class="socials">
                                    <a target="_blank" class="socials__item socials__item--vk" href="https://vk.com/CitizenPetersburg"></a>
                                    <a target="_blank" class="socials__item socials__item--tw" href="https://twitter.com/golospetersburg"></a>
                                    <a target="_blank" class="socials__item socials__item--fb" href="https://www.facebook.com/CitizenPetersburg"></a>
                                    <a target="_blank" class="socials__item socials__item--ig" href="https://www.instagram.com/citizenpetersburg/"></a>
                                    <a target="_blank" class="socials__item socials__item--tm" href="https://t.me/CitizenPetersburg"></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="footer__item">
                                <p>© Голос Петербурга. Все права защищены. <a href="<?= \yii\helpers\Url::toRoute(['/pages/default/view', 'id' => 2]); ?>">Пользовательское соглашение.</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<?= \frontend\widgets\MetricsWidget\MetricsWidget::widget(); ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
