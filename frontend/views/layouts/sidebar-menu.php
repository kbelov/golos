<?php

/**
 * Sidebar menu layout.
 *
 * @var \yii\web\View $this View
 */

use frontend\widgets\Menu;

$menuItems = [
    [
        'label' => 'Голосования',
        'url'   => ['/polls/default/index']
    ],
//    [
//        'label' => 'Магазин поощрений',
//        'url'   => ['/gifts/default/index']
//    ],
    [
        'label' => 'В Питере',
        'url'   => ['/news/default/index']
    ],
    [
        'label' => 'Проект на карте',
        'url'   => ['/polls/map/index']
    ],
    [
        'label' => 'О проекте',
        'url'   => '#',
        'submenu'   => true,
        'items' => Menu::getSubmenu()
    ]
];

if (Yii::$app->getUser()->getIsGuest()) {
    $menuItems[] = [
        'label' => 'Вход',
        'url'   => ['/site/login']
    ];
} else {
    $menuItems[] = [
        'label' => Yii::$app->getUser()->getIdentity(),
        'url'   => '#',
        'submenu'   => true,
        'items'     => [
            [
                'label' => 'Личный кабинет',
                'url'   => ['/users/default/index']
            ],
            [
                'label' => 'Выход',
                'url'   => ['/site/logout']
            ]
        ]
    ];
}

?>
<div class="collapse">
    <nav class="navbar">
        <button class="navbar-toggle" data-target="#navbar"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
        <div class="collapse navbar-collapse" id="navbar">
            <?= Menu::widget([
                'options' => [
                    'class' => 'nav navbar-nav'
                ],
                'submenuTemplate' => '<ul class="dropdown-menu" aria-labelledby="nav1">{items}</ul>',
                'linkTemplate' => '<a href="{url}"><span>{label}</span></a>',
                'items' => $menuItems
            ]); ?>
        </div>
    </nav>
</div>
