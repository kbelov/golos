<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Обратная связь';
?>
<section>
    <div class="row">
        <div class="col-md-12">
            <header class="section-header">
                <h2><?= $this->title; ?></h2>
            </header>
        </div>
        <div class="col-md-12">
            <div class="article-content">
                <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'email')->textInput([
                            'placeholder'   => 'e-mail для ответа'
                        ])->label(false) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'subject')
                            ->dropDownList($model->getTemplatesArray(), [
                                'prompt'    => 'Выберете тему ответа'
                            ])->label(false) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <?= $form->field($model, 'body')->textarea([
                            'rows' => 6,
                            'placeholder'   => 'Текст комментария'
                        ])->label(false) ?>
                    </div>
                </div>

                <div class="form-group">
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>
