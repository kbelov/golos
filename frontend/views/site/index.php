<?php

/* @var $this yii\web\View */

use frontend\modules\news\widgets\NewsWidget\NewsWidget;
use frontend\modules\polls\widgets\PollsWidget\PollsWidget;

$this->title = Yii::$app->name;

?>

<?= PollsWidget::widget(); ?>

<?= NewsWidget::widget(); ?>
