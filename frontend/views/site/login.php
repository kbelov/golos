<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use common\esia\helpers\EsiaHelper;
use esia\OpenId;
use frontend\ThemeAsset;

$this->title = 'Авторизация на сайте';

$assetUrl = $this->getAssetManager()->getAssetUrl($this->getAssetManager()->getBundle(ThemeAsset::className()), '');
?>
<section>
    <div class="row">
        <div class="col-md-12">
            <header>
                <h1><?=$this->title; ?></h1>
            </header>
            <div class="article-content">
                <?php $esia = new OpenId(EsiaHelper::buildConfig()); ?>
                <div class="row form-group text-center">
                    <div class="col-md-12">
                        <img src="<?= $assetUrl.'/dist/img/logo-simple.png'; ?>" alt="" />
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-md-12">
                        <a class="btn btn-primary" href="<?=$esia->getUrl()?>">Войти через портал госуслуги</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
