<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.10.2017
 * Time: 18:42
 */

namespace frontend\widgets;
use common\components\Html;
use common\components\Url;
use frontend\modules\pages\models\Pages;
use yii\helpers\ArrayHelper;

/**
 * Class Menu
 * @package frontend\widgets
 */
class Menu extends \yii\widgets\Menu
{
    /**
     * Renders the content of a menu item.
     * Note that the container and the sub-menus are not rendered here.
     * @param array $item the menu item to be rendered. Please refer to [[items]] to see what data might be in the item.
     * @return string the rendering result
     */
    protected function renderItem($item)
    {
        if (isset($item['url'])) {
            $template = ArrayHelper::getValue($item, 'template', $this->linkTemplate);

            if (isset($item['submenu'])) {
                $template = '<a href="{url}" 
                    data-toggle="dropdown" 
                    id="nav1" 
                    aria-haspopup="true" 
                    aria-expanded="false"
                ><span>{label}</span></a>';
                strtr($template, [
                    '{url}' => Html::encode(Url::to($item['url'])),
                    '{label}' => $item['label'].'<span class="caret"></span>',
                ]);
                return strtr($template, [
                    '{url}' => Html::encode(Url::to($item['url'])),
                    '{label}' => $item['label'].'<span class="caret"></span>',
                ]);
            }

            return strtr($template, [
                '{url}' => Html::encode(Url::to($item['url'])),
                '{label}' => $item['label'],
            ]);
        } else {
            $template = ArrayHelper::getValue($item, 'template', $this->labelTemplate);

            return strtr($template, [
                '{label}' => $item['label'],
            ]);
        }
    }

    /**
     *
     */
    public static function getSubmenu():array
    {
        $pages = Pages::find()->inMenu()->active()->all();
        $menus = [];
        $menus[] = [
            'label' => 'Новости',
            'url'   => ['/news/project/index']
        ];
        foreach ($pages as $page) {
            $menus[] = [
                'label' => $page->name,
                'url'   => ['/pages/default/view', 'id' => $page->id]
            ];
        }
        $menus[] = [
            'label' => 'Обратная связь',
            'url'   => ['/site/feedback']
        ];
        return $menus;
    }
}
