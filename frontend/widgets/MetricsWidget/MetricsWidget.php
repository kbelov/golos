<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 07.10.2017
 * Time: 15:03
 */

namespace frontend\widgets\MetricsWidget;

use yii\base\Widget;

/**
 * Class MetricsWidget
 * @package frontend\widgets\MetricsWidget
 */
class MetricsWidget extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('index');
    }
}
