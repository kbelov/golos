import gulp from "gulp";
import runSequence from "run-sequence";

gulp.task('build', (callback) => {
    runSequence(
        'clean',
        [
            'fonts',
            'images',
            'templates',
            'stylesheets',
            'replace'
        ],
        'browserify',
        callback
    );
});
