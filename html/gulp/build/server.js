import gulp from "gulp";
import config from "../config";
import developmentServer from "../modules/server/development";
import productionServer from "../modules/server/production";

gulp.task('server', () => {
    if (config.development) {
        gulp.watch(`${config.appDir}/img/**/*`, ['images']);
        gulp.watch(`${config.appDir}/fonts/**/*`, ['fonts']);
        gulp.watch(`${config.appDir}/css/**/*`, ['stylesheets']);
        gulp.watch([`${config.appDir}/*.pug`, `${config.appDir}/template/**/*.pug`], ['templates']);

        developmentServer();
    }
    else {
        productionServer();
    }
});
