'use strict';

$(document).ready(function () {
    $('.navbar .navbar-toggle').on('click', function (e) {
        $('.navbar').toggleClass('navbar--open');
        $('html').toggleClass('nav-open')
    });

    var elem = document.getElementsByClassName('grid');

    if (elem.length) {
        for (var i = 0; i < elem.length; i++) {
            new Masonry(elem[i], {
                itemSelector: '.grid-item',
                columnWidth: '.grid-sizer',
                percentPosition: true
            });
        }
    }

});