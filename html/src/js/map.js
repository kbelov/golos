var selectClick = new ol.interaction.Select({
    condition: ol.events.condition.click,
    style: new ol.style.Style({
        image: new ol.style.Icon({
            src: '/img/pin.png',
            imgSize: [46, 58]
        })
    })
});

var container = document.getElementById('popup');
var content = document.getElementById('popup-content');
var closer = document.getElementById('popup-closer');

var overlay = new ol.Overlay(({
    element: container,
    autoPan: true,
    autoPanAnimation: {
        duration: 250
    }
}));

selectClick.on('select', function (evt) {
    if (evt.selected.length) {
        console.log(evt);
        var data = evt.selected[0].get('dt');

        var coordinate = data.coord;

        content.innerHTML = '' +
            '<header>' +
            '<h3 class="article__title">' + data.title + '</h3>' +
            '</header>' +
            '<div class="article__text">' + data.text + '</div>' +
            '<footer class="article__footer">' +
            '<div class="meta__group">' +
            '<div class="meta">' +
            '<div class="meta__label">Начало</div>' +
            '<div class="meta__data">' + data.start + '</div>' +
            '</div>' +
            '<div class="meta">' +
            '<div class="meta__label">Завершение</div>' +
            '<div class="meta__data">' + data.end + '</div>' +
            '</div>' +
            '</div>' +
            '<div class="meta__group">' +
            '<div class="meta">' +
            '<div class="meta__label">Статус</div>' +
            '<div class="meta__data">' + data.pollStatus + '</div>' +
            '</div>' +
            '<div class="meta">' +
            '<div class="meta__label"> </div>' +
            '<div class="meta__data">' + data.yourStatus + '</div>' +
            '</div>' +
            '</div>' +
            '<div class="article__footer__btns">' +
            '<button class="btn btn-default" type="button">Все голосования</button>' +
            '</div>' +
            '</footer>' +
            '';

        overlay.setPosition(coordinate);
    } else {
        overlay.setPosition(undefined);
        closer.blur();
        return false;
    }
});

closer.onclick = function () {
    overlay.setPosition(undefined);
    closer.blur();
    return false;
};

var featuresSource = new ol.source.Vector();

var map = new ol.Map({
    target: 'map',
    layers: [
        new ol.layer.Tile({
            source: new ol.source.XYZ({
                url: 'http://tilessputnik.ru/{z}/{x}/{y}.png'
            })
        }),
        new ol.layer.Vector({
            source: featuresSource,
            style: new ol.style.Style({
                image: new ol.style.Icon({
                    src: '/img/pin.png',
                    imgSize: [46,58]
                })
            })
        })
    ],
    view: new ol.View({
        center: [3376720.3800400887, 8385524.188025261],
        minZoom: 9,
        maxZoom: 19,
        zoom: 11,
        extent: [3230687.4375012587, 8311380.27058864, 3474674.4317875416, 8439412.29296631]
    }),
    controls: [],
    overlays: [overlay]
});

map.addInteraction(selectClick);


$.get('/api/features.json', function (data) {
    var features = [];

    _.each(data.features, function (dt) {
        var feature = new ol.Feature({
            geometry: new ol.geom.Point(dt.coord),
            dt: dt
        });

        features.push(feature);
    });

    featuresSource.addFeatures(features);
});